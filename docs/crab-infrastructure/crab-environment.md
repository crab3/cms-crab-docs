# CRAB Environment

In CRAB, we have mainly 3 environment,

- `prod` (production) for serving users workload.
- `preprod` (pre-production) for testing feature we want to deploy where we have environment configuration like production,
- `dev` (development) for testing new feature. Our software cannot test locally (yet).

But each service in CRAB have difference way on how we use.

## CRABServer (CRAB REST/TaskWorker/Publisher)

The services of crab server are consists of CRAB REST, TaskWorker, Publisher, and dataabase. We deploy CRAB REST in Kubernates cluster. TaskWorker and Publusher are deployed on VM. Oracle database for each environment. CRAB environment are differentiate by CRAB REST instances.

In total, we have many cluster for `dev`. 1 for `preprod` and 1 for `prod`. Please see "Environment mapping" table in [CRAB Inventory](../inventory/crab-inventory.md#environment-mapping) for list of environments and its mapping.

Note that,

- TaskWorker/Publisher always talk to REST to access CRAB data, only REST can talk to db.
- WMCore REST, which is library CRAB REST use, has code path that can connect to any database (if configuration provided) by selection database name from url path.
    - e.g. <https://cmsweb-test11.cern.ch/crabserver/prod/info/> which is use CRAB REST in `test11` to request data from production database.
        - This is for debugging only.
- `crab-{prod,preprod}-tw02` is hot spare servers. `crab-prod-tw02` also have monitoring tools installed and are running.
- `crab-dev-tw04` is for Puppet dev environemnt only.

## S3 (previously, CRABCache)

For upload user sandboxes, crabConfig, etc., we upload them to Openstack object storage containers (equivalent to S3 buckets) in "CMS CRAB" project.

In short,

- `prod` and `preprod` use `crabcache_prod` bucket (not sure why we setup like this, maybe misconfiguration).
- `dev` use `crabcache_dev`, for all dev environment.

## Schedd

HTCondor in CMS has 2 pools that CRAB can use, **Global Pool** for production workload, **ITB Pool** for testing condor/node configuration. Schedd which connect to `Global Pool` are `prod` environment, and schedd connect `ITB Pool` are `preprod` environment. But, we have 2 special case:

- `vocms059` is connect to `Global Pool` but we never let TaskWorker pick this machine automatically when schedule the tasks. It is only for operators to use it with CRAB's non-production environemnt (e.g. run test tasks).
- `crab-preprod-scd03` is connect to `ITB Pool`, is for Puppet code testing only.

To check the machine connect to which pool, see `glidecondor_pool` variable in Puppet Hiera. Example [vocms069.cern.ch.yaml](https://gitlab.cern.ch/ai/it-puppet-hostgroup-vocmsglidein/-/blob/7b5483c8cfb2dfe0d2e78cd8f629df3b78bdbe27/data/fqdns/vocms069.cern.ch.yaml).

Note that do not confuse with service SAN name of 'servicecert.pem', the schedd `vocms059` is first `crabschedd` machine so previous developer name it as `schedd/crab-dev-scd01.cern.ch` but it connect to `Global Pool` (Well, it not to be schedule any end-users tasks). Also, `crab-preprod-scd03` we use it as test schedd, should be name `crab-preprod-scd03` but mistake has made :)

## Puppet

Basically, we have

- `production` for production VM (both TaskWorker and schedd).
- `qa` for schedd in `ITB Pool`, and `crabqa` for non-production TaskWorker VM.
- `crabdev_*` and `crab_playground`

We will eventually resolve diverged `qa` branch and do develop Puppet as same guideline provide by CERN IT.

For more detail how we use it, please see [CRAB Puppet](../crab-infrastructure/crab-puppet/using-puppet-in-crab.md)

## CRABClient

We have 3 environment for client:

- `crab-prod`, which is `prod` environment, `crab` command you type in `lxplus` are symlink to this.
- `crab-pre`, not *preprod* but *previous*. We use it as backup if something go wrong to `crab-prod`. Thanks to Shahzad [(ref)](../crab-components/crab-client.md#if-we-screw-up), we can switch between prod and pre by simply trigger jenkins job.
- `crab-dev`, which is `dev` environment. New code

The tricky part is, CRABClient are deploy on CVMFS which 2 environment

- Integration Build `ib-cms.cern.ch`, which nightly built and it will eventually update next morning after updated specfile was merged.
- Production `cms.cern.ch`. We need to wait CMSSW cut new release (or deploy yourself via [jenkins job](../crab-components/crab-client.md#how-to-do-it)).

For more detail, please see [CRAB Client](../crab-components/crab-client.md)
