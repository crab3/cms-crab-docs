# Appendix

## Migration to CC7

Old document how to migrate from CC6 to CC7 is in [Migration to CC7 in Twiki](https://twiki.cern.ch/twiki/bin/view/CMSPublic/CRABPuppet#Migration_to_CC7).

## Local user

Local user to deploy, run, and operate the service for TaskWorker VM is `crab3` and `crabtw` for schedd VM.
