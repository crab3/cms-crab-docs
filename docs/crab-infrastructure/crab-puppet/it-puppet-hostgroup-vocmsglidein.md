# it-puppet-hostgroup-vocmsglidein

Repo: <https://gitlab.cern.ch/ai/it-puppet-hostgroup-vocmsglidein>

## Code Layout
```
[thanayut@fedora it-puppet-hostgroup-vocmsglidein]$ tree -d

├── code
│   ├── files
│   │   ├── crabschedd
│   │   └── crabtaskworker
│   │   └── modules
│   ├── manifests
│   │   ├── crabschedd
│   │   ├── crabtaskworker
│   │   └── modules
│   └── templates
│       ├── crabtaskworker
│       ├── crabschedd
│       └── modules
└── data
    ├── fqdns
    └── hostgroup
        └── vocmsglidein
```

- `modules` is use in hostgroup that share across sub-(sub-)hostgroup.
- Put all configuration variable (Hiera data) in fqdns, except some configuration we intent to do it sub-hostgroup's wide (This practice can be changed).


## Revisit autoloading

Autoloading is behavior of Puppet on how to read manifest files and apply to host.

### Manifest file (*.pp)

For top level hostgroup, entrypoint is `init.pp` in `code/manifests`.
For sub-hostgroup, puppet apply `init.pp` first, follow by `<sub-hostgroup>.pp`
For sub-sub-hostgroup, puppet apply `init.pp`, `<sub-hostgroup>.pp`, then `<sub-hostgroup>/<sub-sub-hostgroup>.pp`.

In our case, Puppet will start load from [init.pp](https://gitlab.cern.ch/ai/it-puppet-hostgroup-vocmsglidein/-/blob/master/code/manifests/init.pp) and we explicitly call [node.pp](https://gitlab.cern.ch/ai/it-puppet-hostgroup-vocmsglidein/-/blob/master/code/manifests/node.pp) inside `init.pp`. Then, it load [crabschedd.pp](https://gitlab.cern.ch/ai/it-puppet-hostgroup-vocmsglidein/-/blob/master/code/manifests/crabschedd.pp) or [crabtaskworker.pp](https://gitlab.cern.ch/ai/it-puppet-hostgroup-vocmsglidein/-/blob/master/code/manifests/crabtaskworker.pp) according to sub-hostgroup

### Variables (Hiera data, *.yaml)

It load same way as manifests files, plus overriding specific machine by look up `<FQDN>.yaml` (e.g. `crab-dev-tw02.cern.ch.yaml` only override variable for `crab-dev-tw02` machine.

Variable precedence are applied following this, from highest to lowest: `FQDN > sub-sub-hostgroup > sub-hostgroup > hostgroup`.


## Note about node.pp file

We have a [node.pp](https://gitlab.cern.ch/ai/it-puppet-hostgroup-vocmsglidein/-/blob/master/code/manifests/node.pp) file in our CRAB repository. The purpose of the file is to execute/create a particular resource(s) in a virtual machine(VM) or set of VMs.

If we want to test something in puppet for our VMs then we can use this file in the following way:

```puppet
if $facts['networking']['hostname'] in ['vocms059'] {
    #define resources here
}
```

## External module

Be aware that we use 2 external code, both are for setup Schedd

- [it-puppet-hostgroup-vocms/code/manifests/schedd_htcondor/tuning.pp](https://gitlab.cern.ch/ai/it-puppet-hostgroup-vocms/-/blob/a8815fd998c2c4f082c8343dfed1ed61113f732f/code/manifests/schedd_htcondor/tuning.pp)
- [it-puppet-module-vocmshtcondor](https://gitlab.cern.ch/ai/it-puppet-module-vocmshtcondor)
