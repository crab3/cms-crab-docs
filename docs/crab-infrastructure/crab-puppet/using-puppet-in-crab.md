# Using Puppet in CRAB

## Puppet enviromnet

We would have liked to follow the policy in the following table:

| Puppet environment | Used for                             | VMs                                                               |
|--------------------|--------------------------------------|-------------------------------------------------------------------|
| crabdev_schedd     | Experiment, learn, test, fool around | crab-preprod-scd03,                                               |
| crabdev_taskworker | Experiment, learn, test, fool around | crab-dev-tw04,                                                    |
| qa                 | pre-production                       | vocms059 (GLOBAL), vocms068 (ITB), vocms069 (ITB), crab-preprod-tw*, crab-dev-tw0{1-3} |
| production         | production                           | all production schedd and crab tasworker                          |


### Create new Puppet environment

Please read instruction from official docs <https://configdocs.web.cern.ch/details/environments.html>

Usually, `crabdev_taskworker` and `crabdev_schedd` environment are enough for testing stuff with puppet. But, if you want to test huge change or someone is acquired `crabdev`, you can create new puppet environment at [ai/it-puppet-environments](https://gitlab.cern.ch/ai/it-puppet-environments/)  by yourselve and set machine to use your new environment.

Example of request new puppet environemnt (Merge request): [ai/it-puppet-environments!10735](https://gitlab.cern.ch/ai/it-puppet-environments/-/merge_requests/10735/diffs).

## Hostgroup

In CRAB, we have 2 sub-hostgroup inside `vocmsglidein` hostgroup:

- `vocmsglidein/crabschedd` for schedd machines.
- `vocmsglidein/crabtaskworker` for TaskWorker machines.

You can view all machines, what hostgroup (and enviroment) it belong in [Foreman WebUI: vocmsglidein](https://judy.cern.ch/hosts?search=parent_hostgroup+%3D+%22vocmsglidein%22).

## Puppet-lint

We use `puppet-lint` to enforce Puppet code style.

You can install it with `gem` (Ruby package manager):
```
gem install puppet-lint puppet-lint-strict_indent-check  puppet-lint-manifest_whitespace-check  puppet-lint-unquoted_string-check  puppet-lint-leading_zero-check  puppet-lint-absolute_classname-check  puppet-lint-trailing_comma-check  puppet-lint-file_ensure-check  puppet-lint-legacy_facts-check
```

Then, run `puppet-lint -f code/manifests/files` to fix code style in-place. You may need to run more than once to fix all code style error.

## Puppet validation
Find syntax errors before `puppet agent -tv` (or even worse, puppet run by Foreman fails) with
```
puppet parser validate code/manifests/...<point to .pp file here>
```
it can be run on `aiadm` or locally.

It does not find all problems, a stronger validation can be done as in this example on `aiadm.cern.ch` from [this]() MatterMost thread
```
belforte@aiadm00/it-puppet-hostgroup-vocmsglidein> puppet  apply --noop --modulepath=/mnt/puppetnfsdir/environments/crab_alma9/hostgroups  -e 'include hg_vocmsglidein::modules::condor' 
Error: This Resource Statement is unacceptable as a top level construct in this location (file: /mnt/puppetnfsdir/environments/crab_alma9/hostgroups/hg_vocmsglidein/manifests/modules/condor.pp, line: 51, column: 1)
Error: This Resource Statement is unacceptable as a top level construct in this location (file: /mnt/puppetnfsdir/environments/crab_alma9/hostgroups/hg_vocmsglidein/manifests/modules/condor.pp, line: 60, column: 1)
Error: This Resource Statement is unacceptable as a top level construct in this location (file: /mnt/puppetnfsdir/environments/crab_alma9/hostgroups/hg_vocmsglidein/manifests/modules/condor.pp, line: 71, column: 1)
Error: Language validation logged 3 errors. Giving up
belforte@aiadm00/it-puppet-hostgroup-vocmsglidein> 
```
Unfortunately nor `puppet apply --help` nor `man puppet apply` have a description. We only have that example.

More help is in [CERN doc](https://config.docs.cern.ch/misc/syntaxandtest.html)



## Monitoring scripts distribution.

The script that is used to send data to Elastic search is the following:
<https://gitlab.cern.ch/ai/it-puppet-hostgroup-vocmsglidein/blob/master/code/files/crabtaskworker/GenerateMONIT.py>

The operator maintaining this needs to be sure there is only one instance of the script running at a time, because we do not need redundant data feed to ES and also if the script is placed at a machine which cannot run it properly the cron job starts flooding with emails everybody in the e-group. By design the host supposed to run the script should be the TaskWorker but could also be any other machine (even a dedicated one). So far in order to ensure the single instance of the script we use the following preventive measures:

1. We have the name of the machine supposed to run it hardcoded in the script, which is not the best method but used it just as an extra precaution.
2. We have one parameter in the TaskWorker profile which is read by the hiera function and is used to manage the distribution of the script to the machines supposed to run it. In order to enable the script on one machine in the .yaml file used to configure it (code/qdns/crab-(dev|preprod|prod)-tw0\[0-9\]\*.cern.ch.yaml) put the following enable/disable flag:

    ```yaml
    enable_generatemonit: true
    ```

Example: <https://gitlab.cern.ch/ai/it-puppet-hostgroup-vocmsglidein/blob/master/data/fqdns/crab-prod-tw01.cern.ch.yaml#L30>

The 'hiera' parameter is read here: <https://gitlab.cern.ch/ai/it-puppet-hostgroup-vocmsglidein/blob/master/code/manifests/profiles/crabtaskworker.pp#L13>

and is used here: <https://gitlab.cern.ch/ai/it-puppet-hostgroup-vocmsglidein/blob/master/code/manifests/profiles/crabtaskworker.pp#L249-262>
