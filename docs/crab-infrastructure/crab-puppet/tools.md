# Tools

## `ai-*` command

You can find all commands and how to use in [CERN Configuration Management System User Guide](https://config.docs.cern.ch/nodes/index.html) in "Managing nodes" section.

!!! note
    `ai-*` only available in `aiadm.cern.ch` (ssh) machine.

!!! note
    Replace these variable to

    - `$osrelease` OS name template from `ai-*`. Use `cc7` (CentOS 7).
    - `$foremanenv` foreman/puppet environment. e.g., `qa`.
    - `$hostgroup` in example: `vocmsglidein/crabtaskworker`
    - `$openstackflav` VM flavor e.g. `m2.medium`
    - `$hostname` Your choice of hostname. some `ai-*` command need FQDN (with `.cern.ch` suffix to `$hostname`).

    For example, `crab-dev-tw03`:
    ```
    osrelease=alma9
    foremanenv=qa
    hostgroup=vocmsglidein/crabtaskworker
    openstackflav=m2.medium
    hostname=crab-dev-tw03.cern.ch
    ```


### Changing Foreman environment/hostgroup

```bash
# change environment
ai-foreman updatehost $hostname -e $foremanenv
# change hostgroup
ai-foreman updatehost $hostname -c $hostgroup
```

### Setup before run `ai-build`/`ai-rebuild`/`ai-kill`

Set your `OS_PROJECT_NAME` to `CMS CRAB` (default is `Personal $USER`).

```bash
export OS_PROJECT_NAME='CMS CRAB'
```

Check if you set it up correctly by listing the VM using the openstack command.

```bash
openstack server list
```

Output should look like this (at least there is `crab-dev-tw01` in output):

```
[tseethon@aiadm10 ~]$ openstack server list
+--------------------------------------+---------------------------------+--------+---------------------------------------------------------+-------+------------+
| ID                                   | Name                            | Status | Networks                                                | Image | Flavor     |
+--------------------------------------+---------------------------------+--------+---------------------------------------------------------+-------+------------+
| 2be32076-4c07-446e-9a8b-b2162f24932a | crab-dev-tw02                   | ACTIVE | CERN_NETWORK=188.184.75.1, 2001:1458:d00:4e::100:18d    |       | m2.medium  |
| 6f23e64a-1e7e-4611-a7dc-f6547e10528c | vocms069                        | ACTIVE | CERN_NETWORK=188.184.97.91, 2001:1458:d00:39::100:36c   |       | r2.2xlarge |
| b83d2bc7-bf96-4e5c-986c-b02294d2d456 | crab-dev-tw01                   | ACTIVE | CERN_NETWORK=137.138.159.63, 2001:1458:d00:14::5df      |       | m2.medium  |
| <output ommit>                                                                                                                                                 |
```

### Create new VM

[Creating a new Puppet managed virtual node.](https://configdocs.web.cern.ch/nodes/create/index.html)

`--landb-mainuser` and `--landb-responsible` **MUST BE**  `CMS-SERVICE-CRAB3HTCONDOR` and `CMS-SERVICE-CRAB3HTCONDOR-ADMINS` respectively.

```bash
ai-bs \
    -g $hostgroup \
    --$osrelease \
    --foreman-environment $foremanenv \
    --landb-mainuser CMS-SERVICE-CRAB3HTCONDOR \
    --landb-responsible CMS-SERVICE-CRAB3HTCONDOR-ADMINS \
    --nova-flavor $openstackflav \
    --landb-ipv6ready \
    $hostname
```

### Rebuild new VM

[Rebuilding a Puppet-managed virtual node.](https://configdocs.web.cern.ch/nodes/rebuild.html)

It will destroy and recreate VM on the same physical node, OS image, DNS/IP/MAC. Note that it only wipe out data on root disk and reattach block volume(s) back.

```bash
ai-rebuild $hostname
```

### Remove VM

```
ai-kill $hostname
```


## tbag

All commands and how to use are in "Secrets" section in [CERN Configuration Management System User Guide](https://config.docs.cern.ch/nodes/index.html). For Teigi (Puppet module), see example in [certificate.pp](https://gitlab.cern.ch/ai/it-puppet-hostgroup-vocmsglidein/-/blob/f290ee6f9d76981c08d3b7b6d4e73986fa7d7efb/code/manifests/crabtaskworker/certificate.pp#L72-79).

### List key

```bash
# host
tbag showkeys --host $hostname
# hostgroup
tbag showkeys --hg $hostgroup
```

Output:

```
[tseethon@aiadm81 ~]$ tbag showkeys --hg vocmsglidein/crabtaskworker
{
     "vocmsglidein/crabtaskworker": {
          "secrets": [
               "tseethon_test_secret",
               "tw_condor_token_global_pool",
               "tw_condor_token_itb_pool",
               "tw_crab_dev_tw01_servicecert_rucio_user",
               "tw_crab_dev_tw01_servicekey_rucio_user"
          ]
     }
}
[tseethon@aiadm81 ~]$
```

### Add (or change value to) a key

```bash
# from prompt shell
tbag set --host $hostname mykey
# from file
tbag set --host $hostname --file cert.pem mycert
# hostgroup
tbag set --hg $hostgroup --file cert.pem mycert
```
