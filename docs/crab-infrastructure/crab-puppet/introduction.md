# Introduction to Puppet

In CRAB, we deploy TaskWorker, Publisher and HTCondor schedd's services in VM using Puppet provide by CERN IT, along side with Foreman for manage puppet environment, and in-house `ai-*` tools for interact between Puppet/Foreman/Openstack.


## Training and Documentations

Even if you have experienced manage Puppet, we recommend to read and practice [Config Training](https://configtraining.web.cern.ch/configtraining/) before start read/edit our puppet.

In this training, you will hopefully learn how CERN IT deploy and manage Puppet with Foreman. If you want to learn how do we (CERN) write Puppet code, we suggest to look at other hostgroup such as [it-puppet-hostgroup-vocms](https://gitlab.cern.ch/ai/it-puppet-hostgroup-vocms) or [it-puppet-hostgroup-lxplus](https://gitlab.cern.ch/ai/it-puppet-hostgroup-lxplus).

Additional to training, please read [CERN Puppet user documentations](https://configdocs.web.cern.ch) for other infomation does not cover in Puppet training.

If you have any question, feel free to ask in [Puppet channel MatterMost](https://mattermost.web.cern.ch/it-dep/channels/puppet). In case technical stuff relate to our VM please open [SNOW ticket](https://cern.service-now.com/service-portal?id=sc_cat_item&name=Configuration-Management-Incident&se=Configuration-Management).

## Permission/Access

You need to join following e-group:

- `ai-admins` and `LxAdm-Authorized-Users` for access Puppet system (Foreman, Gitlab AI's group, aiadm machines). Please see [Puppet training](https://configtraining.web.cern.ch/configtraining/introduction/permissions.html)
- `cms-service-glideinwms-admins` for editing CRAB Puppet manifests in [it-puppet-hostgroup-vocmsglidein](https://gitlab.cern.ch/ai/it-puppet-hostgroup-vocmsglidein) repository.
- `cms-service-crab-sysadmins` to access VMs via ssh.
- `cms-openstack-vocmscrab-admins` to create machine in "CMS CRAB" Openstack projects.

## Summary basic Puppet at CERN

### (Foreman) Hostgroup

Hostgroup is the way on how we apply configuration to set of machine using auto-loading machanism to read specific manifest file and apply it.

For example, in crab, all machine are in `vocmsglidein` hostgroup. Puppet will read manifest files from [it-puppet-hostgroup-vocmsglidein](https://gitlab.cern.ch/ai/it-puppet-hostgroup-vocmsglidein) starting from `code/manifests/init.pp` and apply to machine.

In `vocmsglidein` has 2 sub-hostgroup, `crabtaskworker` and `crabschedd`. After apply config from `code/manifests/init.pp`, puppet read `code/manifests/crabtaskworker.pp` and apply to `crabtaskworker` machines only. Also same for `crabschedd` Puppet read `code/manifests/crabschedd.pp` and apply to `crabschedd` machines.

### Environment/Branch

Puppet have "environment" concepts same as other software deployment. All environment from all hostgroup are define in [it-puppet-environments](https://gitlab.cern.ch/ai/it-puppet-environments). The enviroment is associate with branch of git repository. In practice, CERN use same branch name as puppet environment name.

Puppet team already setup 2 pre-defined enviromnet for every hostgroup, `production` use `master` branch and `qa` use `qa` branch.

### Modules

Puppet modules in CERN are store in `ai/it-puppet-module-*` such as [it-puppet-module-vocmshtcondor](https://gitlab.cern.ch/ai/it-puppet-module-vocmshtcondor/) that CRAB currently using for install condor.

You can import class from hostgroup but it is **not recommend**.


### When will puppet configuration apply?

Every a hour, but it will not run at the same time (to prevent load in Puppet Server).

To apply puppet manully, run command as root:

```bash
puppet agent -tv
```

Or, if it takes too long and you care only about a specific section, you
can use [tags](https://configdocs.web.cern.ch/misc/tags.html),
( read more about it in the official 
[docs](https://www.puppet.com/docs/puppet/8/lang_tags.html) ). For example,
every class is automatically tagged and can be referenced with its full name:

```bash
puppet agent -tv --tags hg_vocmsglidein::crabtaskworker::crabspark
```



### ai-tools

CERN IT provide `ai-*` tools to manage the Puppet managed VM. To create/rebuild/delete VM you **MUST** use `ai-*` tools to do it. All `ai-*` tools only available in `aiadm.cern.ch` machine. See how-to in [Tools#ai-command](./tools.md#ai-command) page.

Foreman WebUI <https://judy.cern.ch> (CERN Network only) for viewing all machine in hostgroup and status/logs from Puppet configuration apply to machine. Use [this link](https://judy.cern.ch/hosts?search=parent_hostgroup+%3D+%22vocmsglidein%22) to filter only CRAB machines (`vocmsglidein` top-level hostgroup).

### Secret Managements

CERN IT deployed the secret management server called `tbag`. Every Puppet-managed machine can access those secrets using `teigi` Puppet module.

Please consult [CRAB Inventory](../../inventory/crab-inventory.md#puppet-secrets-tbag)'s page to see the list of secrets we use in puppet, and how to use in [Tools#tbag](./tools.md#tbag) .

Note that tbag keys of individual host will have higher precedence than keys from hostgroup.
