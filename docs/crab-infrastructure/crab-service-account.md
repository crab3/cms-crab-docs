# CRAB Service Account

The CRAB team uses the service account `cmscrab` for handling some automated tasks.

Service accounts are a resource provided by CERN IT, they are computing-only accounts.

A service account is owned by a user account, and ownership can be transferred.
This is crucial: when a member of the CRAB team leaves, he/she can transfer the ownership
of the service account to a new member, and the automatic tasks will continue working
as nothing changed.

The following sections are more of a logbook, they intend to keep track of what dario
needed to do in order to have a service account that suits CRAB needs.

## Overview of the status quo

We have a service account called `cmscrab`.

### robot certificate

We hava a robot certificate associated to this service account

```bash
# aiadm
tbag show --hg vocmsglidein/crabtaskworker cmscrab_robotcert
```

### keytab

We have a keytab for that service account saved in a tbag:

```bash
# aiadm
tbag show --hg vocmsglidein/crabtaskworker tw_cmscrab_keytab
```

This keytab has been generated following instructions detailed below.

We install that keytab in crabtaskworker machines with puppet,
see [krb.pp](https://gitlab.cern.ch/ai/it-puppet-hostgroup-vocmsglidein/-/blob/qa/code/manifests/crabtaskworker/krb.pp?ref_type=heads).

you can use that keytab to generate krb tickets with

```plaintext
> ssh crab-dev-tw02
[root@crab-dev-tw02 ~]# su crab3
[crab3@crab-dev-tw02 root]$ cd
[crab3@crab-dev-tw02 ~]$ ./renewKrbToken.sh
[crab3@crab-dev-tw02 ~]$ klist
Ticket cache: FILE:/tmp/krb5cc_1000
Default principal: cmscrab@CERN.CH

Valid starting       Expires              Service principal
07/09/2024 12:00:42  07/10/2024 13:00:42  krbtgt/CERN.CH@CERN.CH
        renew until 07/14/2024 12:00:42
07/09/2024 12:00:42  07/10/2024 13:00:42  afs/cern.ch@CERN.CH
        renew until 07/14/2024 12:00:42
[crab3@crab-dev-tw02 ~]$
```

You can check that the keytab works by accessing a directory on CERN EOS that
should be accessible by cmscrab. If your result is different that what follows,
then something is wrong!

```plaintext
[crab3@crab-dev-tw02 ~]$ ls /eos/project-c/cmsweb/www/CRAB
AdvTutorial         NotRootFilesStatusAndAction.html  SuspiciousFiles.list
BadInputFiles.html  pycurl3                           SuspiciousFilesStatusAndAction.html
Branches            RecallRules-docker.html           TruncatedFiles.list
gocurl              RecallRules.html                  TruncatedFilesStatusAndAction.html
NotRootFiles.list   RecallRules.html.bak
```



## One-time actions

This section illustrates how we got to the status quo. It is useful in case
some actions are required for maintentance.

### create the service account

Create the service account from [Account Management](https://account.cern.ch/account/Management/NewAccount.aspx).

Dario created the account `cmscrab` in early 2023.

### create the certificate for the service account

1. Login into [ca.cern.ch](https://ca.cern.ch) with the service account credentials,
click on "Request permission to obtain a Grid Robot certificate", wait for a
few minutes, logout and back in, then click on "New Grid Robor Certitificate"
to generate the robot certificate.
2. Make sure to request an import password.
3. Download the cert as `YYYYMMDD-cmscra-robot.p12`. A new certificate is needed every year !
3. Save the import password in `YYYYMMDD-cmscra-robot.passprhase`
4. put both files in crab-secrets/service-account directory
5. git pull
5. encrypt the password using `sops -e YYYYMMDD-cmscra-robot.passprhase YYYYMMDD-cmscra-robot.sops.passprhase`
6. git add and git push
7. create cert and key pem files to be used in the VM's. Note the `-nodes` option to create  `key.pem` file are w/o passphrase (you will need import passwrod from step 2. to use below commands)
```
openssl pkcs12 -clcerts -nokeys -in 20240719-cmscrab-robot.p12 -out cmscrab-robotcert.pem
openssl pkcs12 -nocerts -nodes -in 20240719-cmscrab-robot.p12 -out cmscrab-robotkey.pem
```

Now update the certificate to tbag so that we can install the robot certificate
to the crab VMs via puppet

```bash
# aiadm
tbag set --hg vocmsglidein/crabtaskworker --file cmscrab-robotcert.pem cmscrab_robotcert
tbag set --hg vocmsglidein/crabtaskworker --file cmscrab-robotkey.pem cmscrab_robotkey
```

### create the keytab for the service account

#### create a keytab

!!! note
    2024 july: TL;DR use the `cern-get-keytab` script.
    please use [these instructions](https://cern.service-now.com/service-portal?id=kb_article&n=KB0003405)
    to generate the keytab for the service account.

!!! warning
    The following instructions are outdated!

from lxplus8 or lxplus9, create a keytab for the service account, reference [cmsweb docs](https://cms-http-group.docs.cern.ch/vm_cluster/routine_maintenance/#generate-keytab-file):

```bash
> ssh lxplus.cern.ch
> ktutil
ktutil:  addent -password -p cmscrab@CERN.CH -k 1 -e rc4-hmac
Password for cmscrab@CERN.CH:
ktutil:  addent -password -p cmscrab@CERN.CH -k 1 -e aes256-cts
Password for cmscrab@CERN.CH:
ktutil:  wkt cmscrab.keytab
ktutil:  quit
```

validate the keytab

```bash
# check the content of the keytab
klist -k cmscrab.keytab

# use the keytab to generate a token.
# you can specify both cmscrab and cms.crab as principals
kinit -k -t cmscrab.keytab cmscrab
kinit -k -t cmscrab.keytab cms.crab
```

if the previous validation fails, then run it in debugging mode

```bash
KRB5_TRACE=/dev/stdout kinit -k -t cmscrab.keytab cmscrab
```

if the log contains a line such as ` Selected etype info: etype aes256-cts, salt "CERN.CHcms.crab", params ""`, then
you need to make sure that the keytab contains an entry also for `cms.crab@CERN.CH`, not only for `cmscrab@CERN.CH`.

In order to achieve this, you can try to generate the keytab with (suggested [here](https://gist.github.com/OmeGak/9530124?permalink_comment_id=3264418#gistcomment-3264418) see alse [KB0003405](https://cern.service-now.com/service-portal?id=kb_article&n=KB0003405))

```bash
cern-get-keytab --keytab cmscrab.keytab --user --login cmscrab
```

If you can get a valid token with a keytab generated with `ktutil`, with an
error message similar to "Preauthentication failed while getting initial credentials"
then generate a new keytab with `cern-get-keytab` on lxplus8 or lxplus9.
In 2023-08 some krb5 serer configuration have been changed, Dario hve not found
yet valid instructions on how to use ktutil to get a keytab compatible with the
recent services, so he suggest to just stick to `cern-get-keytab` run from a recent system.

#### use the keytab to get a krb5 token on a VM

If the test goes well, which means if you can obtain a kerberos ticket for the
principal `cmscrab@CERN.CH` with the keytab `cmscrab.keytab`,
then you can deploy the keytab to the desired machine(s)

```bash
ssh crab-dev-tw02
sudo cp cmscrab.keytab /home/crab3
sudo chown crab3:zh /home/crab3/cmscrab.keytab
sudo su crab3
kinit -k -t /home/crab3/cmscrab.keytab cmscrab
```

### Give the service account access to the cmsweb EOS directory

This is required to publish the results of the rucio taperecall monitoring.
See [here](../crab-components/crab-taskworker/deploy.md) for more details.

#### give the service account the permissions it requires

We want to write to `/eos/project/c/cmsweb/www/CRAB/` with the account `cmscrab`,
so that an automatic routine can publish its results to this public website
[https://cms-docs.web.cern.ch/CRAB/](https://cms-docs.web.cern.ch/CRAB/)

Write access to that directory is granted to every user in the egroup `cms-eos-web-CRAB`.
We crab operator should have permission to add a service account to that egroup,
otherwise ask Andreas Pfeiffer for help.

#### validate the permissions

Wait for one day for the changes to the egroup to propagate around.

Then:

```plaintext
dmapelli@lxplus921 $ ssh crab-dev-tw02
dmapelli@crab-dev-tw02 $ sudo su crab3
crab3@crab-dev-tw02 $ cd
crab3@crab-dev-tw02 $ kdestroy
crab3@crab-dev-tw02 $ kinit -k -t cmscrab.keytab cmscrab
crab3@crab-dev-tw02 $ # check that the krbtgt ticket is there
crab3@crab-dev-tw02 $ klist
Ticket cache: FILE:/tmp/krb5cc_1000
Default principal: cmscrab@CERN.CH

Valid starting       Expires              Service principal
06/19/2023 17:06:43  06/20/2023 18:06:43  krbtgt/CERN.CH@CERN.CH
        renew until 06/24/2023 17:06:43
06/19/2023 17:06:43  06/20/2023 18:06:43  afs/cern.ch@CERN.CH
        renew until 06/24/2023 17:06:43

crab3@crab-dev-tw02 $ # the service account has not the same permissions of a regular user
crab3@crab-dev-tw02 $ ls /eos/project/c/cmsweb/
ls: cannot open directory /eos/project/c/cmsweb/: Operation not permitted
crab3@crab-dev-tw02 $ # but it can read/write where we need
crab3@crab-dev-tw02 $ ls /eos/project/c/cmsweb/www/CRAB
AdvTutorial  aFile  dmapelli.txt  gocurl  pycurl3
crab3@crab-dev-tw02 $ echo "hello" > /eos/project/c/cmsweb/www/CRAB/dmapelli.txt
crab3@crab-dev-tw02 $ # there is a new ticket, used for eos!
crab3@crab-dev-tw02 $ klist
Ticket cache: FILE:/tmp/krb5cc_1000
Default principal: cmscrab@CERN.CH

Valid starting       Expires              Service principal
06/19/2023 17:06:43  06/20/2023 18:06:43  krbtgt/CERN.CH@CERN.CH
        renew until 06/24/2023 17:06:43
06/19/2023 17:06:43  06/20/2023 18:06:43  afs/cern.ch@CERN.CH
        renew until 06/24/2023 17:06:43
06/19/2023 17:07:15  06/20/2023 18:06:43  xrootd/eoshome.cern.ch@CERN.CH
        renew until 06/24/2023 17:06:43
06/19/2023 17:07:15  06/20/2023 18:06:43  xrootd/eosproject-i02.cern.ch@CERN.CH
        renew until 06/24/2023 17:06:43
```

#### troubleshoot EOS operation not permitted

##### Problem description

on 2023 06 28, Aroosha spotted a problem with krb5 tickets and eos access,
she opened a ticket here https://cern.service-now.com/service-portal?id=ticket&table=incident&n=INC3516468


```plaintext
[crab3@crab-dev-tw02 ~]$ kdestroy
[crab3@crab-dev-tw02 ~]$ kinit -k -t cmscrab.keytab cmscrab
[crab3@crab-dev-tw02 ~]$ export EOS_MGM_URL=root://eosproject-i02.cern.ch
[crab3@crab-dev-tw02 ~]$ XrdSecDEBUG=1 eos whoami
sec_Client: protocol request for host eosproject-i02.cern.ch token='&P=krb5,xrootd/
[...]
Virtual Identity: uid=162295 (99,162295) gid=2766 (99,2766) [authz:krb5] host=crab-dev-tw02.cern.ch domain=cern.ch
[crab3@crab-dev-tw02 ~]$ echo "hello" > /eos/project/c/cmsweb/www/CRAB/cmscrab.txt
bash: /eos/project/c/cmsweb/www/CRAB/cmscrab.txt: Operation not permitted
```

So, I could obtain a proper krb5 ticket from the keytab, eos whoami response is ok,
however I could not access the desired directory.

#### Solution

Germano (CMS VOC) suggested to just reboot the machine. And **it worked** ! :tada:

### Add a service account to the unix zh group

We need our service account to be a member of the `zh` unix computing group.

This is required, for example, for the service account to write to the hdfs directory

```
drwxrwxr-x+  - valya    zh          0 2023-08-17 13:45 /cms/users
```

when in the spark job that processes data from condor raw metrics we need to store
intermediate parquet files. See [here](../crab-components/crab-monitoring/crab-spark.md)
for more information about the CRAB Spark pipeline.

You can either as Andreas Pfeiffer, our you ask directly from the resource portal (Instructions checked by Dario and Stefano on 2023 august):

- login into [account.cern.ch](https://account.cern.ch) with the service account credentials, which are available in secrets repository.
- browse [https://resources.web.cern.ch/resources/Manage/Linux/Subscribe.aspx](https://resources.web.cern.ch/resources/Manage/Linux/Subscribe.aspx). You can also arrive here from
    - https://account.cern.ch/account/
    - Resources and Services
    - List Services
    - LXPLUS and Linux
    - Computing Groups
- under the section "specify project or experimenti environment" select `zh`, write a meaningful comment and send the request.

### Create robot certificate

A robot certificate can be created by a service account only. Browse [https://ca.cern.ch/ca/user/Request.aspx?template=ee2robot](https://ca.cern.ch/ca/user/Request.aspx?template=ee2robot), ask for the permission to generate a robot account, wait for a couple of hourse, then generate the robot certificate.

At the moment, we have:

- service account `cmscrab`: DN `CN=Robot: cms crab, CN=817881, CN=cmscrab, OU=Users, OU=Organic Units, DC=cern, DC=ch`, serial number `6e005d3b138304d64fe387da340001005d3b13`, expires 2024-11-02
- the p12 together with its password is saved on crab-secrets repository

What do we need to do with this robot certificate?

login with rucio:

- send the DN to CMS Rucio team, Eric or Rahul

## Rucio Service Accounts

We own 4 Rucio accounts that used by Taskworker and testsuite.

1. `crab_server`

    This is default account for TaskWorker when talking to Rucio server. It has the same power as normal users.

    The Certificate DN mapping is manual process. Use `rucio-admin identity add` command to add a new DN to this account. However, you need privilege account to do so.

2. `crab_tape_recall`

    `crab_tape_recall` is for assign tape recall rules to users when crab task requests tape recall. This account has admin privilege in order to create rule with `Activity="Analysis TapeRecall"`.
    The Certificate DN mapping is manual process same as `crab_server`.

3. `crab_input`

    `crab_input` is for locking input datasets on disk until task expired. It has admins privilege to be able to have infinite quota for locking.

    The Certificate DN mapping is manual process same as `crab_server`.


4. `crab_test_group`

    This is test Rucio's group account, equivalent to group LFN `/store/group/<group_name>`.

    Rucio has separate account to handle group use case. Account always has `_group` suffix, but path and scope do not contains `_group` suffix, i.e., `group.crab_test:/store/group/rucio/crab_test/path/to/file.root` for File DID in group scope.

    This group is synced from e-group `cms-service-crab-test`, which map into `cms-crab-GroupDataManager` in `crab_test_group`.

    Reference: <https://its.cern.ch/jira/browse/CMSTRANSF-713>


### Add DN to Rucio account

For account that manually add DN, you need to login Rucio using privilege account, either `crab_tape_recall` or `crab_input`, by using `cmscrab` proxy. Then use `rucio-admin identity add` command to add a new DN to account.

1. Issuing proxyfile using `voms-proxy-init` command.

    ```bash
    voms-proxy-init --rfc --voms cms -valid 192:00 --cert /path/to/cmscrab_cert.pem --key /path/to/cmscrab_cert.pem --out /tmp/cmscrab_proxy
    ```

2. Export variable
    ```bash
    export X509_USER_PROXY=/tmp/cmscrab_proxy
    export RUCIO_ACCOUNT=crab_input
    ```

3. Check if `rucio` execute with `crab_input` account
    ```bash
    rucio whoami
    ```
    The `account` field should be `crab_input`.


4. Add new identity
    ```bash
    rucio-admin identity add --account crab_server --type X509 --id "/DC=ch/DC=cern/OU=Organic Units/OU=Users/CN=crabint1/CN=373708/CN=Robot: CMS CRAB Integration 1" --email "cms-service-crab-operators@cern.ch"
    ```
    Replace
    - `crab_server` with the account you want to add the new DN.
    - `/DC=ch/DC=cern/OU=...` to the new DN.

Ref: <https://its.cern.ch/jira/browse/CMSTRANSF-965>
