# How to use crab-secrets

## Install sops and GPG

See ["sops and GPG" section in Development Environment](../../development/environment/introduction.md#gnupg2-and-sops).

## Create your gpg key

!!! note
    Default gpg home directory is at `~/.gnupg` with `0700` permission. You can change gpg home directory by `export GNUPGHOME=/your/path/here`.

Run `gpg --full-generate-key` to generate key. After you provide all information, gpg will ask password for encrypt the private key. Save it somewhere safe, you will need to use it when re-encrypt secret.

!!! warning
    gpg will pop open a dialog window in order to get the passphrase, depending on your laptop if you do this on e.g. `lxplus` you may need to make sure that X11 forwarding is enabled, e.g. connect with `ssh -XY lxplus8.cern.ch`


Example logs from `gpg --full-generate-key`

??? quote "gpg --full-generate-key"
    ```
    [tseethon@lxplus8s19 ~]$ gpg --full-generate-key
    gpg (GnuPG) 2.2.20; Copyright (C) 2020 Free Software Foundation, Inc.
    This is free software: you are free to change and redistribute it.
    There is NO WARRANTY, to the extent permitted by law.

    Please select what kind of key you want:
       (1) RSA and RSA (default)
       (2) DSA and Elgamal
       (3) DSA (sign only)
       (4) RSA (sign only)
      (14) Existing key from card
    Your selection? 1
    RSA keys may be between 1024 and 4096 bits long.
    What keysize do you want? (2048) 4096
    Requested keysize is 4096 bits
    Please specify how long the key should be valid.
             0 = key does not expire
          <n>  = key expires in n days
          <n>w = key expires in n weeks
          <n>m = key expires in n months
          <n>y = key expires in n years
    Key is valid for? (0) 0
    Key does not expire at all
    Is this correct? (y/N) y

    GnuPG needs to construct a user ID to identify your key.

    Real name: Myfirstname Mylastname
    Email address: myemail@cern.ch
    Comment:
    You selected this USER-ID:
        "Myfirstname Mylastname <myemail@cern.ch>"

    Change (N)ame, (C)omment, (E)mail or (O)kay/(Q)uit? O
    We need to generate a lot of random bytes. It is a good idea to perform
    some other action (type on the keyboard, move the mouse, utilize the
    disks) during the prime generation; this gives the random number
    generator a better chance to gain enough entropy.
    We need to generate a lot of random bytes. It is a good idea to perform
    some other action (type on the keyboard, move the mouse, utilize the
    disks) during the prime generation; this gives the random number
    generator a better chance to gain enough entropy.
    gpg: key 5A6CB4E0D3DA52A8 marked as ultimately trusted
    gpg: directory '/afs/cern.ch/user/t/tseethon/testgnupg/openpgp-revocs.d' created
    gpg: revocation certificate stored as '/afs/cern.ch/user/t/tseethon/testgnupg/openpgp-revocs.d/C243FCE7A6D25A1B64CFC7D35A6CB4E0D3DA52A8.rev'
    public and secret key created and signed.

    pub   rsa4096 2022-10-21 [SC]
          C243FCE7A6D25A1B64CFC7D35A6CB4E0D3DA52A8
    uid                      Myfirstname Mylastname <myemail@cern.ch>
    sub   rsa4096 2022-10-21 [E]
    ```

### Import public/secret key to another machine

!!! warning
    GnuPG 2.1 or later have changed "keybox" to `$GNUPGHOME/pubring.kbx` for both public and private key. See <https://superuser.com/a/1037402>

If you want to use your private key on a different machine from the one where the key has been generated,
then you need to import it.
Recommended way is to export the key, copy it, import it

```bash
# on local machine
gpg --output private.gpg --armor --export-secret-key stefano.belforte@cern.ch
rsync -av private.gpg lxplus8:private/private.gpg
ssh lxplus8
# on lxplus8
gpg --import private/private.gpg
rm private/private.gpg
```

Alternate way is to import the pubring.gpg and secring.gpg into the new machine.
First, copy `~/.gnupg/securing.gpg` and `~/gnupg/pubring.gpg` to the new machine, then in the new machine run

```bash
gpg --import ~/.gnupg/secring.gpg
gpg --import ~/.gnupg/pubring.gpg

gpg --edit-key yourname@youremail.com
> trust
(5: trust ultimately)
(y: yes)
> quit
```

## Add new key to `crab-secrets`

1. Export your public key (Use `gpg --list-key` to get the key fingerprint).
```
gpg --armor --export FINGERPRINT_or_EMAIL
```
2. Commit it to `pubkey/yourcernid.pub` in [crab-secrets](https://gitlab.cern.ch/crab3/crab-secrets/-/tree/master/pubkey).

3. Add your key's fingerprint to [.sops.yaml](https://gitlab.cern.ch/crab3/crab-secrets/-/blob/master/.sops.yaml?ref_type=heads).

4. Ask anyone whose key is already there to update all encrypted files with your new key.

## Edit, encrypt, decrypt a file

To edit encrypt files, run `sops <filepath>`, save and exit editor. Do not forget to commit and push.

If you want to manually decrypt and encrypt yourself, you can do:

```bash
$ sops -e kubernetes/test12/CRABServerAuth.py > CRABServerAuth.sops.py
$ sops -d kubernetes/test12/CRABServerAuth.sops.py > CRABServerAuth.py
```

Note that you **do not need** to decrypt the files manually in order to apply config in kubernetes. See [Apply secrets to Kubernetes](#apply-secrets-to-kubernetes).


!!! note "Behind the scene"
    when you run `sops <filepath>`. `sops` decrypt the file, save to `/tmp` and open with `$EDITOR`. After you save and exit editor, `sops` re-encrypt and update original file. `sops` know which key and encryption backend by reading configuration file from [crab-secrets/.sops.yaml](https://gitlab.cern.ch/crab3/crab-secrets/-/blob/master/.sops.yaml)



## Re-encrypt files with new key

!!! warning
    - Only person whose key is already in use can update new keys.
    - After update new key, please annouce to team. Everyone need to import new key to their own gpg database.

1. Import new key into your gpg db
```
gpg --import pubkey/mycernuser.pub
```
2. Update [.sops.yaml](https://gitlab.cern.ch/crab3/crab-secrets/-/blob/master/.sops.yaml) with newly imported in your db. Do not forget to put `,` at the end of fingerprint except last line:
3. Run `sops updatekeys` command to re-encrypt files with new key.
    ```
    sops updatekeys CRABServerAuth.py.sops
    ```
    For update all files in db use this command (thanks to <https://github.com/mozilla/sops/discussions/919>).
    ```
    find . -name '*.sops.py' -exec sops updatekeys --yes {} \;
    ```

## Apply secrets to Kubernetes

!!! warning
    - Do this on a machine with cloud clients (e.g. lxplus8), gpg will require your passphrase. Usually a text window appears in your terminal, but in some cases the passphrase input is routed to $DISPLAY, in that case make sure that X11 forwarding works.
    - Make sure that you have openstack token and that KUBECONFIG points to relevant config of the cluster.

Use [deploy-secrets.sh](https://gitlab.cern.ch/crab3/crab-secrets/-/blob/master/deploy-secrets.sh) from crab-secrets repo instead.

Run `deploy-secrets.sh` same way as original one in from CMSKubernetes. Script will decrypt `*.sops.py` and apply to kubernetes for you.
```
# run at root of repo's directory
./deploy-secrets.sh  crab crabserver kubernetes/test12/
```
Then restart pods to make them pick new files from /etc/secrets.
```
kubectl -n crab rollout restart deployment/crabserver
```

## Odd problems with gpg

If you get odd errors when gpg tries to use your keys, it may be due to a stale agent being running.
Try `killall gpg-agent`

## Show diffs in cleartext with git

It is possible to use sops in conjunction with git to show the diff of the content
of a file across commits.
These instructions have been taken from [github.com/mozilla/sops](https://github.com/mozilla/sops#47showing-diffs-in-cleartext-in-git)

We already created the file `.gitattributes` with the proper content. No need to edit it.

You need to change your git repo config to tell git to use `sops` when using `git diff`.
In order to achieve it, just run

```bash
$ git config diff.sopsdiffer.textconv "sops -d"
```

then check its changes with

```bash
$ grep -A 1 sopsdiffer .git/config
[diff "sopsdiffer"]
        textconv = "sops -d"
```

Now you can use `git diff` as always, for example

```diff
> git diff 2aa3cf3...acd451e kubernetes/test11/crabserver/CRABServerAuth.sops.py
diff --git a/kubernetes/test11/crabserver/CRABServerAuth.sops.py b/kubernetes/test11/crabserver/CRABServerAuth.sops.py
index 17b5c32..bfe7e05 100644
--- a/kubernetes/test11/crabserver/CRABServerAuth.sops.py
+++ b/kubernetes/test11/crabserver/CRABServerAuth.sops.py
@@ -29,7 +29,7 @@ dbconfig = {'preprod': {'.title': 'Pre-production',
             'devtwo': {'.title': 'Development2',
                         '.order': 3,
                         '*': {'clientid': 'cmsweb-dev2@%s' %(fqdn),
-                              'dsn': 'int9r',
+                              'dsn': 'int2r',
                               'liveness': 'select sysdate from dual',
                               'password': 'xxxxxxxxxxxxxxxx',
                               'schema': 'cmsweb_crab_9r_dev',
```
