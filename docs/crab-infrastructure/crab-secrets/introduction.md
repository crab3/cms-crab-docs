# CRAB Secrets

We store all credentials we share together in our team in the `crab-secrets` repository, including Oracle DB password (in form of K8s Secret), CERN service account user and password, etc.

Repository: [crab3/crab-secrets](https://gitlab.cern.ch/crab3/crab-secrets/)

We use `sops` with `gpg` to encrypt/decrypt the credentials. The repository contains both encrypted and non-encrypted files.

!!! note
    Encrypted files are marked by inserting `.sops` in the name.

Read more on [sops](https://github.com/getsops/sops/blob/main/README.rst) and [gpg](https://www.gnupg.org/gph/en/manual.html) handbook.

## `crab-secrets` directory tree

```
├── deploy-secrets.sh
├── kubernetes
│   ├── test2
│   │   └── crabserver
│   │       ├── config.py
│   │       └── CRABServerAuth.sops.py
│   ├── preprod
│   │   └── crabserver
│   │       ├── config.py
│   │       └── CRABServerAuth.sops.py
│   ├── ...
│   └── sample
│       ├── config.py
│       └── CRABServerAuth.py
├── pubkey
│   ├── tseethon.pub
│   └── youremail.pub
├── service_account
│   ├── accountname.sops.password
│   ├── ...
├── ...
└── .sops.yaml
```

- `kubernetes` CRAB REST K8s Secrets.
- `deploy-secrets.sh` script to deploy secrets to kubernetes.
- `pubkey` public key used to encrypt with sops+gpg.
- `opensearch`: secrets to send data to opensearch using https
- `service_account`: passwords, robot certs and passphrases for service account
- `.sops.yaml` sops configuration, store key fingerprint.


## Note about syncing REST secrets to CMSWEB

The REST secrets are stored in [CRABServerAuth.sops.py](https://gitlab.cern.ch/crab3/crab-secrets/blob/master/kubernetes/prod/crabserver/CRABServerAuth.sops.py). However, CMSWEB operators does not have access to our repository. So, we need to make a copy and put it into [`CRABServerAuth.py.encrypted` in `service_config`](https://gitlab.cern.ch/cmsweb-k8s/services_config/-/blob/prod/crabserver/CRABServerAuth.py.encrypted) where CMSWEB operators can access it. Make sure this file is up-to-date.

`CRABServerAuth.py.encrypted` is encrypted by different method compare to us. Please consult [CMSWEB docs](https://cms-http-group.docs.cern.ch/k8s_cluster/encrypt-decrypt-secrets/) on how to encrypt/decrypt this file.

This is crucial especially production cluster (i.e. file in `prod` branch). When CMSWEB operators migrate the services to new clusters, they will read this file and apply to new clusters.

Please see Q&A from Stefano and Dario in [services\_config!243](https://gitlab.cern.ch/cmsweb-k8s/services_config/-/merge_requests/243#note_7619474) for the real use case that happened in the past.
