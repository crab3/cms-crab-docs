# S3 how tos, by prajesh

!!! warning "disclaimer"
    Dario imported the content from original file in by prajesh [google doc](https://docs.google.com/document/d/15fKJ-AgMVlN3Y5mlnb8gar7iLm5V9sNlaGNzfWSUqSY/edit#bookmark=id.lki2b88bt9d5).
    He did not proofread every single detail, he just made sure that the formatting in markdown looks ok


## S3 Buckets in CRAB

ASW DOCs about S3 presigned URL: [Accessing an object using a presigned URL - Amazon Simple Storage Service](https://docs.aws.amazon.com/AmazonS3/latest/userguide/ShareObjectPreSignedURL.html)

Python bindings for S3 bucket: [Presigned URLs — Boto 3 Docs 1.9.185 documentation](https://boto3.amazonaws.com/v1/documentation/api/1.9.185/guide/s3-presigned-urls.html)

OpenStack quota request for S3 object store: [https://clouddocs.web.cern.ch/object_store/README.html](https://clouddocs.web.cern.ch/object_store/README.html)


## Openstack - manage buckets

### How to login in OpenStack

```plaintext
ssh prsharma@aiadm.cern.ch
```

This command will show you all associated projects with your user. By default your personal project is activated i.e. when you fire a command it will execute on your personal project.

```plaintext
[prsharma@aiadm53 ~]$ openstack project list
+--------------------------------------+-------------------------+
| ID                               	| Name                	|
+--------------------------------------+-------------------------+
| 0a020f1f-729c-4186-be10-6f81bd9a9f43 | CMS HTCondor - LCG  	|
| 580b613d-85da-453c-9b59-012549ebfd9e | CMS CRAB            	|
| 6d587738-5e51-42f9-9666-406a4df6ee25 | CMS HTCondor - Physical |
| e7703cf3-7391-4cb9-a360-3ff8296aaeee | Personal prsharma   	|
+--------------------------------------+-------------------------+
```

 Some time it is difficult to understand output printed by openstack command in table format. You can change output format in the following way:

```plaintext
[prsharma@aiadm53 ~]$ openstack project list -f json
[
  {
	"ID": "0a020f1f-729c-4186-be10-6f81bd9a9f43",
	"Name": "CMS HTCondor - LCG"
  },
  {
	"ID": "580b613d-85da-453c-9b59-012549ebfd9e",
	"Name": "CMS CRAB"
  },
  {
	"ID": "6d587738-5e51-42f9-9666-406a4df6ee25",
	"Name": "CMS HTCondor - Physical"
  },
  {
	"ID": "e7703cf3-7391-4cb9-a360-3ff8296aaeee",
	"Name": "Personal prsharma"
  }
]
```

You may also use yaml format.

```plaintext
[prsharma@aiadm53 ~]$ openstack project list -f yaml
```


### How to change project

```plaintext
[prsharma@aiadm53 ~]$ eval $(ai-rc 'CMS CRAB')
OpenStack environment variables set for Project = 'CMS CRAB'
```

verify that you have proper token issued by openstack and corresponding project id

```plaintext
[prsharma@aiadm53 ~]$ openstack token issue -f json
{
  "project_id": "580b613d-85da-453c-9b59-012549ebfd9e",       
  "expires": "2021-02-19T18:59:21+0000",
  "user_id": "prsharma",
  "id": "gAAAAABgL5oJHqyKk4b4fmPEbB4hnGIZk4xALIORT5k8AHDhcGsjqZVzRl06VuQsQrGY-5Ec5ntQmge4qPk0nc3iXDyJ1ueVgpsPmbe-sQ8Nu2iZwLqHP0O0BMwxz-WorWrE2UHsM0IKYf4PibsmnCtjKSCm7tl03s5kKpUYkCGzXB2KudWt_7riC__d2LLZ2NPb2cL_7Ejtex9AanohhTuuMxSqkLnfwQ"
}

[prsharma@aiadm53 ~]$ openstack project list -f json
[
  {
	"ID": "0a020f1f-729c-4186-be10-6f81bd9a9f43",
	"Name": "CMS HTCondor - LCG"
  },
  {
	"ID": "580b613d-85da-453c-9b59-012549ebfd9e",
	"Name": "CMS CRAB"
  },
  {
	"ID": "6d587738-5e51-42f9-9666-406a4df6ee25",
	"Name": "CMS HTCondor - Physical"
  },
  {
	"ID": "e7703cf3-7391-4cb9-a360-3ff8296aaeee",
	"Name": "Personal prsharma"
  }
]
```


### How to list existing buckets in “CMS CRAB” project

In openstack terminology bucket means Container

Now I am assuming that we are in “CMS CRAB” project set by previous section

```plaintext
[prsharma@aiadm53 ~]$ openstack container list
+---------+
| Name	|
+---------+
| bucket1 |
+---------+
[prsharma@aiadm53 ~]$ openstack container list --long
+---------+-------+-------+
| Name	| Bytes | Count |
+---------+-------+-------+
| bucket1 |	25 | 	1 |
+---------+-------+-------+
```

### How to show properties of a bucket

```plaintext
[prsharma@aiadm53 ~]$ openstack container  show bucket1
+--------------+---------+
| Field    	| Value   |
+--------------+---------+
| account  	| v1  	|
| bytes_used   | 25  	|
| container	| bucket1 |
| object_count | 1   	|
+--------------+---------+
```

### How to create a bucket



```
[prsharma@aiadm53 ~]$ openstack container  create   bucket2
+---------+-----------+-----------------------------------------------------+
| account | container | x-trans-id                                      	|
+---------+-----------+-----------------------------------------------------+
| v1  	| bucket2   | tx0000000000000135879ac-00602f9ef7-14b58265-default |
+---------+-----------+-----------------------------------------------------+


[prsharma@aiadm53 ~]$ openstack container list --long
+---------+-------+-------+
| Name	| Bytes | Count |
+---------+-------+-------+
| bucket1 |	25 | 	1 |
| bucket2 | 	0 | 	0 |
+---------+-------+-------+
```

## aws cli - how to manage buckets from the cli

### setup the aws cli

Install the aws cli using the official [docs](https://docs.aws.amazon.com/cli/latest/userguide/getting-started-install.html) or the packages provided by your distro. Dario recommends
installing the aws cli v2, since the v1 is not updated since a while

for example:

```bash
# fedora
sudo dnf install awscli2
```

then setup the credentials for the "CMS CRAB" openstack
project

```plaintext
> cat .aws/credentials
[crab-2024]
aws_access_key_id = ${EC2_ACCESS_KEY_FROM_OPENSTACK}
aws_secret_access_key = ${EC2_SECRET_KEY_FROM_OPENSTACK}
```

then test that it works with

```plaintext
> aws --endpoint-url=https://s3.cern.ch --profile crab-2024 s3 ls
2024-02-06 14:56:03 crab_ci
2022-02-09 12:02:11 crab_public
2022-06-28 17:47:17 crab_shared
2021-05-13 15:32:59 crab_test1
2021-05-13 15:33:09 crab_test2
2021-05-13 15:21:45 crabcache_dev
2021-05-13 15:32:36 crabcache_preprod
2021-05-13 15:32:47 crabcache_prod
```

You can list your credentials with


```bash
openstack ec2 credential list
```

you can see more details related to some credential with

```bash
openstack ec2 credential show $ec2_acces_key 
```

### aws cli - manage expiration for a whole bucket

Ref:

- https://docs.aws.amazon.com/cli/latest/reference/s3api/get-bucket-lifecycle-configuration.html
- https://docs.aws.amazon.com/cli/latest/reference/s3api/put-bucket-lifecycle-configuration.html

!!!warning
    `(get|put)-bucket-lifecycle-configuration` are the preferred apis, `(get|put)-bucket-lifecycle` is kept only for backward compatibility

create a json file `./config_preprod_lifecycle.json`

```json
{
    "Rules": [
        {
            "Expiration": {
                "Days": 63
            },
            "Prefix": "",
            "Status": "Enabled"
        }
    ]
}
```

pushn the configuration with

```bash
aws \
  --endpoint-url=https://s3.cern.ch --profile crab-2024 \
  s3api \
  put-bucket-lifecycle-configuration \
  --lifecycle-configuration file://config_preprod_lifecycle.json \
  --bucket crabcache_preprod
```

read the configuration with

```bash
aws \
  --endpoint-url=https://s3.cern.ch --profile crab-2024 \
  s3api \
  get-bucket-lifecycle-configuration \
  --bucket crabcache_preprod
```

## Boto2: How to access a bucket from python boto package

To access a bucker we need openstack Credentials

```plaintext
[prsharma@aiadm53 ~]$ S3_HOST=$(openstack catalog show s3 -f json -c endpoints | jq --raw-output '.endpoints[] | select(.interface | contains("public")) | .url' | cut -f3 -d/)

[prsharma@aiadm53 ~]$ ACCESS_KEY=$(openstack ec2 credentials create -f value -c access)

[prsharma@aiadm53 ~]$ SECRET_KEY=$(openstack ec2 credentials show $ACCESS_KEY -f value -c secret)

[prsharma@aiadm53 ~]$ echo $S3_HOST
s3.cern.ch

[prsharma@aiadm53 ~]$ echo $ACCESS_KEY
4f4a362a480043bea61ea165ef50XXXX


[prsharma@aiadm53 ~]$ echo $SECRET_KEY
66cd6XXXXXXXXXXXXXXXXXXXXXXXXXXX
```

Use the above host and keys in your S3-compatible application. Please note that, although the generation of the credentials is instantaneous, it can take up to 15mn before the key gets propagated - you will be denied access in the meantime.

I put XXXX in the last four digits of Keys for security reasons.


### Boto2 example to upload a file in a bucket

```plaintext
[prsharma@testmac2 s3]$ python --version
Python 2.7.5

>>> boto.Version
'2.49.0'
```

```python
#upload.py
import boto
from boto.s3.key import Key

hostname="s3.cern.ch"
access_key="FIX_ME"
secret_key="FIX_ME"

bucket_name="bucket1"

connection = boto.connect_s3(host=hostname,aws_access_key_id=access_key,aws_secret_access_key=secret_key)
s3_bucket = connection.get_bucket(bucket_name)

#===================================================

k = Key(s3_bucket)
k.key = 'datafile_data.txt'
k.set_contents_from_filename('data.txt')

#===================================================
```


data.txt file should be in your current directory

```plaintext
[prsharma@testmac2 s3]$ python upload.py
[prsharma@testmac2 s3]$ cat data.txt
Hello from data.txt
```


### Boto2 example to download a file from a bucket

```python
#download.py

import boto
from boto.s3.key import Key

hostname="s3.cern.ch"
access_key="FIX_ME"
secret_key="FIX_ME"

bucket_name="bucket1"

connection = boto.connect_s3(host=hostname,aws_access_key_id=access_key,aws_secret_access_key=secret_key)
s3_bucket = connection.get_bucket(bucket_name)

#===================================================

k = Key(s3_bucket)
k.key = 'datafile_data.txt'
k.get_contents_to_filename('data_down.txt')

#===================================================
```

```plaintext
[prsharma@testmac2 s3]$ cat data_down.txt
Hello from data.txt
```

### Boto2: How to download a file from bucket using pre-signed url

We do not have public access to the bucket as well as file(key) within the bucket but we can generate pre-signed url.

After that we can download file via this pre-signed url from any public location.

```python
# [prsharma@testmac2 s3]$ cat presigned-download.py
import boto
from boto.s3.key import Key

hostname="s3.cern.ch"
access_key="FIX_ME"
secret_key="FIX_ME"
bucket_name="bucket1"

connection = boto.connect_s3(host=hostname,aws_access_key_id=access_key,aws_secret_access_key=secret_key)
bucket = connection.get_bucket(bucket_name)

#===================================================

file_key = bucket.get_key('datafile_data.txt')
file_url = file_key.generate_url(60, query_auth=True, force_http=True)
print file_url

#===================================================
```


```plaintext
[prsharma@testmac2 s3]$ python presigned-download.py
http://bucket1.s3.cern.ch/datafile_data.txt?Signature=2zGVFO0hNLXVe1cm7EHR%2BW4fmhE%3D&Expires=1614633866&AWSAccessKeyId=5d4270f1e022442783646c34cf552d55
```


### Boto2: How to make a file(key) public within a private bucket

```python
# [prsharma@testmac2 s3]$ cat  unsigned-download.py
import boto
from boto.s3.key import Key

hostname="s3.cern.ch"
access_key="FIX_ME"
secret_key="FIX_ME"
bucket_name="bucket1"

connection = boto.connect_s3(host=hostname,aws_access_key_id=access_key,aws_secret_access_key=secret_key)
bucket = connection.get_bucket(bucket_name)

#===================================================

file_key = bucket.get_key('key1')
file_key.set_canned_acl('public-read')               #<------------
file_url = file_key.generate_url(0, query_auth=False, force_http=True)
print file_url

#===================================================
```

```bash
[prsharma@testmac2 s3]$ python unsigned-download.py
http://bucket1.s3.cern.ch/key1

[prsharma@testmac2 s3]$ curl http://bucket1.s3.cern.ch/key1
This is a test key1 of S3
```

## Boto3: programming examples

```plaintext
[prsharma@testmac2 s3-boto3]$ cat /etc/os-release | head -2
NAME="CentOS Linux"
VERSION="7 (Core)"
[prsharma@testmac2 s3-boto3]$ sudo pip2 install boto3
[prsharma@testmac2 s3-boto3]$ python --version
Python 2.7.5
>>> boto3.__version__
'1.17.17'

[prsharma@testmac2 s3-boto3]$ sudo pip3 install boto3
[prsharma@testmac2 s3-boto3]$ python3 --version
Python 3.6.8
>>> boto3.__version__
'1.17.24'
```

### How to use text credential file in boto3:
In the following program, I am using following file 

```plaintext
[prsharma@lxplus770 s3-boto3]$ cat credentials
[default]
aws_access_key_id = <put you key here without “ ” >
aws_secret_access_key = <access key>
```

Set the following env variable according to the above file location:
export AWS_SHARED_CREDENTIALS_FILE=/afs/cern.ch/user/p/prsharma/myscripts/s3-boto3/credentials
Now there is no need to put keys in the python code and functions.

For example:

```python
import boto3
import logging

from botocore.exceptions import ClientError

bucketname='bucket1'

endpoint='https://s3.cern.ch'
file_name="upload.txt"
object_name="crab/data/upload_from_boto3"

conn = boto3.client('s3',endpoint_url=endpoint)

try:
    	response = conn.upload_file(file_name, bucketname, object_name)

except ClientError as e:

    	logging.error(e)
```

### Boto3: Upload file

```python
# [prsharma@lxplus770 s3-boto3]$ cat upload_file.py
import boto3
import logging

from botocore.exceptions import ClientError

bucketname='bucket1'
endpoint='https://s3.cern.ch'
file_name="upload.txt"
object_name="upload_from_boto3"

conn = boto3.client('s3', endpoint_url=endpoint, verify=False)

try:
    	response = conn.upload_file(file_name, bucketname, object_name)

except ClientError as e:

    	logging.error(e)
```

Execution via Python 2 : It is working but showing warnings.

```plaintext
[prsharma@lxplus770 s3-boto3]$ python  upload_file.py
/afs/cern.ch/user/p/prsharma/.local/lib/python2.7/site-packages/boto3/compat.py:86: PythonDeprecationWarning: Boto3 will no longer support Python 2.7 starting July 15, 2021. To continue receiving service updates, bug fixes, and security updates please upgrade to Python 3.6 or later. More information can be found here: https://aws.amazon.com/blogs/developer/announcing-end-of-support-for-python-2-7-in-aws-sdk-for-python-and-aws-cli-v1/
  warnings.warn(warning, PythonDeprecationWarning)
```

Execution via Python 3 : It is working. Silent execution i.e. no error no warning.

```plaintext
[prsharma@testmac2 s3-boto3]$  python3  upload_file.py
[prsharma@testmac2 s3-boto3]$
```

### Boto3: download file

```python
# [prsharma@testmac2 s3-boto3]$ cat download_file.py
import boto3
import logging


bucketname='bucket1'
endpoint='https://s3.cern.ch'
file_name="download.txt"
object_name="upload_from_boto3"

conn = boto3.client('s3', endpoint_url=endpoint, verify=False)

try:
    	response = conn.download_file(bucketname, object_name,file_name)

except ClientError as e:

    	logging.error(e)
```

Execution of python2 and python3: Same as above

### Boto3: download via presigned url

```python
# [prsharma@testmac2 s3-boto3]$ cat download_via_presigned.py
import boto3
import logging

from botocore.exceptions import ClientError

bucket_name='bucket1'
access_key="xyz"
secret_key="abc"

endpoint='https://s3.cern.ch'
object_name="upload_from_boto3"
expiration="60"

conn = boto3.client('s3',endpoint_url=endpoint)

try:
    	url = conn.generate_presigned_url('get_object',Params={'Bucket': bucket_name,'Key': object_name},ExpiresIn=expiration)
    	print(url)


except ClientError as e:

    	logging.error(e)
```

Execution of python2 and python3: Same as above

### Boto3: upload via presigned url

```python
# [prsharma@testmac2 s3-boto3]$ cat upload_via_presigned.py
import boto3
import logging
import requests


from botocore.exceptions import ClientError

bucket_name='bucket1'
access_key="xyz"
secret_key="abc"

endpoint='https://s3.cern.ch'
object_name="upload_from_presigned_boto3"
expiration="60"

conn = boto3.client('s3',endpoint_url=endpoint)

try:
    	response  = conn.generate_presigned_post(bucket_name,object_name,ExpiresIn=expiration)
)
    	print(response )

# when called with object_name= 'belforte/22:belforte_uffa/sandbox' it returns a dicionary like
# {'url': u'https://s3.cern.ch/bucket1', 'fields': {'policy': u'eyJjb25kaXRpb25zIjogW3siYnVja2V0IjogImJ1Y2tldDEifSwgeyJrZXkiOiAiYmVsZm9ydGUvMjI6YmVsZm9ydGVfdWZmYS9zYW5kYm94In1dLCAiZXhwaXJhdGlvbiI6ICIyMDIxLTAzLTI0VDE3OjUxOjEzWiJ9', 'AWSAccessKeyId': u'5d4270f1e022442783646c34cf552d55', 'key': b'belforte/22:belforte_uffa/sandbox', 'signature': u'pm58cUqxNQHBZXS1B/Er6P89IhU='}}

# need to convertt into a single url to be used in a POST

    	with open(object_name, 'rb') as f:
            	files = {'file': (object_name, f)}
            	http_response = requests.post(response['url'], data=response['fields'], files=files)


    	# If successful, returns HTTP status code 204
    	logging.info('File upload HTTP status code: {http_response.status_code}')



except ClientError as e:

    	logging.error(e)
```

we can’t use python requests in CRABClient, and will rather user curl like

`curl --cert=$X509_USER_PROXY --key=....   -X POST …….  Etc. etc.` (exact syntax to be sorted out)

This works, from https://gist.github.com/henriquemenezes/61ab0a0e5b54d309194c
(line breaks added for readibility)

```plaintext
curl -v  
-F "policy=eyJjb25kaXRpb25zIjogW3siYnVja2V0IjogImJ1Y2tldDEifSwgeyJrZXkiOiAic2FuZGJveCJ9XSwgImV4cGlyYXRpb24iOiAiMjAyMS0wMy0yNFQyMDo0OTo0NFoifQ=="
 -F "AWSAccessKeyId=5d4270f1e022442783646c34cf552d55"
 -F "key=sandbox"
 -F "signature=BUgZwaT6ABfU05qLwFGyVnD3BzM="
 -F "file=@/etc/hosts"
 https://s3.cern.ch/bucket1
```


In any case the “presigned URL” is not an URL in the end. It is a data structure to be properly parsed, quoted, et. in order to be used as arguments in an curl command or in a python module like requests or pycurl_manager or ….

Execution of python2 and python3: Same as above


### Boto3: File upload to the directory within bucket

We can create a directory structure in S3 bucket and upload/download files to/from there.
For example: crab is a directory in bucket1 and data is another directory within the crab directory.(line 12)

```python
# [prsharma@testmac2 s3-boto3]$ cat -n upload_file_todir2.py
import boto3
import logging
from botocore.exceptions import ClientError
bucketname='bucket1'
access_key="abc"
secret_key="xyz"
endpoint='https://s3.cern.ch'
file_name="upload.txt"
object_name="crab/data/upload_from_boto3"
# in practice we will use things like ‘user/prsharma/210320_120314:prsharma_something/log.txt”
# need to check if we have to translate ‘:’ and/or ‘_,-, .’ etc.
conn = boto3.client('s3', endpoint_url=endpoint, aws_access_key_id=access_key, aws_secret_access_key=secret_key)
try:
	response = conn.upload_file(file_name, bucketname, object_name)
except ClientError as e:
	logging.error(e)
```

### Boto3: List files from bucket

```python
#[prsharma@testmac2 s3-boto3]$ cat list_bucket_keys.py
import boto3

bucketname='bucket1'
access_key="xyz"
secret_key="abc"

endpoint='https://s3.cern.ch'

conn = boto3.client('s3',endpoint_url=endpoint,aws_access_key_id=access_key,aws_secret_access_key=secret_key)


for key in conn.list_objects(Bucket=bucketname)['Contents']:
    	print(key['Key'])
```

```plaintext
[prsharma@testmac2 s3-boto3]$ python3 list_bucket_keys.py
crab/
crab/data/
crab/data/upload_from_boto3
crab/upload_from_boto3
datafile_data.txt
key1
key2
upload_from_boto3
upload_from_presigned_boto3
```

### Boto3: Create/Delete file/folder in the bucket

If a key name contains “/” at the end, then boto will treat it as a directory otherwise file.
If “/” is between names then it will create the whole path i.e. if there are some dirs in the path then it will create it itself. If the key (file) is there already, it will be overwritten (replaced).

I also tested the following functionality in a file upload example.

```python
# Naming rule :	https://docs.aws.amazon.com/AmazonS3/latest/userguide/object-keys.html
import boto3

bucket_name='bucket1'
endpoint='https://s3.cern.ch'
conn = boto3.client('s3',endpoint_url=endpoint)


key_name="user/prsharma/210320_120314:prsharma_something/log.txt"

#Then we can imagine to have APIs in CRABServer REST which read the file, like the following examples:
#https://cmsweb.cern.ch/crabserver/prod/crabcache?getlog&username=prsharma&logname=210320_120314:prsharma_something/clientlog.txt
#https://cmsweb.cern.ch/crabserver/prod/crabcache?clientlog&username=prsharma&taskname=blah
#https://cmsweb.cern.ch/crabserver/prod/crabcache?TWlog&username=prsharma&taskname=blah


r=conn.put_object(Bucket=bucket_name, Key=key_name)

#r=conn.delete_object(Bucket=bucket_name, Key=key_name)

print("*"*80)
print(r)
print("*"*80)

i=1
for key in conn.list_objects(Bucket=bucket_name)['Contents']:
    	print(i,key['Key'])
    	i=i+1
```


### Boto3: Make a file public and download it via url

I did not find any direct method to get the url of public file i.e. no concept for an un-signed url.

There is no meaning to create a public folder (tested), i.e. If we create a file in a public folder then the file will not be public by default.

```python
[prsharma@testmac2 s3-boto3]$ cat -n make_public_and_download.py
 	1  import boto3
 	2
 	3
 	4  bucket_name='bucket1'
 	5  endpoint='https://s3.cern.ch'
 	6
 	7  conn = boto3.resource('s3',endpoint_url=endpoint)
 	8  object_name="upload_from_boto3"
 	9
	10
	11  object_acl = conn.ObjectAcl(bucket_name,object_name)
	12  r = object_acl.put(ACL='public-read')
	13
	14  print(r)
	15  print("*"*80)
	16
	17
	18
	19  print("http://"+bucket_name+".s3.cern.ch/"+object_name)
	20
```

```plaintext
[prsharma@testmac2 s3-boto3]$ python3  make_public_and_download.py
{'ResponseMetadata': {'RequestId': 'tx00000000000000589eb9d-0060505078-1755ec7b-default', 'HostId': '', 'HTTPStatusCode': 200, 'HTTPHeaders': {'bucket': 'bucket1', 'content-length': '0', 'content-type': 'application/xml', 'date': 'Tue, 16 Mar 2021 06:30:16 GMT', 'x-amz-request-id': 'tx00000000000000589eb9d-0060505078-1755ec7b-default'}, 'RetryAttempts': 0}}
********************************************************************************
http://bucket1.s3.cern.ch/upload_from_boto3
```

### Boto3: Set expiration on a folder in the bucket

- it is not possible to expire days in less than a day.
- expiration is always at midnight on a daily basis, which may be less than `creationtime+(24*expirationday)hours`. E.g. a file created at 11:55pm with 1 day expiration time will be removed after 5 min.

```python
import boto3
import logging

from botocore.exceptions import ClientError
bucketname='bucket1'
endpoint='https://s3.cern.ch'
conn = boto3.client('s3',endpoint_url=endpoint)

try:
    	policy_status = conn.put_bucket_lifecycle_configuration( Bucket=bucketname,
                    	LifecycleConfiguration={
                            	'Rules':
                            	[
                                    	{
                                            	'Expiration':
                                            	{
                                                    	'Days': 2,

                                            	},
                                            	'Prefix': 'expireInTwoDay/',
                                            	'Filter': {
                                                            	'Prefix': 'expireInTwoDay/'
                                                      	},
                                            	'Status': 'Enabled'
                                    	}
                            	]})
except ClientError as e:
 	print("Unable to apply bucket policy. \nReason:{0}".format(e))
```

specify an empty prefix to make rule apply to whole bucker, see https://github.com/boto/boto3/issues/2718#issuecomment-757009060  which refers to
https://docs.aws.amazon.com/AmazonS3/latest/userguide/lifecycle-configuration-examples.html




### Boto3: Set expirartion for a whole bucket


Example:

```python
LFC={'Rules': [ {'Expiration': {'Days': 7}, 'Filter': {'Prefix': ''}, 'Status':'Enabled' } ]}
responseP= s3_client.put_bucket_lifecycle_configuration(Bucket='CRABCacheDev', LifecycleConfiguration=LFC)
pprint.pprint(responseP)
print('==========================================================================')
response3 = s3_client.get_bucket_lifecycle_configuration(Bucket='CRABCacheDev')
pprint.pprint(response3['Rules'])
```

Output from that:

```plaintext
{'ResponseMetadata': {'HTTPHeaders': {'bucket': 'CRABCacheDev',
                                      'content-length': '0',
                                      'content-type': 'application/xml',
                                      'date': 'Thu, 22 Apr 2021 11:57:24 GMT',
                                      'x-amz-request-id': 'tx000000000000007153841-00608164a4-18c14690-default'},
                      'HTTPStatusCode': 200,
                      'HostId': '',
                      'RequestId': 'tx000000000000007153841-00608164a4-18c14690-default',
                      'RetryAttempts': 0}}
==========================================================================
[{u'Expiration': {u'Days': 7},
  u'ID': '41lxbtln2cf15xvbgwn8n9m7q8t3ayo5fc400k19njei9jjk',
  u'Prefix': '',
  u'Status': 'Enabled'}]
```

### Boto3: Display size of each key in a folder

```python
import boto3
bucketname='bucket1'
endpoint='https://s3.cern.ch'
conn = boto3.client('s3',endpoint_url=endpoint)

print("_"*110)
print("%-10s\t%-50s\t%-50s" % ('Size','Key','LastModified'))
print("_"*110)
t=0
for key in conn.list_objects(Bucket=bucketname,Prefix='crab/data/')['Contents']:
    	print("%-10d\t%-50s\t%-50s" % (key['Size'],key['Key'],key['LastModified']))
    	t=t+key['Size']
print("_"*110)
print("Total Size:",t)
```

```plaintext
[prsharma@testmac2 s3-boto3]$ python3 list_folder_keys.py
___________________________________________________________________________________________________
Size        	Key                                                 	LastModified
___________________________________________________________________________________________________
0           	crab/data/                                          		2021-03-10 05:37:01.840000+00:00
9           	crab/data/upload_from_boto3                         	2021-03-10 20:48:52.019000+00:00
28          	crab/data/upload_from_boto3_Daina              2021-03-10 21:01:18.752000+00:00
___________________________________________________________________________________________________
Total Size: 37
```

### Boto2: S3 Scale test

Shell script to create files of size 1 MB to 100 MB

```bash
# [root@testmac2 scale]# cat -n genfile.sh
#!/bin/bash
i=1
while [ $i -le 100 ]
do
    dd if=/dev/zero of=files/$i bs=$(echo "$i*1024" | bc) count=1024
    i=`expr $i + 1`
done
```

```python
#!/usr/bin/python
import thread
import time
from random import randrange
import boto
from boto.s3.key import Key
def upload(name):
    	hostname="s3.cern.ch"
    	access_key="XYZ"
    	secret_key="XYZ"
    	bucket_name="bucket1"
   	connection = boto.connect_s3(host=hostname,aws_access_key_id=access_key,aws_secret_access_key=secret_key)
    	s3_bucket = connection.get_bucket(bucket_name)
    	#===================================================
    	k = Key(s3_bucket)
    	k.key = 'stest/' + str(name)
    	k.set_contents_from_filename('/root/scale/files/'+str(name))
    	#===================================================
# function for the thread
def print_time( threadName):
global t
count = 1
#delay=30
while count <= 200:
  	#time.sleep(delay)
  	count += 1
  	name=randrange(1,100)
  	print("%s: %s : %d : %d" % ( threadName, time.ctime(time.time()) ,t,name))
  	upload(name)
t=t+1
# Create 10 threads as follows
try:
global t
t=1
i=1
while i<=10:
       	thread.start_new_thread( print_time, ("Thread-"+str(i),) )
       	i=i+1
except:
print("Error: unable to start thread")
while t<=10:
pass
```


## Some hints from Dan van Der Steer about listing objects

I have tested these and am implementing now in CRABServer RESTCache


Stefano Belforte

> Hi experts. I think that we'll need some organization in our Bucket, as per https://docs.aws.amazon.com/AmazonS3/latest/userguide/using-prefixes.html and while single objet upload/download is now under control and integrated in CRAB code, I realized that listing object(keys) may be  a bit tricky given the limit of 1K keys in each list_object call. (E.g. I would like to do crabache/username/.... but we have more than 1K users). I guess we can figure out via trial and error, but if you have some example on how to use boto3 to sort of list the directories there... it will make our work easier and faster. Thanks
(edited)

Dan van der Ster

> Hi. Have you tried boto3 pagination?
> https://adamj.eu/tech/2018/01/09/using-boto3-think-pagination/
> Using boto3? Think pagination! - Adam Johnson



Stefano Belforte

> never heard of it before. I will check. thanks.

Dan van der Ster

> This uses "listobjectsv2", which ceph didn't support until recently. Please let us know if it's working for you :slightly_smiling_face:
For my ceph colleagues watching along, here is where ceph added support for v2 pagination: https://tracker.ceph.com/issues/42280

Stefano Belforte

> looks like it will solve the >1K user issue w/o needing to resort to user/b/belforte etc.

Dan van der Ster

> The first comment here looks better:
https://www.reddit.com/r/aws/comments/7p5rhl/using_boto3_think_pagination_adam_johnsons_blog/
how many objects would you expect in this bucket at full scale?

Stefano Belforte

> Still, since I am a bit thick, an example of how to deal with prefixes and terminators to mimic ls ... would be great.

Dan van der Ster

> https://stackoverflow.com/questions/44373854/in-boto3-how-to-create-a-paginator-for-list-objects-with-additional-keyword-arg

Stefano Belforte

> I expect the following hierarchy of objectds:
bucket/username(up to 2k)/sandboxes(up to a few 100's/
bucket/username(up to 2k)/taskname(up to a 1k)/a handful of objects per task
yeah.... given that we expire things in a month, maybe we can have O(10k) object in the bucker at any time and retrieve them all and sort paths in memory client side...
I do not have a good number, but surely <100K

Dan van der Ster

> 10k is trivial.
10M would be at the limit.
100M would be infeasible.
so <100k is trivial too :slightly_smiling_face:
if you want > 10M, use multiple buckets.

Stefano Belforte

> no way we can run more than 100K crab tasks in one month.

Dan van der Ster

> ok perfect. this should be simple on our s3 server side

Stefano Belforte

> we need to start pushing some O(100MB) files and see how you do :slightly_smiling_face:

Dan van der Ster

> indeed, if you point the whole grid at s3.cern.ch we'll suffer

Stefano Belforte

> it is O(1Hz)

Dan van der Ster

> ok no worries then :slightly_smiling_face:


## Useful links

- https://boto3.amazonaws.com/v1/documentation/api/latest/reference/services/s3.html?highlight=expiration#S3.Client.put_bucket_lifecycle_configuration
- https://docs.aws.amazon.com/AmazonS3/latest/userguide/DeletingObjects.html
- https://docs.aws.amazon.com/AmazonS3/latest/userguide/using-prefixes.html
- https://stackoverflow.com/questions/44373854/in-boto3-how-to-create-a-paginator-for-list-objects-with-additional-keyword-arg
- https://stackoverflow.com/questions/17561432/amazon-s3-listing-directories

