
# CRAB Cache - now an object storage

Currently, CRAB cache service has been replcaced by an s3-compatible object storage
provided by the CERN IT openstack service.

Details about the implementation are available on the github wiki

- [S3 for CRAB](https://github.com/dmwm/CRABServer/wiki/S3-for-CRAB)
- [CRABCache replacement with S3](https://github.com/dmwm/CRABServer/wiki/CRABCache-replacement-with-S3)

## Permissions and access

### Openstack EC2 Credentials

The access to openstack object storage is managed by the keystone service. 
In particular, you can access the object storage from a CLI with the "EC2 credentials",
which are comprised by a "EC2 Access Key" and "EC2 Secret Key".
These credentials are personal (different for every person) and project-specific 
(different for every project a person has access to).

You can retrieve your ec2 credentials for a specific project from the website
openstack.cern.ch, then select the project, then on the menu on the left open click
on "Project / API Access", then click on the button "View Credentials" on the top right.

!!!note

    In case you need to reset your credentials, click on "Recreate EC2 Credentials",
    this will invalidate you previous credentials and create new ones.

    If you try to use invalid credentials, you will get a "ClientError: An error occurred (InvalidAccessKeyId) when [...]"

You can also use the openstack cli from lxplus to access your credentials:

```plaintext
> ssh lxplus9
> export OS_PROJECT_NAME="CMS CRAB"
> openstack ec2 credentials list
+----------------------------------+--------+--------------------------------------+----------+
| Access                           | Secret | Project ID                           | User ID  |
+----------------------------------+--------+--------------------------------------+----------+
| d542228b5cc74605885bd10e80fe3e8f | ??     | 580b613d-85da-453c-9b59-012549ebfd9e | dmapelli |
| e57ff634b5334df9819ae3f956de5ca6 | ??     | 580b613d-85da-453c-9b59-012549ebfd9e | dmapelli |
| 366df32325234dff89ccf53417b4806b | ??     | 0a020f1f-729c-4186-be10-6f81bd9a9f43 | dmapelli |
| 1bf0acbedaa9472081a22bf4dff6dd8a | ??     | 14c793f1-64a2-4e0a-bebb-e28cac3188af | dmapelli |
+----------------------------------+--------+--------------------------------------+----------+
```

See the official [documentation](https://docs.openstack.org/python-openstackclient/pike/cli/command-objects/ec2-credentials.html) for more information.

!!! note "What to keep in mind when using the openstack cli"
    The command `openstack ec2 credentials create` does not delete previous credentials!

### Use the EC2 Credentials to access the object storage

You can test if your openstack ec2 credentials work with the CRAB object storage
by using the `boto3` python library, which is available
in recent [LCG](https://ep-dep-sft.web.cern.ch/document/lcg-releases) releases:

```bash
# on lxplus
source /cvmfs/sft.cern.ch/lcg/views/LCG_104/x86_64-el9-gcc11-opt/setup.sh
python3
```

then execute

```python
import boto3
import pprint

access_key='???'
secret_key='???'

endpoint = 'https://s3.cern.ch'
s3_client = boto3.client('s3', endpoint_url=endpoint, 
                         aws_access_key_id=access_key,
                         aws_secret_access_key=secret_key)
s3_bucket = 'crab_test1'
prefix = "dmapelli" # sort of directory inside a bucket

# get some properties of the bucket
response = s3_client.get_bucket_lifecycle_configuration(Bucket=s3_bucket)
pprint.pprint(response)

# list the content of the bucket
response = s3_client.list_objects_v2(Bucket=s3_bucket)
pprint.pprint(response)

# upload a file
testfile = "testfile1.txt"
with open(testfile, "w") as f:
    f.write("123456")
response = s3_client.upload_file(testfile, s3_bucket, f"{prefix}/{testfile}")
pprint.pprint(response)

# list the content of the bucket again, but with a different method
paginator = s3_client.get_paginator('list_objects_v2')
operation_parameters = {'Bucket': s3_bucket,
                        'Prefix': prefix}
page_iterator = paginator.paginate(**operation_parameters)
for page in page_iterator:
    print(page['Contents'])
```

