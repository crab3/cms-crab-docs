# Development With PyPI Images Tutorial

This is tutorial on how to **build**, **deploy**, and **run test** with the new PyPI images on dev environment.

## Limitation

- The current code still not able to install WMCore from forked repository, even you change the [wmcore\_requirements.txt](https://github.com/dmwm/CRABServer/blob/master/cicd/crabserver_pypi/wmcore_requirements.txt).

- Because official PyPI image is not ready yet. Please build your own using instruction below.

## Build

!!! note
    To build and deploy it automatically using pipeline, please see [CRAB Gitlab CI Tutorials (build-deploy-test)](../gitlab/tutorial.md).


The new images can build from your local machine. You need to have good internet connection (for push and pull docker images).

1. Make sure you are at git root directory.

    ```bash
    cd $(git rev-parse --show-toplevel)
    ```
2. Login docker registry
    ```
    docker login registry.cern.ch
    ```

    Use password from "CLI secret" to login (See [Harbor docs](https://goharbor.io/docs/1.10/administration/configure-authentication/oidc-auth/) on "Using OIDC from the Docker or Helm CLI").

3. Run `docker build` and push to registry.cern.ch:

    ```bash
    IMAGE_TAG=whateveryouwant

    # crabserver
    # only require argument is directory or "context" in docker term, here is CRABServer's root directory
    docker build -t registry.cern.ch/cmscrab/crabserver:$IMAGE_TAG -f cicd/crabserver_pypi/Dockerfile $PWD
    docker push registry.cern.ch/cmscrab/crabserver:$IMAGE_TAG
    # crabtaskworker
    docker build -t registry.cern.ch/cmscrab/crabtaskworker:$IMAGE_TAG -f cicd/crabtaskworker_pypi/Dockerfile $PWD
    docker push registry.cern.ch/cmscrab/crabtaskworker:$IMAGE_TAG
    ```

## Deploy

### REST

Make sure that you update chart to the version newer than commit [dd7a094b](https://github.com/dmwm/CMSKubernetes/commit/dd7a094b0080cca3d82a8008e01a9820a5218c48).

When helm chart is deployed, use following command to deploy the image you build:

```bash
IMAGE_TAG=whateveryouwant # same tag you push to
kubectl set image deployment/crabserver "crabserver=registry.cern.ch/cmscrab/crabserver:$IMAGE_TAG
```

### TaskWorker

Using the following commands:

```bash
# running as crab3 user inside dev-tw machine
sudo -i -u crab3
IMAGE_TAG=whateveryouwant
chmod +x runContainer.sh
docker rm -f TaskWorker && /home/crab3/runContainer.sh -v $IMAGE_TAG -s TaskWorker
```

## Test

Manual testing workflow is still the same. You can exec to container and do stop, start, checkout git in `/data/repos/CRABServer`, start from from git, etc.

Note for **TaskWorker** :

- If TW process is not running, please see the error logs with `docker logs TaskWorker` (The docker logs is replace the `nohup.out` in RPM images)
- [UpdateTMRuntime.sh](https://github.com/dmwm/CRABServer/blob/5ca7cae6d4ebe374f945eb46a7768a64c2b24fa2/src/script/Deployment/TaskWorker/updateTMRuntime.sh) is replaced by [updateDatafiles.sh](https://github.com/dmwm/CRABServer/blob/5ca7cae6d4ebe374f945eb46a7768a64c2b24fa2/cicd/crabtaskworker_pypi/updateDatafiles.sh) and it will be run when use `start.sh -g` (before execution processes).
