# CI Best Practices

The collections of CI practices. Not limit to Gitlab but can apply to all CI systems.


## Shell scripts

To make CI easy to debug later, I have enforced these rules while writing shell (bash) scripts:

1. With appropriate setup of cvmfs/voms/myproxy/container, all scripts must be able to run locally.
2. Always run the scripts inside `$WORK_DIR` (default: `./workdir`, with some exception). This makes running the script on a local machine easy to clean up.
3. Following point 2, `$ROOT_DIR` is ALWAYS available when `$WORK_DIR` is defined and always points to the git root directory. This makes it easy to call other scripts inside the repo.
4. `$SCRIPT_DIR`, [one-liner to get the script's location](https://stackoverflow.com/a/246128), is use when script wants to call another script where it is at at the same level or subdirectory. Use `$ROOT_DIR` instead when calling an outside script's directory (e.g, use `${ROOT_DIR}/test/path-to/script.sh` instead of `../../test/path-to/another-script.sh`).
5. `set -euo pipefail` is a must. However, when source scripts in cvmfs we need to disable it because some scripts does not write with unbound variable or failure of pipe command in mind.
