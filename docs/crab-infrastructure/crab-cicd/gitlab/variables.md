# Variables

List of variables that use in project [crab3/CRABServer](https://gitlab.cern.ch/crab3/CRABServer/-/settings/ci_cd).

![project-variables.png](../../../images/gitlab-ci-pipeline/project-variables.png)

## `CMSCRAB_REGISTRY_URL`, `CMSCRAB_REGISTRY_USER`, and `CMSCRAB_REGISTRY_PASSWORD`

Harbor url, (robot) username, and password.

Put `registry.cern.ch` for `CMSCRAB_REGISTRY_URL`.
Then, Generate rebot credential from `cmscrab` project, [Robot Accounts](https://registry.cern.ch/harbor/projects/1891/robot-account) tab in Harbor WebUI.

## `CRAB_TW_SSH_KEY`

SSH key is used to remote shell from Runner to TW machine.

To setup:

- Generate key using `ssh-keygen -t ed25519 -f <filename> -P ''`.
- Put the public key to TW host (For example: [crabtaskworker.pp#L76-81](https://gitlab.cern.ch/ai/it-puppet-hostgroup-vocmsglidein/-/blob/f6000a5cc12971fca5844d405d2c026a290d8eff/code/manifests/crabtaskworker.pp#L76-81))
- Put private key to CI variable `CRAB_TW_SSH_KEY`. **Make sure you have new line at the end when pasting in WebUI, otherwise ssh client cannot read it.**

## `KUBECONFIG_FILE`

Kubernetes client configuration that contains clusters endpoints and user credentials, to deploying a new built image to Kubernetes cluster.

The content is maunally copy from [`crab-secrets/cicd/ci_crab_tw_ssh_key.sops`](https://gitlab.cern.ch/crab3/crab-secrets/-/blob/master/cicd/ci_crab_tw_ssh_key.sops).

Please see [official kubernetes docs](https://kubernetes.io/docs/tasks/access-application-cluster/configure-access-multiple-clusters/) how to config multiple clusters in single file.

### Test cluster

You can copy configuration file from cmsweb [user\_configs](https://gitlab.cern.ch/cmsweb-k8s/users_config) from every test clusters, append each cluster/user/context field to to `KUBECONFIG_FILE` but rename the `name` field to  `cmsweb-test(2|11|12|14|bed)`.

Might better to see example from current `ci_crab_tw_ssh_key.sops`.

### Testbed cluster

1. Put configuration to `KUBECONFIG_FILE` the same way as test clusters, except `users` field.
2. (To be done) Ask CMSWEB operators to apply serviceaccount for us. We do not have permission to do this. The RBAC files is in [CMSKubernetesTBD]().
3. Copy generated token from `crab-gitlab-ci-secret` to to kubeconfig file. Do:
    ```
    kubectl get secret -o jsonpath='{.data.token}' crab-gitlab-ci-secret  | base64 -d
    ```
    Copy the output of above commant to `users[].token`, for example:
    ```yaml
    users:
      - name: cmsweb-testbed
        user:
          token: <token_string>
    ```

!!! warning
    It is not-that-safe in gereral to create the long live token and use it forever, but it is most convienent way and has less maintainance.

## `X509_USER_PROXY`

This variable actually optional.

We check content of this variable with [credFile.sh](https://github.com/dmwm/CRABServer/blob/033af370bf8a3c8affdb7acf13bfc7dc38d74174/cicd/gitlab/credFile.sh) script. If it malform or expire, it will issue proxy with the `voms-proxy-init` command by read the `crabint1` certificate inside the runner VM (only available on shell executor, `crab3-shell`).

The `crabint1` is installed in `/home/gitlab-runner/.globus` directory via [tbag/puppet](https://gitlab.cern.ch/ai/it-puppet-hostgroup-vocmsglidein/-/blob/e0f5f9eca1c2a508e1b3748fb316fe812baca045/code/manifests/cirunner.pp#L80-94).
