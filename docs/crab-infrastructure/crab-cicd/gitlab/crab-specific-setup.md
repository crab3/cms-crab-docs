# CRAB specific setup

## Repo Configuration

Default job's time out is not enough (1h) for testsuite, especially status tracking. You can increase timeout by go to [Project CI/CD settings](https://gitlab.cern.ch/crab3/CRABServer/-/settings/ci_cd#js-general-pipeline-settings]), search `Timeout`, then change the value from `1h` to `6h`.

## Deployment

To deploy the services from CI, there are some set up need to be done.

### REST/K8s

#### Deploying the helm chart

The CI does only set the new container image that use in Deployment. But never touch any Deployment, ConfigMap, and other k8s objects.

So, you need to apply helm chart once before you run pipeline.

See [Deploy REST](../../../crab-components/crab-rest/deploy.md#deploy-on-kubernetes-use-helm-template-to-generate-manifest-preferred) on how to do it.

#### Create service account to deploy the new image in cmsweb-testbed

The testbed cluster is a bit tricky because unlike test clusters, you need to use openstack token to authenticate. But we can workaround by using Kubernetes serviceaccount [long-live token](https://kubernetes.io/docs/tasks/configure-pod-container/configure-service-account/#manually-create-a-long-lived-api-token-for-a-serviceaccount) to authenticate.

You need 2 things:

- Service account, a.k.a. the user that we use to talk to Kubernetes. it is done automatically when you apply helm. But we need to extract token from there
- RBAC, the authorization part that allow the service account to update the new container to Deployment.

Please see detail in [`KUBECONFIG_FILE` Variable](./variables.md#kubeconfig_file) section on how to create this.

### TW/PUB

#### SSH from runner machine to TW machines without 2FA

We use ssh to execute deployment script [runContainer.sh](https://github.com/dmwm/CRABServer/blob/494192d8fcd0e928dbb887f6cb5509f114f86ce4/src/script/Container/runContainer.sh) in TaskWorker machine. But all machines have restricted ssh connections with 2FA.

This already been done by including `vocmsglidine/cirunner` to the trust machine list in [crabtaskworker/mfa.pp](https://gitlab.cern.ch/ai/it-puppet-hostgroup-vocmsglidein/-/blob/3630bf6ecd97dc657308c6db8e4ecd0aaa828282/code/manifests/crabtaskworker/mfa.pp#L22-L27).

For the setup ssh key, please see `CRAB_TW_SSH_KEY` section in [Variables](./variables.md#crab_tw_ssh_key).

## Mirror sync and automatic triggering pipeline

We use [Gitlab's Repository Mirroring](https://docs.gitlab.com/ee/user/project/repository/mirror/) to sync all branch and tags to Gitlab, then Gitlab trigger the pipeline according to the configuration in `.gitlab-ci.yml` file.

To release a new CRABServer version, our practice is we create a same tag name as the release name. Then wait for mirror sync run and trigger the [release pipeline](./pipelines/release.md).

See the [Running The Release Pipeline](./tutorial.md#running-the-release-pipeline) on how to do it manually.

# crane

[crane](https://github.com/google/go-containerregistry/blob/main/cmd/crane/doc/crane.md) is userspace commandline tools for working with container registries. We use it to [retag the release images without download images to local machine again](https://github.com/dmwm/CRABServer/blob/f88a9b98f80f9a38e3180ed0a5b2b193a75ee5c0/.gitlab-ci.yml#L243-L256).

This is equivalent to:

```
docker pull registry.cern.ch/cmscrab/crabtaskworker:${IMAGE_TAG}
docker tag registry.cern.ch/cmscrab/crabtaskworker:${IMAGE_TAG} registry.cern.ch/cmscrab/crabtaskworker:${RELEASE_IMAGE_TAG}
docker push registry.cern.ch/cmscrab/crabtaskworker:${RELEASE_IMAGE_TAG}
```

But without download and re-upload image layers.

`crane` is part of [registry.cern.ch/cmscrab/buildtools](https://github.com/dmwm/CRABServer/blob/f88a9b98f80f9a38e3180ed0a5b2b193a75ee5c0/cicd/buildtools/Dockerfile#L13) image.

## Helper scritps

- [retry.sh](https://github.com/dmwm/CRABServer/blob/f88a9b98f80f9a38e3180ed0a5b2b193a75ee5c0/cicd/gitlab/retry.sh) is the wrapper to workaround "limited retry count"+"[deley retry](https://gitlab.com/gitlab-org/gitlab/-/issues/19231)" (like in the Jenkins). The script retry only when callee script exit with code `4`. The script use `RETRY_MAX` (default: 10) and `RETRY_SLEEP_SECONDS` (default: 900 secs/15 mins) environment variable to control it behavior.
- [credFile.sh](https://github.com/dmwm/CRABServer/blob/f88a9b98f80f9a38e3180ed0a5b2b193a75ee5c0/cicd/gitlab/credFile.sh) is for avoid wrong permission problem from some apps like `voms-proxy-init` and `ssh` which always enforcing the permission checking. It copy creds file from Gitlab variable to working directory, `chown` to CI user and `chmod` to `0600`. The script has 2 modes:
    - `ssh`: simply copy file and set proper permission.
    - `x509`: if file in `$X509_USER_PROXY` (can be configured in CI variable) is invalid, malformed, or expired, it will use default cert installed in the runner machine to create proxyfile using `voms-proxy-init` commands (default to certificate files in `/home/gitlab-runner/.globus/`).


## Appendix 1: Table of cloned scripts and its original

| cicd/gitlab/<file>                                                                                                           | Origin                                                                                                                                                                                                          |
|------------------------------------------------------------------------------------------------------------------------------|-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| [deployTW.sh](https://github.com/novicecpp/CRABServer/blob/ci_gitlab_pypi/cicd/gitlab/deployTW.sh)                           | Jenkins Job [CRABServer_Deploy_TW](https://cmssdt.cern.ch/dmwm-jenkins/job/CRABServer_Deploy_TW/configure)                                                                                                      |
| [executeStatusTracking.sh](https://github.com/novicecpp/CRABServer/blob/ci_gitlab_pypi/cicd/gitlab/executeStatusTracking.sh) | Jenkins job [CRABServer_CheckTestResults](https://cmssdt.cern.ch/dmwm-jenkins/job/CRABServer_CheckTestResults/configure)                                                                                        |
| [executeTests.sh](https://github.com/novicecpp/CRABServer/blob/ci_gitlab_pypi/cicd/gitlab/executeTests.sh)                   | Jenkins job [CRABServer_ExecuteTests](https://cmssdt.cern.ch/dmwm-jenkins/job/CRABServer_ExecuteTests/configure)                                                                                                |
| [taskSubmission.sh](https://github.com/novicecpp/CRABServer/blob/ci_gitlab_pypi/cicd/gitlab/taskSubmission.sh)               | [test/container/testingScripts/taskSubmission.sh](https://github.com/dmwm/CRABServer/blob/5f3ab24a9f3544995d920610b84405d2d2b4dc3a/test/container/testingScripts/taskSubmission.sh)                             |
| [setupCRABClient.sh](https://github.com/novicecpp/CRABServer/blob/ci_gitlab_pypi/cicd/gitlab/setupCRABClient.sh)             | [test/container/testingScripts/setupCRABClient.sh](https://github.com/dmwm/CRABServer/blob/5f3ab24a9f3544995d920610b84405d2d2b4dc3a/test/container/testingScripts/setupCRABClient.sh)                           |
| [st/statusTracking.sh](https://github.com/novicecpp/CRABServer/blob/ci_gitlab_pypi/cicd/gitlab/st/statusTracking.sh)         | [test/container/testingScripts/TaskSubmissionStatusTracking.sh](https://github.com/dmwm/CRABServer/blob/5f3ab24a9f3544995d920610b84405d2d2b4dc3a/test/container/testingScripts/TaskSubmissionStatusTracking.sh) |
| [st/statusTracking.py](https://github.com/novicecpp/CRABServer/blob/ci_gitlab_pypi/cicd/gitlab/st/statusTracking.py)         | [test/container/testingScripts/statusTracking.py](https://github.com/dmwm/CRABServer/blob/5f3ab24a9f3544995d920610b84405d2d2b4dc3a/test/container/testingScripts/statusTracking.py)                             |
