# CRAB CI on Gitlab

If you are not familiar with Gitlab CI, please read [Gitlab CI Basic](./gitlab-ci-basic.md) (skimming is enough).

Or you can start from [CRAB Gitlab CI Tutorials](./tutorial.md).

Pipeline designs are in [Pipelines](pipelines/build-deploy-test.md) section.
