# Gitlab CI Basic

## Pipeline configuration (Entrypoint)

Start at (git root)/[.gitlab-ci.yml](https://github.com/dmwm/CRABServer/blob/master/.gitlab-ci.yml).

Important keys (see full detail in [CI/CD YAML syntax reference](https://docs.gitlab.com/ee/ci/yaml/)):

- `default`: Default value for all job in pipeline. Can be overriden in job level.
- `variables`: The pipeline variable.
- `stages`: List of stage, order dependent.
- Hidden key: Gitlab-CI will not read the key prefix with dot (e.g. `.default_rules`).
- Non keyword: Job definition.
    - `rules`: [Control how jobs run](https://docs.gitlab.com/ee/ci/jobs/job_control.html).
    - `stage`: Job's stage name.
    - `tag`: Job's tag, for selecting runner to execute.
- `!reference [.hidden_keys, nested\_keys]`: Replace this tag with values of `.hidden_keys.nested_keys` (see [reference-tags](https://docs.gitlab.com/ee/ci/yaml/yaml_optimization.html#reference-tags)).


## Pipeline

The first section of [CI/CD pipelines](https://docs.gitlab.com/ee/ci/pipelines/)'s official docs has great summary about what the pipeline is.

Here is an Pipeline UI:

![pipeline-page.png](../../../images/gitlab-ci-pipeline/pipeline-page.png)

Job page:

![job-page.png](../../../images/gitlab-ci-pipeline/job-page.png)

### Artifact

[Official docs](https://docs.gitlab.com/ee/ci/jobs/job_artifacts.html).

It is output of the jobs you want to keep for following stage.

The main use cases:

- Convenience way to inspect the files produce by the job.
- Output of job (e.g., the compiled binary files)
- Share the file across pipeline. Files is available automatically

### Cache

Recommend reading:

- <https://docs.gitlab.com/ee/ci/caching/#how-cache-is-different-from-artifacts>
- <https://about.gitlab.com/blog/2022/09/12/a-visual-guide-to-gitlab-ci-caching/>

Rule of thumb: use cache as much as possible because cache space is owned by us (S3) from our custom runner.

Cache only does pull/push as needed, and it is possible to fetch cache across the pipeline.

## Runners

The process that pull job from CI and  execute. [The executor determines the environment each job runs in](https://docs.gitlab.com/runner/#executors).
The runner announce "runner tags". Then, job provide possible tag name that can be run on runner, i.e., you select the runner by tag.

## Variables

There are at least 4 types of variables.

### Predefined CI/CD variables

See [Predefined CI/CD variables reference](https://docs.gitlab.com/ee/ci/variables/predefined_variables.html).

Highlight:

- `CI_PIPELINE_ID`: Pipeline ID.
- `CI_COMMIT_REF_SLUG`: URL friendly version of tag/branch name, limit at 64 chars.
- `CI_COMMIT_TAG`: Git tag.
- `CI_PROJECT_DIR`: Working directory (full path).


### Project/Group/Instance variables.

Custom variables can be defined in Project/Group/Instance settings UI.

We use this variable to store the credential. See [Variables](./variables.md) page.

### CI configuration variables (define in .gitlab-ci.yml)

Variable define in `variables` key in `.gitlab-ci.yml`.

Note that variables are evaluated at the pipeline start; All job will have same value as define in `.gitlab-ci.yml`.

### (Dynamic) Pipeline variables

There are 2 sources of this variable type:
    - From triggering Pipeline (UI/API). You will see extra variable list on the right side of job page.
    - From push options. New git version can provide extra information to git server via push option.

## Reference

- Getting start: <https://docs.gitlab.com/ee/ci/pipelines/>
- Job Artifact: <https://docs.gitlab.com/ee/ci/jobs/job_artifacts.html>
- Cache: <https://about.gitlab.com/blog/2022/09/12/a-visual-guide-to-gitlab-ci-caching/>
- Rules: <https://docs.gitlab.com/ee/ci/jobs/job_control.html>
- Predefined CI Variables: <https://docs.gitlab.com/ee/ci/variables/predefined_variables.html>
- Push Option: <https://docs.gitlab.com/ee/user/project/push_options.html>
