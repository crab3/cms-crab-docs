# Triggering

## Tag push

We use "Tag push" as a signal to trigger the pipelines. In other word, if you simply push new commit to any branch, pipelines will not run.

The benefit of tags is:

- Idempotent. Because tags always is point to specific commit and never change (unless force push), we can rely on that and can re-run pipeline on specific tag, compare to branch where you need to reset it to switch between commit you want to test.
- Easy to work with, especially when debugging the CI. Gitlab always run CI against specific commit. Tag is idempotent, so you simply push a new tag to test the CI code. No temporary edit CI to run specifc commit where CI code store (like we do in jenkins).

The downside is:

- Remote Gitlab repo will clutter with so many tags. Git logs will not look nice (clutter with the CI tags).
- Risk to tag the wrong commit. Especially when manual trigger the release pipeline (see [comment on GH PR#8421](https://github.com/dmwm/CRABServer/pull/8421#issuecomment-2133042652)).

## Git tag name

To run pipeline, you need to name your tag to conform with tag pattern for each pipeline.

For example `pypi-test2-stefanotest1` will run [Build-Deploy-Test pipeline](pipelines/build-deploy-test.md#triggering) on `test2` environment, `v3.240618` will trigger [Release pipeline](pipelines/release.md#triggering) which always run on `preprod` env.

See "Triggering" section of each pipeline on how to use.

## Mirror sync

We use mirror sync to sync a new tag from GitHub to Gitlab then trigger the pipeline. So, if you push tag to GitHub, it will eventually sync to Gitlab but we are not recommend to do that.

To make things clean, here are the practices we follow:

- GH is "the source-of-truth". If diverge branch/tag happens, stick with what we have in GH and reset Gitlab side.
- To run any pipelines manually, push tag to [crab3/CRABServer](https://gitlab.cern.ch/crab3/CRABServer) **Gitlab** directly. **DO NOT PUSH NON-RELEASE TAG TO GH!!** as you should not not push anything to dmwm/CRABServer in general.
- All tags and branch in Gitlab are temporary and subjects to be removed later.

!!! note
    The tags are piling up due to the overuse of git tags as pipeline signals. It would be great if we cleaned it up regularly. But, it is not urgent or become a problem (yet). So we will leave all tags as is.


## Triggering scripts

We pass some variables to CI using [push options](https://docs.gitlab.com/ee/topics/git/commit.html#push-options-for-gitlab-cicd) `-o ci.variable="KEY=VALUE"` to control the pipelines and provide some extra information to the jobs.

Because we need to push a new tag to run a pipeline, the trigger scripts are available for use in [CRABServer/cicd/trigger-ci](https://github.com/dmwm/CRABServer/tree/master/cicd/trigger-ci) directory. There are scripts that push specific CI variables, to execute specific jobs instead of the whole pipeline.

For example, to trigger a pipeline without tests, simply run `bash tag_and_push_skip_submit_tests.sh`, the script will do tag and push to Gitlab for you.


## Variables in rules

If no variiables are specified, all tests and stages will run. `tagging_release` stage is for release pipeline (tag like v3.XXXXXX) only. We use CI variables to control which jobs can be skipped. For example, if you set `CLIENT_VALIDATION=false`, CI will skip running client validation test. You can pass this variable via [Push Option](https://docs.gitlab.com/ee/topics/git/commit.html#push-options) together when you push a tag. The value must be `true` or `false`. By default, the test variables CLIENT_VALIDATION and CLIENT_CONFIGURATION_VALIDATION are set to true while STATUS_TRACKING is set to false and skip variables (SKIP_BUILD, SKIP_DEPLOY, SKIP_SUBMIT) are set to false. The variables are used in `rules` in the job description in `if:` directive.

Here is comprehensive list of variables we have right now:

![pipeline-rule-variables.png](../../../images/gitlab-ci-pipeline/pipeline-rule-variables.png)
