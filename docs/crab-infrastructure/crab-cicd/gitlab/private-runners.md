# Private Runner

We have 2 private runner:

- `crab3`: Docker executor, run job inside docker container.
- `crab3-shell`: Shell executor, run job as `gitlab-runner` on VM, like you login to VM and run the commands.

These two runners are running on the same machine, handled by different daemon ([due to CI bug](https://gitlab.com/gitlab-org/gitlab-runner/-/issues/27443), even it close but still not work properly).


## Why we need our own ruunner?

CERN's Gitlab has [limit of parallel job](https://gitlab.docs.cern.ch/docs/Build%20your%20application/CI-CD/Runners/cvmfs-eos-runners#fair-use). So, we create our own instead to use as much as we want, in exchange of more things to maintenance. But, also we can introduce a new Gitlab-CI runner features, or other things that need linux privilege permission.

### cmssw-el*

We need `crab3-shell` or `shell` executor in order to run apptainer CMSSW (e.g., run `cmssw-el9` in `lxplus`). Running apptainer inside docker container is not trivial (at least we do not know how).

### Docker-in-Docker (DinD)

We relies on docker [buildx/BuildKit](https://docs.docker.com/build/concepts/overview/#buildx)'s feature to do caching stuff for us. This require run the Docker-in-Docker service to build a new container, which require root privilege on docker executor. CERN also provides [privilege runner](https://gitlab.docs.cern.ch/docs/Build%20your%20application/CI-CD/Runners/docker-privileged-runners) but in very limited resources.

## Setup

### Create a machine

We use [cirunner.pp](https://gitlab.cern.ch/ai/it-puppet-hostgroup-vocmsglidein/-/blob/3630bf6ecd97dc657308c6db8e4ecd0aaa828282/code/manifests/cirunner.pp) to setup new runner machine. Use usual `ai-bs` command to create puppet managed machine. Note that the machine need to be in `vocmsglidein/cirunner` hostgroup and runner machine name should have prefix `crab-ci-runner*`.

Puppet do:

- Install `gitlab-runner`.
- Run 2 daemons with different config.
- Pull `crabint1` cert in tbag to use in task submission and checking job.


#### Registering Runner for the first time

If you need a new runner register runner to CERN Gitlab first to get skeleton of configuration file. You do this only once, but not for the new machine that have same runner.

Follow the guide in [official docs](https://docs.gitlab.com/runner/register/). Check if you get configuration you want by looking the path `/etc/gitlab-runner.config.toml`.

Then configure the proper runner tag in "Runner" section in CI/CD settings.

Note that runner is bound to [crab3/CRABServer](https://gitlab.cern.ch/crab3/CRABServer/) project but it can enable on any project. But, please limit the usage of our runner only for [crab3](https://gitlab.cern.ch/crab3/) projects.

### Runner configuration

Once you have configuration file, put the whole contents into `tbag` (because the configuration contains runner token and s3 credentials) and then we copy to runner machine by `teigi::secret` module in puppet.

The tbag keys we use:

| Keyname                           | runner configuration |
|-----------------------------------|----------------------|
| `gitlab-runner.config.toml`       | `crab3`              |
| `gitlab-runner-local.config.toml` | `crab3-shell`        |

#### Docker-in-docker

According to [DinD docs](https://docs.gitlab.com/ee/ci/docker/using_docker_build.html#docker-in-docker-with-tls-enabled-in-the-docker-executor), you need to run DinD in in *privilege mode* and have *mount volumes for `/certs/client` path*.

Make sure that in `[runners.docker]`:

- `volumes` contains `/certs/client`.
- `privileged` is `true`
