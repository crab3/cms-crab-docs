# Build-Deploy-Test pipeline

This is the main pipeline for build and validate docker images.

The pipeline does the build-deploy-test, where the "build" is building the new PyPI image and the "test" is executing the "status tracking" testsuite.

## The pipeline
The CI contains 5 stages:

![buid-deploy-test-pipeline.png](../../../../images/gitlab-ci-pipeline/buid-deploy-test-pipeline.png)

1. `prepare_env`: Prepare `.env` file for use across job in pipeline.

    The [parseEnv.sh](https://github.com/dmwm/CRABServer/blob/f88a9b98f80f9a38e3180ed0a5b2b193a75ee5c0/cicd/gitlab/parseEnv.sh) script parse the tag format `pypi-<env>-<name>` to get environment. Then, write appropriate environment variables to `.env`.

    `.env` is available for all jobs in the pipeline as pipeline artifacts (see [.gitlab-ci.yml#L35-L38](https://github.com/dmwm/CRABServer/blob/f88a9b98f80f9a38e3180ed0a5b2b193a75ee5c0/.gitlab-ci.yml#L35-L38))

2. `build_docker`: Build the containers

    Build the containers from [cicd/crabserver\_pypi/Dockerfile](https://github.com/dmwm/CRABServer/blob/f88a9b98f80f9a38e3180ed0a5b2b193a75ee5c0/cicd/crabserver_pypi/Dockerfile) and [cicd/crabtaskworker\_pypi/Dockerfile](https://github.com/dmwm/CRABServer/blob/f88a9b98f80f9a38e3180ed0a5b2b193a75ee5c0/cicd/crabserver_pypi/Dockerfile) using kaniko.

3. `deploy`: Deploy REST and TaskWorker.

    - **Deploy REST in K8s (`deploy_rest`)**: Change the image to the image built by this pipeline, wait until the readiness test passes. This step does not apply the Helm template or any kube  manifests to reduce job complexity. Operators need to apply proper manifests before running the pipeline.
    - **Deploy TW on the TW machine (`deploy_tw`)**: use the [deployTW.sh](https://github.com/dmwm/CRABServer/blob/a4415934102953cf4410e71cbc025d13518f53c8/cicd/gitlab/deployTW.sh) to deploy the built image to TW machine (similar to Jenkins Job [CRABServer\_Deploy\_TW](https://cmssdt.cern.ch/dmwm-jenkins/job/CRABServer_Deploy_TW/configure).

4. `run_testsuite`: Submitting status tracking test suite.

    `executeTests.sh` is the entrypoint of job to execute `taskSubmission.sh` inside appropriate cmssw container to run `crab` command. I have removed all Github bot code from scripts to simplify the code, plan to add back later. Please see `executeTests.sh` and  `taskSubmission.sh` source code in "Appendix 1" on how it works.

5. `check_test_result`: check the test result submit by previous step.

    Same as step 4 `run_testsuite`, `executeStatusTracking.sh` is the entrypoint and `taskSubmission.sh` is real execution  which call `taskSubmission.py` later. The `executeStatusTracking.sh` is wrapped inside [retry.sh](https://github.com/dmwm/CRABServer/blob/f88a9b98f80f9a38e3180ed0a5b2b193a75ee5c0/cicd/gitlab/retry.sh) helper to emulate the retry machanism in Gitlab-CI (CERN gitlab retry is limit only 3 time).

## Triggering

(If you do not know what it is, please see [Triggering](../triggering.md))

Tag must match regex pattern `^pypi-(testX|...)-.+`.

For example:

- `pypi-test2-stefanotest1` will run build then deploy and test on `test2` environment.
- `pypi-test12-1624756001` will deploy on `test12` instead.

## rules variables

### Build (ALL) images only

Variable: `$BUILD`

This is for the situation where you want to build the image only. It run `build_docker` stage only.

Note that it will build all containers including monit and filebeat images.

### Run testsuite

Variable: `$SUBMIT_STATUS_TRACKING`

This is intend for execute run testsuite similar to Jenkins's [CRABServer\_ExecuteTests](https://cmssdt.cern.ch/dmwm-jenkins/job/CRABServer_ExecuteTests/) job.

Pipeline will run only `run_testsuite` and `check_test_result` stages.

### Debugging `check_test_result` job

Variable: `$MANUAL_CI_PIPELINE_ID`

Only `check_test_result` job will be run. This is for debug `executeStatusTracking.sh` and `retry.sh`.

Also serve as example how to use advance cache in CIs. In [task\_submission\_status\_tracking](https://github.com/dmwm/CRABServer/blob/f88a9b98f80f9a38e3180ed0a5b2b193a75ee5c0/.gitlab-ci.yml#L197-L205), we "push" files to cache in and later "pull" from cache in [check\_test\_result job](https://github.com/dmwm/CRABServer/blob/f88a9b98f80f9a38e3180ed0a5b2b193a75ee5c0/.gitlab-ci.yml#L233-L240). The `key` and `fallback_keys` indicate that CI will start finding the cache file from `key` first, then all `fallback_keys`, respectively.


## Appendix 1: Run locally

The scripts ([test\_executeTestsStatusTracking.sh](https://github.com/dmwm/CRABServer/blob/f88a9b98f80f9a38e3180ed0a5b2b193a75ee5c0/cicd/gitlab/st/test_executeTestsStatusTracking.sh), [test\_executeStatusTracking.sh](https://github.com/dmwm/CRABServer/blob/f88a9b98f80f9a38e3180ed0a5b2b193a75ee5c0/cicd/gitlab/st/test_executeStatusTracking.sh)) can be example for test running CI job locally.

The content of script is simply clone from [.gitlab-ci.yml](https://github.com/dmwm/CRABServer/blob/f88a9b98f80f9a38e3180ed0a5b2b193a75ee5c0/.gitlab-ci.yml) file to avoid call the scripts too many level.
