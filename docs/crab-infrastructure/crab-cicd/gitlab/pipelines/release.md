# Release pipeline

The release pipeline is based on the [Build-Deploy-Test](./build-deploy-test.md) pipeline, but has extra `prepare_release` and `tagging_release` to create the container tag from the release's git tag.


## Before dive down

Before go through this page, there are some technical details we need to understand.

### Apply release name to binary

Release name is the string that represents the "official" version number of the software.

We create it from GitHub UI:

![release-name.png](../../../../images/gitlab-ci-pipeline/release-name.png)

This is used in 3 places,

- Git tag: We set tag name the same as release.
- Docker image tag: For example, `registry.cern.ch/cmscrab/crabtaskworker:v3.240618`.
- The release binary: *This section. See below.*

Because we are not committing the version string into the source code but altering it when building the image

- For REST, `version` variable is inside [src/python/CRABInterface/\_\_init\_\_.py](https://github.com/dmwm/CRABServer/blob/c69980662cbd4fa76e3801b6f3085856032743d6/src/python/CRABInterface/__init__.py) ([crabserver.spec](https://github.com/cms-sw/cmsdist/blob/a5a0b8640d227a432388285b8c0a11dfd5c0706d/crabserver.spec#L35))
- For TW, `version` variable is inside [src/python/TaskWorker/\_\_init\_\_.py](https://github.com/dmwm/CRABServer/blob/c69980662cbd4fa76e3801b6f3085856032743d6/src/python/TaskWorker/__init__.py) ([crabtaskworker.spec](https://github.com/cms-sw/cmsdist/blob/a5a0b8640d227a432388285b8c0a11dfd5c0706d/crabtaskworker.spec#L38))

If we build and deploy the PyPI image from the current master branch in production, we will get a version report from our monitor as `development`, not a proper version like `v3.240305`.

So a pre-release step is needed to automate applying the version number.

### `-stable` suffix

The final image tag will look like this: `registry.cern.ch/cmscrab/crabtaskworker:v3.240305-stable`

We add the `-stable` suffix to the release image tag for 2 things:

- The Retention policy in the registry ([Harbor](https://registry.cern.ch)). The suffix  makes sure the image will not be deleted or replaced. This policy was suggested by the CMSWEB team.
- Separate between RPM and the new PyPI images.

After the test passes, the next job will create the release tag and push it to the registry.

### GH tag vs image

P.S. Note that GH tag v3.YYMMDD becomes image v3-YYMMDD See [here](https://github.com/dmwm/CRABServer/blob/97f447747265684589ac1f5be773eed80de02239/.gitlab-ci.yml#L8) 
Also, from [doc](https://docs.gitlab.com/ee/ci/variables/predefined_variables.html) CI_COMMIT_REF_SLUG is
CI_COMMIT_REF_NAME in lowercase, shortened to 63 bytes, and with everything except 0-9 and a-z replaced with -. No leading / trailing -. Use in URLs, host names and domain names. 

Hence, two images are available: one of the format v3-YYMMDD and another after running the release pipeline of the form v3.YYMMDD-stable

## The pipeline

Here is the example of the pipeline: <https://gitlab.cern.ch/crab3/CRABServer/-/pipelines/7597505>

![release-pipeline.png](../../../../images/gitlab-ci-pipeline/release-pipeline.png)

There are 2 new stages:

`prepare_release`: Apply the version number to the code. The job uses the git tag $`CI_COMMIT_TAG` as a value to apply.
`tagging_release`: Re-tagging the temporary image tag with the final image tag and updating the registry. Reminder that the pipeline uses `$CI_COMMIT_REF_SLUG` as a temporary image tag to deploy and run the test.

The final image tag we get from the pipeline will have a pattern like `v3.YYMMDD-stable`.

## Triggering

Tag must match regex pattern `v3.[0-9]{6}.*`.

For example:

- [v3.240618](https://gitlab.cern.ch/crab3/CRABServer/-/pipelines/7596938)
- [v3.240618.fixci5_v](https://gitlab.cern.ch/crab3/CRABServer/-/pipelines/7597505)

But this pipeline expect to trigger via mirror sync from GitHub when we creating a new release.

The normal procedure is to create a new release on GitHub with the same git tag as the release name. Then the mirror sync we setup pull the new tag from GitHub and trigger the pipeline.

## rules variables

### Release without test

Variable: `ONLY_BUILD_DEPLOY`

`ONLY_BUILD_DEPLOY` is what you do when you want to skip the deploy and test step entirely.

Here is what the pipeline looks like with and without `ONLY_BUILD_DEPLOY`:


![force-release-example.png](../../../../images/gitlab-ci-pipeline/force-release-example.png)

If you want to change final image tags, you need to do it manually.
