# CRAB Gitlab CI Tutorials (build-deploy-test)

This tutorial will introduce you to the new pipeline system on Gitlab.

We start from `Build-Deploy-Test`, to build the new code and run testsuite against it.

![ci-main-pipeline.png](../../../images/gitlab-ci-pipeline/buid-deploy-test-pipeline.png)

Full design documentation is at [Build-Deploy-Test](./pipelines/build-deploy-test.md) pipeline's page.

## Prerequisite

### Add Gitlab remote repository
Add [crab3/CRABServer](https://gitlab.cern.ch/crab3/CRABServer) as `gitlab` remote to your local git.

```bash
git remote add gitlab ssh://git@gitlab.cern.ch:7999/crab3/CRABServer.git
```

### Applying helm chart

See [Applying helm chart](../pypi/tutorial.md#rest).

## Trigger The Pipeline

Before we start running the pipeline. Keep in mind that `Build-Deploy-Test` pipeline:

- **Only trigger when the new tag is pushed.**
- **Tag must have `pypi-{preprod,test2,test11,test12}-` as prefix.**

## Running The Development Pipeline

1. **Tag-and-Push**: Tag the current commit with parttern `pypi-{preprod,test2,test11,test12}-*`. Pipeline only run when **tag match the pattern** ([source](https://github.com/dmwm/CRABServer/blob/7c6f30a21a862475e6fa43762ee0e713359a486a/.gitlab-ci.yml#L9-L13)), then push to `gitlab`. For example:
    ```bash
    TAG=pypi-test12-$(date +"%s"); echo $TAG
    git tag $TAG
    git push gitlab $TAG
    ```

    Note that we **TAG** what we want to build, Gitlab CI will spawn pipeline against **TAG** name you pushed. Push branch will not trigger the pipeline.

2. Go to <https://gitlab.cern.ch/crab3/CRABServer/-/pipelines>. Search the pipeline ID with your tag. Then, click pipeline ID or status badge to see your triggered pipeline. For example, tag `pypi-test12-1714515707` created pipeline [#7315451](https://gitlab.cern.ch/crab3/CRABServer/-/pipelines/7315451) as show in image below:

    ![all-pipeline.png](../../../images/gitlab-ci-pipeline/tutorials/all-pipeline.png)

    ![pipeline-id-7315451.png](../../../images/gitlab-ci-pipeline/tutorials/pipeline-id-7315451.png)

3. To see job's logs, in pipeline's page, click the job (For example, `get_env`) to enter job's page.

    ![pipeline-click-get-env.png](../../../images/gitlab-ci-pipeline/tutorials/pipeline-click-get-env.png)

    You will see the log of the job and "Related jobs" on the right side of job page.
    ![jobs-and-related.png](../../../images/gitlab-ci-pipeline/tutorials/jobs-and-related.png)

3. If any job fail, by default, the jobs that in the stage after the failed jobs will be skip.

    ![failed-job.png](../../../images/gitlab-ci-pipeline/tutorials/failed-job.png)

    You can retry job (even successful job) by clicking blue button with retry symbol on the
    upper left of job's page. Retried job have different job ID but it still belong to the same
    pipeline, which mean CI use the same commit to run jobs again.

### New code, new pipeline(s)

If you have new code to build, simply run **Tag-and-Push** again as described in step 1.

!!! warning
    Make sure you cancel your previous pipeline before pushing the new tag. We did not setup pipeline to prevent race condition from using the same environment (yet). We can improve it with [resource\_group](https://docs.gitlab.com/ee/ci/yaml/#resource_group).

### Build Container Only

If you want to build container only (no deploy and test), trigger CI with variables `SKIP_DEPLOY=t` as git push option.
    ```bash
    TAG=pypi-test12-$(date +"%s"); echo $TAG
    git tag $TAG
    git push gitlab -o ci.variable="SKIP_DEPLOY=t" $TAG
    ```

See all available variables in [Build-Deploy-Test's `rules` variables](./pipelines/build-deploy-test.md#rules-variables).

## Running The Release Pipeline

1. Create new release in Github with usual tag name. The tag must conform `^v3\.[0-9]{6}.*` regexp pattern. Otherwise, pipeline will not trigger.

    ![new-gh-release.png](../../../images/gitlab-ci-pipeline/tutorials/new-gh-release.png)


2. Pipeline will automatically triggered, with extra jobs `release_stable`.
    Note that this pipeline will use `preprod` as environment. Make sure `preprod` is free to use.

    ![release-stable.png](../../../images/gitlab-ci-pipeline/tutorials/release-stable.png)

3. Once the pipeline is completed. You will get container with suffix `-stable`, e.g., `v3.240502-stable`.

4. In case you want to skip test on release pipeline for any reason, you can do [Release without test (`ONLY_BUILD_RELEASE`)](./pipelines/release.md#release-without-test).
    4.1 Go to [CRABServer/cicd/trigger-ci](), edit the [`tag_and_push_only_build_release.sh`]() file.
    4.2 Change variable `TAG` to the version you want as final tag.
    4.3 Run the script
        ```
        [thanayut@localhost trigger-ci]$ bash tag_and_push_only_build_release.sh
        ```
    4.4 Go to the pipeline page to see it running.

### Run it manually

If you want to test full pipeline but do not want to publish the new release on GitHub, you can push tag that match `v3` pattern above directly to Gitlab repository.

We suggest to use tag name with prefix `v3.240000`, for example `v3.240000-test12-1725032880`, to indicate that this is a test tag.
