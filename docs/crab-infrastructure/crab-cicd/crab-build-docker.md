# Build TW and crabserver Docker Images


## automatic - on new tag

( import content from [https://twiki.cern.ch/twiki/bin/view/CMSPublic/Crab3CI](https://twiki.cern.ch/twiki/bin/view/CMSPublic/Crab3CI) )

## manual

### build RPM

This is useful to test changes to the `.spec` files.

First of all, ssh into `vocms055` machine, which is equipped to build rpms.

```bash
# this is needed to copy the rpms to the repository
voms-proxy-init -rfc -voms cms

cd /build/$username

export RELEASE_TAG=v3.230714
export CRABSERVER_REPO=dmwm
export BRANCH=master
export WMCORE_REPO=dmwm
# export WMCORE_TAG=
export CMSDIST_REPO=mapellidario
export CMSDIST_BRANCH=20230717-crab-condor
exprot RPM_REPO=dmapelli

wget https://raw.githubusercontent.com/dmwm/CRABServer/master/cicd/build/jenkins_build_rpm.sh

bash jenkins_build_rpm.sh
```

then look at the result at your rpm repository, for example [https://cmsrep.cern.ch/cmssw/repos/comp.dmapelli/slc7_amd64_gcc630/latest/RPMS.json](https://cmsrep.cern.ch/cmssw/repos/comp.dmapelli/slc7_amd64_gcc630/latest/RPMS.json)

### build docker

Login into the website https://registry.cern.ch/harbor/projects, this will activate your login token for docker login.

With lxplus, clone dmwm/CRABServer into your afs area.

Then, ssh into `cmsdocker01`.

```bash

export RELEASE_TAG=v3.230714
export BRANCH=""
export IMAGE_TAG=v3.230714.dmapelli
export RPM_REPO=dmapelli
export WORKSPACE=./

wget https://raw.githubusercontent.com/dmwm/CRABServer/master/cicd/build/jenkins_build_docker.sh

bash jenkins_build_docker.sh
```

---


