# CRAB K8s Helm Chart

We use helm as templating tool to render Kubernetes manifest files ([GH 7843#issuecomment-2025085120](https://github.com/dmwm/CRABServer/issues/7843#issuecomment-2025085120).

The chart is in CMSKubernets [helm/crabserver](https://github.com/dmwm/CMSKubernetes/tree/8bde12f667d03b0033c05d33699e427385dc2b4e/helm/crabserver) repository.

## What is it?

We use `helm template` command to generate manifest, like this:

```
# assume we are in CMSKubernetes/helm/crabserver directory
helm template crabserver . -f values.yaml -f values-testx.yaml > my_manifest
```

We supply the `template` command with 2 values files, one is `values.yaml` which always be used, like default values for the helm chart. Another one is `values-*.yaml` to override the default values. Depending on what you need, for basic REST deployment in dev env, you need to use `values-testx.yaml` to make helm chart generate only the K8s objects related to REST.

Then we apply it with simple `kubectl apply -f my_manifest` command. Of course frst make sure that `$KUBECONFIG` points to the correct file for the cluster which you want to change ([Ref.](https://cms-http-group.docs.cern.ch/k8s_cluster/cmsweb_developers_k8s_documentation/#service-deployment-steps-in-k8s))

### Note related to CMSWEB Operators

As CMSWEB operators still use `helm install/upgrade` command to apply the helm chart from [helm/crabserver](https://github.com/dmwm/CMSKubernetes/tree/8bde12f667d03b0033c05d33699e427385dc2b4e/helm/crabserver).

It is completely fine if she install in the empty cluster, and our suggestion in any case of conflict from their side is to purge all helm/manifests before doing `helm install/upgrade`.

But for us, we recommend to purge all helm chart that exists and reinstall with `kubectl apply` to avoid the conflict from our side in the future.

Also, please keep image tag in [values-prod.yaml](https://github.com/dmwm/CMSKubernetes/tree/8bde12f667d03b0033c05d33699e427385dc2b4e/helm/crabserver/values-prod.yaml) up-to-date for CMSWEB operators.

## Components

Helm chart has 3 major components, REST (and canary), log pipeline, and CI ServiceAccount.

### REST

This is the main CRAB REST service in kubernetes. It is consist of

- deployment.apps/crabserver
- service/crabserver
- configmap/crabserver

a.k.a. K8s objects to start the REST.

#### REST canary

We deploy another image as `crabserver-canary` deployment  (just single `deployment.apps` K8s object) to test against real production workload with flags `canary.enabled: true` in values file. See example in [values-canary](https://github.com/dmwm/CMSKubernetes/tree/8bde12f667d03b0033c05d33699e427385dc2b4e/helm/crabserver/values-canary.yaml).

The traffic will distribute **equally** (round-robin) among `crabserver` and `crabserver-canary` pods. So, if you want to run 10% of traffic on canary, you need deploy `crabserver` 9 pods and `crabserver-canary` 1 pod.

You will see canary pods in our usual [CRAB crabserver timber](https://monit-grafana.cern.ch/d/VML5DJEnk/crab-crabserver-timber?orgId=11&refresh=1m) dashboard. All the monitoring should work as usual.

### Log pipeline

We deploy `logstash/crabserver-filebeat` on the Kubernetes, in the same `crab` namespace.

Set the flag to `logPipeline.enabled: true` to deploy following manifests:

- configmap/filebeat-crab-config
- configmap/logstash-crab
- service/logstash-crab
- daemonset.apps/filebeat-crab
- deployment.apps/logstash-crab

The `service/logstash-crab` also allow us to send data from Filebeat in VM to the Logstash.

### CI Serviceaccount

Generating serviceaccount and token to use in CI pipeline for REST deployment.

Set `ciServiceAccount.enabled: true` to create the following object:

- serviceaccount/crab-gitlab-ci
- secret/crab-gitlab-ci-secret
- role.rbac.authorization.k8s.io/crab-ns-full-access
- rolebinding.rbac.authorization.k8s.io/crab-ns-full-access

Note that RBAC objects need to deploy by cluster admin (CMSWEB Operators). We do not have an access.


## Deployments

The `values-*.yaml` are available to use to deploy specific components.

| values file                                                                                                                                                | description                                                                                               |
|------------------------------------------------------------------------------------------------------------------------------------------------------------|-----------------------------------------------------------------------------------------------------------|
| [values-canary.yaml](https://github.com/dmwm/CMSKubernetes/tree/8bde12f667d03b0033c05d33699e427385dc2b4e/helm/crabserver/values-canary.yaml)               | Deploy `crabserver-canary` along with                                                                     |
| [values-clusteradmin.yaml](https://github.com/dmwm/CMSKubernetes/tree/8bde12f667d03b0033c05d33699e427385dc2b4e/helm/crabserver/values-clusteradmin.yaml)   | For CMSWEB operators to install the helm chart on the new cluster (All components except no REST canary). |
| [values-genall.yaml](https://github.com/dmwm/CMSKubernetes/tree/8bde12f667d03b0033c05d33699e427385dc2b4e/helm/crabserver/values-genall.yaml)               | For debugginga purpose. Generate all manifests in this helm.                                              |
| [values-logpipeline.yaml](https://github.com/dmwm/CMSKubernetes/tree/8bde12f667d03b0033c05d33699e427385dc2b4e/helm/crabserver/values-logpipeline.yaml)     | Deploy logging pipeline components.                                                                       |
| [values-prod.yaml](https://github.com/dmwm/CMSKubernetes/tree/8bde12f667d03b0033c05d33699e427385dc2b4e/helm/crabserver/values-prod.yaml)                   | Deploy REST on production.                                                                                |
| [values-testbed.yaml](https://github.com/dmwm/CMSKubernetes/tree/8bde12f667d03b0033c05d33699e427385dc2b4e/helm/crabserver/values-testbed.yaml)             | Deploy REST on testbed.                                                                                   |
| [values-testx-noprobe.yaml](https://github.com/dmwm/CMSKubernetes/tree/8bde12f667d03b0033c05d33699e427385dc2b4e/helm/crabserver/values-testx-noprobe.yaml) | Deploy REST on test cluster without readiness/liveness probe.                                             |
| [values-testx.yaml](https://github.com/dmwm/CMSKubernetes/tree/8bde12f667d03b0033c05d33699e427385dc2b4e/helm/crabserver/values-testx.yaml)                 | Deploy REST on test cluster                                                                               |

Require to supply `-f values.yaml` together with `-f values-*.yaml` when running `helm template`.
