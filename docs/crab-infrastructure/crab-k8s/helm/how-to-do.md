# CRAB K8s Helm Chart


Before running any command, make sure you have access to the K8s cluster.

```bash
kubectl config get-clusters
kubectl get pods -n crab
```

## Base commands

### Deploy

```
# export the KUBECONFIG
cd CMSKubernetes/helm/crabserver
# remove --dry-run=server at the end to apply
helm template crabserver . -f values.yaml -f values-testx.yaml | kubectl apply -f - --dry-run=server
```

Note that `-f values.yaml` is required. Otherwise, chart does not work.

## Delete

Change `apply` to `delete`

```
helm template crabserver . -f values.yaml -f values-testx.yaml | kubectl delete -f - --dry-run=server
```

### With `./deploy.sh`

Dario develop script to easy to deploy helm on any cluster. It is compatible with multiple KUBECONFIG and CMSWEB style.

Go to the directory `cd CMSKubernetes/helm/crabserver`, edit the file `values-$clustername.yaml` setting the desired image tag, then deploy with

```bash
# clustername can be prod, testbed, test2, test11, test12, test14
./deploy.sh $clustername
```
