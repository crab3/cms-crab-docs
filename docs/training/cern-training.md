Put here a list of training courses and opportunities offered by CERN, how to get access to them etc.
1. Sign up for CERN Udemy license and you can learn courses such as:
[Linux Foundation Course](https://cern.udemy.com/course/linux-foundation-certified-systems-administrator-lfcs/learn/lecture/44434296#overview)

2. [Spark training](https://sparktraining.web.cern.ch/)

3. [Puppet Config Training](https://configtraining.web.cern.ch/configtraining/)

