# Submit first CRAB task

This section want you to have familiar with CRABClient. See how user submit their tasks using LXPLUS to CRAB, without knowing which data we want to process and which application we need to run, which will be your routine when you develop CRAB system.

We use LXPLUS machine to submit CRAB task. CRAB Client is distributed as part of CMSSW stack, which accessible from CMS CVMFS `cms.cern.ch`.

In this tutorial, we use LXPLUS EL8 `lxplus8.cern.ch` machine and CMSSW version `CMSSW_13_0_2`.

## Prerequisite

* Have CERN computing account to access LXPLUS.
* Have grid certificate.
* Have storage space in `T2_CH_CERN` for output
* Have permission to access CMSWEB (See [Check you permission after get approved](../orientation/day1/new-member-setup.md#check-your-permission-after-getting-approved))

## Install grid certificates on LXPLUS

CRABClient authenticate with CRABServer using your grid certificate you get in [Obtain your grid certficate](../orientation/day1/new-member-setup.md#obtain-your-grid-certificate) step, the `myCertificate.p12` file. However, you need to convert pkcs12 to pem format for `voms-clients` to read, by putting thh cert to your `~/.globus`.

To do that:

1. **From your local machine**, go to `myCertificate.p12` file and convert into PEM format.
    ```
    openssl pkcs12 -in myCertificate.p12 -out usercert.pem -clcerts -nokeys
    openssl pkcs12 -in myCertificate.p12 -out userkey.pem -nocerts
    ```

2. **From your local machine again**, upload `usercert.pem` and `userkey.pem` to your `~/private` in LXPLUS. This directory can access by only you.
    ```
    rsync -av usercert.pem lxplus.cern.ch:private/
    rsync -av userkey.pem lxplus.cern.ch:private/
    ```

3. **At LXPLUS machine**, create `~/.globus` directory in your AFS home. Change permission to 0700.
    ```
    mkdir ~/.globus
    ```
    Then, symlink your cert from `~/private` to `~/.globus`
    ```
    ln -s ~/private/usercert.pem ~/.globus/usercert.pem
    ln -s ~/private/userkey.pem ~/.globus/userkey.pem
    ```

4. Run `voms-proxy-init --rfc --voms cms -valid 192:00` in LXPLUS machine to get your short life certficate. Output should look like this:
    ```
    [tseethon@lxplus8s15 ~]$ voms-proxy-init --rfc --voms cms -valid 192:00
    Contacting voms-cms-auth.app.cern.ch:443 [/DC=ch/DC=cern/OU=computers/CN=cms-auth.web.cern.ch] "cms"...
    Remote VOMS server contacted succesfully.


    Created proxy in /tmp/x509up_u145867.

    Your proxy is valid until Sat Jun 25 18:23:02 CEST 2022
    ```

### Setup environment

In order to run CRABClient, you need to install CMSSW and set up correct environment via scripts provided by CMSSW team.

**All of commands in following steps are run in `lxplus8.cern.ch` machine.**

1. Assume your workspace is at  `~/crab`. Pulling CMSSW software stack using `cmsrel`.

    ```bash
    cd ~/crab
    cmsrel CMSSW_13_0_2
    ```

You need to only run it once. Next time you login to ssh machine you can do step 2.

2. set shell environment with command `cmsenv`, you need to run it inside `CMSSW_13_0_2` directory, and run it everytime when your lost ssh session.
    ```bash
    cd CMSSW_13_0_2
    cmsenv
    ```

3. Try to run crab command `crab --version`.
    ```
    [tseethon@lxplus962 ~]$ crab --version
    CRAB client v3.240416
    ```
    Try `crab --help`, `crab <command> --help` to see what argument variables crab accept.

### Check write permission at distination storage site

Before we first submit the crab task. Check if you have permission to write CRAB output to `T2_CH_CERN`.

```bash
crab checkwrite --site T2_CH_CERN
```

Last line of output similar to this:
```
Success: Able to write in /store/user/tseethon on site T2_CH_CERN
```

### Submit your first task

We will submit [hc\_1kj.py](https://github.com/dmwm/CRABServer/blob/8038dcf4a199832792eb8191d164e19c6fffd038/test/statusTrackingTasks/HC-1kj.py) and we need [pset.py](https://github.com/dmwm/CRABServer/blob/8038dcf4a199832792eb8191d164e19c6fffd038/test/statusTrackingTasks/pset.py) file to submit task to CRAB.

* `HC-1kj.py` is CRAB configuration which contain informantion how to run your application with CRAB.
* `pset.py` is configuration for `cmsRun`, to specify how to run your CMSSW application. In short, every CRAB tasks is `cmsRun` process. Some of CRAB configurations will override value inside pset file.

Create new directory inside `CMSSW_13_0_2/src`, for example, `myfirstcrabtask`. Then, download both [hc\_1kj.py](https://raw.githubusercontent.com/dmwm/CRABServer/8038dcf4a199832792eb8191d164e19c6fffd038/test/statusTrackingTasks/HC-1kj.py) and [pset.py](https://raw.githubusercontent.com/dmwm/CRABServer/8038dcf4a199832792eb8191d164e19c6fffd038/test/statusTrackingTasks/pset.py) into this directory.

#### crab submit

Before submit, open `HC-1kj.py` and edit following configuration:

- `config.General.instance` to `'prod'`, or just comment it out.
- `config.Data.totalUnits` to 10

E.g.,
```python
...
config.General.instance = 'prod'
...
config.Data.totalUnits = 10
```

For more information about CRAB configuration, please see [CRAB Configuration file in twiki](https://twiki.cern.ch/twiki/bin/view/CMSPublic/CRAB3ConfigurationFile).

Then, submit task using `crab submit` command:


```bash
crab submit -c HC-1kj.py
```

You will see your CRAB "Task name" after "Success" line from output:
```
Success: Your task has been delivered to the dev CRAB3 server.
Task name: 240524_091451:tseethon_crab_20240524_111447
```

And next lines that state where is your CRAB project directory (1 task = 1 project directory)
```
Project dir: /tmp/crabStatusTracking/crab_20240524_111447
Please use ' crab status -d /tmp/crabStatusTracking/crab_20240524_111447 ' to check how the submission process proceeds.
```

We will use this for `crab status`.

#### crab status

To check status run `crab status` command:

```bash
crab status -d <project_dir>
```

You crab status will be changed from `NEW` -> `QUEUED` -> `SUBMITTED`.
`SUBMITTED` state mean the task is sucessfully submit to grid scheduler, but not mean your task is successfully run.
If status is `SUBMITFAILED`, CRAB reject your task due to, mostly, CRAB misconfiguration.

The important output lines:

| Line                      | Description                                                                                                                                                                                                              | Example                                                                                         |
|---------------------------|--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|-------------------------------------------------------------------------------------------------|
| CRAB project directory    | indicate path to project's directory.                                                                                                                                                                                    | `/tmp/crabStatusTracking/crab_20240524_111447`                                                  |
| Task name                 | CRAB Task name, primary key use across CRAB ecosystem.                                                                                                                                                                   | `240524_091451:tseethon_crab_20240524_111447`                                                   |
| Status on the CRAB server | CRAB Task's status, indicate which state is current. E.g, `SUBMITTED`                                                                                                                                                    | `NEW`, `QUEUED`, `SUBMITTED`, `SUBMITFAILED`                                                    |
| Task URL to use for HELP  | The help URL for user to send to us when asking for help in [Computing Tools](https://cms-talk.web.cern.ch/c/offcomp/comptools/87) forum. The URL is link to Task status on WebUI that convienient for operator to debug | <https://cmsweb-test2.cern.ch/crabserver/ui/task/240524_091451%3Atseethon_crab_20240524_111447> |

Here are example screenshots of terminal output:

![crab-submit1.png](../images/day1/crab-submit1.png)

![crab-submit2.png](../images/day1/crab-submit2.png)


## Next

Now, it is time to try [Official Workbook "CRAB3 Tutorial"](https://twiki.cern.ch/twiki/bin/view/CMSPublic/WorkBookCRAB3Tutorial).
