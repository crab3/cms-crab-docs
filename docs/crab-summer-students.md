# CRAB Summer Students

## 2024: CI/CD Pipeline Optimization: Reducing Docker Image Build Time

- Name: **Almuqaisib, Jana Kh A O**
- Report: <https://cds.cern.ch/record/2907872>

## 2023: Building of CRAB Spark Data Pipeline

- Name: **Atthaphan, Ek-Ong**
- Report: <https://cds.cern.ch/record/2868452>

## 2022: Data Analytics of User Historical Data Access Patterns and CERN Grid Resource Efficiencies

- Name: **Phumekham, Nutchaya**
- Report: <https://cds.cern.ch/record/2825376>
