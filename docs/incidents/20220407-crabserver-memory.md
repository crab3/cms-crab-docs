# 20220407 CRABServer Memory Consumption
Wa, Thanayut Seethongchuen

TL;DR

- Got memory alert. We set a resource limit to 3GB/pod, we get an alert from grafana when we reach 2.5GB.
- New memory metrics from cAdvisor. Great explanation from [IBM blog](https://community.ibm.com/community/user/aiops/blogs/riley-zimmerman/2021/07/02/memory-measurements-complexities-part2).
- Run `top` inside container and see crabserver process using memory around 1 GB. This looks ok.
- kube-eagle memory metrics, same as `container_memory_working_set_bytes`, stop grow around 2.8GiB
- Create new deployment `crabserver-no-filebeat`, `container_memory_working_set_bytes` grow less than `crabserver`.
- In the same `crabserver-no-filebeat`, if we start the filebeat process from inside the python container, the memory is the same as having filebeat in his own container.
- keep in mind: we already have a sidecar that sends the same logs data to s3
- No need to worry for now. Need help from expert.

## Intro

We got alert from grafana that CRABServer use memory more than 2.5 GiB, but we never expected to reach that point.
![crabserver-memory-usage-alert](../images/incidents/20220407/alert.png).

What we changed around this period are:

- move crabserver k8s cluster from frontend to backend. [April 4]
- New filebeat's sidecar. [April 4]
- Deploy new crabserver (py3.220318 to v3.220405) [April 5]


## Better metrics for memory
Memory usage from kube-eagle, according to Imran, are from `kubectl top pod --containers`, which is hard to intepret how memory really use inside container. Luckily, new k8s backend cluster expose cAdvisor metrics directly from kubelet. cAdvisor get memory metrics directly from cgroup  Please see the the description of each metrics name in [cAdvisor docs](https://github.com/google/cadvisor/blob/master/docs/storage/prometheus.md]).

I create new playground dashboard [here](https://monit-grafana.cern.ch/d/xJ3UbG87k/user-tseethon-crabserver-cmsweb-20220407?orgId=11&from=1649035495598&to=1649254419135) with help from Dario to plot the `container_memory_*` metrics that we get from prometheus backend cluster.

Great explanation with reference about memory metrics from **[IBM blog](https://community.ibm.com/community/user/aiops/blogs/riley-zimmerman/2021/07/02/memory-measurements-complexities-part2)**. *Recommend to read to better understand cAdvisor metrics.* Most my understanding is from this blog.

![new-dashboard](../images/incidents/20220407/dashboard.png)

To read the dashboard (left-to-right, up-to-down):

- Panel 1 is metrics from kube-eagle, same panel as in Crabserver cmsweb dashboard
- Panel 2 is `container_memory_working_set_bytes`. Please see explanation about `memory working set` in IBM Blog from link above. It count all memory usage except inactive cache. This metrics is same value as report by kube-eagle/kubectl-top.
- Panel 3 is `container_memory_rss`. It real memory usage of all process in container. Silmilar but not identical as RSS column of `top` command.
- Panel 4 is `working_set_bytes - rss`, Or we can said it is `active cache` + `kernel` + other stuff. Dario want to see how much of this section of memory increasing.
- Panel 5 is `container_memory_cache`. All memory use as cache, not directly manage by application. It like `buff/cache` value of `top` command, but for container.
- Panel 6 is `container_memory_swap`. Just in case swap happen.
- Panel 7 is `container_memory_mapped_file`. Memory use by tmpfs/shmem. Not sure if it correct definition but we can ignore it.
- Panel 8 is `container_memory_usage_bytes`. All memory usage of container.


## What we know

### Memory usage increasing, but not usage by CRABServer directly

- From [kube-eagle](https://monit-grafana.cern.ch/d/xJ3UbG87k/user-tseethon-crabserver-cmsweb-20220407?orgId=11&from=1649035495598&to=1649254419135&viewPanel=29), after deploy to new cluster (around 4 April, 16.00) onward, we saw increasing in memory usage which we never saw it before. I do not know why all pod redeploy once around 19.30.
![kube-eagle.png](../images/incidents/20220407/kube-eagle.png)
- When Stefano deployed new image (v3.220405) in 5 April around 18.10, we still see unusual memory increase pattern. So, it safe to assume that it not because of new image.
- We have stable(?) memory usage pattern from [process_exporter](https://monit-grafana.cern.ch/d/cmsweb_crabserver/crabserver-cmsweb?orgId=11&viewPanel=4&from=1648810962000&to=1649418250000). Confirm by similar value from RSS column in`top` and `container_memory_rss` [from Panel 3](https://monit-grafana.cern.ch/d/xJ3UbG87k/user-tseethon-crabserver-cmsweb-20220407?orgId=11&from=1649035495598&to=1649254419135&var-env=k8s-prod&var-host=All&var-env2=cmsweb-k8s-services.%2B&var-container=crabserver&viewPanel=31). This mean the memory usage did not directly use by app or have responsiblity to manage it.
![mem-process-exporter.png](../images/incidents/20220407/mem-process-exporter.png)
![top.png](../images/incidents/20220407/top.png)
![rss.png](../images/incidents/20220407/rss.png)

### Memory Working Set (WSS)

- Around 15.30, the memory report by kube-eagle, which is same value as `container_memory_working_set_bytes` are stop grow at ~2.8 GB.
- But `container_memory_rss` is still increasing. When subtract RSS to WSS show in like Panel 4 you will see that value are decreasing. That mean, either `active cache` or `kernel` (unlikely) memory are reclaim and give to CRABServer's process.
- It not swap or memory map as you can see in Panel 6 and 7.
- About `container_memory_working_set_bytes` or WSS, some study [here](https://faun.pub/how-much-is-too-much-the-linux-oomkiller-and-used-memory-d32186f29c9d) report that pod restart if it this hit cgroup limit.
- What we learn:
  - If `container_memory_working_set_bytes` hit limit, process will be kill. But, `active cache` and `kernel` (or else, I do not know) can reclaim memory back to RSS.


## It is the Filebeat that sends data to logstash.

We created new deployment call `crabserver-no-filebeat`. It is crabserver without new filebeat sidecar, apply manually on production cluster at 15.55. To see if `container_memory_working_set_bytes` increase in unusual rate like other. See manifest [here](https://gitlab.cern.ch/-/snippets/2138).

Result: Memory pattern of `crabserver-no-filebeat` are same as what we usually see before deploy on backend cluster.
https://monit-grafana.cern.ch/d/xJ3UbG87k/user-tseethon-crabserver-cmsweb-20220407?orgId=11&from=1649175046000&to=1649406630000&viewPanel=34
![crabserver-no-filebeat.png](../images/incidents/20220407/crabserver-no-filebeat.png)

[This grafana](https://monit-grafana.cern.ch/d/xJ3UbG87k/user-tseethon-crabserver-cmsweb-20220407?orgId=11&from=1649296706000&to=1649488113000&viewPanel=34) is good concrete evidence that new filebeat's sidecar cause this. We have 1 pod, `crabserver-d68675f46-7rjg9` restart at 10:32. It have `container_memory_working_set_bytes` greater than `crabserver-no-filebeat-6f77d9cb5c-ngkwh` at 16:24, only 6 hours.
![crabserver-restart.png](../images/incidents/20220407/crabserver-restart.png)

We then used the same k8s deployment of the pod without the filebeat-to-logstash container and started filebeat directly from the python container. The memory behaves the same as when we run filebeat in its own container (orange line, starting 2022-04-11 ~11.00).

![202204-crabserver-no-filebeat-1.png](../images/incidents/20220407/202204-crabserver-no-filebeat-1.png)

Keep in mind, we already have a filebeat container sending the same logs to s3 and that is not causing any problem.

## What we do not know

- **Why and how new filebeat's sidecar cause this?**
- What really is memory `active cache/file`? What condition memory of this region give it back to process.


## Conclusion: Do we need to concern about this?

From IBM Blog, `container_memory_working_set_bytes` **use only for pod eviction in Kubernetes.** cgroup use `container_memory_rss` as scores and killing of process.

So, No need to worry (for now) as long as `container_memory_working_set_bytes` not hit limit yet (it still increasing, but in very low rate), and `container_memory_rss` are stable. We need to ask expert from CMSWEB team and other expert for better explanation.
