# 2023-10-30 Accidentally set all files in DBS phy03 to INVALID

## Summary

On 26-30 October, users reported large numbers of datasets in `global/phys03` where all files were "missing in DAS" (i.e., DAS reporting datasets with 0 files). The actual files are still there but marked as invalid (`is_file_valid=0`) as some users discovered by themselves. This was caused by misinterpreted input from the DBSWriter API, which invalidated all user files. Stefano manually re-validated the file from the datasets the user has reported, publicized this incident, and contacted an expert to restore the database.

## Impact

All tasks submission with input dataset from `dbs/phys03` were failed.

## Root Causes

Invalid syntax of `/files` DBSWriter's API \[1\] caused DBS misinterpreted and changed **all files** status to invalid \[2\].

## Resolution

- Manually set file status back to valid for all datasets users requests \[2\].
- [Publicized](https://cms-talk.web.cern.ch/t/dbs-phys03-files-wrongly-invalidated/31152) incident and suggested users to fix their dataets by theselves as they discover more cases.
- Contacted Yuyi Guo, who redirected to Dennis Lee who is current DBS support person at FNAL in order to have a clean solution via direct intervention on Oracle DataBase

See "Special case/drawback" in "Discussion" section.

## Detection

Many users [reported](https://cms-talk.web.cern.ch/t/big-chunk-of-files-flagged-invalid-in-prod-phys03/31083) they could not find their files from `/USER` in DAS. It was confirmed that it was not user errors by checking files metadata in DAS, `last_modification_date` field was around midnight Oct 25 correlate to the time when operator test the code, and `last_modified_by` was operator's personal DN \[3\].

See "Diagnosis from Stefano report" section for more detail.


## Action Item

  | Item                                             | GH Issue |
  |--------------------------------------------------|----------|
  | None (but please see the note in the discussion) |          |


## Lesson learned

### What went well

- `crab setfilestatus` is stable and can re-validate users dataset easily.

### What went wrong.

- DBSWriter did not validate input properly.

### What we got lucky

None

### Timeline

24 Oct 2023

01:27 Stefano [started developing new CRAB client command](https://github.com/dmwm/CRABClient/issues/5204#issuecomment-1776207417)`setdataset/setfilestatus`

25 October 2023


00:39 All files in `global/phys03` got invalidate ([example](https://cmsweb.cern.ch/das/request?input=file%3D%2Fstore%2Fuser%2Frucio%2Ftseethon%2Ftest-rucio%2Fruciotransfers-1689744021%2FGenericTTbar%2Fruciotransfers-1689744021%2F230719_052024%2F0000%2Foutput_1.root&instance=prod/phys03)).

01:04 Stefano [opened the issue to dbs2go](https://github.com/dmwm/dbs2go/issues/102) about `/files` syntax

30 October 2023

10:59 User was reporting this issue in [cms-talk](https://cms-talk.web.cern.ch/t/big-chunk-of-files-flagged-invalid-in-prod-phys03/31083)

12:12 Stefano [identified the root cause and explained it to users](https://cms-talk.web.cern.ch/t/big-chunk-of-files-flagged-invalid-in-prod-phys03/31083/2).

31 October 2023

12:46 [Incident was officially annouced](https://cms-talk.web.cern.ch/t/dbs-phys03-files-wrongly-invalidated/31152) by Stefano.

8 November 2023

21:20 Stefano [announced to users](https://cms-talk.web.cern.ch/t/dbs-phys03-files-wrongly-invalidated/31152/2?u=tseethon
) that `setfilestatus` is available in `crab-dev` on production CVMFS.


### Reference


\[1\] Passing ``[“file1”,“file2”]`` in the body of the PUT instead of `"file1,file2"`.

\[2\] All users can update metadata to any datasets in `global/phys03` instance.

\[3\]
Revalidate all files in datasets:
```
crab setfilestatus --status VALID --dataset <datasetname> --status=VALID
```
A bash loop over lists of dataet names worked finely and quickly.

\[4\]

DAS interface (both WebUI and CLI) always hide result if invalid files.

In example, all files in `/GenericTTbar/tseethon-ruciotransfers-1689744021-94ba0e06145abd65ccb1d21786dc7e1d/USER` were invalid set by operator. The query returned "No results found".

Query:
```
file dataset=/GenericTTbar/tseethon-ruciotransfers-1689744021-94ba0e06145abd65ccb1d21786dc7e1d/USER
```
![debug-on-das-1.png](../images/incidents/20231030/debug-on-das-1.png)

However, when passing `status=*`, list of files are now shown.
Query:
```
file dataset=/GenericTTbar/tseethon-ruciotransfers-1689744021-94ba0e06145abd65ccb1d21786dc7e1d/USER status=*
```

![debug-on-das-2.png](../images/incidents/20231030/debug-on-das-2.png)

When click at <u>show</u> near `dbs3` to show file's metadata, `last_modified_by` and `last_modification_date` is the operator's DN and time when he develop the code.

![debug-on-das-3.png](../images/incidents/20231030/debug-on-das-3.png)

### Discussion

Until [dbs2go#102](https://github.com/dmwm/dbs2go/issues/102) is closed, beware the format of `PUT /files` API.

And the following notes were copied from original [DBS Phys03 incident report](https://codimd.web.cern.ch/nYF-oXqmR7SRwyc2WWLUNA) written by Stefano.

#### Special case/drawback of "Resolution"

*(The text is copied from "Special case/drawback" section)*

If a dataset had some VALID and some INVALID files before Oct 25, that information is lost. See e.g. [this](https://cms-talk.web.cern.ch/t/big-chunk-of-files-flagged-invalid-in-prod-phys03/31083/11?u=belforte) cms-talk post. In this case we need to re-validate files one by one using ``crab setfilestatus --status VALID --files <filename>``

A better and more general alternative is to identify all filenames which where changed during the incident and only re-validate them. As mentioned in 1. above there is no DBS API to do this, but it should be possible by acting directly on the DataBase with something like this pseudo-SQL:

```
SET is_file_valid=0 WHERE last_modified_by is "/DC=org/DC=terena/DC=tcs/C=IT/O=Istituto Nazionale di Fisica Nucleare/CN=Stefano Belforte belforte@infn.it" AND last_modification_date is between Oct24-Oct25 AND is_file_valid=0
```

Until that is done, users who had invalidated some files before, are unfortunately stuck unless they have a list of valid or invalid files already saved somewhere.


#### "Diagnosis" from Stefano report

*(The text is copied from "Diagnosis" section)*

 Time correlation with Stefano's work on developing `crab setfilestatus` was highly suspicious at least. When it was just one user reporting one dataset, I shruddered, but when it was tens of datasets it was clear that something had happened in DBS

 I had to fear that when I was using the wrong syntax \[\*\] for a list of files and [DBS was hanging](https://github.com/dmwm/dbs2go/issues/102#issue-1960228988), DBS was not "hanging doing nothing" but had (very) badly intepreted the list of (2) files and was busy changing file status for all files !

 This was confirmed when looking at some of the "previously good but now invalid" files in DBS where last_modification_date was around midnight Oct 25 and last_modified_by was my personal DN.

See <https://cms-talk.web.cern.ch/t/big-chunk-of-files-flagged-invalid-in-prod-phys03/31083/2?u=belforte>

All publication in DBS phys03 are done with TaskWorker DN, and lacking a working `set file status` command is very unlikely that users change file status themselves, in which case the user DN would be recorded.

-   \[\*\] I was somehow mislead by documentation saying "a list of file names" and passed `["file1","file2"]` in the body of the PUT instead of `"file1,file2"`. See also [this comment](https://github.com/dmwm/dbs2go/issues/102#issuecomment-1779545331).
    The bug eventually identified in that issue, i.e. that when submitting a list of files with the correct syntax nothing happens, is true and properly diagnosed by Valentin, but unrelated. I will open a new issued in DBS2GO about improving API params validation
