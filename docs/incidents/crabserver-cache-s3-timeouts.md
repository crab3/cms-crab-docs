## CRABServer often times out when contacting s3 server

### introduction

Sometimes, it happens that requests to `/prod/cache` take multiple of 60s.

![](https://mattermost.web.cern.ch/files/o8subc3knpdcpm1cze9zz6t4mo/public?h=u4xggA7W1HsYP9_25-hFJtR-2Ba4rNlLpoQ9WF7te5k)

The bands at multiples of 60s indicate that is more likely an infrasstructure problem than a code/application issue.

### Conclusion

The DNS returns both ipv6 and ipv4 addresses for `s3.cern.ch`, the boto3 client tries to use the ipv6 ones for multiples of 30s but fails because we do not have ipv6 inside the pods, then falls back to ipv4 and evth works perfectly. 

[EDIT] ~~I (Dario) am still not sure how we can address this. Suggestions are welcome.~~

Following the publication of this investigation, CMSWEB team contacted CERN IT and they are following up in the ticket [INC3147173](https://cern.service-now.com/service-portal?id=ticket&table=incident&n=INC3147173)

### Which portion of CRABServer is affected

From the monitoring, we notice that this affects `GET /prod/cache`. 

Adding some time measurements in `src/python/CRABInterface/RESTCache.py` as described in this [PR](https://github.com/dmwm/CRABServer/pull/7256), we noticed that the request that often takes long is [s3_client.head_object()](https://github.com/dmwm/CRABServer/blob/fd5094cbe3fdde498cb1d145b6528e1838a415cf/src/python/CRABInterface/RESTCache.py#L138)

From a production log, we got

```plaintext
...
2022-05-16 18:19:17,112:INFO:RESTCache:DM debug - s3 generate_presigned took: 0.0011827945709228516 seconds
2022-05-16 18:18:32,030:INFO:RESTCache:DM debug - s3 head_object took: 60.08279728889465 seconds
2022-05-16 18:18:31,884:INFO:RESTCache:DM debug - s3 generate_presigned took: 0.0005872249603271484 seconds
...
```

### Replicate the issue

Tests that did not replicate the issue

- Running a script in lxplus that makes a similar request to head_object

Tests that replicated the issue

- making a `GET /dev/cache` request to test1
- running inside the pod a variation of the script used in lxplus.

#### making a request to test1

I noticed that the request

```plaintext
curl --cert $X509_USER_PROXY --key $X509_USER_PROXY  "https://cmsweb-test1.cern.ch:8443/crabserver/dev/cache?subresource=upload&objecttype=sandbox&tarballname=a3aa1a7ca560e14704168a4d8407cad66d38830209e320d327c11ab8569545f3.tar.gz"
```

consistently took ~30s. This is a start.

I took the sandboxname from a task that I submitted to test1. When this sandbox will expire, we can replicate this by simply suhbmitting a new task.

#### running a script inside the pod

From inside the pod, I have been running the script added in this [branch](https://github.com/dmwm/CRABServer/compare/master...mapellidario:20220525-bototest) (I still need to polish it a little bit, i will open a PR to add this to master in the next couple of days).

I noticed that when calling multiple times `check_s3()`, which only get some information about the current bucket, and/or `findfile()`, which only uses `head_object()` to find a file in a bucket, all the request but the first one were quick. The first request to either of the endpoints consistently took 30s.

### Extract information

#### profile python code

The first naive attempt is profiling the python code, to discover which is the offending function that takes 30s.

We can profile the python code with

```bash
cd /data
python3 -m cProfile -o bototest.prof TestBotoClient.py
```

This uses the default python profilers. Pay attention that it is not perfect, there are some [limitations](https://docs.python.org/3/library/profile.html#limitations)! The most important one is 

> There is a fundamental problem with deterministic profilers involving accuracy. The most obvious restriction is that the underlying “clock” is only ticking at a rate (typically) of about .001 seconds. Hence no measurements will be more accurate than the underlying clock.

This is not a problem, if we ever need to make precise measurements we have

> Python 3.3 adds several new functions in time that can be used to make precise measurements of process or wall-clock time. For example, see time.perf_counter().

Then we need to analyze the `bototest.prof` file. I am lazy and used the pod to do so, but this can be done also on our laptops.

The most convinient way that I found is using a combination of [flameprof](https://github.com/baverman/flameprof) for processing the python `.prof` file and Brendan Gregg's [flamegraph](https://www.brendangregg.com/FlameGraphs/cpuflamegraphs.html) for the visualization.

```python
# from inside the pod, you can install the necessary components with
cd /data
source source srv/current/sw..../slc7_amd64_gcc
630/cms/crabserver/.../etc/profile.d/init.sh

export CRYPTOGRAPHY_ALLOW_OPENSSL_102=True
python3 -m ensurepip # installs pip
python3 -m pip install flameprof

wget https://raw.githubusercontent.com/brendangregg/FlameGraph/master/flamegraph.pl

#then analyze the output of the profiling with
python3 -m flameprof --format=log bototest.prof | perl ./flamegraph.pl > bototest.svg
```

Notice that there are two main ways of representing the flame graph:

- direct graph
- inverse / icicle graph, described at "Numa Rebalancing" section of the Brendan Gregg's flamegraph post.

I ouir case, they look like:

[direct](https://dmapelli.web.cern.ch/public/crab/20220525-bototest/bototest-ff.svg)

![direct](https://dmapelli.web.cern.ch/public/crab/20220525-bototest/bototest-ff.svg)

[inverse](https://dmapelli.web.cern.ch/public/crab/20220525-bototest/bototest-ff-ri.svg)

![inverse](https://dmapelli.web.cern.ch/public/crab/20220525-bototest/bototest-ff-ri.svg)

We can notice that in both graph what emerges as taking a long time is `method "connect" of socket.socket`. 

Note for the reader: I extracted these flame graphs from a run of the script in which i queried the `head_object` endpoint three times. However, from the graph it seems that `time.sleep(1)` and `head_object()` have been called only once. I suspect that either

- due to its limitations, the profiler did not notice that the script went through another iteration of the loop
- flameprof is making some approximations. 

In any case, the culprit is clear. It is not in our application, the OS is slow in opening a socket.

#### tcpdump

This felt like the next logical step. 

I installed tcpdump inside the pod with

```bash
sudo su
yum install tcpdump
```

As suggested [here](https://www.wireshark.org/docs/wsug_html_chunked/AppToolstcpdump.html), I started tcpdump with

```bash
tcpdump -s 65535 -w tcpdump.out
```

Then, on another shell i started the script with

```bash
# prepare the py3 env as above
python3 TestBotoClient.py
```

When the script finished, i exited tcpdump. 

The tcpdump output file is available [here](https://dmapelli.web.cern.ch/public/crab/20220525-bototest/tcpdump.out)

I analyzed the output with wireshark, because i like GUIs :)

First of all, we notice that short after the script is started (2s from the start of the tcpdump capture started), the client sent some DNS requests for `s3.cern.ch`

![](https://mattermost.web.cern.ch/files/67447j8j3jdnfkzr9yasjkj1uo/public?h=9zdJ6DO5DlWJi-Nt-GyUbghHjjZOyPxQm_bRGKHfr-o)

The results are:
for ipv6:

```
Answers
    s3.cern.ch: type AAAA, class IN, addr 2001:1458:d00:12::3e0
    s3.cern.ch: type AAAA, class IN, addr 2001:1458:d00:18::1ee
    s3.cern.ch: type AAAA, class IN, addr 2001:1458:d00:1a::24b
    s3.cern.ch: type AAAA, class IN, addr 2001:1458:d00:13::1e5
    s3.cern.ch: type AAAA, class IN, addr 2001:1458:d00:4e::100:4ae
    s3.cern.ch: type AAAA, class IN, addr 2001:1458:d00:4e::100:5d
    s3.cern.ch: type AAAA, class IN, addr 2001:1458:d00:1c::405
    s3.cern.ch: type AAAA, class IN, addr 2001:1458:d00:3f::100:2bd
    s3.cern.ch: type AAAA, class IN, addr 2001:1458:d00:18::390
    s3.cern.ch: type AAAA, class IN, addr 2001:1458:d00:3f::100:2d9
```

for ipv4

```plaintext
Answers
    s3.cern.ch: type A, class IN, addr 137.138.152.241
    s3.cern.ch: type A, class IN, addr 137.138.44.245
    s3.cern.ch: type A, class IN, addr 188.185.86.117
    s3.cern.ch: type A, class IN, addr 188.184.73.131
    s3.cern.ch: type A, class IN, addr 188.185.87.72
    s3.cern.ch: type A, class IN, addr 137.138.77.21
    s3.cern.ch: type A, class IN, addr 137.138.33.10
    s3.cern.ch: type A, class IN, addr 137.138.151.203
    s3.cern.ch: type A, class IN, addr 188.184.74.136
    s3.cern.ch: type A, class IN, addr 137.138.33.24

```

Then we notice that the client fails to connect to (the first ipv6 IP of `s3.cern.ch`):

- `2001:1458:d00:12::3e0`
- `2001:1458:d00:18::1ee`
- `2001:1458:d00:1a::24b`

I skip over some lines, then we have at ~30s from the start of the tcp capture

![](https://mattermost.web.cern.ch/files/b1z4f8jtmfgjpr1otqe6ssp5mw/public?h=EkuadeTYqrCwtuO1291edb7jqMfTikoRg1dLL-nQsEg)

We notice that the client fails to connect to `2001:1458:d00:3f::100:2d9` (the last `s3.cern.ch` ipv6 IP), then successfully connects to `137.138.152.241` (the first ipv4 IP of `s3.cern.ch`).

At this point everything work as expected and quickly after the script ends.


### what is the problem?

From lxplus we can contact s3 via ipv6

```plaintext
> [16:56] lxplus8s21 ~
> curl "http://[2001:1458:d00:12::3e0]"
<?xml version="1.0" encoding="UTF-8"?><ListAllMyBucketsResult xmlns="http://s3.amazonaws.com/doc/2006-03-01/"><Owner><ID>anonymous</ID><DisplayName></DisplayName></Owner><Buckets></Buckets></ListAllMyBucketsResult>% 
```

While from inside the pod we can contact s3 onnly via ipv4

```plaintext
[_crabserver@crabserver-6b9b7b849f-4rmv4 data]$ curl 137.138.44.245
<?xml version="1.0" encoding="UTF-8"?><ListAllMyBucketsResult xmlns="http://s3.amazonaws.com/doc/2006-03-01/"><Owner><ID>anonymous</ID><DisplayName></DisplayName></Owner><Buckets></Buckets></ListAllMyBucketsResult>

[_crabserver@crabserver-6b9b7b849f-4rmv4 data]$ curl "http://[2001:1458:d00:12::3e0]"
curl: (7) Failed to connect to 2001:1458:d00:12::3e0 port 80: No route to host
```

The boto3 client is not aware beforehand that the connection via ipv6 is blocked, so it tried to contact the s3 server at every ipv6 IP before "falling back" to ipv4.

The boto3 client is waiting this whole time for the connection to open and it is reasonable to assume that `socket.socket._connect` does not hold the GIL.

### Reproducing the issue in a production pod

If you replicate the curl call from a production pod, you will get

```plaintext
[_crabserver@crabserver-567d5ccc97-7nqdc data]$ curl "http://[2001:1458:d00:12::3e0]"
curl: (3) [globbing] error: bad range specification after pos 9
```

You can use the `-g` parameter

```plaintext
[_crabserver@crabserver-567d5ccc97-7nqdc data]$ curl -g "http://[2001:1458:d00:12::3e0]"
...


Which will instead hang for a looong time... before timing out

```plaintext
[_crabserver@crabserver-567d5ccc97-7nqdc data]$ time curl -g "http://[2001:1458:d00:12::3e0]"
curl: (7) Failed connect to 2001:1458:d00:12::3e0:80; Connection timed out

real    2m8.384s
user    0m0.009s
sys     0m0.007s
```





