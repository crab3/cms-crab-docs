## CRAB REST memory

- we do not have an incident. the service is running fine. 
- However, there is something that we do not understand with the metrics we are getting and we may need some help deciphering them.

### PRESENT. python3 + filebeat(s3) + filebeat(logstash)

k8s config: [here](https://github.com/dmwm/CMSKubernetes/blob/1d010714e23e23cfe4f5994f86f24ade1da39960/kubernetes/cmsweb/services/crabserver.yaml), deployed on 2022-04-05.

Evidence: memory usage as reported by kubeeagle has a different [pattern](https://monit-grafana.cern.ch/d/xJ3UbG87k/user-tseethon-crabserver-cmsweb-20220407?from=now-14d&to=now&viewPanel=29). Keep in mind: we set 3GB/pod resource limit. 

With the new cluster, we have new metrics `cAdvisors`. 

1. `container_memory_working_set_bytes`: feels the same as kube-eagle, `kubectl top pod`, [plot here](https://monit-grafana.cern.ch/d/xJ3UbG87k/user-tseethon-crabserver-cmsweb-20220407?from=now-7d&to=now&viewPanel=34)
1. `container_memory_rss`: feels the same as `top` from inside the specific container. [plot here](https://monit-grafana.cern.ch/d/xJ3UbG87k/user-tseethon-crabserver-cmsweb-20220407?from=now-7d&to=now&viewPanel=31)

evidence:

- python+filebeat(s3): kube-eagle looks similar to top inside the container
- python+filebeat(s3)+filebeat(logtash): kube-eagle seems to include some memory that we do not see from inside our containers (we tried running fileabeat both in a separate sidecar and in the same container as python webserver )

Have a look at the plot [here](https://monit-grafana.cern.ch/d/xJ3UbG87k/user-tseethon-crabserver-cmsweb-20220407?orgId=11&var-env=k8s-prod&var-host=All&var-env2=cmsweb-k8s-services.%2B&var-container=crabserver&var-pod=crabserver-no-filebeat-6f77d9cb5c-ngkwh&var-pod=crabserver-no-filebeat-6f77d9cb5c-shgx9&var-pod=crabserver-d68675f46-jc75n&viewPanel=34)

- `jc75n`: py3 + filebeat(s3) + filebeat(logstash)
- `ngkwh`: py3 + filebeat(s3)
- `shgx9`: py3 with filebeat(logstash) inside + filebeat(s3)

```plaintext
> kubectl -n crab top pod --containers
POD                                       NAME                        CPU(cores)   MEMORY(bytes)
[...]
crabserver-d68675f46-jc75n                crabserver                  74m          2663Mi
crabserver-d68675f46-jc75n                crabserver-filebeat         6m           32Mi
crabserver-d68675f46-jc75n                crabserver-filebeat-monit   10m          39Mi
[...]
crabserver-no-filebeat-6f77d9cb5c-ngkwh   crabserver                  276m         1447Mi
crabserver-no-filebeat-6f77d9cb5c-ngkwh   crabserver-filebeat         6m           32Mi
crabserver-no-filebeat-6f77d9cb5c-shgx9   crabserver                  85m          1762Mi
crabserver-no-filebeat-6f77d9cb5c-shgx9   crabserver-filebeat         5m           32Mi
```

Main question: why adding a sidecar filebeat makes the memory (kubeagle / `container_memory_working_set_ytes`) of the python container increase?!

## backup

### PAST. python3 + filebeat(s3)

This has been running smoothly for us. At the beginning we worried too much, we used the word "memory leak" despite not having any evidence that we were facing a leak. Eventually, python3+cherrypy+cx_oracle manages memory in a different way with respect to python2+cherrypy+cx_oracle. E.g. memory needs for strings (JSON structures) increases 3-fold due to unicode.

In py3, it hovered around 1GB/pod [(as measured by kube-eagle)](https://monit-grafana.cern.ch/d/xJ3UbG87k/user-tseethon-crabserver-cmsweb-20220407?orgId=11&from=1647215285635&to=1649043079849&viewPanel=29) with the old configuration (let's call it `A`), consisting of a container with the python web server and a container with filebeat to send the webserver logs to s3.

In py2, it was very consistently around 200MB/pod
