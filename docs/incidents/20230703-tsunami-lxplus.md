## Attack using "tsunami" framework on lxplus

Ref: [https://advisories.web.cern.ch/483478fe110b11ee85dd0a580a4c25c0](https://advisories.web.cern.ch/483478fe110b11ee85dd0a580a4c25c0)

Dario run the following checks on CRAB's VMs and found no evidence that CRAB machines
have been compromised. We should be safe.

```plaintext
> # ssh to aiadm
> # check that you can run sudo on our machines
> wassh -c vocmsglidein/crabtaskworker 'sudo whoami'
crab-dev-tw01.cern.ch: root
[...]

> wassh -c vocmsglidein/crabschedd 'sudo whoami'
crab-preprod-scd03.cern.ch: root
[...]

# check that the syntax is ok by running a command on a single machine
> wassh -h crab-dev-tw02 'sudo grep -RE "/\." /var/spool/cron/'
crab-dev-tw01.cern.ch:

> # real checks
> wassh -c vocmsglidein/crabtaskworker 'sudo grep -RE "/\." /var/spool/cron/'
crab-dev-tw01.cern.ch:
[...]

> wassh -c vocmsglidein/crabtaskworker 'sudo grep -RE "/update" /var/spool/cron/'
crab-dev-tw01.cern.ch:
[...]

> wassh -c vocmsglidein/crabtaskworker 'sudo ls -laR /dev/shm'
crab-dev-tw01.cern.ch:
   /dev/shm:
   total 0
   drwxrwxrwt.  2 root root   40 Jun 10 01:25 .
   drwxr-xr-x. 18 root root 2900 Jun 10 01:25 ..
[...]

> wassh -c vocmsglidein/crabtaskworker 'sudo find / -mount -name authorized_keys -type f -exec grep "AAAAB3NzaC1yc2EAAAADAQABAAABAQCzml2PeIHOUG+78TIk0lQcR5JC/mlDElDtplEfq8KDiJFwD8z9Shhk2kG0pwzw9uUr7R24h8lnh9DWpiKfoy4MeMFrTO8akT1hXf4yn9IEEHdiq9hVz1ZkEnUdjyzuvXGIOcRe2FqQaovFY15gSDZzJc5K6NMT8uW1aitHAsYXZDW8uh+/SJAqcCCVUtVnZRj4nlhQxW2810CJGQQrixkkww7F/9XRlddH3HkNuRlZLQMk5oGHTxeySKKfqoAoXgZXac9VBAPRUU+0PrBrOSWlXFbGBPJSdvDfxBqcg4hguacD1EW0/5ORR7Ikp1i6y+gIpdydwxW51yAqrYqHI5iD" {} \;'
crab-dev-tw01.cern.ch:
[...]

> wassh -c vocmsglidein/crabtaskworker 'sudo find / -mount -name authorized_keys -type f -exec grep "AAAAB3NzaC1yc2EAAAADAQABAAABAQDXqEDZQYyXbee0lTSoc3xVcuzX7j+7AAvPuJ5cnEbSMRnPpvh9MdJGnbpWvMckmtgJ7VQNCCJIhAfgyE4BbTgh3X5JUN2S2SYi2LlEsP+tn+bTk0aEUtTJYNWfYR9X43aW6Uo98U0xMR0TmAO6Fpja7ZvkwrulDKyCr/h2Eb6ml9Davk0xY5P/xB9/ivfqN8BAXGLiheILKckXXQv52qeTSbQwFB3eLR+b4NIq8UCNL0jJkvQlx0tCcgRHIvt5Df2fDlu0H9WUeiqBZr0EeD2YkEWL7m00jcdb8+jyY4rkWUzPgbV8NnFNHaNriT5YO81rPQfkJOqEyYPh/X5gGpyP" {} \;'
crab-dev-tw01.cern.ch:
[...]

> wassh -c vocmsglidein/crabschedd 'sudo grep -RE "/\." /var/spool/cron/'
crab-preprod-scd03.cern.ch:
[...]

> wassh -c vocmsglidein/crabschedd 'sudo grep -RE "/update" /var/spool/cron/'
crab-preprod-scd03.cern.ch:
[...]

> wassh -c vocmsglidein/crabschedd 'sudo ls -laR /dev/shm'
crab-preprod-scd03.cern.ch:
   /dev/shm:
   total 0
   drwxrwxrwt  2 root root   40 Jan 24 14:05 .
   drwxr-xr-x 18 root root 2840 Jan 24 14:05 ..
[...]

> wassh -c vocmsglidein/crabschedd 'sudo find / -mount -name authorized_keys -type f -exec grep "AAAAB3NzaC1yc2EAAAADAQABAAABAQCzml2PeIHOUG+78TIk0lQcR5JC/mlDElDtplEfq8KDiJFwD8z9Shhk2kG0pwzw9uUr7R24h8lnh9DWpiKfoy4MeMFrTO8akT1hXf4yn9IEEHdiq9hVz1ZkEnUdjyzuvXGIOcRe2FqQaovFY15gSDZzJc5K6NMT8uW1aitHAsYXZDW8uh+/SJAqcCCVUtVnZRj4nlhQxW2810CJGQQrixkkww7F/9XRlddH3HkNuRlZLQMk5oGHTxeySKKfqoAoXgZXac9VBAPRUU+0PrBrOSWlXFbGBPJSdvDfxBqcg4hguacD1EW0/5ORR7Ikp1i6y+gIpdydwxW51yAqrYqHI5iD" {} \;'
crab-preprod-scd03.cern.ch:
[...]

> wassh -c vocmsglidein/crabschedd 'sudo find / -mount -name authorized_keys -type f -exec grep "AAAAB3NzaC1yc2EAAAADAQABAAABAQDXqEDZQYyXbee0lTSoc3xVcuzX7j+7AAvPuJ5cnEbSMRnPpvh9MdJGnbpWvMckmtgJ7VQNCCJIhAfgyE4BbTgh3X5JUN2S2SYi2LlEsP+tn+bTk0aEUtTJYNWfYR9X43aW6Uo98U0xMR0TmAO6Fpja7ZvkwrulDKyCr/h2Eb6ml9Davk0xY5P/xB9/ivfqN8BAXGLiheILKckXXQv52qeTSbQwFB3eLR+b4NIq8UCNL0jJkvQlx0tCcgRHIvt5Df2fDlu0H9WUeiqBZr0EeD2YkEWL7m00jcdb8+jyY4rkWUzPgbV8NnFNHaNriT5YO81rPQfkJOqEyYPh/X5gGpyP" {} \;'
crab-preprod-scd03.cern.ch:
[...]

> wassh -c vocmsglidein/crabschedd 'sudo find /home -mount -name authorized_keys -type f -exec grep "AAAAB3NzaC1yc2EAAAADAQABAAABAQDXqEDZQYyXbee0lTSoc3xVcuzX7j+7AAvPuJ5cnEbSMRnPpvh9MdJGnbpWvMckmtgJ7VQNCCJIhAfgyE4BbTgh3X5JUN2S2SYi2LlEsP+tn+bTk0aEUtTJYNWfYR9X43aW6Uo98U0xMR0TmAO6Fpja7ZvkwrulDKyCr/h2Eb6ml9Davk0xY5P/xB9/ivfqN8BAXGLiheILKckXXQv52qeTSbQwFB3eLR+b4NIq8UCNL0jJkvQlx0tCcgRHIvt5Df2fDlu0H9WUeiqBZr0EeD2YkEWL7m00jcdb8+jyY4rkWUzPgbV8NnFNHaNriT5YO81rPQfkJOqEyYPh/X5gGpyP" {} \;'
crab-preprod-scd03.cern.ch:
[...]

> wassh -c vocmsglidein/crabschedd 'sudo find /home -mount -name authorized_keys -type f -exec grep "AAAAB3NzaC1yc2EAAAADAQABAAABAQCzml2PeIHOUG+78TIk0lQcR5JC/mlDElDtplEfq8KDiJFwD8z9Shhk2kG0pwzw9uUr7R24h8lnh9DWpiKfoy4MeMFrTO8akT1hXf4yn9IEEHdiq9hVz1ZkEnUdjyzuvXGIOcRe2FqQaovFY15gSDZzJc5K6NMT8uW1aitHAsYXZDW8uh+/SJAqcCCVUtVnZRj4nlhQxW2810CJGQQrixkkww7F/9XRlddH3HkNuRlZLQMk5oGHTxeySKKfqoAoXgZXac9VBAPRUU+0PrBrOSWlXFbGBPJSdvDfxBqcg4hguacD1EW0/5ORR7Ikp1i6y+gIpdydwxW51yAqrYqHI5iD" {} \;'
crab-preprod-scd03.cern.ch:
[...]

```
