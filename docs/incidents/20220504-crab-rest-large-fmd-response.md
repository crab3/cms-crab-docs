# 20220504 CRAB REST large FileMetadata response.

In short:

- We suffer from `DatabaseUnavailable()` exception from WMCore. We did not know why it happend.
- We found some GET /filemetadata requests got large response data, make long response time.
- Stefano fix by limit number of item in response body. Problem solved.

See <https://github.com/dmwm/CRABServer/issues/7204>
