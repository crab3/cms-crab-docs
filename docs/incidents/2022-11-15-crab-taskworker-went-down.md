# 2022-11-15 CRAB TaskWorker went down after too many task submissions.

## Summary

On 14 November 2022 at 18.05, one user started abusing system by submitting many jobs with large sandbox files. Taskworker process as much as it can until TW disk is full and make all worker process crash. We also noticed vocms0106/vocms0122 were freezing because of OOM and needed to restart. We banned the user and removed all running dagman from schedd.

## Impact

- Users can submit tasks, but new tasks are stuck in the "NEW" state for hours.
- New tasks have the status "SUBMITFAILED" when number of running DAGMAN hit the limit of 200.
- vocms0106/vocms0122 was in a degraded state (not sure how it affected the jobs).

## Root Causes

- Tasks backlog started to pile up from 2022-11-15 22.15, with 4.38k tasks at peak.
  - One user submitted 6530 tasks [1] to CRAB in 22 hours.
- TW can keep up with that submission rate, but it quickly fills `/data` disk until it is full.
  - Banned user submitted 105.17 MB of sandbox file (there is a root file inside sandbox).
- When the disk is full, all TW worker processes crash and become zombie. No new can be process until operator restart.


## Trigger

Banned user submit many task with large user sandbox files.

## Resolution

- Manually restart TW and vocms0106/vocms0112 VM.
    - Still unknow why restarting TW free up disk space.
- Temporary ban user using `config.TaskWorker.bannedUsernames` in TaskWorkerConfig.py.
- Remove all DAGMAN of the banned user from all schedd nodes using `condor_rm` command (see \[2\])

## Detection

- Dario looks at the [TASKS ENTERING STATUS PER 2h INTERVAL - PROD](https://monit-grafana.cern.ch/d/CsnjLe6Mk/crab-overview?orgId=11&editPanel=76&from=1668443145442&to=1668553585309) in the morning (as crab operator duty), and notice that we have a lot of "NEW" task status.
- Wa got an alert from [NUMBER OF RUNNING DAGMANS PER SCHEDD](https://monit-grafana.cern.ch/d/--01vBk4k/user-tseethon-crab-alert?orgId=11&editPanel=60&from=1668430721953&to=1668535468619) in his private dashboard, which implies there is someone submit a lot of task in a short period.

## Action Item

| Item                                                                            | GH Issue                                                   |
|---------------------------------------------------------------------------------|------------------------------------------------------------|
| Limit task submission                                                           | [GH #7485](https://github.com/dmwm/CRABServer/issues/7485) |
| New alert checking if taskworker is healthy                                     | Done (Dario)                                               |
| Make TW container easier to switch config between deployed by puppet and adhoc. | [GH #7489](https://github.com/dmwm/CRABServer/issues/7489) |
| Change how we handle user sandbox                                               | [GH #7461](https://github.com/dmwm/CRABServer/issues/7461) |
| Make python Masterworker handle child process that become zombie                | [GH #7457](https://github.com/dmwm/CRABServer/issues/7457) |
| Prevent metrics lost when CRAB REST go down Postmortem                          | [GH #7486](https://github.com/dmwm/CRABServer/issues/7486) |
| Make script/runbook for executing the same action on all schedd                 | [GH #7487](https://github.com/dmwm/CRABServer/issues/7487) |
| Find out why schedd machine got OOM and prevent it in the future                | [GH #7488](https://github.com/dmwm/CRABServer/issues/7488) |


## Lesson learned

### What went well

- Monitoring dashboard are work as expect, from [TASKS ENTERING STATUS PER 2h INTERVAL - PROD](https://monit-grafana.cern.ch/d/CsnjLe6Mk/crab-overview?orgId=11&from=now-3d&to=now&viewPanel=76) and [NUMBER OF RUNNING DAGMANS PER SCHEDD](https://monit-grafana.cern.ch/d/CsnjLe6Mk/crab-overview?orgId=11&from=now-3d&to=now&viewPanel=60). Easy to spot problem.
  - Plus alert was firing to [CRAB - service alerts channel](https://mattermost.web.cern.ch/cms-o-and-c/pl/cyshbj7oztbwjcsm68ohs3adgw)         when problem occurs.
- Banning user works as expected.
- Dario have habit in looking at monitoring and notice problem first.

### what went wrong

- No limit of task submission per user cooldown of submission in TW.
- No one notice when taskworker stop working, until we look at dashboard.
- schedd machine got oom. we do not know reason why it happens and how to prevent it.

### what we got lucky

- Restart TW actually fix the root cause that we never know until write down a postmortem.
- There is error in FE at the same time we have trouble :)

## Timeline

14 November 2022
- 18:05 "number of running dagman" start rising <https://monit-grafana.cern.ch/d/--01vBk4k/user-tseethon-crab-alert?orgId=11&editPanel=60&from=1668430721953&to=1668535468619>
- 18:12 disk usage on `/data/` in crab-prod-tw01 is rising <https://monit-grafana.cern.ch/d/rYdddlPWkk/node-exporter-full-crab-vms?orgId=11&from=1668442998700&to=1668507374826&var-DS_PROMETHEUS=cmsmonitoring-prometheus&var-job=crab-taskworker&var-node=crab-prod-tw01:9100&var-diskdevices=%5Ba-z%5D%2B%7Cnvme%5B0-9%5D%2Bn%5B0-9%5D%2B>
- 21:25 "number of running dagman" hitting limit (200) on many schedd node.
- 22:04 "number of running dagman" alert are firing <https://mattermost.web.cern.ch/cms-o-and-c/pl/cyshbj7oztbwjcsm68ohs3adgw>
- 22:05 "NEW" tasks start piling up <https://monit-grafana.cern.ch/d/CsnjLe6Mk/crab-overview?orgId=11&from=1668454153411&to=1668532108299&editPanel=8>
- 22:53 TW worker crash \[3\]
- 22:56 `/data` disk was full.

15 November 2022

- 9.27 Dario notice the problem and ask in `~CRAB DevOps` MatterMost channel <https://mattermost.web.cern.ch/cms-o-and-c/pl/j5trjsz4iprc9j81ssh394jgwa>
- 10:03 Dario restart TW.
- 11:22 Dario query number of task per user \[4\] and found user `byates` has 4k task in "NEW", and confirm by \[5\].
- 11:33 Wa restart TW to banning user.
- 12:11 Dario and Wa starting remove banned user all running dagman.
- 13:13 Dario contact user.

16 November

- 11.53 Stefano remove banned user.

## Reference

- [1] SQL: count number of tasks for specific user in range of time

    ``` sql
    SELECT
    Count(*)
    FROM TASKS
    WHERE TM_START_TIME > TO_TIMESTAMP('14-11-2022 16:00:00', 'dd-mm-yyyy hh24:mi:ss')
    AND TM_START_TIME < TO_TIMESTAMP('15-11-2022 13:00:00', 'dd-mm-yyyy hh24:mi:ss')
    AND TM_TASKNAME LIKE '%byates%'
    ```

- [2] remove running dagman specific user. Note that no need to kill jobuniverse 5 and 12, it will got kill automatically by condor

    ```bash
    condor_rm $(condor_q -con 'jobuniverse==7&&crab_userhn=="byates"&&jobstatus==2' -af clusterid)
    ```

- [3] Trick to find out when taskworker got restart is the directory name of processes logs (i.e. /data/container/TaskWorker/logs/processes/OldLogs-221115_100328)

- [4] Commamd viewing number of tasks per user

    ```bash
    curl -s --cert $X509_USER_PROXY --key $X509_USER_PROXY "https://cmsweb-testbed.cern.ch:8443/crabserver/prod/task?subresource=summary&minutes=144000" | grep -i "new"
    ````
- [5] how much specific user have running dag in schedd

    ```bash
    for schedd in $(condor_status -sched -con 'CMSGWMS_Type=="crabschedd"' -pool cmsgwms-collector-global.cern.ch -af machine ); do  ssh $schedd "echo schedd: $schedd && condor_q -con 'jobuniverse==7&&crab_userhn==\"byates\"&&jobstatus==2' -af jobstartdate | wc -l" ; done
    ```
## Discussion

-  Identify limit or policy on how many task/job crab can handle.
    - running tasks: `200*15`
        - NB: if we increase this limit, the same resources will serve jobs from more tasks, so tasks will take longer to complete, increasing the chance of problems.
    - dagman on hold: no limit
    - tasks in idle: no limit from condor, limit from stefano: we do not have any idle task. wen we have no capacity for running a task, we fail submission.
    - tasks in idle: we can imagine something different, such as queuing tasks in idle in the schedds (keep dagman_bootstrap in idle until we have lees than 200 running dagman_bootstraps), but there is no way to enforce fair share at this point. it will simply be a FIFO, that can be "abused" by people who submit many tasks, a user who submits once a month can risk waiting because of tasks from a user who submit every 1h. OR move tasks back in "new state". we decided we do **not** do any of these.
