# CRAB Disaster Recovery and Backup

## Prerequisite

Things need to be online before we can run CRAB system:

- Grid
    - At least one T2 sites
        - Dataset to be processed are on disk.
        - User-writable storage for storing jobs output.
    - glideinWMS HTCondor GLOBAL pool needs to be accessible
- CERN Infrastructure
    - Oracle DB
    - CVMFS `cms.cern.ch`
    - myproxy
    - VOMS
    - FTS
    - S3
    - Puppet managed VM
    - CERN Docker registry
- CMS Service
    - CMSWEB
    - CRIC
    - CMS Rucio
    - DBS


## Mandatory CRAB component

### Oracle DB

- CRAB uses Oracle DB to store the task state. With the current policy: we store data for only 30 days.
- If we lose all DB data, we kindly ask users to submit tasks again.
- In any case, open [SNOW ticket](https://cern.service-now.com/service-portal?id=sc_cat_item&name=request&se=oracle-database-service), or if there is an emergency, contact Kate, CMSR DBA, directly via email <katarzyna.maria.dziedziniewicz@cern.ch>.

### S3

- Store user sandbox files (user scripts/cmssw plugin/extra files).
- Files are removed automatically after 33 days (defined in S3 configuration).
- CRAB never needs it again after the task has been submitted to schedd.
- No need to backup. Kindly ask users to submit tasks again if we lose them.
- Open [SNOW ticket](https://cern.service-now.com/service-portal?id=sc_cat_item&name=incident&fe=S3ObjectStorage) for getting support.

### Schedd

- Manage tasks and submit jobs to glideinWMS GLOBAL pool.
- Deploy on VMs, managed by Puppet manifests here: <https://gitlab.cern.ch/ai/it-puppet-hostgroup-vocmsglidein>
- Data (Job, DAGMAN, logs, condor state) are stored in the CEPH volumes attached to VM.
    - If we loose VM but CEPH volumes are saved, we can recreate VM and connect same volumes, tasks will be automatically restarted. If we loose one or both volumes, we loose everything.
    - It will get deleted after 30 days, the same policy as DB.
- No permanent data, so no backup. We can recreate the machine and let users submit missing tasks again.
- Ask our VOCAdmin, Germano Massullo, or if relate to Puppet, open [SNOW ticket](https://cern.service-now.com/service-portal?id=sc_cat_item&name=Configuration-Management-Incident&se=Configuration-Management) to Puppet team for getting support.

### Configuration and Credentials

- CRAB REST
    - Both configuration and credentials are stored in <https://gitlab.cern.ch/crab3/crab-secrets>
- TaskWorker and Publisher
    - Taskworker/Publisher configuration is applied via Puppet manifest.
    - Host and service certificates are deployed by `certmgr` module in Puppet. We do not have a backup or do it ourselves.
    - Prerequisite steps are required before running services. See [Requirement to run TaskWorker](./crab-components/crab-taskworker/introduction.md#requirement-to-run-taskworker).

### Deployment Manifests

- CRAB REST:
    - K8s manifest is in [dmwm/CMSKubernetes](https://github.com/dmwm/CMSKubernetes/) repository. See [crabserver.yaml](https://github.com/dmwm/CMSKubernetes/blob/64985750e377d03b76abb177751b3414d84eee1d/kubernetes/cmsweb/services/crabserver.yaml).
    - To deploy on K8s cluster, see [CRAB REST Deploy](./crab-components/crab-rest/deploy.md#deploy) for more detail.
- TaskWorker/Publisher
    - Simple [runContainer.sh](https://github.com/dmwm/CRABServer/blob/f01f865625031b93709f311425b44b49923adae3/src/script/Container/runContainer.sh) script to start container in VM (run as local `crab3` user).

## Code/Git/Issues

- By the model of git, everyone has a local copy of the code. We can initialize the git repo on the server and push it again.
- Most valuable thing and hard to backup is *[The issues in Github](https://github.com/dmwm/CRABServer/issues)*. If we lose, we lose it forever (except we request Github to restore it, but not sure how).
    - We rely on the backup model from CMS O&C.
- If there is any trouble
    - For Github DMWM organization, contact [Alan Malta Rodrigues](mailto:alan.malta@cern.ch)
    - For Gitlab, Open [SNOW ticket](https://cern.service-now.com/service-portal?id=sc_cat_item&name=git-incident&fe=version-control) to CERN IT gitlab team.

### Our Repository

- <https://github.com/dmwm/CRABServer>
- <https://github.com/dmwm/CRABClient>
- <https://gitlab.cern.ch/crab3>

### Dependency repository managed by other team

- WMCore: <https://github.com/dmwm/WMCore/>
- Kubernetes manifest and docker build files: <https://github.com/dmwm/CMSKubernetes/>
- Building docker image:
    - <https://github.com/cms-sw/cmsdist/tree/comp_gcc630>
    - <https://github.com/dmwm/deployment/>
    - <https://github.com/cms-sw/pkgtools/tree/V00-34-XX>
- Building CRABClient (always use the latest default branch):
    - <https://github.com/cms-sw/cmsdist/blob/IB/CMSSW_13_0_X/master/>

## Docker Image

- As we state in [Prerequisite](#prerequisite), we need CERN Docker registry AND image files ready to pull and deploy on machines.
- In case we need to rebuild image, we need `registry.cern.ch/cmsweb/cmsweb` as base image, tools, and build configuration. Please consult [Build and Docker image](./crab-components/crab-rest/deploy.md#build-and-docker-image) on CRAB REST page on how images are built.
- Ask CMSWEB team for `registry.cern.ch/cmsweb/cmsweb` and other help in [Web Serv & Security MatterMost](https://mattermost.web.cern.ch/cms-o-and-c/channels/web-serv--security) channel.

## Other Component

### CI/CD

- All CI jobs are in https://cmssdt.cern.ch/dmwm-jenkins/, job name has `CRABServer_` prefix.
Some Jenkins jobs still need to move to CRABServer repo.
- It should be easy to run locally (lxplus) the script we have in Jenkins. We could improve documentation inside the scripts themselves to make clear which env variables they rely on.
- We rely on the backup model from DMWM team and/or Shahzad. Ask for help on [DMWM MatterMost](https://mattermost.web.cern.ch/cms-o-and-c/channels/dmwm) channel.

### Logs

- CRABServer
    - Store in S3 CMSWEB and sync back to the local disk on `vocms0750` node.
    - We rely on the backup model from CMSWEB team.
    - Ask for help from CMSWEB team in [Web Serv & Security MatterMost](https://mattermost.web.cern.ch/cms-o-and-c/channels/web-serv--security) channel.
- TaskWorker/Publisher
    - Only in deployed machines. No backup or transfer to anywhere.

### Monitoring

In any case, contact CMSMONIT team by sending e-mail to <cms-comp-monit-support@cern.ch>.

#### ElasticSearch

- Datasource for Grafana dashboard that store services metrics
- Data come from 2 sources
    - `GenerateMONIT.py` is sending some `condor_q` and crabserver metrics to the old ES instance (We do not know which one, we need to ask Ceyhun).
    - Log Pipeline: filebeat deploy inside docker (TaskWorker/Publisher) and sidecar (CRAB REST) send to logstash to process and save it to ES <https://monit-timberprivate.cern.ch>.
        - Manage by CMSMONIT ans CMSWEB team.
- We rely on the backup model from CERN IT.

#### Grafana

- For monitoring status of services using data store in ES.
- CERN IT MONIT team takes snapshots of Grafana DB. In case of dashboard is lost, we can open a ticket, and they can restore it.
