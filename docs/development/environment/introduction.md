# Development Environment

This is the guide how to setup development environment for developing CRAB services on your local machine.

Applicable for most Linux distribution.

## Tools

| Name              | Description                                                  |
|-------------------|--------------------------------------------------------------|
| SSH and Kerberos  | Shell to VM without password.                                |
| Apptainer         | Singularity forked. For running CRAB Client locally.         |
| Docker            | We use containers to build our services.                     |
| gnupg2 and sops   | For decrypt/encrypt key in `crab-secrets` repository         |
| Openstack client  | To fetch token and authenticate with k8s cluster             |
| Kubernetes client | To interact with k8s cluster.                                |
| stern             | Tail logs from multiple pod.                                 |
| VOMS client       | To issue proxy, required by CRABClient.                      |
| MyProxy client    | To set proxy delegation, required by CRABClient.             |
| gfal2             | Required by CRABClient, for `crab checkwrite` command.       |
| CVMFS             | To use all CMS provided by CMS O&C, including our CRABClient |
| sshuttle          | Poor man VPN.                                                |


### SSH and Kerberos

Please see [SSH and Kerberos](./ssh-and-kerberos.md).


### Apptainer

Get it from os repo.

Fedora:

```
sudo dnf install -y apptainer
```

### Docker

Use install instruction from [Official Docker page](https://docs.docker.com/engine/install/fedora/). You can select instruction for your own distribution in the menu on the left.

### gnupg2 and sops

Most of `gnupg2` already bundled with Linux desktop. Please make sure that you are using **gnupg version 2**.

??? quote "gpg --version"
    ```
    [thanayut@localhost cms-crab-docs]$ gpg --version
    gpg (GnuPG) 2.4.5
    libgcrypt 1.10.3-unknown
    Copyright (C) 2024 g10 Code GmbH
    License GNU GPL-3.0-or-later <https://gnu.org/licenses/gpl.html>
    This is free software: you are free to change and redistribute it.
    There is NO WARRANTY, to the extent permitted by law.

    Home: /home/thanayut/.gnupg
    Supported algorithms:
    Pubkey: RSA, ELG, DSA, ECDH, ECDSA, EDDSA
    Cipher: IDEA, 3DES, CAST5, BLOWFISH, AES, AES192, AES256, TWOFISH,
            CAMELLIA128, CAMELLIA192, CAMELLIA256
    Hash: SHA1, RIPEMD160, SHA256, SHA384, SHA512, SHA224
    Compression: Uncompressed, ZIP, ZLIB, BZIP2
    ```
To installl `sops`, you need to download binary and put it in your `$PATH`.

Goto <https://github.com/mozilla/sops/releases>, look at latest release tag and download binary. In example, we download `sops-v3.7.3.linux.amd64` for amd64 machine and copy to `~/.local/bin`

```bash
curl -LO https://github.com/mozilla/sops/releases/download/v3.7.3/sops-v3.7.3.linux.amd64
chmod +x sops-v3.7.3.linux.amd64
cp ~/.local/bin
```

### Openstack Client


!!! warning
    Only for getting token or manage your personal project!! For `CMS CRAB` project, please use in `aiadm.cern.ch` machine.

Openstack client can install via `pip`. Not recommend to install from os repos because it need specific version to work with CERN Openstack.

You can get latest version that work with CERN by run `pip freeze` in `lxplus`.

```bash
pip freeze | grep -E '(python-(openstack|magnum|barbican)|keystoneauth)'
```

Use folloing command to install to `~/.local` directory:

```bash
pip install --user \
    'python-openstackclient==6.0.1' \
    'python-magnumclient==4.0.0' \
    'python-barbicanclient==5.4.0' \
    'keystoneauth1[kerberos]==5.0.1'
```

Note that `keystoneauth1` require Kerberos development package (Fedora: `krb5-devel`) in order to install.

### Kubernetes client

#### kubectl

Suggesting to download binary from official website and copy it to your local `$PATH` directory (usually `~/.local/bin`).

You need client major version match with server version.

- Direct link (Linux AMD64): <https://dl.k8s.io/v1.27.12/kubernetes-client-linux-amd64.tar.gz>
- Download page: <https://github.com/kubernetes/kubernetes/blob/master/CHANGELOG/CHANGELOG-1.27.md>

#### kubectx, kubens (optional)

If you have multiple kubeconfig files, recommend to, install [kubectx](https://github.com/ahmetb/kubectx#manual-installation-macos-and-linux) to easy to switch between cluster config.

!!! note
    Recommend to use bash version instead go binary which more stable and have full features. Download from source or clone repository and symlink `kubectx` and `kubens` to PATH directory.

Also, it work really nicely with [fzf](https://github.com/junegunn/fzf). You can interactively select cluster/namespace with it.

#### kubeconfig

`kubectl` can read config path from `KUBECONFIG` environment variable, separate by colon `:` for multiple kubeconfig files.

Make sure each config has different name for `clusters`, `users`, and `contexts`. Otherwise, it will get replace by another file (similar principle when you do `dict.update(dict())` in python).


### Stern

Recommend by CMSWEB. Download from <https://github.com/stern/stern/releases/tag/v1.24.0> and copy to your local path.

Or [kubetail](https://github.com/johanhaleby/kubetail) as alternative (Wa: I use this).

### CVMFS

Please follow instruction from <https://cvmfs.readthedocs.io/en/stable/cpt-quickstart.html> page.

The recommend way for non-el to mount cvmfs is to use systemd-automount, avoid using autofs completely.


### VOMS Client

!!! note
    Use [cmssw containers](http://cms-sw.github.io/singularity.html) instead, which already has `gfal-*` binary. <br />
    But still, it is nice to issue temporary proxy in your local machine and mount it into your own container.

Fortunately for Fedora (F37), VOMS Client is already in os repo:

```
dnf install -y voms-clients-java
```

#### Grid CA Certificate

Wa: `voms-clients` will not work if CA in `/etc/grid-certificate` is not update. I do not know how `lxplus` install Grid CA Certificate in `/etc/grid-certificate` and get it update every now and then.

What I am doing is run rsync copy `/etc/grid-certificate` from `lxplus` to local machine every day.


### MyProxy

!!! note
    Use [cmssw containers](http://cms-sw.github.io/singularity.html) instead, which already has `myproxy-*` binary. <br />

Same as VOMS Client.

Fedora:

```
dnf install -y myproxy
```

### gfal2

!!! note
    Use [cmssw containers](http://cms-sw.github.io/singularity.html) instead, which already has `gfal-*` binary. <br />
    But it is still nice to have. E.g., use `gfal-ls` to check if path is corrent when you develop Rucio ASO.

Recommend way is to install from [conda forge](https://anaconda.org/conda-forge/gfal2) inside [miniconda](https://docs.anaconda.com/free/miniconda/miniconda-install/). (TBD)




### sshuttle

(TBD)

For connecting to Oracle database, please consult [GUI DBeaver](../../crab-components/crab-rest/database-admin.md#option-1-all-dns-through-the-tunnel) section.
