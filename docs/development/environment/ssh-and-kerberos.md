# SSH and Kerberos

This is guide how to setup the ssh client and Kerberos on your laptop without providing CERN Computing password everytime you login.

## Install

Most of Linux desktop come with openssh-client by default. You only need to install Kerberos client.
We recommend installing the kerberos client on your laptop with

Debian:

```bash
apt install krb5-user # debian
```

Fedora:

```bash
dnf install krb5-workstation
```

Wa: For non-EL, I recommend install `sssd` as ticket cache backend to persist ticket across reboot. No extra configuration need.

## Setup

Please read comprehensive docs ["Access using Kerberos"](https://linux.web.cern.ch/docs/kerberos-access/#client-configuration-kerberos) by CERN IT.

In summary,

- Setup Kerberos config (optional, but recommend if you only access `CERN.CH` realm.)
- Setup ssh config (see configuration example below)
    - `GSSAPIAuthentication yes` and `GSSAPIDelegateCredentials yes` is mandatory.
    - For 2FA machine, please ensure `ControlMaster auto`, `ControlPersist 120m` and `ControlPath ~/.ssh/link-%r@%h.%p` in the configuration to reuse connection so server will not asking for 2FA again.

## Usage

Get a kerberos ticket on your laptop with:

```bash
kinit cernusername@CERN.CH
```

Check the list of tickets with:

```bash
klist
```

You should see output like this:

??? quote "Output"
    ```
    [thanayut@localhost cms-crab-docs]$ klist
    Ticket cache: KCM:1000
    Default principal: tseethon@CERN.CH

    Valid starting       Expires              Service principal
    04/18/2024 21:08:07  04/19/2024 22:08:06  krbtgt/CERN.CH@CERN.CH
            renew until 04/23/2024 21:08:06
    ```
Ticket will expire in 25 hours, but you can renew it with `kinit -R` without providing password again for 8 days.

## SSH Client config example

Add the following lined to your ssh client, at ~/.ssh/config, keeping in mind

- `GSSAPI*` is what tells the ssh client to use the kerberos ticket for the ssh connection
- dario is lazy and connects to gitlab.cern.ch with a specific ssh key `id_cern`. you do not have to do the same


??? quote "ssh_config"
    ```conf
    Host *
        ServerAliveInterval 59
    #    PasswordAuthentication no
    #    HashKnownHosts yes
    #    IdentitiesOnly yes
        Protocol 2
        # %l client host
        # %h remote host
        # %r remote user
        # %p remote port
        ControlPath ~/.controlmasters_ssh-%l-%r_%h_%p
        ControlMaster auto
        ControlPersist 120m
        # the following option has been removed because on 202302
        # it correlated with ssh agent not being forwarded to optiplex VMs
        #AddKeysToAgent  yes

    # Setup the connection re-use
    # ControlMaster auto
    # Path to Control Socket
    #    %l: local host name
    #        %h: target host name
    #   %p: port
    #      %r: remote login
    # ControlPath ~/.ssh/master_%l_%h_%p_%r

    ## generic cern

    Host gitlab.cern.ch
        HostName gitlab.cern.ch
        Port 7999
        User git
        IdentitiesOnly yes
        IdentityFile ~/.ssh/id_cern

    Host lxplus* lxtunnel* aiadm*
        User dmapelli
        Compression yes
        Protocol 2
        # ForwardAgent yes
        # ForwardX11 yes
        # ForwardX11Trusted yes
        IdentitiesOnly yes
        GSSAPITrustDns yes
        GSSAPIAuthentication yes
        GSSAPIDelegateCredentials yes

    Host lxplus lxplus9
        HostName lxplus9.cern.ch
        DynamicForward 3089

    Host lxplus8
        HostName lxplus8.cern.ch
        DynamicForward 3081

    Host lxplus7
        HostName lxplus7.cern.ch
        DynamicForward 3080

    Host lxtunnel
        HostName lxtunnel.cern.ch

    Host lxtunnel-critical
        # zone: gva-critical, ups power backup
        HostName lxtunnel-critical.cern.ch

    host aiadm
        hostname aiadm.cern.ch

    ## crab hosts

    Host vocms*
        user dmapelli
        ProxyJump lxtunnel

    Host crab-*
        user dmapelli
        ProxyJump lxtunnel
    ```



You should be able to connect to the following hosts directly from your laptop

```bash
# once a day
kinit username@CERN.CH

# without inserting any password
ssh lxplus
ssh vocms059.cern.ch
ssh crab-preprod-tw01.cern.ch

# requires second factor, from totp or yubikey
ssh aiadm
```

## Troubleshoot

If the krb config from your distro is not working with cern kerberos server, then
have a look at the following links:

- [devices.docs.cern.ch](https://devices.docs.cern.ch/devices/mac/AboutKerberosAndSsh/)
- [linux.web.cern.ch](https://linux.web.cern.ch/docs/kerberos-access/)
