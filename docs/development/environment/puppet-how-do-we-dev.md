# Develop and Deploy puppet code - How To

## For taskworker machines

- Prepare machine, either pull `crab-dev-tw0{1-3}` to `crabdev_taskworker` enviroment or use `crab-dev-tw04`. Usually, `crab-dev-tw04` is use for `ai-rebuild` (destroy and recreate VM) to see if it work on new machine.
- Reset `crabdev_taskworker` branch to `your_feature_branch` branch.
- Update code in your feature branch and merge to `crabdve_taskworker` to apply new code to test machine.
- Open MR for `your_feature_branch` to `qa`.
- After MR get merge, **let it sit there for a week** before move to production.
- Merge `qa` to `master`.
- Commit E-log.

## For schedd machines

Same procedure with taskworker machine except

- Use `crab-preprod-sch03`. `vocms068` and `vocms069` are use by SI team, and some HC jobs.
- When `your_feature_branch` merge into `qa`
    - Run test against `vocms059`
    - Pull one production sched node to `qa` enviroment to test against production workload.

## Merge qa into master

The idea of `qa` branch is to test what will be the future `master`. Ideally, `qa`
branch should always be based on top the latest commit of master branch:

```plaintext
+----+     +----+     +-------------+     +----+     +---------+
| c1 | --> | c2 | --> | c3 = master | --> | c4 | --> | c5 = qa |
+----+     +----+     +-------------+     +----+     +---------+
```

However, sometimes a hot fix in `master` is required and this makes it diverge
from `qa`

```plaintext
+----+     +----+     +-------------+     +----+     +---------+
| c1 | --> | c2 | --> |     c3      | --> | c4 | --> | c5 = qa |
+----+     +----+     +-------------+     +----+     +---------+
                        |
                        |
                        v
                      +-------------+
                      | c6 = master |
                      +-------------+
```

### How to resolve the divergence

#### Option 1: git merge, twice

One option would be to simply merge the master branch into qa, then upgrade master
to the same merge commit

```bash
git checkout qa
git pull upstream qa
git fetch upstream    # download most recent upstream/master
```

then run `git merge`, with implicit `--ff-only` which is the git default for merge.
In this case it will create a new merge commit.

```bash
git merge master
```

In case of conflicts, they will need to be resolved manually, see the [official](https://git-scm.com/docs/git-merge#_how_to_resolve_conflicts) docs for instructions.

```bash
git push upstream qa
```

```plaintext
+----+     +----+     +-------------+     +----+     +----+     +---------+
| c1 | --> | c2 | --> |     c3      | --> | c4 | --> | c5 | --> | c7 = qa |
+----+     +----+     +-------------+     +----+     +----+     +---------+
                        |                                         ^
                        |                                         |
                        v                                         |
                      +-------------+                             |
                      | c6 = master | ----------------------------+
                      +-------------+
```


Then open a PR to merge qa into master, gitlab runs the equivalent of the following
commands behind the scenes:

```bash
git checkout master
git merge qa --ff-only
```

at the end you should have:

```plaintext
+----+     +----+     +----+     +----+     +----+     +-----------------+
| c1 | --> | c2 | --> | c3 | --> | c4 | --> | c5 | --> | c7 = master, qa |
+----+     +----+     +----+     +----+     +----+     +-----------------+
                        |                                ^
                        |                                |
                        v                                |
                      +----+                             |
                      | c6 | ----------------------------+
                      +----+
```

#### Option 2: git rebase qa on top of master

A better option is to use git rebase:

```bash
git checkout qa
git pull upstream qa
git fetch upstream    # download most recent upstream/master

git rebase master

git push upstream qa --force
```

```plaintext
+----+     +----+     +----+     +-------------+     +-----+     +----------+
| c1 | --> | c2 | --> | c3 | --> | c6 = master | --> | c4' | --> | c5' = qa |
+----+     +----+     +----+     +-------------+     +-----+     +----------+
```

Where `c4'` and `c5'` should be the same as `c4` and `c5` respectively, but with
a new commit hash because of the rebase. This is why you need to use the `--force`
option for the push.

In case of conflicts, git rebase will stop. Resolve manually the conflicts,
then run

```bash
git add path/to/file
git rebase --continue
```

Then open a PR to merge qa into master, gitlab runs the equivalent of the following
commands behind the scenes:

```bash
git checkout master
git merge qa --ff-only
```

```plaintext
+----+     +----+     +----+     +----+     +-----+     +------------------+
| c1 | --> | c2 | --> | c3 | --> | c6 | --> | c4' | --> | c5' = master, qa |
+----+     +----+     +----+     +----+     +-----+     +------------------+
```

!!! warning Warning

    when using rebase to change the history of the qa branch, it is possible that
    the local clones of the qa branch are left behind w.r.t the remote version
    of the branch.

    In order to resolve these conflicts, when such a conflict arises, the developer
    should make sure that he has no changes to the qa branch that were not pushed, then run

    ```bash
    git fetch upstream
    git checkout qa
    git reset --hard upstream/qa
    ```
