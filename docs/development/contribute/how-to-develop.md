## Brief guide with tips on how to contribute to CMS CRAB code

### Run python code in a fedora envirionment

Fedora is the upstream for RHEL and ALMA releases. Packages build by CERN IT via 
koji can easily be downloaded and installed into a fedora environment. 

An example dockerfile for a fedora container that runs CRAB monitoring script would be 

```dockerfile
FROM fedora:39

RUN dnf -y install strace \
       vim nano \
       gcc python3 python3.8 python3-devel libcurl-devel openssl-devel \
       git \
       &&\
    dnf clean all && rm -rf /var/cache/yum

# add new user and switch to user
ENV WDIR=/data
ENV USER=crab3
RUN useradd ${USER} && install -o ${USER} -d ${WDIR}
USER ${USER}

RUN python3.8 -m ensurepip
RUN python3.8 -m pip install rucio-clients pandas pycurl jwt future

RUN mkdir -p /data/srv/tmp 
WORKDIR ${WDIR}

RUN git clone https://github.com/dmwm/WMCore /data/srv/monit/WMCore
RUN mkdir -p /data/srv/monit/logs
COPY CheckTapeRecall.py /data/srv/monit
COPY ServerUtilities.py /data/srv/monit
COPY RESTInteractions.py /data/srv/monit

ENV PYTHONPATH=$PYTHONPATH:/data/srv/monit/WMCore/src/python
ENV RUCIO_HOME=/cvmfs/cms.cern.ch/rucio/current/
ENV RUCIO_ACCOUNT="crab_server"
# no need to source a rucio environment from cvmfs. rucio client is already installed
# via pip, do not pollute the python env with things you do not need!
# ENV PYTHONPATH=$PYTHONPATH:/cvmfs/cms.cern.ch/rucio/x86_64/el9/py3/current/lib/python3.9/site-packages/
# ENV PYTHONPATH=$PYTHONPATH:/cvmfs/cms.cern.ch/rucio/x86_64/el8/py3/current/lib/python3.6/site-packages/

CMD python3.8 /data/srv/monit/CheckTapeRecall.py && \
  cp -v RecallRules.html /data/eos/RecallRules-fedora.html
```
