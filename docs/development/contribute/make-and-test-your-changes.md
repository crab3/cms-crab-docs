## This guide teaches you how to confidently make and test changes in the code.

### Make A Change

Figure out which Github project or repository you want to change. 
Examples can be **dmwm/CRABServer**, **dmwm/CRABClient**, **ai/it-puppet-hostgroup-vocmsglidein**, etc.

Go to that project and create your very own fork. You can name it after your Github or CERN id (Say `aspiringmind-code` or `vchakrav`)
Everytime you wish to make a change, ensure that you are on your personal fork.

To keep handling of separate issues lucid, create a new branch for every issue that you are trying to solve. Let's say you are
tackling Issue #1234, you can name your branch accordingly, with extra information that helps you track the same such as date, a 
brief description etc. (Say *vchakrav_Issue1234_15/07/24_task_scheduling*)

Now you can freely make your changes in your own fork and issue-specific branch.

### Test A Change Locally on a crab-dev-tw VM

So you have made your first changes in the CRAB code and even if it is just a variable name, you want to make sure your change
doesn't break everything when pushed to production?

We test changes by running a crab task. Depending on the issue, your change might concern itself with one or more components of the
CRAB infrastructure, say the TW, REST, Publisher, Puppet etc. If you're not sure what the change affects, ask a colleague or you can 
consider adding logger information alongside your change and check the log when the task runs. Remember to remove it later.

Log in to your crab-dev-tw and checkout to the issue specific branch. Ensure that all the recent changes have 
been committed and git status has nothing to add, aka you're synced with the latest changes.

Now you can go back and submit a new task on the specific TW. See [here.](https://cmscrab.docs.cern.ch/operators/day1/02-submit-first-crab-task.html)
If you're able to submit successfully, and crab status returns no errors, it is most likely that everything is okay.

Sometimes, to see the effect of your change exclusively you might need to employ extra methods to make sure your changes are okay.
For example, if you have made changes in the database, you might have to run SQL queries on sqlplus or DBeaver to confirm those changes.
If you have made a change in the Puppet configuration, you might need to check logs for the machine on Foreman/Judy.

Once you're satisfied with the tests, create a merge request (MR) from your fork and branch to the production branch of the main repository.
It's a good practice to share snapshots of the tests you ran on the MR so that reviewers can approve the changes.

Once the changes are merged, you can delete the issue specific branch from your fork.
