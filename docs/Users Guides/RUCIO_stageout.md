# CRAB output management via RUCIO

## Using Rucio client on lxplus 

```bash
voms-proxy-init --voms cms

source /cvmfs/cms.cern.ch/rucio/setup-py3.sh

export RUCIO_ACCOUNT=$USER
```

!!! tip
    Be aware that `$USER` stays for your CERN account username


!!! note

    Rucio client commands do not work after "cmsenv" i.e. in the same shell which you use for crab commands
    Please take a look here: https://twiki.cern.ch/twiki/bin/viewauth/CMS/Rucio

## Requirements

- Your RUCIO user account should get some quota at the RSE you specify as destination. You can check it with

```bash
rucio list-account-limits $USER
```

!!! tip

    If you don't have any quota, please ask you T2 local site admin to provide you with one

## Submitting a CRAB job with RUCIO stageout

!!! note

    This feature is in alpha stage. There are some feature that are not available yet, that instead are supported in the legacy submission mode:
    - NO log transfer supported --> please be sure to disable it via `...`
    - publications will stay acquired, since we need to deploy a dedicated publisher for these tasks
    - NO resubmission will solve stageout problem. In the current implementation, transfers STUCK can only be fixed by RUCIO OPS

The following lines show the __mandatory__ part of the CRAB config file need to try the RUCIO stageout workflow:  

```python
from WMCore.Configuration import Configuration
from CRABClient.UserUtilities import config, getUsernameFromCRIC
  
config = Configuration()
  
config.section_("General")
config.General.transferLogs=False
config.section_("Data")
config.Data.outLFNDirBase = '/store/user/rucio/%s/test-workflow' % (getUsernameFromCRIC())
config.Data.publication = True
```


## Monitoring the status of the transfers

Retrive from CRAB status the output dataset name:

```bash
crab status -d ./MY_TASK_DIR | grep "Output dataset"
```

Use the dataset name to retrieve RUCIO rule status for that container:

```bash
rucio list-rules user.$YOUR_USERNAME:$TASKNAME
```

!!! tip

    If you see any stuck rule, the only way to fix it at the moment is to call for help from DM Ops.
    We are in the process of implementing automatic retries from different job retry, it's not there yet though
  
