# CRAB Client

CRAB Client is now deployed by CMSSW automatic release build. A new stable release is made any time the CMSSW master release is updated (typically every 3-4 weeks) and includes whatever version of the client is indicated in the `spec` files for `prod`, `pre` and `dev` versions, which are independently controlled (by us).

The cmsbuild robot build updates those three crab versions every night in a "parallel universe", where things can be tested, but be aware that those are the same `spec` files as the actual release build, so be very careful before changing `crab-prod.spec`.

More on the proper procedure to care for the `spec` files is [below](#managing-versions-and-dependencies-via-cmsdist)

## Using CRABClient

### Using CRABClient last release in CMSSW distribution

1. get a proxy

```bash
voms-proxy-init -rfc -voms cms -valid 192:00
export X509_USER_PROXY=`voms-proxy-info -path`
```

2. setup CMSSW

```
cd CMSSW..
cmsenv
```

note: if you do not have a CMSSW release at hand, just create any recent one, and it will do:

``` command
cmsrel CMSSW_10_4_0
cd CMSSW_10_4_0
cmsenv
```

3. point to your favourite version executing ONE of these
```
crab         # which is an alias for  crab-prod
crab-prod
crab-pre
crab-dev
```
Usually users should stick to `crab` i.e. `crab-prod` unless they want to test, or need to use, some new feature or bug fix only available in `pre` or `dev`.

### Using CRABClient from nightly CMSSW Integration Build

There are two ways:

1. to simply use new code (but not all subtle dependencies like command autocompletion) do this at any time

    ```bash
    export PATH=/cvmfs/cms-ib.cern.ch/latest/common:$PATH
    ```

2. to fully use the IB environment (including new CMSSW builds etc.) do this before `cmsenv` (or `cmsrel`):
   ```bash
   source /cvmfs/cms-ib.cern.ch/latest/cmsset_default.sh
   ```


### Using CRABClient API

In order to use the API, besides setting up `cmsenv` as above, you need to source this script.

```bash
source /cvmfs/cms.cern.ch/common/crab-setup.sh
```

and make sure that your python script has this before any other `import` statement:

```python
import CRABClient
```

That script can be safely sourced at all times, even if you do not plan to use the APO.

By default that will point you to the `prod` instance of the CRABClient. Otherwise the instance can be indicated as argument:

```bash
source /cvmfs/cms.cern.ch/common/crab-setup.sh prod
source /cvmfs/cms.cern.ch/common/crab-setup.sh pre
source /cvmfs/cms.cern.ch/common/crab-setup.sh dev
```

Or simply

```bash
source /cvmfs/cms.cern.ch/common/crab-setup.sh -h
```

If you are using the nightly IB, you should use instead

```bash
source /cvmfs/cms-ib.cern.ch/latest/common/crab-setup.sh
```

### Examples

The following, usually working, scripts give concrete examples of how to setup things, I usuall execute one of this via a short alias every time I login for CRAB action. My alias also grabs a voms proxy.

```
/afs/cern.ch/user/b/belforte/public/CRAB/SETUP
/afs/cern.ch/user/b/belforte/public/CRAB/SETUP-IB
/afs/cern.ch/user/b/belforte/public/CRAB/SETUP-LOCAL
```

## Develop for CRABClient

### Using CRABClient directly from your GitHub repository

All binary dependency for CRABClient are now in CMSSW, therefore you only need to point to the python code and its python dependencies:

#### To do once:

1. go to your favorite development place

    ```bash
    cd some_dir
    GitDir=`pwd`
    ```

2. clone repositories and make sure to checkout proper versions. Master HEAD may not always work, but if you want to develop code yoy may have to start there. In addition to CRABClient you will need one file from CRABServer. If you want to eventually make git Pull Requests, you will fork first and clone from your fork the repository you want to work on. If that's the case you surely know how to modify following commands. If you simplt want to look around, debug things, or just use latest code, copy/pasting the following should do. You can check `crab-*.spec` files in [cmsdist](https://github.com/cms-sw/cmsdist) to see which CRABClient and CRABServer versions are used with current CRABClient.

```bash
git clone https://github.com/dmwm/CRABClient
git clone https://github.com/dmwm/CRABServer
cp CRABServer/src/python/ServerUtilities.py CRABClient/src/python/
rm -rf CRABServer
mkdir -p WMCore/src/python/WMCore
cp CRABClient/src/python/CRABClient/WMCoreConfiguration.py WMCore/src/python/WMCore/Configuration.py
touch WMCore/src/python/WMCore/__init__.py
```


#### To do at each login

1. get a proxy

```bash
voms-proxy-init -rfc -voms cms -valid 192:00
export X509_USER_PROXY=`voms-proxy-info -path`
```

2. setup CMSSW

```bash
cd CMSSW..
cmsenv
```

3. execute these lines

```bash
GitDir=<the_directory_where_you_cloned>
MY_CRAB=${GitDir}/CRABClient
MY_WMCORE=${GitDir}/WMCore

export PYTHONPATH=${MY_WMCORE}/src/python:$PYTHONPATH
export PYTHONPATH=${MY_CRAB}/src/python:$PYTHONPATH

export PATH=${MY_CRAB}/bin:$PATH
source ${MY_CRAB}/etc/crab-bash-completion.sh
```

you can see a complete, usually working, example in `/afs/cern.ch/user/b/belforte/public/CRAB/SETUP-LOCAL`

## Managing Versions and dependencies via CMSDIST

### Dependencies

CRAB Client needs WMCore, needs a couple files from CRABServer (Client and Server need to stay aligned on some defintions and settings. The way to manage such dependencies is described in <https://twiki.cern.ch/twiki/bin/view/CMSPublic/NotesAboutReleaseManagement#Cross_service_dependencies>

### Where and What

CRAB Client spec files are now in <https://github.com/cms-sw/cmsdist>

- You must use the master branch, **not** the `comp_*` branch !! If in doubt check with CMSSW release experts. Note that in `cmsdist` repository there is no branch called simply `master`, and the master branch changes with time. It is usually the default one when you visit <https://github.com/cms-sw/cmsdist> and has a name like `IB/CMSSW_11_2_X/master`
    - You can quickly list such branches on your clone of the repo with `git branch -rv | grep "^[ ]*upstream/IB/CMSSW_[0-9_X]*/master"`
- There are only 3 files which you can/should modify.
- You must change in your private fork and make a Pull Request
- At time of writing this twiki those are:
    - <https://github.com/cms-sw/cmsdist/blob/IB/CMSSW_11_2_X/master/crab-dev.spec>
    - <https://github.com/cms-sw/cmsdist/blob/IB/CMSSW_11_2_X/master/crab-pre.spec>
    - <https://github.com/cms-sw/cmsdist/blob/IB/CMSSW_11_2_X/master/crab-prod.spec>

Those files all have a content like this (showing `crab-dev.spec` here):

```
############## IMPORTANT #################
#For new crabclient_version, set the version_suffix to 00
#For any other change, increment version_suffix
##########################################
%define version_suffix 01
%define crabclient_version v3.200531
### RPM cms crab-dev %crabclient_version.%{version_suffix}
%define wmcore_version     1.3.3
%define crabserver_version v3.200531
```

Numbers there refer to tags in github. For CRAB the tag is always v3.YYMMDD. Usually you wll simply want to change the CRAB Client release number. Example:

```
from:
%define crabclient_version v3.200531
to:
%define crabclient_version v3.200620
```

If needed, we can also modify the following two lines for the WMCore and CRABServer versions respectively:

```
%define wmcore_version 1.3,3
%define crabserver_version v3.200531
```


- **IMPORTANT NOTE** (see also the comments at the beginning of the spec file) if you want to change **only** the WMCore or CRABServer version or anything in the build, but not CRABClient tag, then you MUST increment the `version_suffix` <br />Note that same applies if you change the `crab-build` file which is imported in the spec at the last line.

### When and How

You need to wait for the CMSSW/cmsdist admins (Shahzad) to trigger testing of the Pull Request via github comments which act as command for the build robot. Since CRAB changes can't bread CMSSW they usually approve everything unless the build fails, which you need to fix. Communications with CMSSW admins is via comments in the Pull Request. Once build is successful, admins will merge the Pull Request.

Changes to the `crab-*.spec` files are propagated nightly to the Integration Build and can the tested using the procedure indicated [at the top](#using-crabclient-from-nightly-cmssw-integration-build) Instead deployment to CVMFS common directory, for [default use](#using-crabclient-last-release-in-cmssw-distribution) is only done when a new release is build for the CMSSW version indicated in the name of the master branch, i.e. the most recent release, which happens about every 3 or 4 weeks.

You can consult the [calendar for scheduled CMSSW releases](https://twiki.cern.ch/twiki/bin/view/CMS/ReleaseSchedule) to know the planned dates.

Corollary:

- the CRAB Client release for users can only be updated about once/month
- the CRAB Client release for users must be well tested (in `dev` and `prod`) because there is no way to roll back (well.. if a major disaster strick Shahzad and Bockjoo can hack things by hand, but let's avoid it).

#### The release procedure

1. develop and test locally as indicated [above](#using-crabclient-last-release-in-cmssw-distribution)
2. when confident, make a new release in github and update the `crab-dev.spec`. Example: [cms-sw/cmsdist#7994](https://github.com/cms-sw/cmsdist/pull/7994).
3. the day after you can start checking that build went well and that everything works in IB
4. it there's no urgency, wait for this to be deployed on "lxplus" for more extensive checking, e.g. involving users
5. finally update `crab-prod.spec` and set `crab-pre.spec` to same old version as crab-prod was, so if there is a disaster users can roll back by replacing crab with crab-pre command. `pre` in CRABClient context is `previous`. Example: [cms-sw/cmsdist#8011](https://github.com/cms-sw/cmsdist/pull/8011).

## Do it yourself CRABClient deployment

It is now possible for us to deploy a CRABClient update on CVMFS at any time ! While it will still happen that when the top CMSSW series is updated, CRABClient is also deployed, since it is now a dependency in CMSSW.

However, if for instance we want to update CRABClient only in IB and don't want it to be deployed as crab-dev / crab-prod, then we can update spec files in IB/CMSSW\_12\_2\_X/devel branch. This is only used by IBs so it will not be included in the release! Note that at the time of writing release is CMSSW\_12\_2\_0, but it changes, so always check what is current release and use relevant devel branch.

CMSSW release schedules and dates can be found here: <https://twiki.cern.ch/twiki/bin/viewauth/CMS/CMSSW_12_2_0> (again, change the release version to the relevant one)

### how to do it

```
note: slithgly edited by stefano
On 04/05/2021 18:38, Malik Shahzad Muzaffar wrote:

    https://cmssdt.cern.ch/jenkins/job/cms-build-package/  jenkins job should allow you to build/upload and deploy crab client.
    Daina and Stefano are amdinistrator for this job and can run it. All you need is to

 - select "crab" as "CMS_PACKAGE"
 - select "UPLOAD" if you want to upload the newly built package otherwise job will only build
 - select "DEPLOY_ON_CVMFS" if you want to deploy it on cvmfs ("UPLOAD" should be selected too)

 Note that for crab client you need to build "crab" which should build its deps (crab-pre, crab-dev, crab-prod if needed).
 For example if we have merged changes for crab-dev.spec only then the job will build crab-dev and crab and upload/deploy both of these.

 I have tested it to build/deploy SCRAMV1 and it works as expected.

 Cheers, --Shahzad
```

Important additions as of June 2021

Note: from the time Jenkins job is completed, it takes about 10m for changes to be visible on lxplus, while the change percolates across the various CVMFS caching strata

### CAVEAT

Please remember, this jobs builds crab RPM and upload it to cms repository. CMSSW release build job also builds and uploads the same crab RPM. So if you run this job while we are building a CMSSW release then this can cause issues and trigger a re-build of CMSSW release. So always be carefull when you run this job. One way to check if there is any cmssw release build going on is to look for <https://cmssdt.cern.ch/jenkins/job/build-release/> job. As this is deploying stuff on production CVMFS repository, so please avoid running it too often. Try to do the testing in IBs env first.

### If we screw up

```
Date: Mon, 31 May 2021 14:54:59 +0200
From: Malik Shahzad Muzaffar <shahzad.malik.muzaffar@cern.ch>
To: Stefano Belforte <Stefano.Belforte@cern.ch>, Daina Dirmaite <daina.dirmaite@cern.ch>

One thing which you can already do is to set env variable CRABCLIENT_TYPE=pre and then crab command will run crab-pre version.
env variables are bit different than alias. alias are not by default available in sub-shell/scripts but env variables are

ab also roll back users by making `crab` point to `crab-pre` instead of `crab-prod` via:

https://cmssdt.cern.ch/jenkins/view/CVMFS-CMS/job/cvmfs-cms-crab-symlink/ job should allow you to change the crab symlink.

Note that this only changes the /cvmfs/cms.cern.ch/common/crab symlink. For crab python api, one still needs to source /cvmfs/cms.cern.ch/common/crab-setup.sh prod|pre|dev
```

### Tricky technical points

All CRAB Clients instances use a common build file <https://github.com/cms-sw/cmsdist/blob/IB/CMSSW_11_1_X/master/crab-build.file>

If it is changed via a Pull Request, it will affect all instances (`prod, pre, dev`) at same time. The way to test changes is to make those changes in the `crab-dev.spec` only.

see <https://github.com/cms-sw/cmsdist/pull/5561#issuecomment-587293336> and <https://github.com/cms-sw/cmsdist/pull/5561#issuecomment-587483867>

## Refs.

Some discussions about client deployment:

- <https://github.com/dmwm/CRABClient/issues/4844>
- <https://hypernews.cern.ch/HyperNews/CMS/get/crabDevelopment/2459.html>
- <https://hypernews.cern.ch/HyperNews/CMS/get/webInterfaces/1379/1/2/1.html>

## Note about CRABClient and CMSSW version

!!! note
    This section is currently in draft.

We have policy/agreement that we should support any version of CMSSW that still support by CMSSW team. That mean, we need to make sure CRABClient be able to read older CMSSW's config, run on older python version, older environment scripts and older binary like cURL. (Wa: As far as I know, CMSSW 7, still support until now, use python 2.6).

<https://github.com/dmwm/CRABClient/issues/4844> discussed about how to pack CRABClient to CMSSW package. (Wa: this one I still do not understand why they need to distributed along with CMSSW, even we only support for latest version of CRABClient. And when you run cmsenv from older CMSSW, you always use latest version we have deploy on production).
