# CRAB Schedds - manage user tokens

## First experiments

Dario started exploring how to use condor to manage user tokens in crab schedulers.

### references

A list of links that dario think are necessary to understand what's happening

- https://twiki.cern.ch/twiki/bin/viewauth/CMS/IAMschedd - a guide form Farrukh, that
  explains how to submit a job to condor using CMS IAM and a hashicorp vault
  server to manage tokens
- https://github.com/fermitools/htgettoken cli tool to retrive a user token from
  CMS IAM and store it in hashicorp vault
- track WMCore progress
  - https://github.com/dmwm/WMCore/wiki/Tokens-in-WMCore
  - https://cms-wmcore.docs.cern.ch/wmcore/Tokens-in-WMCore
  - https://cms-wmcore.docs.cern.ch/wmcore/Tokens-in-WMAgent

Changes made by Dario to prepare the schedd to support tokens in user jobs:

- puppet: [crabdev_dmapelli](https://gitlab.cern.ch/ai/it-puppet-hostgroup-vocmsglidein/-/compare/qa...crabdev_dmapelli?from_project_id=4758&straight=false),
- `vocms069` is in puppet environment `crabdev_dmapelli`.

### submit a job

```bash
ssh vocms069
chmod 644
# make sure that you have a valid krb ticket
klist
# change the permissions so that crabtw unix user can read the dmapelli@CERN.CH
# krb ticket
chmod 644 $(klist | grep "cache" | cut -d ":" -f 3)

#
export XDG_RUNTIME_DIR=/run/user/501/
cd dmapelli/submit-jdl
condor_submit hello.sub
```

See [this](how-tos.md#submit-a-job-directly-to-condor) how-to for an example
of the hello.sub file.

If you want condor to manage the user tokens for you, you need to add the following
lines to the job description, which means that `hello.sub` should contain

```plaintext
use_oauth_services = cms
cms_oauth_permissions_readonly = storage.read:/
cms_oauth_resource_readonly = "https://wlcg.cern.ch/jwt/v1/any"
```

### check that job was successfull and that the token is refreshed

make sure that the payload prints the output of the token

??? note "payload.sh example"

    ```bash
    #!/bin/bash

    set -x

    date
    whoami
    echo arguments: $@
    env
    ls ${CONDOR_CREDS}
    ls ${_CONDOR_CREDS}
    date
    cat ${_CONDOR_CREDS}/cms_readonly.use

    echo
    sleep 3700
    date
    cat ${_CONDOR_CREDS}/cms_readonly.use

    echo
    sleep 3700
    date
    cat ${_CONDOR_CREDS}/cms_readonly.use

    set +x
    ```

make sure that the jobs is complete

```plaitnext
[crabtw@vocms069 submit-jdl]$ condor_wait log/hello.193959.log
All jobs done.
```

??? quote "example output"

    ```plaintext
    [crabtw@vocms069 submit-jdl]$ cat output/hello.193959.0.out
    WARN $TMPDIR(/tmp/glide_wJWR0q/execute/dir_1748666) is not a writable directory setting $TMPDIR = $PWD
    Fri Jul 19 09:01:32 UTC 2024
    [...]
    _CONDOR_CREDS=/srv/.condor_creds
    [...]
    _condor_stderr
    _condor_stdout
    payload.sh
    cms_readonly.use
    Fri Jul 19 09:01:32 UTC 2024
    eyJraWQiOiJyc2EyIiwiYWxnIjoiUlMyNTYifQ.eyJ3bGNnLnZlciI6IjEuMCIsInN1YiI6ImVkZWQ2ZGUwLWJjMmEtNGRlMi04MGRkLWQzMmYxZmM5NmExNiIsImF1ZCI6IlwiaHR0cHM6XC9cL3dsY2cuY2Vybi5jaFwvand0XC92MVwvYW55XCIiLCJhY3QiOnsic3ViIjoiNDg2MjBlZmEtZTAwMS00YzFkLTg0ZmMtNDY0MGQ5NDk4YTExIn0sIm5iZiI6MTcyMTM3OTE3Nywic2NvcGUiOiJzdG9yYWdlLnJlYWQ6XC8iLCJpc3MiOiJodHRwczpcL1wvY21zLWF1dGgud2ViLmNlcm4uY2hcLyIsImV4cCI6MTcyMTM4Mjc3NywiaWF0IjoxNzIxMzc5MTc3LCJqdGkiOiJiYWVhNDM0OS00NDNmLTRjYjYtOWRiMS04ZjFjMWQwZTdjOWEiLCJjbGllbnRfaWQiOiI0ODYyMGVmYS1lMDAxLTRjMWQtODRmYy00NjQwZDk0OThhMTEifQ.a4E9myoGrcEx77JaHIe5Put9Bjeq541IiQv9IjPji1qGBXxMIDc9l1UgWAilKmUwoZjKR6pNMwGggTflzwZxU3JZPRML4MB64Lue67JCIimYedpcw5dZPntqazCoE04qBORB8LtiijGFsljaJU44wmrUm0NKrsYtzZsw5mGRrXIJf2q-aKfCgy_Cbz1QDdeWtLXolyjI9uBrTfJKHJ7wShNgcbiJGlN4U3CyHtHAdLunJZRp0gCo9AcQN3HS_B5NFJ6rgWw8w3ylttJzM8lgr6raGoUoT8wVIkD7PsrmUGxvsUYX-9hJl7rHF9sgFwJFun3DEvU5BZ-_3IwEdhbfrw

    Fri Jul 19 10:03:12 UTC 2024
    eyJraWQiOiJyc2EyIiwiYWxnIjoiUlMyNTYifQ.eyJ3bGNnLnZlciI6IjEuMCIsInN1YiI6ImVkZWQ2ZGUwLWJjMmEtNGRlMi04MGRkLWQzMmYxZmM5NmExNiIsImF1ZCI6IlwiaHR0cHM6XC9cL3dsY2cuY2Vybi5jaFwvand0XC92MVwvYW55XCIiLCJhY3QiOnsic3ViIjoiNDg2MjBlZmEtZTAwMS00YzFkLTg0ZmMtNDY0MGQ5NDk4YTExIn0sIm5iZiI6MTcyMTM4Mjc4MSwic2NvcGUiOiJzdG9yYWdlLnJlYWQ6XC8iLCJpc3MiOiJodHRwczpcL1wvY21zLWF1dGgud2ViLmNlcm4uY2hcLyIsImV4cCI6MTcyMTM4NjM4MSwiaWF0IjoxNzIxMzgyNzgxLCJqdGkiOiIxYTQwMjBkMi05ZDEzLTQyYzctYTAxMC1iODIxYmFhNzg2YjEiLCJjbGllbnRfaWQiOiI0ODYyMGVmYS1lMDAxLTRjMWQtODRmYy00NjQwZDk0OThhMTEifQ.ec7btkDwjd9bJ8QVyOhGN1QnXgO9-OCWEomwgi3ZxAQhQy5pTdORVGfbPKfv0_sMCdnecKlKY9pnWtudQyn5H8znsfIFcovhi6y8gsQPPsU6zbCFifZm7AxrL3-VZGxhxj5IJAnj7394Nv4t4tgQLcabwdQDV77Ifm96TCHx7BSh6xD9ygtkec73XFJ3EOgi68DCNI3Z3LVzJQmt6Fdr-SuY44WtbU8iJNbrDXWVVdGuRH9rySS8f8fBgHazeyIOeCIZ_UX8RI6OgDVWe-BvM2Bhxt64-k5pmJ1MWifEeWjEHB7Iik6O3KyFOgjrBGfbWZYFXdehGUfXfjUs4Mymyg

    Fri Jul 19 11:04:52 UTC 2024
    eyJraWQiOiJyc2EyIiwiYWxnIjoiUlMyNTYifQ.eyJ3bGNnLnZlciI6IjEuMCIsInN1YiI6ImVkZWQ2ZGUwLWJjMmEtNGRlMi04MGRkLWQzMmYxZmM5NmExNiIsImF1ZCI6IlwiaHR0cHM6XC9cL3dsY2cuY2Vybi5jaFwvand0XC92MVwvYW55XCIiLCJhY3QiOnsic3ViIjoiNDg2MjBlZmEtZTAwMS00YzFkLTg0ZmMtNDY0MGQ5NDk4YTExIn0sIm5iZiI6MTcyMTM4NjM4NSwic2NvcGUiOiJzdG9yYWdlLnJlYWQ6XC8iLCJpc3MiOiJodHRwczpcL1wvY21zLWF1dGgud2ViLmNlcm4uY2hcLyIsImV4cCI6MTcyMTM4OTk4NSwiaWF0IjoxNzIxMzg2Mzg1LCJqdGkiOiJjNmM2ZjIxNC0yOWU3LTQ1NDQtYjRhNy01MWVjNzM2M2QwYWEiLCJjbGllbnRfaWQiOiI0ODYyMGVmYS1lMDAxLTRjMWQtODRmYy00NjQwZDk0OThhMTEifQ.DFF1k18Y7QH6wMe6J7QKz8QUO-4oySn8gXjpLBNBph6PAS114NShrBlGvZQ945Cyw343pMPtzwVnvht8pUm95ffxcbQo_O2M13shcbGQBvBzqbeHcSUeCJKiBtW8Af0lRbkThd0VIFVLSHQvrMLaEQl3F-ZOiI2v_gm9mMH7UvQz4C8jiTo2Io0NG5lQYT9BperwkaPLh-gDs9Zg0WvfOYErGYYIbRZz8LxtFSNbfB4GJhoLbKZ369C9YX-XpfjgoZYDhPjCL-SwQhXUjhFJdWsA_CwrLMQNcxrreucT_-sSwpS1qtkw_QDJicuaRBiT2aWlDxN-tNvJGpqqdLKgCQ
    ```

then parse the tokens to check that the latest token is issued _after_ the job
has been submitted

```plaintext
> token_decode -t eyJraWQiOiJyc2EyIiwiYWxnIjoiUlMyNTYifQ.eyJ3bGNnLnZlciI6IjEuMCIsInN1YiI6ImVkZWQ2ZGUwLWJjMmEtNGRlMi04MGRkLWQzMmYxZmM5NmExNiIsImF1ZCI6IlwiaHR0cHM6XC9cL3dsY2cuY2Vybi5jaFwvand0XC92MVwvYW55XCIiLCJhY3QiOnsic3ViIjoiNDg2MjBlZmEtZTAwMS00YzFkLTg0ZmMtNDY0MGQ5NDk4YTExIn0sIm5iZiI6MTcyMTM4NjM4NSwic2NvcGUiOiJzdG9yYWdlLnJlYWQ6XC8iLCJpc3MiOiJodHRwczpcL1wvY21zLWF1dGgud2ViLmNlcm4uY2hcLyIsImV4cCI6MTcyMTM4OTk4NSwiaWF0IjoxNzIxMzg2Mzg1LCJqdGkiOiJjNmM2ZjIxNC0yOWU3LTQ1NDQtYjRhNy01MWVjNzM2M2QwYWEiLCJjbGllbnRfaWQiOiI0ODYyMGVmYS1lMDAxLTRjMWQtODRmYy00NjQwZDk0OThhMTEifQ.DFF1k18Y7QH6wMe6J7QKz8QUO-4oySn8gXjpLBNBph6PAS114NShrBlGvZQ945Cyw343pMPtzwVnvht8pUm95ffxcbQo_O2M13shcbGQBvBzqbeHcSUeCJKiBtW8Af0lRbkThd0VIFVLSHQvrMLaEQl3F-ZOiI2v_gm9mMH7UvQz4C8jiTo2Io0NG5lQYT9BperwkaPLh-gDs9Zg0WvfOYErGYYIbRZz8LxtFSNbfB4GJhoLbKZ369C9YX-XpfjgoZYDhPjCL-SwQhXUjhFJdWsA_CwrLMQNcxrreucT_-sSwpS1qtkw_QDJicuaRBiT2aWlDxN-tNvJGpqqdLKgCQ
{'act': {'sub': '48620efa-e001-4c1d-84fc-4640d9498a11'},
 'aud': '"https://wlcg.cern.ch/jwt/v1/any"',
 'client_id': '48620efa-e001-4c1d-84fc-4640d9498a11',
 'exp': 1721389985,
 'iat': 1721386385,
 'iss': 'https://cms-auth.web.cern.ch/',
 'jti': 'c6c6f214-29e7-4544-b4a7-51ec7363d0aa',
 'nbf': 1721386385,
 'scope': 'storage.read:/',
 'sub': 'eded6de0-bc2a-4de2-80dd-d32f1fc96a16',
 'wlcg.ver': '1.0'}
iat: 2024-07-19 10:53:05 +0000 UTC
nbf: 2024-07-19 10:53:05 +0000 UTC
exp: 2024-07-19 11:53:05 +0000 UTC
```

??? note "token parse - python script example: token_decode.py"

    ```python
    #!/usr/bin/python3

    import jwt
    import pprint
    import datetime
    import pytz

    import argparse
    parser = argparse.ArgumentParser(formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument("-t", "--token",
      help="token, string",
      type=str,
      required=True,
      )
    args = parser.parse_args()
    #print(f"encoded token: {args.token}")

    def parse_utc(utc_seconds):
        d = datetime.datetime.fromtimestamp(utc_seconds)
        d = d.astimezone(tz=pytz.utc)
        d_str = d.strftime("%Y-%m-%d %H:%M:%S %z %Z")
        return d_str

    def main():
        token_decoded = jwt.decode(args.token, options={"verify_signature": False})
        pprint.pprint(token_decoded)

        print(f"iat: {parse_utc(token_decoded.get('iat'))}")
        print(f"nbf: {parse_utc(token_decoded.get('nbf'))}")
        print(f"exp: {parse_utc(token_decoded.get('exp'))}")

    if __name__ == "__main__":
        main()
    ```

### troubleshoot

#### condor_submit fails

If condor_submit fails to retrive a token, then try to run `htgettoken` yourself in debug mode.

```bash
htgettoken -d -a dwdvault.cern.ch:8200 -i cms --scopes=storage.read:/ -o /data/certs/creds/token-cms-dwdvault.txt
```

if the output contains

```plaintext
[crabtw@vocms069 submit-jdl]$ htgettoken -d -a dwdvault.cern.ch:8200 -i cms --scopes=storage.read:/ -o /data/certs/creds/token-cms-dwdvault.txt
[...]
htgettoken: Initiating authentication to https://dwdvault.cern.ch:8200 failed: HTTPError: HTTP Error 500: Internal Server Error: rpc error: code = Unavailable desc = connection error: desc = "transport: error while dialing: dial unix /tmp/plugin1860223503: connect: no such file or directory"
```

Then you should send an email do Dave Dykstra and ask him to fix/restart/massage
his vault instance.

### prepare the schedd to submit jobs with user tokens

Most of the heavylifting is done in the [tokens.pp](https://gitlab.cern.ch/ai/it-puppet-hostgroup-vocmsglidein/-/blob/crabdev_dmapelli/code/manifests/crabschedd/tokens.pp?ref_type=heads) file in the
`crabdev_dmapelli` branch in our usual puppet repository.

briefly:

- install `htgettoken` rpm package from the OSG repositories
- install `condor-credmon-vault` rpm package from usual cern condor repositories
- add condor_credmon configurations to the file `/etc/condor/config.d/99-vault-credmon-crab.conf`, the content comes from [here](https://gitlab.cern.ch/ai/it-puppet-hostgroup-vocmsglidein/-/blob/crabdev_dmapelli/code/files/crabschedd/41-vault-credmon-crab.conf?ref_type=heads)
