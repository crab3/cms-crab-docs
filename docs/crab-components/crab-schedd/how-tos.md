# condor - how-tos

## Introduction

References:

- Official HTCondor documentation: [https://htcondor.readthedocs.io/en/latest/](https://htcondor.readthedocs.io/en/latest/)
    - official classads description: [https://htcondor.readthedocs.io/en/latest/classad-attributes/job-classad-attributes.html](https://htcondor.readthedocs.io/en/latest/classad-attributes/job-classad-attributes.html)
- tables with HTCondor magic numbers: [https://htcondor-wiki.cs.wisc.edu/index.cgi/wiki?p=MagicNumbers](https://htcondor-wiki.cs.wisc.edu/index.cgi/wiki?p=MagicNumbers)

the main commands that CRAB operators will use are:

manage jobs

- `condor_q`: queries the scheduler, to retrieve list of the jobs it manages
- `condor_submit`: submits a job, given a JDL file
- `condor_hold`: puts a job on hold
- `condor_rm`: remove a job from the scheduler queues, as sudo on the schedd machine

manage the schedd process

- `condor_on`: starts the scheduler process, as sudo on the schedd machine
- `condor_off`: stops the scheduler process, as sudo on the schedd machine. 
   achtung! schedds are very clever in turning themselves down. Do not pressure them,
   do not hard-reboot a machine just to make sure that the schedd is down.
   Let the schedd do its things, it will stop sooner or later, even after some minutes.

other commands

- `condor_status`: get information about the pool

## reboot a scheduler when it stops responding

Sometimes our schedulers run out of RAM and they stop being responsive. They respond to ping but you can not ssh into them.

You can monitor the scheduler status from

- [CRAB Overview](https://monit-grafana.cern.ch/d/CsnjLe6Mk/crab-overview?orgId=11&refresh=1h): how many responsive scheduler do we have?
- [Crab Status Summary](https://monit-grafana.cern.ch/d/GpYWxoiMz/crab-status-summary?orgId=11): which scheduler is not responsive?
- [VM metrics](https://monit-grafana.cern.ch/d/NF9Zm8tWz/crab-vms-host-metrics?orgId=11): select the scheduler, do you see anything unusual before the schedd stopped responding to prometheus?

When a schedd is no longer responsive, the only thing you can do is to reboot it:

```plaintext
laptop> ssh aiadm.cern.ch
aiadm> export OS_PROJECT_NAME="CMS CRAB"
aiadm> openstack server reboot --hard $instance
```

Check on the "Crab Status Summary" dashboard the status of the scheduler. 
If is continues to be red, then check if you can ssh into the vm and follow
this checklist:

- If yes,then check if the scheduler process is running with `condor_q`. 
- If yes, then check if the metrics exporter is running with `ps ufx | grep
  exporter` or `curl http://localhost:9100/metrics`
- If no exporter process is running, then start it with
  `/root/exporters/start.sh`

## get generic information about the pool(s)

get information about the `collector`

```bash
host cmsgwms-collector-global.cern.ch
```

```plaintext
cmsgwms-collector-global.cern.ch is an alias for vocms0814.cern.ch.
vocms0814.cern.ch has address 128.142.248.159
vocms0814.cern.ch has IPv6 address 2001:1458:301:73::100:9e
```

get list of crab schedds connected to a pool

```bash
# global pool
condor_status -sched -con 'CMSGWMS_Type=="crabschedd"' -pool cmsgwms-collector-global.cern.ch -af machine

#itb pool
condor_status -sched -con 'CMSGWMS_Type=="crabschedd"' -pool cmsgwms-collector-itb.cern.ch -af machine
```


## Operate on jobs

### recommendations

`condor_q` is a very powerful tool, that queries the scheduler process to aggregate
details of the jobs it manages. Providing a comprehensive list of commands that a
CRAB operator can use in every possible situation is almost impossible.
However, a couple of concepts are enough to write the command you will ever need.

First of all, every condor job has a list of attributes, called `ClassAds`. 
ClassAds names are case insensitive, their values are typed. 
In CRAB we mainly use int and strings. 

You can get the full list of classads of a job with `-long` option. 
Since this is verbose, when you only need to get a quick peek around it is
advisable to limit the output of condor_q to only one job :

```
condor_q -limit 1 -long
```

The generic structure of a `condor_q` command is

```bash
condor_q [general options ] [restriction list ] [output options ] 
```

By combining restriction list and output options as needed, you can use
condor_q to make is SQL-like queries of the form `SELECT ... WHERE ...` on the 
jobs "table".

Keep in mind that some classads are available for every jobs, while condor allows
adding custom classads to jobs. Not all the jobs have the same list of classads! 
But all the jobs have the same basic ones. Moreover, CRAB submits three types of 
jobs and each type has a definite schema.

```
# grid job, vanilla universe
condor_q -con 'jobuniverse==5'  -limit 1 -long

# dagman_bootstrp, schedd universe
condor_q -con 'jobuniverse==7'  -limit 1 -long

# task_process, local universe
condor_q -con 'jobuniverse==12'  -limit 1 -long
```

### restriction list

You can filter the jobs that you are interested in with an expression that uses
the job classads values with the `-constraint` option.

- equality, `int` value: `-con 'classad==N'`
- equality, srting value: `-con 'classad=="value"'`. You need to marshal the string
  value in double quotes. If needed, these double quotes need to be escaped.
- AND: `-con 'classad1==N&&classad2=="value"'`, or also `-con 'classad1==N' -con 'classad2=="value"'`


### output

You can retrieve only the value of a specific classad for a list of files with the
`-autoformat` or `-af` option.

- output only two classads with `-af classad1 classad2` or `-af classad1 -af classad2`

### examples

get list of running dagmans

```bash
condor_q -con 'jobuniverse==7&&jobstatus==2'
```

get list of running dagmans that have been submitted in the last 5 hours

```bash
condor_q -con 'jobuniverse==7&&jobstatus==2' -con "qdate>$(date -d '4 hour ago' +%s)"
```

get list of running dagmans on all schedds, sorted and grouped by username

```bash
condor_q -global -con 'jobuniverse==7&&jobstatus==2' -af CRAB_UserHN | sort | uniq -c | sort -nr
```

get list of jobs in vanilla universe, given a dagman job id

```bash
condor_q 8889420.0 -dag
```

given a job id, get the path of its spool directory

```bash
condor_q $job_id -af iwd
```

given job id, get CRAB taskName

```bash
condor_q $jobid -af crab_reqname
```

remove from a schedd all jobs that are owned by user `dmapelli`

```bash
# login into a schedd
sudo su
condor_rm -con 'crab_userhn=="dmapelli"' 
```

If a user complains that its jobs are not running, then you can

- check on das where the input dataset is
- check on site status iof the site is able to run jobs
- check if the user overrode the global blacklist (at his own risk!)
- log into the schedd and get the list of sites where the user jobs have been
  shceduled to run at

    ```bash
    condor_q -con 'jobuniverse==5&&crab_userhn=="dmapelli"&&jobstatus==1' -long | grep -i "^desired_sites" | sort | uniq
    ```
- if the jobs have been scheduled to run at a site that can not run jobs, then the user can only wait.

Run the same command on all the schedds, in the only sane way, 
without getting crazy escaping quotes

```bash
for schedd in $(condor_status -sched -con 'CMSGWMS_Type=="crabschedd"' -pool cmsgwms-collector-global.cern.ch -af machine )
do ssh $schedd bash -s <<'EOF'
    echo "schedd: " $(hostname)
    condor_q -con 'jobuniverse==5&&jobstatus==2' -con 'crab_userhn=="byates"' -af crab_userhn | wc -l
EOF
done
```

### migrate a DAG from une schedd to another

Around 2023-03 we have been requested to assess the cpu usage of the CRAB VMs 
inside cern openstack. We investigated the possibility of switching off schedd
machines in a ~1d timescale. This would require to migrate DAGs from one schedd
to another, so we asked CMS SIon how to do it and they redirected the question
to HTCondor devs. This is their answer:



> Here’s summary for how to transplant a running DAG to a new schedd.
On the old schedd, condor_rm the dag job. Wait for it to leave the
queue. The dagman daemon will remove all of the node jobs and write
out a rescue DAG.
Then, move the entire directory tree of files for the DAG (if it’s not
on a shared filesystem) to the new schedd machine. As long as none of
the submit files have any absolute paths, the files don’t have to be
in the same location in the filesystem as on the old machine.
Then, submit the dag to the new schedd. Dagman will see the rescue dag
file to see where it left off in the workflow and resume there.
There is the possibility of duplicate job ids in your job event logs
between the two schedds, but that shouldn’t confuse dagman, since
their lifetimes won’t overlap in the log.
> 
> - Jaime

### list of notable classads:

- `clusterid`: job id
- `qdate`: submission date, in seconds from epoch
- `RemoteWallClockTime` is the same as `CompletionDate - JobCurrentStartDate`, in seconds from epoch
  - achtung! this does not take into account multi-core jobs. If you want the full core time, then use
    `RemoteWallClockTime * CpusProvisioned`

## Operate on the schedd

get information about the schedd process

```bash
condor_top
```

### troubleshoot

Sometimes some process running on the schedd (that we have not identified yet) starts
using too much ram and make the machine not responsive. At a certain point the OOM killer
intervenes and the machine gets back to its normal state. In order to make sure that
the schedd is running properly, login with ssh and run `systyemctl` and `condor_q`.
If they are ok, then you are good to go.

However, sometimes the machines gets into a zombie state where the aforementioned
command do not work, or you can not ssh into the vm, and the only solution is to
hard reboot the vm from openstack

## gather specific information

count CPUs available in the global pool, grouped by architecture

```bash
condor_status -pool vocms0814.cern.ch -af arch | sort | uniq -c
```

## develop or debug the schedd and the pool

### submit a job directly to condor

condor allow to submit jobs from the command line.

If a crab operator logs in into a crab schedd, it can use the condor cli to submit
jobs directly to that schedd.

Log into a schedd, use `su crabtw` to change to the crabtw user, create a directory
under `/home/grid/crabtw/`.

You need to provide condor with a "payload", a program that will be executed
in all the jobs that you submit. and exmaple of a payload is:

```bash
#!/bin/bash

#file: payload.sh

set -x

date
whoami
env
ls ${CONDOR_CREDS}
ls ${_CONDOR_CREDS}
cat ${_CONDOR_CREDS}/cms_readonly.use
echo arguments: $@
date

set +x
```

Then you need to provide condor with some configuration for the job(s) that you
want to submit. The condor job configuration is written in "job description language" or JDL.
An example of a payload and job configuration that suits the specifications of
the CMS global and ITB pools is the following:

```plaintext
# file: hello.sub

# generic condor
universe = vanilla
executable            = payload.sh
arguments             = $(ClusterId) $(ProcId)
output                = output/hello.$(ClusterId).$(ProcId).out
error                 = error/hello.$(ClusterId).$(ProcId).err
log                   = log/hello.$(ClusterId).log

should_transfer_files = YES
when_to_transfer_output = ON_EXIT

# cms specific configuration
requirements = stringListMember(GLIDEIN_CMSSite,DESIRED_Sites)
+DESIRED_Sites = "T2_CH_CERN,T2_CH_CSCS,T2_IT_Pisa"
accounting_group = analysis
+AccountGroup = analysis

RequestCPUs = 1
RequestMemory = 2048

+CMS_Type = "DONOTMONIT"
+CMS_JobType = "crab_test"
+CMS_TaskType = "crab_test"
+IS_TEST_JOB="YES"

# submit only one such job
queue 1
```

You can now submit a job to condor with

```
condor_submit hello.sub
```

you can monitor the progress of the job with

```
condor_wait log/hello.NNN.log
```

If the jobs are not complete yet, then you will get some text back and condor_wait
will (sigh) wait. If the jobs are done, then you get a reassuring `All jobs done.`
back.

When the jobs are done, you can read the stdout and the stderr of the remote jobs executed on
the grid at `output/hello.NNN.0.out` and `error/hello.NNN.0.err` respectively.
