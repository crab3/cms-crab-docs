# Migrate CRAB Schduelers to a new OS

CRAB Scheduler is different from other service, there is no container.
HTCondor client (so called HTCondor Access Point) software (schedd, master, shared port and other needed daemons) as well as HTCondor Dagman's and CRAB's scripts which deal with Pre(Dag/Job) phase, PostJob, status caching and ASO, all run in the host (an OpenStack VN of course) using as base the current Operating System.

Therefore updating the VM to a new OS is non-trivial. We also want to preserve HTCondor queue and history and make it possible to resume jobs and tasks which were running before, possible beacuse task's WEB_DIR, SPOOL_DIR and HTCondor queue and logs are on CEPH volumes which can be reattached after OS migration.

## Main steps
 1. port puppet manifests to new OS : puppet modules and classes themselves may have different names or syntax, a lo t of `if $facts['os']['release']['major'] == ` may be needed

 2. HTCondor installation manifests in puppet need to be made compatible with new OS, wh
ich usually implies adding new RPM repositories.

 3. All needed CRAB scripts must be able to run with both OSes

 4. ideally all tasks in the system should have been submitted with a TW version which pushed the compatible scripts to the schedulers,

 5. drain scheduler to < 100 running jobs and < 10 running tasks

 6. migrate

Below is a description of how we dealt with that for the CC7-to-Alma9 migration in 2024. Next time will be different, but hopefully this will be a useful guide.

### 1. puppet

That took the most time, but details are really OS specific. Hopefully someone will have migrated their puppettized VM to new OS already and you can look a their manifests. Also do not be shy about asking in IT/puppet MatterMost channel, people there are very very helpful.
We struggled a bit with `sssd` and FTS Client.

We created a new puppet environmen to isolate changes to one or two test machines until ready for putting them in `qa` and moved `qa` to master when everything was done and `qa` was working stably.

### 2. HTCondor

In our case, Submission Infrastructure had went through this already. Presumably will be the case again. We used the ad-hoc puppet environment to mix our puppet branch with SI's vocmshtcondor branch before both had all needed things in `qa`

### 3. CRAB scripts

Remember to check and test both `bash` and `python` scripts. Everything that goes in the tarball sent by TW to the scheduler (detailed content changes over time). In our case we were running python3 both in CC7 and Alma9, so changes were not large, mostly environment settings for Rucio Client.

### 4. Task code update

On this we failed do the preparation early on enouhg, so used a script to replace code in the `SPOOL_DIR` of each task

We called this phase 'Preparation'


### 5. Drain 

Drain a scheduler by setting weigth to `0` in [cmsweb-shedds-config.json](https://gitlab.cern.ch/crab3/CRAB3ServerConfig/-/blob/master/cmsweb-shedds-config.json)

In principle draining is not necessary. HTCondor will restart everything. Anyhow, in case something goes wrong and to reduce startup time and load to collector if one schedd tries to start 10K jobs "at same time", it is better to drain. Numbers above are guidelines, not absolute thresholds.
It also depends on how long you want/can wait. When doing CC7 to Alma9 load was light and one week was sufficient.

### 6. Migrate

We divided in two steps

 - Preapration: before machine goes down for rebuilding
 -  Migration: turnt things off, change OS, restart, make it work again

And of course do not forget to **TEST** before putting scheduler back in production (reverse Drain step by putting weigth back to `1`)

### 7. **TEST**

Make sure to test at least these cases:

 * normal submission with FTS stageout
 * normal submission with Rucio stageout
 * automatic splitting (exercises pieces of TW DAG creation and submission which runs in the scheduler)


## These are the details:

From the document used and checklist for the migration

#### Preparation
1. Put scheduler in draining in [cmsweb-schedds-config.json](https://gitlab.cern.ch/crab3/CRAB3ServerConfig/-/blob/master/cmsweb-shedds-config.json)
2. Wait O(1 week)
3. Insert the schedd in puppet environment `qa`
   * **on aiadm** after `export OS_PROJECT_NAME="CMS CRAB"`
      ```ai-foreman updatehost vocms0XXX.cern.ch -e qa```
      
4. Update all scripts to Alma9 compatible version :
   * **on the scheduler** as `crabtw` execute
      ```/afs/cern.ch/user/b/belforte/public/PatchAll.sh```


??? info "content of Patchall.sh"

    ```bash
    #!/bin/bash
    # find SPOOL_DIR of active tasks and hold running task_process
    # select DAGMAN's in Running or Hold

    spool_dirs=`condor_q -con 'jobuniverse==7&&jobstatus==2||jobstatus==5' -af iwd`
    condor_hold -con 'jobuniverse==12&&jobstatus==2'

    echo there are `echo $spool_dirs|wc -w` DAGMANs in hold or running
    # update scripts
    for dir in $spool_dirs
    do
      echo $dir
      cd $dir
      # save current files
      cp dag_bootstrap.sh dag_bootstrap.sh.SAVED
      cp dag_bootstrap_startup.sh dag_bootstrap_startup.sh.SAVED
      cp task_process/task_proc_wrapper.sh task_process/task_proc_wrapper.sh.SAVED
      cp task_process/FTS_Transfers.py task_process/FTS_Transfers.py.SAVED

      # replace with alma9 version
      cp -r /afs/cern.ch/user/b/belforte/public/PATCH/* .
    done

    # restart task_processes which we held previously (DAGMAN's in Hold will be
    # restarted when users do crab resubmit. DAGMAN's running
    # will be restarted when we move to Alma9) We can not
    # blindly restart all DAGMAN's

    condor_release -con 'jobuniverse==12&&jobstatus==5'
    ```
??? info "Content of the PATCH directory used in previous script"
    ```text
    belforte@lxplus806/public> tree PATCH/
    PATCH/
    |-- dag_bootstrap.sh
    |-- dag_bootstrap_startup.sh
    `-- task_process
        |-- FTS_Transfers.py
        `-- task_proc_wrapper.sh
    ```
     

#### Migration 
  1. **on the scheduler** as `root`
      1. are there running DAGs ?
      ```condor_q -con 'jobuniverse==7&&jobstatus==2'```
      2. if yes, hold running DAGs and mark them for later release
          ```condor_qedit -con 'jobuniverse==7&&jobstatus==2' HoldKillSig "SIGKILL"```
          ```condor_hold  -con 'jobuniverse==7&&jobstatus==2' -reason ALMA9```
      1. stop condor with `condor_off`
      1. Wait until it is done, when the last line in `/var/log/condor/MasterLog` says `All daemons are gone.`
  1. **on aiadm** after `export OS_PROJECT_NAME="CMS CRAB"`
      1. prepare new `data/fqdns/<schedname>.cern.ch.yaml` file in `qa` branch with correct condor version
          * **ATTENTION we run `10.0.9-1.el7` now but there is no condor`10.0.9-1.el9` HTCondor for alma9 starts with `10.2.0-1.el9`. Use that !**
      1. commit the new file `<schedname>.cern.ch.yaml` to `qa`
      3. issue
      ```ai-rebuild --alma9 <schedname>.cern.ch```
         * **BEWARE** very bad things will happen if you reboot the machine in alma9 w/o having updated the yaml file. I had to manually change ownership of thousands of files from `systemd-oom:cvmfs` to `condor:condor`
      5. wait for machine to boot and puppet to run 1st time. This only prepares puppet itself. You still can not log in. 
      6. Use this time to remove keys from `~/.ssh/knows-hosts` since they will change
      7. wait for foreman to run puppet a second time and pull the full config, at this point there should be no errors in the report and you can log in. This may take up O(30min). [Is there a way to reduce this ?]
  1. **on the scheduler** as `root` (note: first time after rebuild must use `ssh node.cern.ch`, not the shortened `ssh node`)
      1. If case `condor` uid/gid is not `992:998`, change owner of files and directory in `/data/srv/glidecondor/condor_local/` to the new uid/gid.
          1. Change the owner of `/data/srv/glidecondor/condor_local/{execute,job_history,lock,log,run}` (recursively) to `condor:condor` 
              ```
              cd /data/srv/glidecondor/condor_local/
              chown -R condor:condor {execute,job_history,lock,log,run}
              ```
          2. Change the owner of files and directory inside `/data/srv/glidecondor/condor_local/spool` and its child `0` directory to `condor:condor`
              ```
              cd /data/srv/glidecondor/condor_local/spool
              find . -maxdepth 1 -exec chown condor:condor {} \; 
              find . -maxdepth 1 -exec chown condor:condor {}/0 \; 
              ```
      3. HTCondor was installed but sometimes not started, if needed start it with
          ```sudo systemctl start condor```
      1. release DAGs which where held earlier
          ```
          condor_qedit -con 'jobuniverse==7&&jobstatus==5&&HoldReason=="ALMA9 (by user condor)"' HoldKillSig "SIGUSR1"
          condor_release -con 'jobuniverse==7&&jobstatus==5&&HoldReason=="ALMA9 (by user condor)"'
          ```
  1. **All set.** Check things with `condor_q`
  
##### Unresolved odd issues

  1. at times I just need to wait a long time before i can log in new machine (>30min) even if there is no new puppet run recorded in foreman
  2. at times puppet fails to properly start `httpd`, running `puppet agent -tv` manually (after I can log in) fixed it. Maybe there's a way to prevent this by changing the manifest ? Error is:
  ```
  change from 'absent' to 'file' failed: Could not set 'file' on ensure: No such file or directory - A directory component in /etc/httpd/conf/httpd.conf20240329-4510-1jywyli.lock does not exist or is a dangling symbolic link (file: /mnt/puppetnfsdir/environments/crab_alma9/hostgroups/hg_vocmsglidein/manifests/crabschedd/httpd.pp, line: 16)
  ```

