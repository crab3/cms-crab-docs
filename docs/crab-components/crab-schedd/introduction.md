# CRAB Schedd

## About CRAB3 Schedd

CRAB3 Schedds are just an HTCondor Schedd with some custom configs.

### Usage scenario of CRAB3 schedds and hardware requirements

CRAB submission is done in unit of "tasks". A task is one workflow aimed at processing one dataset and is expanded into up to a maximum of 10k HTCondor jobs managed by one HTCondor DAGMAN metascheduler. Those DAGMAN processes run on the crab schedd in the scheduler universe and compete with memory with the schedd and the condor shadow processes. For this reason we limit the number of tasks which can run at same time on each schedd (as of April 2020 at 200) and select machines with large RAM. CPU requirements are usually low, so we will pick amont the machine classess offered in OpenStack those with large RAM but limited number of cores.

CRAB schedd's also need disk space to host the log files which are needed for DAGMANs and jobs status tracking and the logs returned from the remotey executed jobs which are made available to users and operators for debugging. Since disk access for HTCondor is critical, we decided to protect it by using two separate disk volumes.

- One volume has all condor logs, pointed to as the `SPOOL_DIR` for each task, and select a CEPH High I/O volume for this.
- One other volume is used as home directory for the various pool accounts and (potentially largish) user jobs log files are moved there once retrieved by condor schedd. This is called `WEB_DIR`. The `WEB_DIR` for each task is made accessible on the web via HTTP (hence the name) via an httpd Apache daemon running on each schedd.

For easy navigation each task's webdir is `/home/grid/username/taskname` and contains a symbolic link to `SPOOL_DIR` for that task, and each `SPOOL_DIR` has a symbolic link to the `WEB_DIR` for that task. For security reasons, those apache servers are not exposed outside CERN firewall and access to those files is proxied through CMSWEB wich provides a secure redirection, see [Redirectioin rules for CMSWEB](#redirection-rules-for-cmsweb)


## Machine details

All schedd machine are managed by puppet, with following specs:

| **Specs**             |                                  |
|-----------------------|----------------------------------|
| Flavor                | r2.2xlarge                       |
| VCPUs                 | 16                               |
| RAM                   | 58.59 GB                         |
| Disk                  | 160 GB                           |
| **Volumes Attached**  |                                  |
| Standard volume       | 1 TB, /dev/vdb, ext4             |
| HighIO volume         | 1 TB, /dev/vdc, ext4              |
| **Metadata**          |                                  |
| Openstack Tenant name | CMS CRAB                         |
| OS                    | CentOS7 (CC7 - x86_64 [latest])  |
| landb-mainuser        | CMS-SERVICE-CRAB3HTCONDOR        |
| landb-responsible     | CMS-SERVICE-CRAB3HTCONDOR-ADMINS |
| landb-ipv6ready       | yes                              |
| Puppet Hostgroup      | vocmsglidein/crabschedd          |
| Puppet environment    | qa                               |


For production node, ask CMS VOC [JIRA](https://its.cern.ch/jira/projects/CMSVOC/issues) or via email <cms-voc@cern.ch> to provide a machine for us. We do not have enough quota for open new production machine by ourselves.  If you interest in create it yourself, the test machine, please see [Create machine yourself](./install.md#create-machine-yourself).


## Puppet code and hostgroup

All schedd machine are live inside `vocmsglidein/crabschedd` hostgroup.

You can view Puppet configuration specific to schedd in [crabschedd.pp](https://gitlab.cern.ch/ai/it-pupbbpet-hostgroup-vocmsglidein/-/blob/qa/code/manifests/crabschedd.pp).

## Volume mount

Be sure that file [volumes.pp](https://gitlab.cern.ch/ai/it-puppet-hostgroup-vocmsglidein/blob/master/code/manifests/modules/volumes.pp) is up to date with the attached volumes info.

- **HighIO** volume must be mounted on `/data`
- **Standard** volume must be mounted on `/home/grid`.

If is not updated ask VOC to update it or do it by youself.

## Schedd credential for CMS HTCondor pool

Ask SI team (MatterMost chat or email to CatA Operator SI team) to request new token to join cms GLOBAL/ITB pool. They will add our node to Central Manager and Frontends and generate new token for us.

To check if key set. Use `tbag showkeys` command in `aiadm` machine. Output should look like this:
    ```text
    [tseethon@aiadm43 ~]$ tbag showkeys --host vocms059.cern.ch
    [
         "token_global_vocms059",
         "token_tier0_vocms059"
    ]
    ```

Token deployment is done by [vocmshtcondor module](https://gitlab.cern.ch/ai/it-puppet-module-vocmshtcondor/-/blob/0993ad02647ea37030db8f7c6f4dba65dd7c0075/code/manifests/config.pp#L257).

## Redirection rules for CMSWEB

This is needed to allow users outside CERN to access log files on CERN schedds. For every new schedd you should add a rule on [backends-k8s-preprod.txt](https://github.com/dmwm/deployment/blob/master/frontend/backends-k8s-preprod.txt) and [backends-k8s-prod.txt](https://github.com/dmwm/deployment/blob/master/frontend/backends-k8s-prod.txt).

Add a line with the redirect rule, like this one:

``` config
^/auth/complete/scheddmon/crab-preprod-scd03/ crab-preprod-scd03
```
Also, you need to add new mapping entry to [cmsweb-shedds-config.json](https://gitlab.cern.ch/crab3/CRAB3ServerConfig/-/blob/master/cmsweb-shedds-config.json) to let crabserver know and pass correct endpoint of schedd server to client. (CRABClient access `status_cache` file in `/home/grid/<user>/<taskname>` through CMSWEB).


## WMArchiveUploader

WMArchive is system for collect statistic of CMS production/analysis jobs running in condor. The statistic is produced as json files created by [<which_scripts?>](???) scripts everytime jobs execute.

All json files are saved to `/data/srv/WMArchiveWD/new` directory. WMArchiveUploader service installed in Schedd machine are responsible to upload json file to aggregator server in CMSWEB and use certficate to communicate with service.

Service certificate's name are configure in Hiera's FQDN files (i.e. [vocms0199.cern.ch.yaml](https://gitlab.cern.ch/ai/it-puppet-hostgroup-vocmsglidein/-/blob/dbb296501e745200aac204ba4c074ea08fd26d98/data/fqdns/vocms0199.cern.ch.yaml#L39). As usuaull, this cert need to register in CRIC in order to authorize with aggregator. Please see [CRAB TaskWorker install](../crab-taskworker/introduction.md#service-certificate) on how to register it in CRIC.
