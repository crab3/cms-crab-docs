# CRAB Schedd install

## Deployment Step

- Ask CMSVOC admin to create new machine with 2 volume, Standard and HighIO, attached.
- Ask SI to setup condor token for schedd.
- Add new changes to [ai/it-hostgroup-vocmsglidein](https://gitlab.cern.ch/ai/it-puppet-hostgroup-vocmsglidein)
    - Add new disk mount in `volume.pp.` (See example [this commit](https://gitlab.cern.ch/ai/it-puppet-hostgroup-vocmsglidein/-/commit/18c90f519402c7bb5cf0cc06b7a991704923db2b))
        - Get UUID from `blkid` command (disk UUID will appear after format volume in previous step.
        - After Puppet run, HighIO volume should mount to `/data` and Standard volume to `/home/grid`
    - Add new `data/fqdn/<hostname>.cern.ch.yaml`. Copy file from [vocms0199.cern.ch.yaml](https://gitlab.cern.ch/ai/it-puppet-hostgroup-vocmsglidein/-/blob/0a908c0c679b32280bbebaaabcd4b2ea48ef6bec/data/fqdns/vocms0199.cern.ch.yaml) and change these variables:
        - `glidecondor_pool` to the CMS HTCondor pool you want to connect (`global` or `itb`)
        - `condor_version` to the same version as machine currently running.
        - `certmgr_san` to new cert client name (see example in [vocms0199.cern.ch.yaml#L39](https://gitlab.cern.ch/ai/it-puppet-hostgroup-vocmsglidein/-/blob/dbb296501e745200aac204ba4c074ea08fd26d98/data/fqdns/vocms0199.cern.ch.yaml#L39)).
    - Commit and push to `crabdev_schedd` branch.
- Change Puppet environment to `crabdev_schedd` using `ai-foreman` in `aiadm` machine.
    ```bash
    ai-foreman updatehost <your-hostname>.cern.ch -e crabdev_schedd -c vocmsglidein/crabschedd
    ```
- Run `puppet agent -tv` to apply Puppet code, run it at least 2 times. Check if there is any error from output logs. Ignore any error relate to execution `condor_*` command for now.
- Restart machine.
- Check if condor is running by run `condor_q` command. It should return result look like this (result show here have some job already running):

    ??? quote "condor_q logs"
        ```
        [tseethon@crab-preprod-scd03 ~]$ condor_q


        -- Schedd: crab3@crab-preprod-scd03.cern.ch : <188.185.36.134:4080?... @ 09/12/22 21:39:15
         ID      OWNER            SUBMITTED     RUN_TIME ST PRI SIZE CMD
           1.0   crabtw          8/30 13:19   0+00:38:16 C  10   0.1 dag_bootstrap_startup.sh RunJobs
          13.0   crabtw          8/30 15:02   0+01:23:22 C  10   0.1 dag_bootstrap_startup.sh RunJobs
          25.0   crabtw          8/30 16:25   0+00:41:41 C  10   0.1 dag_bootstrap_startup.sh RunJobs
          37.0   crabtw          8/31 11:48   0+06:45:50 C  10   0.1 dag_bootstrap_startup.sh RunJobs

        Total for query: 1 jobs; 0 completed, 0 removed, 0 idle, 1 running, 0 held, 0 suspended
        Total for all users: 1 jobs; 0 completed, 0 removed, 0 idle, 1 running, 0 held, 0 suspended
        ```

- Run `puppet agent -tv` again. Now we should not have any error.
- Try to submit crab task to this machine, e.g. [HC-1kj](https://github.com/dmwm/CRABServer/blob/687920c5d92dc7ec635a9258c81c6d4d4341aef3/test/statusTrackingTasks/HC-1kj.py) (Do not forget specific `config.Debug.scheddName` to new schedd name in new machine, and `config.Debug.collector` if new machine connect to ITB pool).
- Add new redirect rule to allow clients outside CERN to access job status
  - Add new mapping entry to [cmsweb-shedds-config.json](https://gitlab.cern.ch/crab3/CRAB3ServerConfig/-/blob/master/cmsweb-shedds-config.json).
  - Add new rules to Frontend in dmwm/deployment [backends-k8s-preprod.txt](https://github.com/dmwm/deployment/blob/master/frontend/backends-k8s-preprod.txt) and [backends-k8s-prod.txt](https://github.com/dmwm/deployment/blob/master/frontend/backends-k8s-prod.txt) (Ask CMSWEB team for review). Also make same change in [cmsweb services config](https://gitlab.cern.ch/cmsweb-k8s/services_config/-/blob/cmsweb/frontend-ds/backends.txt).

## Troubleshooting

### collectd-curl fail to install when yum update execute

New schedd machine will fail to install `collectd-curl` via puppet scripts because somehow the machine has version `5.12.0` from `collectd-qa` repo and `versionlock` on this version (see `yum versionlock status`). When puppet triggering installs, it will get `collectd-curl-5.8.1` from `collectd-stable` instead. It needs the same collectd version, triggers `yum install` to fail. You will see error reports in puppet like this:

![fail-to-install-collectd-curl.png](../../images/fail-to-install-collectd-curl.png)

To fix this,

- Remove current `collectd`
    ```bash
    yum remove collectd -y
    ```

- Install `collectd-curl-5.12.0` and pass disabling plugin `versionlock` to yum:
    ```bash
    yum install --disableplugin=versionlock collectd-curl
    ```

Note that `collectd-curl` are install via root hostgroup config (CERN IT), not in our code.

### Create machine yourself

# deploy new schedd

## Since we use puppet, we do all commands from aiadm

## create a PUPPETTIZED VM

in the example below a small test flavor `m2.medium` is used. Our production schedulers all have flavor `r2.2xlarge` which means 16 cores and 60GB RAM

```text
belforte@aiadm09/~> export OS_PROJECT_NAME="CMS CRAB"
belforte@aiadm01/~> ai-bs --landb-responsible cms-service-crab3htcondor-admins \
  --nova-flavor m2.medium --alma9 --foreman-environment "crabdev_schedd" \ 
   -g "vocmsglidein/crabschedd" crab-sched-901.cern.ch
```
the only non-obvious parameter is `-g` which is the foreman hostgroup

Example
```text
belforte@aiadm01/~> ai-bs --landb-responsible cms-service-crab3htcondor-admins --nova-flavor m2.medium --alma9 --foreman-environment "crabdev_schedd" -g "vocmsglidein/crabschedd" crab-sched-901
Using default domain (cern.ch) as 'crab-sched-901' does not look like an FQDN
Trying to bootstrap 'crab-sched-901.cern.ch'...
Using 'ALMA9 - x86_64' as the latest 'ALMA9' image available
Flavor: m2.medium (physical: False)
Booting from image: alma9 (o_d: ALMA)
Node tenant: CMS CRAB
Node region: cern
LANDB IPv6 ready: True
Foreman environment: crabdev_schedd
Foreman hostgroup: vocmsglidein/crabschedd
Puppet master: it-puppet-masters-public.cern.ch
Certmgr server: hector.cern.ch
Certmgr port: 8008
Roger server: woger-direct.cern.ch
Roger port: 8201
Preparing dynamic user data...
Using '/usr/share/ai-tools/userdata/linux/puppetinit' as userdata script template to init Puppet
Adding host 'crab-sched-901.cern.ch' to Foreman...
Host 'crab-sched-901.cern.ch' created in Foreman
Staging host 'crab-sched-901.cern.ch' on Certmgr...
Host 'crab-sched-901.cern.ch' staged
Creating node 'crab-sched-901'...
Request to create node 'crab-sched-901' sent
Adding 'crab-sched-901.cern.ch' to Roger
----------------------------------------------------------------------
* Your machine is booting and the network is being configured right now,
  Puppet will run immediately after a successful boot process.
* It typically takes around 30 minutes between this command is
  executed and the first Puppet report arrives to Foreman:
  https://judy.cern.ch/hosts/crab-sched-901.cern.ch/config_reports
  (although this depends a lot on the complexity of your configuration)
* After the initial configuration, if you've set rootegroups or
  rootusers in Foreman or Hiera you should be able to log in as
  root using your Kerberos credentials. The LANDB responsible
  has also root access by default.
* A custom LANDB responsible/user has been set. It will be visible
  in Foreman a few minutes after the node is booted. In the
  meantime, the Foreman owner will be the issuer of this command.
  (belforte@CERN.CH)
* You can check the status of the node creation request by running:
  'openstack server show crab-sched-901'
* In case of problems, if you provided a SSH key when creating the node
  use it to log into the box and take a look at /var/log/cloud-init*.log.
  Console log can be retrieved by using 'openstack console log show'.
----------------------------------------------------------------------

```
 
 
## rebuild a machine e.g. with a different Nova flavor (cpu/ram/disk)
1. `ai-kill <hostname>`
    * N.B. if you deleted the VM already in OpenStack: `ai-kill <hostname> --nova-disable`
2. `ai-bs ....` like above

## HTCondor
when puppet agent runs the first time it will fail to install HTCondor and other things because `/data` is missing. That will be fixed when mounting the volumes (next step). After that you need to run puppet agent to create the files and then reboot to start everything "properly".
It is also needed that `/home/grid` is owned by `crabtw:zh`. Currently neither of those is enforced in puppet.
Stefano is not sure if that should be added.

## create two volumes
 1. one standard volume - 1000GB
 2. one high io volume (io1) - 1000GB

Note that there is separate quota for standard and io1 volumes
```text
belforte@aiadm01/~> openstack quota show|grep gigabytes_io1
| gigabytes_io1 | 18884 |
| ------------- | ----- |
belforte@aiadm01/~> openstack quota show|grep gigabytes_standard
| gigabytes_standard    | 27200
```
this is also visible in project top page in the  UI
![](https://codimd.web.cern.ch/uploads/upload_35122cae8b97cdf46073ace337060f72.png)


## connect VM and Volumes
```bash
export OS_PROJECT_NAME="CMS CRAB"
# the first to be added will be /dev/vdb
openstack server add volume vocmscrabsc901 vocmscrabsc901_standard
# the second will be /dev/vdc
openstack server add volume vocmscrabsc901 vocmscrabsc901_high

```
example
```
belforte@aiadm01/~> openstack server add volume vocmscrabsc901 vocmscrabsc901_standard
+-----------+--------------------------------------+
| Field     | Value                                |
+-----------+--------------------------------------+
| ID        | e902ab68-4ae2-4c50-bc22-f15f2658f7e9 |
| Server ID | 3284eef7-9089-4ef8-a43e-58c49cd9d434 |
| Volume ID | e902ab68-4ae2-4c50-bc22-f15f2658f7e9 |
| Device    | /dev/vdb                             |
| Tag       | None                                 |
+-----------+--------------------------------------+
belforte@aiadm01/~> openstack server add volume vocmscrabsc901 vocmscrabsc901_high
+-----------+--------------------------------------+
| Field     | Value                                |
+-----------+--------------------------------------+
| ID        | 99e38bcc-3bc2-4fb1-9e17-6be2c6404521 |
| Server ID | 3284eef7-9089-4ef8-a43e-58c49cd9d434 |
| Volume ID | 99e38bcc-3bc2-4fb1-9e17-6be2c6404521 |
| Device    | /dev/vdc                             |
| Tag       | None                                 |
+-----------+--------------------------------------+
belforte@aiadm01/~> 
```
## on the new VM
```txt
[root@vocmscrabsc901 ~]#  mkfs -t ext4 -L  STANDARD-VOLUME /dev/vdb
[root@vocmscrabsc901 ~]#  mkfs -t ext4 -L  HIGH-IO-VOLUME /dev/vdc
```
then 
```bash
[root@vocmscrabsc901 ~]# blkid
/dev/vda15: LABEL_FATBOOT="EFI" LABEL="EFI" UUID="F431-B807" TYPE="vfat" PARTLABEL="EFI System Partition" PARTUUID="0fe360e1-d097-411b-b588-d6152b7dcae1"
/dev/vda1: LABEL="ROOT" UUID="f624d517-bc90-41c8-b3f9-5a8875765e8b" TYPE="xfs" PARTUUID="f99a890c-1956-4ff5-aaf3-4050e44a7f18"
/dev/vdb: LABEL="STANDARD-VOLUME" UUID="8852b5b1-532f-45a5-8471-0b002e8deb10" TYPE="ext4"
/dev/vdc: LABEL="HIGH-IO-VOLUME" UUID="294fdaf1-9b74-431a-b96c-8bb7321c8f6e" TYPE="ext4"
/dev/vda14: PARTUUID="b661b502-d81f-4ad4-8981-14cd9e38ef95"
[root@vocmscrabsc901 ~]# 
```
Note the UUID of the volumes

## in gitlab
 add those volumens in `code/manifests/modules/volumes.pp`. Should be pretty easy to follow the example of existing nodes.
 * Mount STANDARD-VOLUME as `/home/grid` and HIGH-IO-VOLUME as `/data`

