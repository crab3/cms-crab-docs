## Automatic Schedd Restart

We want to be able to reboot our schedulers every six months or so in order to 
start using new kernels and other security updates.

The main tool that this feature will rely on it called `roger` and is provided
by CERN IT. You can read about it in the [config docs](https://configdocs.web.cern.ch/state/index.html) 
and in Nikos's presentation at an O&C weekly 
[meetings](https://indico.cern.ch/event/1267808/contributions/5324574/attachments/2616286/4522008/From%20IT%20with%20Love.pdf).

`roger` allows you to manage the state of a VM. You can access the state of
a VM from the file `/etc/roger/curremt.yaml`.

??? note "example: aiadm"

    ```plaintext
    > [14:01] aiadm99 ~
    > cat /etc/roger/current.yaml
    app_alarmed: true
    appstate: production
    expires: ''
    hw_alarmed: true
    message: In Service
    nc_alarmed: true
    os_alarmed: true
    update_time: '1722922202'
    ```

You can setup a cronjob that uses the roger cli to change the state of a VM
once every six monthx and changes the state from for example "production" to
"drain", then you can setup another daily cronjob to set the schedd in drain
for a week or two, then reboot it. (dario knows that the previous sentence
is vague, the details will need to be sorted out. maybe there is a condor cli
command to put a schedd in drain, maybe we can automatically check the schedds
roger status and update our TW configuration).

References

- how lxplus VMs are automatically rebooted: [lxplus puppet](https://gitlab.cern.ch/ai/it-puppet-hostgroup-lxplus/-/blob/qa/code/manifests/private/graceful.pp?ref_type=heads)
- how lxbatch bigbirdXX.cern.ch schedulers are automatically rebooted:
  references not found yet

