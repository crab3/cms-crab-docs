# CRAB Database - developer

This page will focus on the database from the **developer** point of view.
The main focus here is personal database creation, table creation, destroy and update.

We use Oracle Database to store the task state in the "tasks" table,
the file metadata in the "FILEMETADATA" table and the transfers state in the
"FILETRANSFERSDB". The partitions from the last two tables are dropped after 90 days.

CERN IT DBA can roll back our schemas at any point in time in the last six months. 
The only technicality that we should keep in mind is that they prefer to restore
an old status of our schema not to the current schema but to a new temporary one.

## Setup env

*Note: In case Oracle command is not working, please consult <https://cern.service-now.com/service-portal?id=kb_article&n=KB0001167>*

!!!note
    The following steps are only tested and guaranteed to work on **LXPLUS8** machines, python 3.6.

1. Setup env (wa)
    1. Clone CRABServer and WMCore to working directory.
        ```
        git clone -b 2.1.7 https://github.com/dmwm/WMCore
        git clone -b master https://github.com/dmwm/CRABServer
        ```
    1. Install dependecies
        ```
        python3 -m pip install --user future
        ```
    1. Export `PYTHONPATH`
        ```
        export PYTHONPATH=./CRABServer/src/python/:./WMCore/src/python/:$PYTHONPATH
        ```
    1. Edit `WMCore/bin/wmcore-db-init` and change shebang from  `#! /usr/bin/env python` to `#! /usr/bin/env python3`

    1. Run following command:
       ```
	   export ORACLE_CERN=/afs/cern.ch/project/oracle
	   . $ORACLE_CERN/script/profile_oracle.sh
       setoraenv -s 19090
	   ```

2. Create a configuration for connecting to the Oracle database (in this guide, `dbconfig.py`).

    ```python
    from WMCore.Configuration import Configuration
    config = Configuration()
    config.section_('CoreDatabase')
    config.CoreDatabase.connectUrl = 'oracle://<oracle_username>:<oracle_password>@devdb11'
    ```

## Create Tables and indexes

Run following commmand

```bash
./WMCore/bin/wmcore-db-init --config ./dbconfig.py --create --modules=Databases.TaskDB,Databases.FileMetaDataDB,Databases.FileTransfersDB
```

If `default database connection tested` appear, at least script can connect to database.

??? example "example of output is everything works well"

    ```plaintext
    > ./WMCore/bin/wmcore-db-init --config ./oracle-db-mgmt/dbconfig.py --create --modules=Databases.TaskDB,Databases.FileMetaDataDB,Databases.FileTransfersDB
    checking default database connection
    DEBUG:root:Log file ready
    DEBUG:root:Using SQLAlchemy v.1.3.2
    INFO:root:Instantiating base WM DBInterface
    default database connection tested
    DEBUG:root:Tables for Databases.TaskDB created
    DEBUG:root:Tables for Databases.FileMetaDataDB created
    DEBUG:root:Tables for Databases.FileTransfersDB created
    ```


!!! warning
    If the output is not the expected one, something is wrong. For example, if it contains the following error logs it means that it failed to create some indexes. Upgrade dmwm/CRABServer to the latest version, drop all tables and try again
    ??? note
        ```
        [tseethon@lxplus8s05 rest_oracle]$ ./WMCore/bin/wmcore-db-init --config ./dbconfig.py --create --modules=Databases.TaskDB,Databases.FileMetaDataDB,Databases.FileTransfersDB
        checking default database connection
        DEBUG:root:Log file ready
        DEBUG:root:Using SQLAlchemy v.1.3.2
        INFO:root:Instantiating base WM DBInterface
        default database connection tested
        DEBUG:root:Tables for Databases.TaskDB created
        DEBUG:root:Tables for Databases.FileMetaDataDB created
        DEBUG:root:Problem creating database table

        CREATE INDEX TM_WORKER_STATE ON FILETRANSFERSDB (TM_ASO_WORKER, TM_TRANSFER_STATE) COMPRESS 2

        (cx_Oracle.DatabaseError) ORA-00955: name is already used by an existing object
        [SQL: CREATE INDEX TM_WORKER_STATE ON FILETRANSFERSDB (TM_ASO_WORKER, TM_TRANSFER_STATE) COMPRESS 2]
        (Background on this error at: http://sqlalche.me/e/4xp6)
        Traceback (most recent call last):
          File "/usr/lib64/python3.6/site-packages/sqlalchemy/engine/base.py", line 1244, in _execute_context
            cursor, statement, parameters, context
          File "/usr/lib64/python3.6/site-packages/sqlalchemy/engine/default.py", line 552, in do_execute
            cursor.execute(statement, parameters)
        cx_Oracle.DatabaseError: ORA-00955: name is already used by an existing object

        The above exception was the direct cause of the following exception:

        Traceback (most recent call last):
          File "/afs/cern.ch/user/t/tseethon/rest_oracle/WMCore/src/python/WMCore/Database/DBCreator.py", line 57, in execute
            transaction = transaction)
          File "/afs/cern.ch/user/t/tseethon/rest_oracle/WMCore/src/python/WMCore/Database/DBCore.py", line 149, in processData
            returnCursor=returnCursor)
          File "/afs/cern.ch/user/t/tseethon/rest_oracle/WMCore/src/python/WMCore/Database/DBCore.py", line 62, in executebinds
            resultProxy = connection.execute(s)
          File "/usr/lib64/python3.6/site-packages/sqlalchemy/engine/base.py", line 982, in execute
            return self._execute_text(object_, multiparams, params)
          File "/usr/lib64/python3.6/site-packages/sqlalchemy/engine/base.py", line 1155, in _execute_text
            parameters,
          File "/usr/lib64/python3.6/site-packages/sqlalchemy/engine/base.py", line 1248, in _execute_context
            e, statement, parameters, cursor, context
          File "/usr/lib64/python3.6/site-packages/sqlalchemy/engine/base.py", line 1466, in _handle_dbapi_exception
            util.raise_from_cause(sqlalchemy_exception, exc_info)
          File "/usr/lib64/python3.6/site-packages/sqlalchemy/util/compat.py", line 383, in raise_from_cause
            reraise(type(exception), exception, tb=exc_tb, cause=cause)
          File "/usr/lib64/python3.6/site-packages/sqlalchemy/util/compat.py", line 128, in reraise
            raise value.with_traceback(tb)
          File "/usr/lib64/python3.6/site-packages/sqlalchemy/engine/base.py", line 1244, in _execute_context
            cursor, statement, parameters, context
          File "/usr/lib64/python3.6/site-packages/sqlalchemy/engine/default.py", line 552, in do_execute
            cursor.execute(statement, parameters)
        sqlalchemy.exc.DatabaseError: (cx_Oracle.DatabaseError) ORA-00955: name is already used by an existing object
        [SQL: CREATE INDEX TM_WORKER_STATE ON FILETRANSFERSDB (TM_ASO_WORKER, TM_TRANSFER_STATE) COMPRESS 2]
        (Background on this error at: http://sqlalche.me/e/4xp6)

        During handling of the above exception, another exception occurred:

        Traceback (most recent call last):
          File "./WMCore/bin/wmcore-db-init", line 159, in <module>
            create(cfgObject)
          File "./WMCore/bin/wmcore-db-init", line 134, in create
            wmInit.setSchema(modules, params = params)
          File "/afs/cern.ch/user/t/tseethon/rest_oracle/WMCore/src/python/WMCore/WMInit.py", line 168, in setSchema
            transaction=myThread.transaction)
          File "/afs/cern.ch/user/t/tseethon/rest_oracle/WMCore/src/python/WMCore/Database/DBCreator.py", line 62, in execute
            raise WMException(msg,'WMCORE-2')
        WMCore.WMException.WMException: <@========== WMException Start ==========@>
        Exception Class: WMException
        Message: Problem creating database table

        CREATE INDEX TM_WORKER_STATE ON FILETRANSFERSDB (TM_ASO_WORKER, TM_TRANSFER_STATE) COMPRESS 2

        (cx_Oracle.DatabaseError) ORA-00955: name is already used by an existing object
        [SQL: CREATE INDEX TM_WORKER_STATE ON FILETRANSFERSDB (TM_ASO_WORKER, TM_TRANSFER_STATE) COMPRESS 2]
        (Background on this error at: http://sqlalche.me/e/4xp6)
                ClassName : None
                ModuleName : WMCore.Database.DBCreator
                MethodName : execute
                ClassInstance : None
                FileName : /afs/cern.ch/user/t/tseethon/rest_oracle/WMCore/src/python/WMCore/Database/DBCreator.py
                LineNumber : 62
                ErrorNr : WMCORE-2

        Traceback:
          File "/afs/cern.ch/user/t/tseethon/rest_oracle/WMCore/src/python/WMCore/Database/DBCreator.py", line 57, in execute
            transaction = transaction)

          File "/afs/cern.ch/user/t/tseethon/rest_oracle/WMCore/src/python/WMCore/Database/DBCore.py", line 149, in processData
            returnCursor=returnCursor)

          File "/afs/cern.ch/user/t/tseethon/rest_oracle/WMCore/src/python/WMCore/Database/DBCore.py", line 62, in executebinds
            resultProxy = connection.execute(s)

          File "/usr/lib64/python3.6/site-packages/sqlalchemy/engine/base.py", line 982, in execute
            return self._execute_text(object_, multiparams, params)

          File "/usr/lib64/python3.6/site-packages/sqlalchemy/engine/base.py", line 1155, in _execute_text
            parameters,

          File "/usr/lib64/python3.6/site-packages/sqlalchemy/engine/base.py", line 1248, in _execute_context
            e, statement, parameters, cursor, context

          File "/usr/lib64/python3.6/site-packages/sqlalchemy/engine/base.py", line 1466, in _handle_dbapi_exception
            util.raise_from_cause(sqlalchemy_exception, exc_info)

          File "/usr/lib64/python3.6/site-packages/sqlalchemy/util/compat.py", line 383, in raise_from_cause
            reraise(type(exception), exception, tb=exc_tb, cause=cause)

          File "/usr/lib64/python3.6/site-packages/sqlalchemy/util/compat.py", line 128, in reraise
            raise value.with_traceback(tb)

          File "/usr/lib64/python3.6/site-packages/sqlalchemy/engine/base.py", line 1244, in _execute_context
            cursor, statement, parameters, context

          File "/usr/lib64/python3.6/site-packages/sqlalchemy/engine/default.py", line 552, in do_execute
            cursor.execute(statement, parameters)

        <@---------- WMException End ----------@>
        ```
    In case you are not interested in the indexes, you can check if tables exist in db using sqlplus, i.e. `SELECT * FROM TASKS` should not return any error.

## Destroy Table

To scratch the database you will need to run the following commands:

```bash
./WMCore/bin/wmcore-db-init --config /data/dbconfig.py --destroy --modules=Databases.TaskDB,Databases.FileMetaDataDB,Databases.FileTransfersDB
```

Or directly from lxplus with

```sql
drop table FILEMETADATA cascade constraints;
drop table FILETRANSFERSDB cascade constraints;
drop table TASKS cascade constraints;
```

## Update Table

To update the database when new code requires new columns in Task data base, the DB table needs to be modified by hand. The SQL commands to execute are added to [updateOracle.sql](https://github.com/dmwm/CRABServer/blob/master/etc/updateOracle.sql) by the developer who introduces the DB changes. Those command can be e.g. copied and pasted to the `sqlplus` on lxplus.

??? note "Output example"
    ```
    belforte@lxplus0067/ISTRUZ> sqlplus crab3_belforte/c3s-1705@devdb11

    SQL*Plus: Release 11.2.0.3.0 Production on Fri Jul 24 11:59:04 2015

    Copyright (c) 1982, 2011, Oracle.  All rights reserved.

    Enter password:

    Connected to:
    Oracle Database 11g Enterprise Edition Release 11.2.0.4.0 - 64bit Production
    With the Partitioning, Real Application Clusters, OLAP, Data Mining
    and Real Application Testing options

    SP2-0310: unable to open file "?/sqlplus/admin/glogin.sql"
    SQL> select table_name from user_tables;

    TABLE_NAME
    ------------------------------
    TASKS
    FILEMETADATA
    FILETRANSFERSDB
    JOBGROUPS

    SQL>
    SQL> alter table tasks add (tm_user_files CLOB DEFAULT '[]');
    alter table tasks add (tm_user_files CLOB DEFAULT '[]')
                           *
    ERROR at line 1:
    ORA-01430: column being added already exists in table
    [EDITOR NOTE: IT IS OK TO TRY TO ADD A  COLUMN WHICH IS THERE ALREADY]

    SQL> alter table tasks add (tm_publish_groupname VARCHAR(1) DEFAULT 'F');

    Table altered.

    SQL> alter table tasks add constraint check_tm_publish_groupname check (tm_publish_groupname IN ('T', 'F')) ENABLE;

    Table altered.

    SQL> alter table tasks add (tm_nonvalid_input_dataset VARCHAR(1) DEFAULT 'T');
    alter table tasks add constraint ck_tm_nonvalid_input_dataset check (tm_nonvalid_input_dataset IN ('T', 'F')) ENABLE;

    Table altered.

    SQL>
    Table altered.

    SQL> quit
    Disconnected from Oracle Database 11g Enterprise Edition Release 11.2.0.4.0 - 64bit Production
    With the Partitioning, Real Application Clusters, OLAP, Data Mining
    and Real Application Testing options
    belforte@lxplus0067/ISTRUZ>
    ```

## Useful SQL commands

```text
/* list tables */
select table_name from user_tables;
/* list columsn in a table */
describe filemetadata;
/* count rows in a tale */
select count(*) from tasks; 
/* exit sql */
quit
```


