# CRAB Oracle database - administration

This document will focus on the database from the administration point of view.

The main focus here is which tables are available in which DB, which indexes,
which jobs, procedures and functions, ...

## Databases

See the [Inventory](../../inventory/crab-inventory.md#oracle-dbs)

## Administration tools

(TODO. dario will fill this section soon)

- [x] instructions on how to connect to an oracle db with dbeaver
- [x] instructions on how to connect to an oracle db with rlwrap and sqlplus
- [ ] instructions, section by sections, on how to retrieve the information mentioned

### CLI - sqlplus

The most simple option to connect to an Oracle DB at cern is to use `sqlplus` from lxplus.

First of all, ssh into lxplus (lxplus9 works as well), then set the proper env variables with

```bash
export ORACLE_CERN=/afs/cern.ch/project/oracle
. $ORACLE_CERN/script/profile_oracle.sh
setoraenv -s 19090 # client version
```

then connect to the desired DB with

```bash
rlwrap sqlplus ${username}@${DBNAME}

# PROD, admin, risky!
rlwrap sqlplus cms_analysis_reqmgr@cmsr
# PROD, read/select, safer, without write permissions
rlwrap sqlplus cms_analysis_reqmgr_r@cmsr

#PREPROD
rlwrap sqlplus cmsweb_analysis_preprod@int2r
```

!!! note

    When using Oracle DB, a "user" is usually called "schema". This can be confusing!

sqlplus output is often not properly formatted and can be tricky to read.
We recommend setting the output format to csv, it is more readable

```sql
set markup csv on
```

If you do not like csv output, then an alternative is to increase the linesize and disable line wrapping

```sql
set linesize 200;
set wrap off;
```

It is possible to save to a file all the queries and results with

```sql
spool myfile.txt
-- queries ...
spool off
```

!!! note

    beware, the file is only written to disk when you execute the command `spool off`

??? example "example: spool to csv"

    ```sql
    set markup csv on
    spool myfile.csv
    select * from tasks order by tm_taskname desc fetch first 5 rows only;
    spool off
    ```

### GUI - DBeaver

There are multiple GUI tools to connect to an oracle database. Wa and Dario think that DBeaver suits their needs so far.

!!! note
    We do not recommend to make any admin change via the GUI. We suggest that the modifications
    are tracked in a text file in dmwm/CRABServer.

    However it is convinient to use the gui for quick select statements, for checking which procedures are installed and so on.

In order to connect with dbeaver v22 to oracle:

- click on "new connection", type oracle
- in the tab "main", under connection type select the tab "TNS".
    - from lxplus, copy the file `/afs/cern.ch/project/oracle/admin/tnsnames.ora` to your laptop
    - add the path of the directory where "tnsname.ora" file is located.
- after it loads the files in the directory, there the dropdown list "Network Alias" gets filled.
- for preprod and dev schemas/databases
    - select `int2r`. it is lowercase letters, lo scroll down a bit to find it
    - then put username and password (for example `cmsweb_crab_9r_dev` and its password)
    - then browse "schemas", "cmsweb_crab_9r_dev", tables
- for prod schema/database
    - select `CMSR`.
    - then put username and password (for example `cms_analysis_reqmgr_r` and its password)
    - then browse "schemas", "cms_analysis_reqmgr" (since "cms_analysis_reqmgr_r" is empty!)
- a new page open, you can select here the table you want, it will show you its sql schema (list of columns and their types)
- if you click on the "data" tab, it will show you the first 200lines.

You can now access tables, indexes, jobs, procedures, functions.

The oracle databases can be accesses only from cern network. Since CERN does not provide with a VPN, an alternative is to
use an ssh tunnel.

#### option 1: all dns through the tunnel

A nice option is to use sshuttle

```plaintext
sshuttle --dns -NHr lxplus8 10.0.0.0/8

## achtung: If you have other VPNs active, then you can experience problems with DNS.
```

If you can resolve the domain `int2r-s.cern.ch` you should be good to go

```bash
dig int2r-s.cern.ch
```

#### option 2: only cern.ch through the tunnel

alternative: allows to resolve dns for oracle db and for any other local service you have in your lan

- get ip of an internal dns at cern
    - ssh into lxplus
    - `nslookup nodebug=off lxplus.cern.ch`,
    - in the result, you will find something like `ip-dns-1.cern.ch`
    - then `dig ip-dns-1.cern.ch`, take its IP
- start ssh tunnel
    - on the laptop, start the tunnel with `sshuttle -NHr lxplus9 --ns-hosts=137.138.16.5 10.0.0.0/8`
    - do not use `--dns`, use `--ns-hosts=...` instead. this means that we will use a specific ip for the requests that go into the tunnel
- laptop: change `systemd-resolved` to
    - specify that connections to a specific domain (`cern.ch`) need to use a specific dns, not the usual default one
    - add to ` /etc/systemd/resolved.conf`

    ```plaintext
    [Resolve]
    DNS=137.138.16.5
    Domains=~cern.ch
    ```

    - restart systemd-resolv ` sudo systemctl restart systemd-resolved`

    - when you are done, reset this config

ref:

- [https://github.com/sshuttle/sshuttle/issues/153](https://github.com/sshuttle/sshuttle/issues/153)
- [https://www.baeldung.com/linux/specific-dns-for-specific-domain](https://www.baeldung.com/linux/specific-dns-for-specific-domain)
- [https://www.ibm.com/docs/en/zos/2.4.0?topic=command-nslookup-options](https://www.ibm.com/docs/en/zos/2.4.0?topic=command-nslookup-options)

### GUI - Oracle SQLDeveloper

Another option for a GUI is the oracle SQL Developer,
available at [https://www.oracle.com/database/sqldeveloper/](https://www.oracle.com/database/sqldeveloper/).
Similarly to DBeaver, it requires the TNS file from lxplus and the sshuttle tunnel if
you want to connect from outside the cern network.

Then the GUI is pretty simple to setup.

## Tables

CRAB revolves around three tables: `TASKS`, `FILEMETADATA`, `FILETRANSFERSDB`, that you can list with

```sql
select table_name from user_tables;
```

??? example "example: list tables in dmapelli@int2r"
    ```plaintext
    SQL> select table_name from user_tables;

    "TABLE_NAME"
    "FILEMETADATA"
    "FILETRANSFERSDB"
    "TASKS"
    ```

### inventory

(TODO)

prod db

- [x] remove TASKS_INT from prod db
- [x] remove job_groups from prod DB
    - [x] make sure that the trigger `jobgroups_id_trg` is deleted (yes, i dropped the table with `CASCADE CONSTRAINTS`)
- [x] remove CMP3$10027924 from prod db

preprod db

- [x] remove filemetadata_int
- [x] remove filemetadata_save
- [x] remove jobgroups
- [x] remove prajeshtest
- [x] remove prajeshtest2
- [x] remove prajesh_test
- [x] remove CMP3$14630427

### check size of current tables and indexes

You can get the size in bytes and number of rows of table partitions with

```sql
SELECT
    s.segment_name,
    sum(s.bytes),
    sum(t.num_rows)
FROM user_segments s, user_tab_partitions t
WHERE 1=1
AND s.segment_type='TABLE PARTITION'
AND s.segment_name=t.table_name
AND s.partition_name=t.partition_name
group by s.segment_name, t.table_name
;
```

You can get the number of rows per partition with:

```sql
SELECT
    t.table_name,
    t.partition_name,
    t.num_rows
FROM dba_tab_partitions t
WHERE 1=1
AND table_owner = 'TABLE_OWNER'
;

```

and you can get the size in bytes and number of rows of normal tables
(not-partitioned) with

```sql
SELECT t.table_name AS table_name,
    t.num_rows AS nrows, s.bytes AS table_size
FROM user_segments s, user_tables t
WHERE s.segment_name=t.table_name
AND s.segment_type='TABLE'
;
```

You can get the index size with

```sql
SQL> set markup csv on
SELECT
    i.table_name,
    i.index_name,
    s.bytes
FROM user_segments s, user_indexes i
WHERE s.segment_name=i.index_name
AND s.segment_type='INDEX'
;
```

Keep in mind that you can access the `user_*` tables with your usual schema on
`preprod`, `dev`, `devtwo` and `devthree`.
However, on prod you can access the information via the
`cms_analysis_reqmgr @ cmsr` schema accessing the `user_*` tables,
or you can `cms_analysis_reqmgr_r @ cmsr` schema accessing `dba_*` tables.
We needed to ask Oracle support (kate) for the `_r` schema to have access to
the `dba_*` tables, it is not enabled by default.

??? example "example: table size and number of rows in prod"

    Using `cms_analysis_reqmgr @ cmsr`

    ```sql
    SELECT
        s.segment_name,
        sum(s.bytes),
        sum(t.num_rows)
    FROM user_segments s, user_tab_partitions t
    WHERE 1=1
    AND s.segment_type='TABLE PARTITION'
    AND s.segment_name=t.table_name
    AND s.partition_name=t.partition_name
    group by s.segment_name, t.table_name
    ;
    ```

    ```plaintext
    "SEGMENT_NAME","SUM(S.BYTES)","SUM(T.NUM_ROWS)"
    "TASKS",1.2824E+10,4365372
    "FILETRANSFERSDB",7.7534E+10,104012978
    "FILEMETADATA",2.3814E+11,212872165
    ```

??? example "example: indexes size in prod"

    ```sql
    SQL> set markup csv on
    SELECT
        i.table_name,
        i.index_name,
        s.bytes
    FROM user_segments s, user_indexes i
    WHERE s.segment_name=i.index_name
    AND s.segment_type='INDEX'
    ;
    ```

    ```plaintext
    "TABLE_NAME","INDEX_NAME","BYTES"
    "FILETRANSFERSDB","FILETRANSFERSDB_NEW2",2063597568
    "FILETRANSFERSDB","ID_PK",1.0118E+10
    "JOBGROUPS","JOBGROUP_ID_PK",65536
    "FILEMETADATA","PK_TASKLFN_PART_DBMS",7.1581E+10
    "TASKS","TASKNAME_PK",503316480
    "TASKS_INT","TMP$$_TASKNAME_PK0",226492416
    "FILETRANSFERSDB","TM_TASKNAME_IDX",1.1698E+10
    "FILETRANSFERSDB","TM_WORKER_PUB_STATE",3422552064
    "FILETRANSFERSDB","TM_WORKER_STATE",2043674624
    ```

### cleanups - hot to properly drop a table

use

```sql
DROP TABLE <table name> CASCADE CONSTRAINTS;
```

??? example "example: dropped tables in prod and preprod databases in march 2024"

    prod

    ```plaintext
    -- DROP TABLE CMS_ANALYSIS_REQMGR.TASKS_INT CASCADE CONSTRAINTS;
    -- DROP TABLE CMS_ANALYSIS_REQMGR.JOBGROUPS CASCADE CONSTRAINTS;
    -- DROP TABLE CMS_ANALYSIS_REQMGR.CMP3$10027924 CASCADE CONSTRAINTS;
    ```

    preprod (yeah, dario dropped these tables before learning about the `cascade constraints` argument, he is sorry :) )

    ```plaintext
    -- drop table FILEMETADATA_SAVE ;
    -- drop table  "CMP3$14630427" ;
    -- drop table  "FILEMETADATA_INT" ;
    -- drop table  "JOBGROUPS" ;
    -- drop table  "PRAJESHTEST" ;
    -- drop table  "PRAJESHTEST2" ;
    -- drop table  "PRAJESH_TEST" ;
    ```

## Indexes

TODO, Actions

- [x] fix dmwm/CRABServer so that it creates the proper indexes when creating the table
    - [ ] check if it is true that primary key constraints already create an index in oracle!
- [ ] create on preprod the same indexes as on prod

### how to retrieve information about the indexes

You can access information about existing indexes in Oracle mainly from two tables:

- `all_indexes`
- `all_ind_columns`

and you can filter the result using the column `table_owner`.

??? example "example: get indexes in preprod"

    ```sql
    SQL> select * from all_indexes where table_owner = 'CMSWEB_ANALYSIS_PREPROD';
    ```

    ```plaintext
    "OWNER","INDEX_NAME","INDEX_TYPE","TABLE_OWNER","TABLE_NAME","TABLE_TYPE","UNIQUENESS","COMPRESSION","PREFIX_LENGTH","TABLESPACE_NAME","INI_TRANS","MAX_TRANS","INITIAL_EXTENT","NEXT_EXTENT","MIN_EXTENTS","MAX_EXTENTS","PCT_INCREASE","PCT_THRESHOLD","INCLUDE_COLUMN","FREELISTS","FREELIST_GROUPS","PCT_FREE","LOGGING","BLEVEL","LEAF_BLOCKS","DISTINCT_KEYS","AVG_LEAF_BLOCKS_PER_KEY","AVG_DATA_BLOCKS_PER_KEY","CLUSTERING_FACTOR","STATUS","NUM_ROWS","SAMPLE_SIZE","LAST_ANALYZED","DEGREE","INSTANCES","PARTITIONED","TEMPORARY","GENERATED","SECONDARY","BUFFER_POOL","FLASH_CACHE","CELL_FLASH_CACHE","USER_STATS","DURATION","PCT_DIRECT_ACCESS","ITYP_OWNER","ITYP_NAME","PARAMETERS","GLOBAL_STATS","DOMIDX_STATUS","DOMIDX_OPSTATUS","FUNCIDX_STATUS","JOIN_INDEX","IOT_REDUNDANT_PKEY_ELIM","DROPPED","VISIBILITY","DOMIDX_MANAGEMENT","SEGMENT_CREATED","ORPHANED_ENTRIES","INDEXING","AUTO","CONSTRAINT_INDEX"
    "CMSWEB_ANALYSIS_PREPROD","PK_TASKLFN_PART_DBMS","NORMAL","CMSWEB_ANALYSIS_PREPROD","FILEMETADATA","TABLE","UNIQUE","DISABLED",,"CMSWEB_ANALYSIS_PREPROD_DATA",2,255,65536,1048576,1,2147483645,,,,,,10,"YES",4,192510,3482873,1,1,3402267,"VALID",3482873,3482873,"25-DEC-2022","1","1","NO","N","N","N","DEFAULT","DEFAULT","DEFAULT","NO",,,,,,"YES",,,,"NO","NO","NO","VISIBLE",,"YES","NO","FULL","NO","NO"
    "CMSWEB_ANALYSIS_PREPROD","PK_TASKLFN_PART","NORMAL","CMSWEB_ANALYSIS_PREPROD","FILEMETADATA_INT","TABLE","UNIQUE","DISABLED",,"CMSWEB_ANALYSIS_PREPROD_DATA",2,255,65536,1048576,1,2147483645,,,,,,10,"YES",3,32483,911932,1,1,608452,"VALID",911932,31218,"21-NOV-2017","1","1","NO","N","N","N","DEFAULT","DEFAULT","DEFAULT","NO",,,,,,"YES",,,,"NO","NO","NO","VISIBLE",,"YES","NO","FULL","NO","NO"
    "CMSWEB_ANALYSIS_PREPROD","PK_TASKLFN","NORMAL","CMSWEB_ANALYSIS_PREPROD","FILEMETADATA_SAVE","TABLE","UNIQUE","DISABLED",,"CMSWEB_ANALYSIS_PREPROD_DATA",2,255,65536,1048576,1,2147483645,,,,,,10,"YES",4,213317,7763516,1,1,7592400,"VALID",7763516,7763516,"18-MAY-2021","1","1","NO","N","N","N","DEFAULT","DEFAULT","DEFAULT","NO",,,,,,"YES",,,,"NO","NO","NO","VISIBLE",,"YES","NO","FULL","NO","NO"
    "CMSWEB_ANALYSIS_PREPROD","I_ASO_WORKER_PUBLICATION_STATE","NORMAL","CMSWEB_ANALYSIS_PREPROD","FILETRANSFERSDB","TABLE","NONUNIQUE","ENABLED",2,"CMSWEB_ANALYSIS_PREPROD_DATA",2,255,65536,1048576,1,2147483645,,,,,,10,"YES",2,2372,6,395,18932,113597,"VALID",1071346,1071346,"16-MAR-2023","1","1","NO","N","N","N","DEFAULT","DEFAULT","DEFAULT","NO",,,,,,"YES",,,,"NO","NO","NO","VISIBLE",,"YES","NO","FULL","NO","NO"
    "CMSWEB_ANALYSIS_PREPROD","TM_WORKER_STATE","NORMAL","CMSWEB_ANALYSIS_PREPROD","FILETRANSFERSDB","TABLE","NONUNIQUE","ENABLED",2,"CMSWEB_ANALYSIS_PREPROD_DATA",2,255,65536,1048576,1,2147483645,,,,,,10,"YES",2,3264,8,408,14037,112296,"VALID",1071346,1071346,"16-MAR-2023","1","1","NO","N","N","N","DEFAULT","DEFAULT","DEFAULT","NO",,,,,,"YES",,,,"NO","NO","NO","VISIBLE",,"YES","NO","FULL","NO","NO"
    "CMSWEB_ANALYSIS_PREPROD","TM_USERNAME_IDX","NORMAL","CMSWEB_ANALYSIS_PREPROD","FILETRANSFERSDB","TABLE","NONUNIQUE","DISABLED",,"CMSWEB_ANALYSIS_PREPROD_DATA",2,255,65536,1048576,1,2147483645,,,,,,10,"YES",2,3674,61,60,1925,117448,"VALID",1071346,1071346,"16-MAR-2023","1","1","NO","N","N","N","DEFAULT","DEFAULT","DEFAULT","NO",,,,,,"YES",,,,"NO","NO","NO","VISIBLE",,"YES","NO","FULL","NO","NO"
    "CMSWEB_ANALYSIS_PREPROD","ID_PK","NORMAL","CMSWEB_ANALYSIS_PREPROD","FILETRANSFERSDB","TABLE","UNIQUE","DISABLED",,"CMSWEB_ANALYSIS_PREPROD_DATA",2,255,65536,1048576,1,2147483645,,,,,,10,"YES",2,57085,1071346,1,1,1071336,"VALID",1071346,1071346,"16-MAR-2023","1","1","NO","N","N","N","DEFAULT","DEFAULT","DEFAULT","NO",,,,,,"YES",,,,"NO","NO","NO","VISIBLE",,"YES","NO","FULL","NO","NO"
    "CMSWEB_ANALYSIS_PREPROD","TM_TASKNAME_IDX","NORMAL","CMSWEB_ANALYSIS_PREPROD","FILETRANSFERSDB","TABLE","NONUNIQUE","DISABLED",,"CMSWEB_ANALYSIS_PREPROD_DATA",2,255,65536,1048576,1,2147483645,,,,,,10,"YES",3,23764,8003,2,43,345108,"VALID",1071346,1071346,"16-MAR-2023","1","1","NO","N","N","N","DEFAULT","DEFAULT","DEFAULT","NO",,,,,,"YES",,,,"NO","NO","NO","VISIBLE",,"YES","NO","FULL","NO","NO"
    "CMSWEB_ANALYSIS_PREPROD","JOBGROUP_ID_PK","NORMAL","CMSWEB_ANALYSIS_PREPROD","JOBGROUPS","TABLE","UNIQUE","DISABLED",,"CMSWEB_ANALYSIS_PREPROD_DATA",2,255,65536,1048576,1,2147483645,,,,,,10,"YES",0,1,241,1,1,183,"VALID",241,241,"18-MAY-2021","1","1","NO","N","N","N","DEFAULT","DEFAULT","DEFAULT","NO",,,,,,"YES",,,,"NO","NO","NO","VISIBLE",,"YES","NO","FULL","NO","NO"
    "CMSWEB_ANALYSIS_PREPROD","TASKNAME_PK","NORMAL","CMSWEB_ANALYSIS_PREPROD","TASKS","TABLE","UNIQUE","DISABLED",,"CMSWEB_ANALYSIS_PREPROD_DATA",2,255,65536,1048576,1,2147483645,,,,,,10,"YES",2,675,66798,1,1,51283,"VALID",66798,66798,"02-JAN-2023","1","1","NO","N","N","N","DEFAULT","DEFAULT","DEFAULT","NO",,,,,,"YES",,,,"NO","NO","NO","VISIBLE",,"YES","NO","FULL","NO","NO"

    10 rows selected.
    ```

    ```sql
    select * from all_ind_columns where table_owner = 'CMSWEB_ANALYSIS_PREPROD';
    ```

    ```
    "INDEX_OWNER","INDEX_NAME","TABLE_OWNER","TABLE_NAME","COLUMN_NAME","COLUMN_POSITION","COLUMN_LENGTH","CHAR_LENGTH","DESCEND","COLLATED_COLUMN_ID"
    "CMSWEB_ANALYSIS_PREPROD","PK_TASKLFN_PART_DBMS","CMSWEB_ANALYSIS_PREPROD","FILEMETADATA","TM_TASKNAME",1,255,255,"ASC",
    "CMSWEB_ANALYSIS_PREPROD","PK_TASKLFN_PART_DBMS","CMSWEB_ANALYSIS_PREPROD","FILEMETADATA","FMD_LFN",2,500,500,"ASC",
    "CMSWEB_ANALYSIS_PREPROD","PK_TASKLFN_PART","CMSWEB_ANALYSIS_PREPROD","FILEMETADATA_INT","TM_TASKNAME",1,255,255,"ASC",
    "CMSWEB_ANALYSIS_PREPROD","PK_TASKLFN_PART","CMSWEB_ANALYSIS_PREPROD","FILEMETADATA_INT","FMD_LFN",2,500,500,"ASC",
    "CMSWEB_ANALYSIS_PREPROD","PK_TASKLFN","CMSWEB_ANALYSIS_PREPROD","FILEMETADATA_SAVE","TM_TASKNAME",1,255,255,"ASC",
    "CMSWEB_ANALYSIS_PREPROD","PK_TASKLFN","CMSWEB_ANALYSIS_PREPROD","FILEMETADATA_SAVE","FMD_LFN",2,500,500,"ASC",
    "CMSWEB_ANALYSIS_PREPROD","I_ASO_WORKER_PUBLICATION_STATE","CMSWEB_ANALYSIS_PREPROD","FILETRANSFERSDB","TM_ASO_WORKER",1,100,100,"ASC",
    "CMSWEB_ANALYSIS_PREPROD","I_ASO_WORKER_PUBLICATION_STATE","CMSWEB_ANALYSIS_PREPROD","FILETRANSFERSDB","TM_PUBLICATION_STATE",2,22,0,"ASC",
    "CMSWEB_ANALYSIS_PREPROD","TM_WORKER_STATE","CMSWEB_ANALYSIS_PREPROD","FILETRANSFERSDB","TM_ASO_WORKER",1,100,100,"ASC",
    "CMSWEB_ANALYSIS_PREPROD","TM_WORKER_STATE","CMSWEB_ANALYSIS_PREPROD","FILETRANSFERSDB","TM_TRANSFER_STATE",2,22,0,"ASC",
    "CMSWEB_ANALYSIS_PREPROD","TM_USERNAME_IDX","CMSWEB_ANALYSIS_PREPROD","FILETRANSFERSDB","TM_USERNAME",1,30,30,"ASC",
    "CMSWEB_ANALYSIS_PREPROD","ID_PK","CMSWEB_ANALYSIS_PREPROD","FILETRANSFERSDB","TM_ID",1,60,60,"ASC",
    "CMSWEB_ANALYSIS_PREPROD","TM_TASKNAME_IDX","CMSWEB_ANALYSIS_PREPROD","FILETRANSFERSDB","TM_TASKNAME",1,255,255,"ASC",
    "CMSWEB_ANALYSIS_PREPROD","JOBGROUP_ID_PK","CMSWEB_ANALYSIS_PREPROD","JOBGROUPS","TM_JOBGROUPS_ID",1,22,0,"ASC",
    "CMSWEB_ANALYSIS_PREPROD","TASKNAME_PK","CMSWEB_ANALYSIS_PREPROD","TASKS","TM_TASKNAME",1,255,255,"ASC",

    15 rows selected.
    ```

### Inventory: prod vs preprod

```plaintext
(legenda)

TABLE
# status
INDEX_NAME                       COLUMNS

(contenuto)
FILEMETADATA
# same on prod and preprod
PK_TASKLFN_PART_DBMS             [TM_TASKNAME, FMD_LFN]

TASKS
# same on prod and preprod
TASKNAME_PK                       [TM_TASKNAME]


FILETRANSFERSDB
# same
ID_PK                             [TM_ID]
TM_TASKNAME_IDX                   [TM_TASKNAME]
TM_WORKER_STATE                   [TM_ASO_WORKER, TM_TRANSFER_STATE]

# same on prod and preprod, but different name
TM_WORKER_PUB_STATE               [TM_ASO_WORKER, TM_PUBLICATION_STATE]
I_ASO_WORKER_PUBLICATION_STATE    [TM_ASO_WORKER, TM_PUBLICATION_STATE]

# only prod
FILETRANSFERSDB_NEW2              [TM_PUBLISH, TM_PUBLICATION_STATE, TM_TRANSFER_STATE]
TM_CREATION_TIME_LOCAL_IDX        [TM_CREATION_TIME]

# only preprod
TM_USERNAME_IDX                   [TM_USERNAME]

```

## Table partitions

We have two tables in Oracle DB, FILETRANSFERSDB and FILEMETADATA, whose size quickly grows
over time. We can not afford to store information in those tables indefinitely,
so we need to drop old rows. We need to split our tables into partitions, then drop old partitions.

### Get information about current table partitions

Get the list of current partition policies

```sql
select * from user_part_key_columns where object_type = 'TABLE';
```

??? example "example: partition policies on prod DB"

    If you login with `cms_analysis_reqmgr @ cmsr`, then you can use the table `user_part_key_columns`.
    Otherwise, if you login with `cms_analysis_reqmgr_r @ cmsr`, you need to use a different table:

    ```sql
    select * from all_part_key_columns where owner = 'CMS_ANALYSIS_REQMGR' and object_type = 'TABLE';
    ```

    In any case the result is

    ```
    SQL> select * from all_part_key_columns where owner = 'CMS_ANALYSIS_REQMGR' and object_type = 'TABLE';

    "OWNER","NAME","OBJECT_TYPE","COLUMN_NAME","COLUMN_POSITION","COLLATED_COLUMN_ID"
    "CMS_ANALYSIS_REQMGR","FILEMETADATA","TABLE","FMD_CREATION_TIME",1,
    "CMS_ANALYSIS_REQMGR","FILETRANSFERSDB","TABLE","TM_CREATION_TIME",1,
    "CMS_ANALYSIS_REQMGR","TASKS","TABLE","TM_START_TIME",1,
    ```

Get the list of current partitions in all tables

```SQL
select table_name, partition_name, high_value
from user_tab_partitions
order by table_name, partition_position;

select table_name,partition_name,high_value  from all_tab_partitions where table_owner = 'CMS_ANALYSIS_REQMGR' order by table_name, partition_position;
```

??? example "example: partitions on prod DB"

    Example: partitions on prod DB on 2023-09-12.

    If you login with `cms_analysis_reqmgr @ cmsr`, then you can use the table `user_tab_partitions`.
    Otherwise, if you login with `cms_analysis_reqmgr_r @ cmsr`, you need to use a different table:

    ```sql
    select table_name,partition_name,high_value
    from all_tab_partitions
    where table_owner = 'CMS_ANALYSIS_REQMGR'
    order by table_name, partition_position;
    ```

    In any case, the result is

    ```plaintext
    SQL> set markup csv on
    SQL> select table_name,partition_name,high_value from user_tab_partitions order by table_name, partition_position;

    "TABLE_NAME","PARTITION_NAME","HIGH_VALUE"
    "FILEMETADATA","SYS_P512753","TIMESTAMP' 2023-03-14 00:00:00'
    "FILEMETADATA","SYS_P513484","TIMESTAMP' 2023-04-14 00:00:00'
    "FILEMETADATA","SYS_P514429","TIMESTAMP' 2023-05-14 00:00:00'
    "FILEMETADATA","SYS_P515414","TIMESTAMP' 2023-06-14 00:00:00'
    "FILEMETADATA","SYS_P516245","TIMESTAMP' 2023-07-14 00:00:00'
    "FILEMETADATA","SYS_P517153","TIMESTAMP' 2023-08-14 00:00:00'
    "FILEMETADATA","SYS_P517930","TIMESTAMP' 2023-09-14 00:00:00'
    "FILETRANSFERSDB","SYS_P512338","TIMESTAMP' 2023-03-01 00:00:00'
    "FILETRANSFERSDB","SYS_P513146","TIMESTAMP' 2023-04-01 00:00:00'
    "FILETRANSFERSDB","SYS_P514112","TIMESTAMP' 2023-05-01 00:00:00'
    "FILETRANSFERSDB","SYS_P515033","TIMESTAMP' 2023-06-01 00:00:00'
    "FILETRANSFERSDB","SYS_P516039","TIMESTAMP' 2023-07-01 00:00:00'
    "FILETRANSFERSDB","SYS_P516880","TIMESTAMP' 2023-08-01 00:00:00'
    "FILETRANSFERSDB","SYS_P517543","TIMESTAMP' 2023-09-01 00:00:00'
    "FILETRANSFERSDB","SYS_P518309","TIMESTAMP' 2023-10-01 00:00:00'
    "TASKS","P1","TIMESTAMP' 2017-10-01 00:00:00'
    "TASKS","SYS_P279877","TIMESTAMP' 2017-11-01 00:00:00'
    "TASKS","SYS_P279919","TIMESTAMP' 2017-12-01 00:00:00'
    "TASKS","SYS_P279940","TIMESTAMP' 2018-01-01 00:00:00'
    "TASKS","SYS_P279898","TIMESTAMP' 2018-02-01 00:00:00'
    "TASKS","SYS_P279836","TIMESTAMP' 2018-03-01 00:00:00'
    "TASKS","SYS_P280074","TIMESTAMP' 2018-04-01 00:00:00'
    "TASKS","SYS_P280386","TIMESTAMP' 2018-05-01 00:00:00'
    "TASKS","SYS_P280745","TIMESTAMP' 2018-06-01 00:00:00'
    "TASKS","SYS_P281206","TIMESTAMP' 2018-07-01 00:00:00'
    "TASKS","SYS_P281605","TIMESTAMP' 2018-08-01 00:00:00'
    "TASKS","SYS_P282255","TIMESTAMP' 2018-09-01 00:00:00'
    "TASKS","SYS_P282889","TIMESTAMP' 2018-10-01 00:00:00'
    "TASKS","SYS_P283286","TIMESTAMP' 2018-11-01 00:00:00'
    "TASKS","SYS_P283918","TIMESTAMP' 2018-12-01 00:00:00'
    "TASKS","SYS_P284906","TIMESTAMP' 2019-01-01 00:00:00'
    "TASKS","SYS_P285256","TIMESTAMP' 2019-02-01 00:00:00'
    "TASKS","SYS_P285507","TIMESTAMP' 2019-03-01 00:00:00'
    "TASKS","SYS_P285763","TIMESTAMP' 2019-04-01 00:00:00'
    "TASKS","SYS_P286250","TIMESTAMP' 2019-05-01 00:00:00'
    "TASKS","SYS_P286502","TIMESTAMP' 2019-06-01 00:00:00'
    "TASKS","SYS_P286806","TIMESTAMP' 2019-07-01 00:00:00'
    "TASKS","SYS_P286983","TIMESTAMP' 2019-08-01 00:00:00'
    "TASKS","SYS_P287234","TIMESTAMP' 2019-09-01 00:00:00'
    "TASKS","SYS_P287510","TIMESTAMP' 2019-10-01 00:00:00'
    "TASKS","SYS_P287771","TIMESTAMP' 2019-11-01 00:00:00'
    "TASKS","SYS_P481466","TIMESTAMP' 2019-12-01 00:00:00'
    "TASKS","SYS_P481826","TIMESTAMP' 2020-01-01 00:00:00'
    "TASKS","SYS_P482170","TIMESTAMP' 2020-02-01 00:00:00'
    "TASKS","SYS_P482707","TIMESTAMP' 2020-03-01 00:00:00'
    "TASKS","SYS_P483046","TIMESTAMP' 2020-04-01 00:00:00'
    "TASKS","SYS_P483406","TIMESTAMP' 2020-05-01 00:00:00'
    "TASKS","SYS_P483788","TIMESTAMP' 2020-06-01 00:00:00'
    "TASKS","SYS_P484351","TIMESTAMP' 2020-07-01 00:00:00'
    "TASKS","SYS_P484768","TIMESTAMP' 2020-08-01 00:00:00'
    "TASKS","SYS_P485238","TIMESTAMP' 2020-09-01 00:00:00'
    "TASKS","SYS_P485670","TIMESTAMP' 2020-10-01 00:00:00'
    "TASKS","SYS_P486112","TIMESTAMP' 2020-11-01 00:00:00'
    "TASKS","SYS_P486538","TIMESTAMP' 2020-12-01 00:00:00'
    "TASKS","SYS_P487035","TIMESTAMP' 2021-01-01 00:00:00'
    "TASKS","SYS_P487487","TIMESTAMP' 2021-02-01 00:00:00'
    "TASKS","SYS_P487802","TIMESTAMP' 2021-03-01 00:00:00'
    "TASKS","SYS_P488284","TIMESTAMP' 2021-04-01 00:00:00'
    "TASKS","SYS_P488806","TIMESTAMP' 2021-05-01 00:00:00'
    "TASKS","SYS_P489193","TIMESTAMP' 2021-06-01 00:00:00'
    "TASKS","SYS_P489559","TIMESTAMP' 2021-07-01 00:00:00'
    "TASKS","SYS_P490006","TIMESTAMP' 2021-08-01 00:00:00'
    "TASKS","SYS_P490426","TIMESTAMP' 2021-09-01 00:00:00'
    "TASKS","SYS_P490780","TIMESTAMP' 2021-10-01 00:00:00'
    "TASKS","SYS_P491886","TIMESTAMP' 2021-11-01 00:00:00'
    "TASKS","SYS_P492505","TIMESTAMP' 2021-12-01 00:00:00'
    "TASKS","SYS_P493220","TIMESTAMP' 2022-01-01 00:00:00'
    "TASKS","SYS_P493987","TIMESTAMP' 2022-02-01 00:00:00'
    "TASKS","SYS_P494729","TIMESTAMP' 2022-03-01 00:00:00'
    "TASKS","SYS_P495428","TIMESTAMP' 2022-04-01 00:00:00'
    "TASKS","SYS_P496851","TIMESTAMP' 2022-05-01 00:00:00'
    "TASKS","SYS_P498272","TIMESTAMP' 2022-06-01 00:00:00'
    "TASKS","SYS_P499016","TIMESTAMP' 2022-07-01 00:00:00'
    "TASKS","SYS_P500228","TIMESTAMP' 2022-08-01 00:00:00'
    "TASKS","SYS_P501456","TIMESTAMP' 2022-09-01 00:00:00'
    "TASKS","SYS_P502880","TIMESTAMP' 2022-10-01 00:00:00'
    "TASKS","SYS_P504129","TIMESTAMP' 2022-11-01 00:00:00'
    "TASKS","SYS_P506636","TIMESTAMP' 2022-12-01 00:00:00'
    "TASKS","SYS_P508849","TIMESTAMP' 2023-01-01 00:00:00'
    "TASKS","SYS_P511156","TIMESTAMP' 2023-02-01 00:00:00'
    "TASKS","SYS_P512339","TIMESTAMP' 2023-03-01 00:00:00'
    "TASKS","SYS_P513198","TIMESTAMP' 2023-04-01 00:00:00'
    "TASKS","SYS_P514124","TIMESTAMP' 2023-05-01 00:00:00'
    "TASKS","SYS_P515034","TIMESTAMP' 2023-06-01 00:00:00'
    "TASKS","SYS_P516040","TIMESTAMP' 2023-07-01 00:00:00'
    "TASKS","SYS_P516888","TIMESTAMP' 2023-08-01 00:00:00'
    "TASKS","SYS_P517544","TIMESTAMP' 2023-09-01 00:00:00'
    "TASKS","SYS_P518310","TIMESTAMP' 2023-10-01 00:00:00'

    88 rows selected.
    ```

??? example "example: partitions on devtwo DB"

    The DB devtwo has partitions only on FILEMETADATA table, not on TASKS or FILETRANSFERSDB. This will be fixed.

    ```plaintext
    SQL> select table_name,partition_name,high_value from user_tab_partitions order by table_name, partition_position;

    "TABLE_NAME","PARTITION_NAME","HIGH_VALUE"
    "FILEMETADATA","SYS_P77629","TIMESTAMP' 2023-06-14 00:00:00'
    "FILEMETADATA","SYS_P78389","TIMESTAMP' 2023-07-14 00:00:00'
    "FILEMETADATA","SYS_P79169","TIMESTAMP' 2023-08-14 00:00:00'
    ```

### Change the current policies

On 2023-09-10 we noticed that on preprod and dev DBs we did not have the same
partition policies as in the prod DB. It is possible to convert a non-partitioned
table into a partitioned table, as described by official [docs](https://docs.oracle.com/en/database/oracle/oracle-database/19/vldbg/evolve-nopartition-table.html#GUID-6054142E-207A-4DF0-A62A-4C1A94DD36C4)

```sql
alter table FILETRANSFERSDB modify
partition by range (TM_CREATION_TIME)
INTERVAL (NUMTOYMINTERVAL(1, 'MONTH'))
(
    PARTITION P1 VALUES LESS THAN (TO_DATE('2017-04-14 00:00:00', 'SYYYY-MM-DD HH24:MI:SS', 'NLS_CALENDAR=GREGORIAN'))
)
ONLINE
UPDATE INDEXES
;
```

```sql
alter table TASKS modify
partition by range (TM_START_TIME)
INTERVAL (NUMTOYMINTERVAL(1, 'MONTH'))
(
    PARTITION P1 VALUES LESS THAN (TO_DATE('2017-04-14 00:00:00', 'SYYYY-MM-DD HH24:MI:SS', 'NLS_CALENDAR=GREGORIAN'))
)
ONLINE
UPDATE INDEXES
;
```

Actions:

- [x] devtwo, alter tasks
- [x] devtwo, alter filetransfersdb
- [x] devtwo, make sure that partitions are created (2023-11-01)
- [x] preprod, alter tasks (after 2023-11-01, make sure that partitions on devtwo are ok). this took many hours
- [x] preprod, alter filetransfersdb. This took minutes

### Jobs, procedures and functions

Currently (2023 july), we have an automated mechanism to drop old table partitions.
Every month, we create a new partition for FILETRANSFERSDB and FILEMETADATA tables, then drop the partitions older than 90d.

The code is organized in sheduled jobs, partitions and functions
(more information can be found [online](https://www.tutorialspoint.com/difference-between-function-and-procedure)):

- A scheduled jobs is similar to crontab in Linux and execute at defined intervals.
- A procedure is a set of instructions that takes an input and perform a certain task.
  A procedure does not return any value.
- A functions is set instructions that takes and input and return a value.
  Some statements are forbidden inside a function, such as Insert, Delete, Update, ... .
  A function can be called by a procedure.

In particular, we have the following three scheduler jobs:

1. JOB_DEL_PART_MAIN - This job will execute DELETE_PARTITION_MAIN
        procedure and collect all info related to partition dropping.
2. JOB_DEL_PART_FILETRANSFERSDB - This job will execute
        DEL_PART_FILETRANSFERSDB procedure and remove oldest partition of
        FILETRANSFERSDB table,selected by DELETE_PARTITION_MAIN procedure.
3. JOB_DEL_PART_FILEMETADATA - This job will execute
        DEL_PART_FILEMETADATA procedure and remove the oldest partition of
        FILEMETADATA table,selected by DELETE_PARTITION_MAIN procedure.

See the [schema](#schema-of-the-stored-jobs-procedures-and-functions) for a better overview

### Schema of the stored jobs, procedures and functions

![](../../images/code_sql.jpg)

### Install the PL/SQL code from scratch

Clone dmwm/CRABServer, then execute `etc/oracle-admin/partitions.sql` with

```plaintext
rlwrap sqlplus cmsweb_crab_9r_dev@int2r
SQL> @etc/oracle-admin/partitions.sql
```

This created the proper functions, procedures and scheduled jobs.

In case the user/schema misses some permissions, for example sending emails,
then Oracle will not compile the procedues and the script will return an error
message. Contact Oracle Support and ask for all the necessary permissions.

??? exmaple "example: partitions.sql runs correctly"

    ```plaintext
    SQL> @etc/oracle-admin/partitions.sql
    Function created.
    Function created.
    Function created.
    Function created.
    Function created.
    Function created.
    Function created.
    Procedure created.
    Procedure created.
    Procedure created.
    Procedure created.
    Procedure created.
    Procedure created.
    Procedure created.
    PL/SQL procedure successfully completed.
    PL/SQL procedure successfully completed.
    ```

actions

- [x] devtwo, ask for permission to send emails
- [x] devtwo, ask for permission to create scheduled jobs

### Get information about what is currently installed

You can get information about currently installed procedures both with sqlplus and dbeaver,
for example if you want to check that the content of the jobs-procedures-functions
[schema](#schema-of-the-stored-jobs-procedures-and-functions) is still up to date

We will write instructions on how to use sqplus to do so, since using dbeaver is only a matter
of clicking on the correct buttons.

List which jobs are scheduled

```sql
select owner as schema_name,
       job_name,
       start_date,
       last_start_date,
       next_run_date,
       state
from sys.all_scheduler_jobs
order by owner,
        job_name;
```

??? example "example: list scheduled jobs"


    ```sql
    select owner as schema_name, job_name, state from sys.all_scheduler_jobs order by owner, job_name;
    ```

    ```sql
    select owner as schema_name,
        job_name,
        start_date,
        last_start_date,
        next_run_date,
        state
    from sys.all_scheduler_jobs
    order by owner,
            job_name;
    ```

    ```plaintext
    "SCHEMA_NAME","JOB_NAME","START_DATE","LAST_START_DATE","NEXT_RUN_DATE","STATE"
    "CMS_ANALYSIS_REQMGR","JOB_DEL_PART_FILEMETADATA","27-MAY-21 11.00.00.000000 AM +02:00","27-JUL-23 11.00.00.464050 AM +02:00","27-AUG-23 11.00.00.489535 AM +02:00","SCHEDULED"
    "CMS_ANALYSIS_REQMGR","JOB_DEL_PART_FILETRANSFERSDB","26-MAY-21 11.00.00.000000 AM +02:00","26-JUL-23 11.00.00.237595 AM +02:00","26-AUG-23 11.00.00.242668 AM +02:00","SCHEDULED"
    "CMS_ANALYSIS_REQMGR","JOB_DEL_PART_MAIN","25-MAY-21 11.30.00.000000 AM +02:00","25-JUL-23 11.30.00.801931 AM +02:00","25-AUG-23 11.30.00.807029 AM +02:00","SCHEDULED"
    ```

    Or get more info with

    ```sql
    select owner as schema_name,
        job_name,
        job_style,
        case when job_type is null
                    then 'PROGRAM'
                else job_type end as job_type,
        case when job_action is null
                    then program_name
                    else job_action end as job_action,
        start_date,
        case when repeat_interval is null
                then schedule_name
                else repeat_interval end as schedule,
        last_start_date,
        next_run_date,
        state
    from sys.all_scheduler_jobs
    order by owner,
            job_name;
    ```

    ```plaintext
    "SCHEMA_NAME","JOB_NAME","JOB_STYLE","JOB_TYPE","JOB_ACTION","START_DATE","SCHEDULE","LAST_START_DATE","NEXT_RUN_DATE","STATE"
    "CMS_ANALYSIS_REQMGR","JOB_DEL_PART_FILEMETADATA","REGULAR","STORED_PROCEDURE","DEL_PART_FILEMETADATA","27-MAY-21 11.00.00.000000 AM +02:00","FREQ=MONTHLY","27-JUL-23 11.00.00.464050 AM +02:00","27-AUG-23 11.00.00.489535 AM +02:00","SCHEDULED"
    "CMS_ANALYSIS_REQMGR","JOB_DEL_PART_FILETRANSFERSDB","REGULAR","STORED_PROCEDURE","DEL_PART_FILETRANSFERSDB","26-MAY-21 11.00.00.000000 AM +02:00","FREQ=MONTHLY","26-JUL-23 11.00.00.237595 AM +02:00","26-AUG-23 11.00.00.242668 AM +02:00","SCHEDULED"
    "CMS_ANALYSIS_REQMGR","JOB_DEL_PART_MAIN","REGULAR","STORED_PROCEDURE","DELETE_PARTITION_MAIN","25-MAY-21 11.30.00.000000 AM +02:00","FREQ=MONTHLY","25-JUL-23 11.30.00.801931 AM +02:00","25-AUG-23 11.30.00.807029 AM +02:00","SCHEDULED"
    ```


List stored procedures and functions

```sql
SELECT * FROM user_procedures WHERE object_type = 'PROCEDURE';
SELECT * FROM user_procedures WHERE object_type = 'FUNCTION';
-- or also
SELECT owner, object_name FROM all_procedures WHERE object_type = 'PROCEDURE';
SELECT owner, object_name FROM all_procedures WHERE object_type = 'FUNCTION';
```

??? example "example: list procedures - prod"

    Example of successfull output:

    ```plaintext
    # prod
    SQL> SELECT owner, object_name FROM all_procedures WHERE object_type = 'FUNCTION' and owner = 'CMS_ANALYSIS_REQMGR';

    "OWNER","OBJECT_NAME"
    "CMS_ANALYSIS_REQMGR","INFO_INDEX"
    "CMS_ANALYSIS_REQMGR","INFO_JOB"
    "CMS_ANALYSIS_REQMGR","INFO_PARTITION"
    "CMS_ANALYSIS_REQMGR","INFO_USER_ERRORS"
    "CMS_ANALYSIS_REQMGR","PARTITION_NAME_TOBE_DELETE"
    "CMS_ANALYSIS_REQMGR","TIME_TAKEN"

    6 rows selected.

    SQL> SELECT owner, object_name FROM all_procedures WHERE object_type = 'PROCEDURE' and owner = 'CMS_ANALYSIS_REQMGR';

    "OWNER","OBJECT_NAME"
    "CMS_ANALYSIS_REQMGR","BUILD_LOCAL_INDEX"
    "CMS_ANALYSIS_REQMGR","CLEAN_DATA"
    "CMS_ANALYSIS_REQMGR","DELETE_PARTITION"=
    "CMS_ANALYSIS_REQMGR","DELETE_PARTITION_FILEMETADATA"
    "CMS_ANALYSIS_REQMGR","DELETE_PARTITION_MAIN"=
    "CMS_ANALYSIS_REQMGR","DEL_PART_FILEMETADATA"=
    "CMS_ANALYSIS_REQMGR","DEL_PART_FILETRANSFERSDB"=
    "CMS_ANALYSIS_REQMGR","DISABLE_JOBS"=
    "CMS_ANALYSIS_REQMGR","ENABLE_JOBS"=
    "CMS_ANALYSIS_REQMGR","JOB_PARTITION"
    "CMS_ANALYSIS_REQMGR","LISTPARTS_FILEMETADATA_60"
    "CMS_ANALYSIS_REQMGR","LIST_OLDPARTS"
    "CMS_ANALYSIS_REQMGR","SEND_EMAIL"=

    13 rows selected.

    # preprod
    [...]
    "CMSWEB_ANALYSIS_PREPROD","DELETE_PARTITION_FILEMETADATA"
    "CMSWEB_ANALYSIS_PREPROD","DELETE_PARTITION_START"
    "CMSWEB_ANALYSIS_PREPROD","INIT_TABLE"
    "CMSWEB_ANALYSIS_PREPROD","JOB_PARTITION"
    "CMSWEB_ANALYSIS_PREPROD","LIST_OLDPARTS"
    [...]
    ```

??? example "example: list procedures - devtwo

    SQL> SELECT owner, object_name FROM all_procedures WHERE object_type = 'PROCEDURE' and owner = 'CMSWEB_CRAB_9R_DEV';
    SQL> SELECT owner, object_name FROM all_procedures WHERE object_type = 'FUNCTION' and owner = 'CMSWEB_CRAB_9R_DEV';




Check the content of a stored procedure or function

```sql
select * from user_source where name = 'DELETE_PARTITION_FILEMETADATA';
```

??? example "example: show content of stored procedure"

    ```plaintext
    # PROD
    SQL> select * from user_source where name = 'DELETE_PARTITION_FILEMETADATA';

    "NAME","TYPE","LINE","TEXT","ORIGIN_CON_ID"
    "DELETE_PARTITION_FILEMETADATA","PROCEDURE",1,"PROCEDURE DELETE_PARTITION_FILEMETADATA",0
    "DELETE_PARTITION_FILEMETADATA","PROCEDURE",2,"IS",0
    "DELETE_PARTITION_FILEMETADATA","PROCEDURE",3,"BEGIN",0
    "DELETE_PARTITION_FILEMETADATA","PROCEDURE",4," DELETE_PARTITION('FILEMETADATA');",0
    "DELETE_PARTITION_FILEMETADATA","PROCEDURE",5,"",0
    "DELETE_PARTITION_FILEMETADATA","PROCEDURE",6,"END DELETE_PARTITION_FILEMETADATA;",0
    ```

### Survey - jobs, procedures and functions on prod

(`[x]` means that Dario knows the answerm not that he already cleaned the content)

- [x] jobs: ok
- [ ] procedures:
    - [ ] clean_data: deprecated?
    - [x] delete_partition_filemetadata: superseded, del_part_fmdb
    - [x] job_partition: superseded, last lines in `partitions.sql`
    - [ ] listparts_filemetadata_60: deprecated?
    - [ ] list_oldparts: deprecated?
- [x] functions: ok

### Survey - jobs, procedures and functions on preprod

- [x] jobs: ok
- [ ] procedures:
    - [x] delete_partition_filemetadata: superseded, del_part_fmdb
    - [x] delete_partition_start: deprecated
    - [x] init_table: THIS MAY BE NICE TO HAVE IN PROD AS WELL (created the table `PTNAME`)
    - [x] job_partition: superseded
    - [x] list_oldparts: deprecated?
- [x] functions:
    - [x] metadata_table: deprecated?

### Survey - jobs, procedures and fuctnions on devtwo

- [x] on devtwo we do not have access to `utl_mail`, so we can not compile the procedure `send_email`,
      so we can not compile any other procedure. Ask kate permission to do so, so that
      I can properly test the procedures there, before touching preprod

### Troubleshoot

#### There are too many partitions around

Keep in mind that

- DELETE_PARTITION_MAIN :
    - get the list of all partitions
    - if there are partitions older than 90d, add the oldest to the table PTNAME
- DEL_PART_FILETRANSFERSDB and DEL_PART_FILEMETADATA
    - delete the partition in PTNAME

So, if for example 4 partitions in FILEMETADATA are older than 90d, only the oldest
one gets delete automatically.

In order to delete all the partitions older than 90, you need to manually
execute the procedures. Every time you execute

```SQL
execute DELETE_PARTITION_MANUAL;
```

the oldest partition of every table is removed. If you keep executing these procedures,
you will eventually reach the point where there are no partitions older than 90d,
the table PTNAME will be empty and DEL_PART_FILEMETADATA and DEL_PART_FILETRANSFERSDB will
not delete anything.

## Dependencies among objects and tables

Oracle allows to retrieve the dependencies between objects, so that for example
it is possible to evaluare the effect of dropping a table.

```sql
select * from user_dependencies;
```

??? example "example: show dependencies between objects on prod DB"


    ```plaintext
    SQL> set markup csv on
    SQL> select * from user_dependencies;

    "NAME","TYPE","REFERENCED_OWNER","REFERENCED_NAME","REFERENCED_TYPE","REFERENCED_LINK_NAME","SCHEMAID","DEPENDENCY_TYPE"
    "DELETE_PARTITION","PROCEDURE","PUBLIC","DUAL","SYNONYM",,5186,"HARD"
    "DELETE_PARTITION_FILEMETADATA","PROCEDURE","PUBLIC","DUAL","SYNONYM",,5186,"HARD"
    "SEND_EMAIL","PROCEDURE","PUBLIC","DUAL","SYNONYM",,5186,"HARD"
    "BUILD_LOCAL_INDEX","PROCEDURE","PUBLIC","DUAL","SYNONYM",,5186,"HARD"
    "DELETE_PARTITION_MAIN","PROCEDURE","PUBLIC","DUAL","SYNONYM",,5186,"HARD"
    "JOBGROUPS_ID_TRG","TRIGGER","PUBLIC","DUAL","SYNONYM",,5186,"HARD"
    "JOB_PARTITION","PROCEDURE","SYS","STANDARD","PACKAGE",,5186,"HARD"
    "INFO_JOB","FUNCTION","SYS","STANDARD","PACKAGE",,5186,"HARD"
    "DELETE_PARTITION","PROCEDURE","SYS","STANDARD","PACKAGE",,5186,"HARD"
    "DISABLE_JOBS","PROCEDURE","SYS","STANDARD","PACKAGE",,5186,"HARD"
    "TIME_TAKEN","FUNCTION","SYS","STANDARD","PACKAGE",,5186,"HARD"
    "DELETE_PARTITION_FILEMETADATA","PROCEDURE","SYS","STANDARD","PACKAGE",,5186,"HARD"
    "INFO_INDEX","FUNCTION","SYS","STANDARD","PACKAGE",,5186,"HARD"
    "SEND_EMAIL","PROCEDURE","SYS","STANDARD","PACKAGE",,5186,"HARD"
    "BUILD_LOCAL_INDEX","PROCEDURE","SYS","STANDARD","PACKAGE",,5186,"HARD"
    "CLEAN_DATA","PROCEDURE","SYS","STANDARD","PACKAGE",,5186,"HARD"
    "DELETE_PARTITION_MAIN","PROCEDURE","SYS","STANDARD","PACKAGE",,5186,"HARD"
    "ENABLE_JOBS","PROCEDURE","SYS","STANDARD","PACKAGE",,5186,"HARD"
    "PARTITION_NAME_TOBE_DELETE","FUNCTION","SYS","STANDARD","PACKAGE",,5186,"HARD"
    "JOBGROUPS_ID_TRG","TRIGGER","SYS","STANDARD","PACKAGE",,5186,"HARD"
    "LISTPARTS_FILEMETADATA_60","PROCEDURE","SYS","STANDARD","PACKAGE",,5186,"HARD"
    "LIST_OLDPARTS","PROCEDURE","SYS","STANDARD","PACKAGE",,5186,"HARD"
    "INFO_PARTITION","FUNCTION","SYS","STANDARD","PACKAGE",,5186,"HARD"
    "INFO_USER_ERRORS","FUNCTION","SYS","STANDARD","PACKAGE",,5186,"HARD"
    "DEL_PART_FILEMETADATA","PROCEDURE","SYS","STANDARD","PACKAGE",,5186,"HARD"
    "DEL_PART_FILETRANSFERSDB","PROCEDURE","SYS","STANDARD","PACKAGE",,5186,"HARD"
    "INFO_INDEX","FUNCTION","PUBLIC","USER_INDEXES","SYNONYM",,5186,"HARD"
    "BUILD_LOCAL_INDEX","PROCEDURE","PUBLIC","USER_PART_KEY_COLUMNS","SYNONYM",,5186,"HARD"
    "BUILD_LOCAL_INDEX","PROCEDURE","PUBLIC","USER_TAB_PARTITIONS","SYNONYM",,5186,"HARD"
    "LISTPARTS_FILEMETADATA_60","PROCEDURE","PUBLIC","USER_TAB_PARTITIONS","SYNONYM",,5186,"HARD"
    "LIST_OLDPARTS","PROCEDURE","PUBLIC","USER_TAB_PARTITIONS","SYNONYM",,5186,"HARD"
    "INFO_PARTITION","FUNCTION","PUBLIC","USER_TAB_PARTITIONS","SYNONYM",,5186,"HARD"
    "INFO_USER_ERRORS","FUNCTION","PUBLIC","USER_ERRORS","SYNONYM",,5186,"HARD"
    "CLEAN_DATA","PROCEDURE","PUBLIC","PLITBLM","SYNONYM",,5186,"HARD"
    "JOB_PARTITION","PROCEDURE","SYS","SYS_STUB_FOR_PURITY_ANALYSIS","PACKAGE",,5186,"HARD"
    "INFO_JOB","FUNCTION","SYS","SYS_STUB_FOR_PURITY_ANALYSIS","PACKAGE",,5186,"HARD"
    "DELETE_PARTITION","PROCEDURE","SYS","SYS_STUB_FOR_PURITY_ANALYSIS","PACKAGE",,5186,"HARD"
    "BUILD_LOCAL_INDEX","PROCEDURE","SYS","SYS_STUB_FOR_PURITY_ANALYSIS","PACKAGE",,5186,"HARD"
    "DELETE_PARTITION_FILEMETADATA","PROCEDURE","SYS","SYS_STUB_FOR_PURITY_ANALYSIS","PACKAGE",,5186,"HARD"
    "INFO_INDEX","FUNCTION","SYS","SYS_STUB_FOR_PURITY_ANALYSIS","PACKAGE",,5186,"HARD"
    "SEND_EMAIL","PROCEDURE","SYS","SYS_STUB_FOR_PURITY_ANALYSIS","PACKAGE",,5186,"HARD"
    "CLEAN_DATA","PROCEDURE","SYS","SYS_STUB_FOR_PURITY_ANALYSIS","PACKAGE",,5186,"HARD"
    "DELETE_PARTITION_MAIN","PROCEDURE","SYS","SYS_STUB_FOR_PURITY_ANALYSIS","PACKAGE",,5186,"HARD"
    "DISABLE_JOBS","PROCEDURE","SYS","SYS_STUB_FOR_PURITY_ANALYSIS","PACKAGE",,5186,"HARD"
    "ENABLE_JOBS","PROCEDURE","SYS","SYS_STUB_FOR_PURITY_ANALYSIS","PACKAGE",,5186,"HARD"
    "PARTITION_NAME_TOBE_DELETE","FUNCTION","SYS","SYS_STUB_FOR_PURITY_ANALYSIS","PACKAGE",,5186,"HARD"
    "LISTPARTS_FILEMETADATA_60","PROCEDURE","SYS","SYS_STUB_FOR_PURITY_ANALYSIS","PACKAGE",,5186,"HARD"
    "LIST_OLDPARTS","PROCEDURE","SYS","SYS_STUB_FOR_PURITY_ANALYSIS","PACKAGE",,5186,"HARD"
    "INFO_PARTITION","FUNCTION","SYS","SYS_STUB_FOR_PURITY_ANALYSIS","PACKAGE",,5186,"HARD"
    "INFO_USER_ERRORS","FUNCTION","SYS","SYS_STUB_FOR_PURITY_ANALYSIS","PACKAGE",,5186,"HARD"
    "DEL_PART_FILEMETADATA","PROCEDURE","SYS","SYS_STUB_FOR_PURITY_ANALYSIS","PACKAGE",,5186,"HARD"
    "DEL_PART_FILETRANSFERSDB","PROCEDURE","SYS","SYS_STUB_FOR_PURITY_ANALYSIS","PACKAGE",,5186,"HARD"
    "CLEAN_DATA","PROCEDURE","PUBLIC","DBMS_LOCK","SYNONYM",,5186,"HARD"
    "BUILD_LOCAL_INDEX","PROCEDURE","PUBLIC","DBMS_OUTPUT","SYNONYM",,5186,"HARD"
    "LISTPARTS_FILEMETADATA_60","PROCEDURE","PUBLIC","DBMS_OUTPUT","SYNONYM",,5186,"HARD"
    "LIST_OLDPARTS","PROCEDURE","PUBLIC","DBMS_OUTPUT","SYNONYM",,5186,"HARD"
    "JOB_PARTITION","PROCEDURE","PUBLIC","DBMS_SCHEDULER","SYNONYM",,5186,"HARD"
    "DISABLE_JOBS","PROCEDURE","PUBLIC","DBMS_SCHEDULER","SYNONYM",,5186,"HARD"
    "ENABLE_JOBS","PROCEDURE","PUBLIC","DBMS_SCHEDULER","SYNONYM",,5186,"HARD"
    "JOB_PARTITION","PROCEDURE","PUBLIC","USER_SCHEDULER_JOBS","SYNONYM",,5186,"HARD"
    "INFO_JOB","FUNCTION","PUBLIC","USER_SCHEDULER_JOBS","SYNONYM",,5186,"HARD"
    "JOBGROUPS_ID_TRG","TRIGGER","CMS_ANALYSIS_REQMGR","JOBGROUPS","TABLE",,5186,"HARD"
    "JOBGROUPS_ID_TRG","TRIGGER","CMS_ANALYSIS_REQMGR","JOBGROUPS_ID_SEQ","SEQUENCE",,5186,"HARD"
    "DELETE_PARTITION","PROCEDURE","CMS_ANALYSIS_REQMGR","TIME_TAKEN","FUNCTION",,5186,"HARD"
    "DELETE_PARTITION_FILEMETADATA","PROCEDURE","CMS_ANALYSIS_REQMGR","TIME_TAKEN","FUNCTION",,5186,"HARD"
    "DELETE_PARTITION_MAIN","PROCEDURE","CMS_ANALYSIS_REQMGR","TIME_TAKEN","FUNCTION",,5186,"HARD"
    "DELETE_PARTITION","PROCEDURE","CMS_ANALYSIS_REQMGR","INFO_INDEX","FUNCTION",,5186,"HARD"
    "DELETE_PARTITION_FILEMETADATA","PROCEDURE","CMS_ANALYSIS_REQMGR","INFO_INDEX","FUNCTION",,5186,"HARD"
    "DELETE_PARTITION_MAIN","PROCEDURE","CMS_ANALYSIS_REQMGR","INFO_INDEX","FUNCTION",,5186,"HARD"
    "DISABLE_JOBS","PROCEDURE","CMS_ANALYSIS_REQMGR","DELETE_PARTITION_MAIN","PROCEDURE",,5186,"HARD"
    "ENABLE_JOBS","PROCEDURE","CMS_ANALYSIS_REQMGR","DELETE_PARTITION_MAIN","PROCEDURE",,5186,"HARD"
    "SEND_EMAIL","PROCEDURE","PUBLIC","UTL_MAIL","SYNONYM",,5186,"HARD"
    "DELETE_PARTITION","PROCEDURE","CMS_ANALYSIS_REQMGR","SEND_EMAIL","PROCEDURE",,5186,"HARD"
    "DELETE_PARTITION_FILEMETADATA","PROCEDURE","CMS_ANALYSIS_REQMGR","SEND_EMAIL","PROCEDURE",,5186,"HARD"
    "DELETE_PARTITION_MAIN","PROCEDURE","CMS_ANALYSIS_REQMGR","SEND_EMAIL","PROCEDURE",,5186,"HARD"
    "DELETE_PARTITION","PROCEDURE","CMS_ANALYSIS_REQMGR","PARTITION_NAME_TOBE_DELETE","FUNCTION",,5186,"HARD"
    "DELETE_PARTITION_FILEMETADATA","PROCEDURE","CMS_ANALYSIS_REQMGR","PARTITION_NAME_TOBE_DELETE","FUNCTION",,5186,"HARD"
    "DELETE_PARTITION_MAIN","PROCEDURE","CMS_ANALYSIS_REQMGR","PARTITION_NAME_TOBE_DELETE","FUNCTION",,5186,"HARD"
    "PARTITION_NAME_TOBE_DELETE","FUNCTION","CMS_ANALYSIS_REQMGR","PTNAME","TABLE",,5186,"HARD"
    "INFO_PARTITION","FUNCTION","CMS_ANALYSIS_REQMGR","PTNAME","TABLE",,5186,"HARD"
    "DELETE_PARTITION_MAIN","PROCEDURE","CMS_ANALYSIS_REQMGR","INFO_PARTITION","FUNCTION",,5186,"HARD"
    "DELETE_PARTITION_MAIN","PROCEDURE","CMS_ANALYSIS_REQMGR","INFO_JOB","FUNCTION",,5186,"HARD"
    "DELETE_PARTITION_MAIN","PROCEDURE","CMS_ANALYSIS_REQMGR","INFO_USER_ERRORS","FUNCTION",,5186,"HARD"
    "DEL_PART_FILEMETADATA","PROCEDURE","CMS_ANALYSIS_REQMGR","DELETE_PARTITION","PROCEDURE",,5186,"HARD"
    "DEL_PART_FILETRANSFERSDB","PROCEDURE","CMS_ANALYSIS_REQMGR","DELETE_PARTITION","PROCEDURE",,5186,"HARD"
    "CLEAN_DATA","PROCEDURE","CMS_ANALYSIS_REQMGR","FILEMETADATA","TABLE",,5186,"HARD"
    ```

Oracle [allows](https://docs.oracle.com/en/database/oracle/oracle-database/19/refrn/ALL_CONSTRAINTS.html) to retrieve the constraints of a table. In crab, we use three main constraints type:

- `C`: Check constraint on a table
- `P`: Primary key
- `R`: Referential integrity, this points to another costraint of a different type.
  For example, a foreign key is of type R, referencing to a type P costraint

```sql
select * from user_constraints;
```

??? example "example: show constraints on tables on prod DB"

    ```plaintext
    SQL> select * from user_constraints order by constraint_type desc;

    "OWNER","CONSTRAINT_NAME","CONSTRAINT_TYPE","TABLE_NAME","SEARCH_CONDITION","SEARCH_CONDITION_VC","R_OWNER","R_CONSTRAINT_NAME","DELETE_RULE","STATUS","DEFERRABLE","DEFERRED","VALIDATED","GENERATED","BAD","RELY","LAST_CHANGE","INDEX_OWNER","INDEX_NAME","INVALID","VIEW_RELATED","ORIGIN_CON_ID"
    "CMS_ANALYSIS_REQMGR","TMP$$_TASKNAME_FK0","R","JOBGROUPS",,,"CMS_ANALYSIS_REQMGR","TMP$$_TASKNAME_PK0","CASCADE","DISABLED","NOT DEFERRABLE","IMMEDIATE","NOT VALIDATED","USER NAME",,,"06-FEB-2018",,,,,0
    "CMS_ANALYSIS_REQMGR","TMP$$_FK_TM_TASKNAME_FTDB1","R","FILETRANSFERSDB",,,"CMS_ANALYSIS_REQMGR","TMP$$_TASKNAME_PK0","NO ACTION","DISABLED","NOT DEFERRABLE","IMMEDIATE","NOT VALIDATED","USER NAME",,,"06-FEB-2018",,,,,0
    "CMS_ANALYSIS_REQMGR","FK_TM_TASKNAME_FTDB","R","FILETRANSFERSDB",,,"CMS_ANALYSIS_REQMGR","TASKNAME_PK","NO ACTION","ENABLED","NOT DEFERRABLE","IMMEDIATE","NOT VALIDATED","USER NAME",,,"06-FEB-2018",,,,,0
    "CMS_ANALYSIS_REQMGR","TASKNAME_FK","R","JOBGROUPS",,,"CMS_ANALYSIS_REQMGR","TASKNAME_PK","CASCADE","ENABLED","NOT DEFERRABLE","IMMEDIATE","NOT VALIDATED","USER NAME",,,"06-FEB-2018",,,,,0
    "CMS_ANALYSIS_REQMGR","FK_TM_TASKNAME_PART_DBMS","R","FILEMETADATA",,,"CMS_ANALYSIS_REQMGR","TASKNAME_PK","NO ACTION","ENABLED","NOT DEFERRABLE","IMMEDIATE","NOT VALIDATED","USER NAME",,,"06-FEB-2018",,,,,0
    "CMS_ANALYSIS_REQMGR","TMP$$_FK_TM_TASKNAME_PART_D0","R","FILEMETADATA",,,"CMS_ANALYSIS_REQMGR","TMP$$_TASKNAME_PK0","NO ACTION","DISABLED","NOT DEFERRABLE","IMMEDIATE","NOT VALIDATED","USER NAME",,,"06-FEB-2018",,,,,0
    "CMS_ANALYSIS_REQMGR","TMP$$_TASKNAME_PK0","P","TASKS_INT",,,,,,"ENABLED","NOT DEFERRABLE","IMMEDIATE","NOT VALIDATED","USER NAME",,,"11-FEB-2014","CMS_ANALYSIS_REQMGR","TMP$$_TASKNAME_PK0",,,0
    "CMS_ANALYSIS_REQMGR","TASKNAME_PK","P","TASKS",,,,,,"ENABLED","NOT DEFERRABLE","IMMEDIATE","VALIDATED","USER NAME",,,"04-FEB-2018","CMS_ANALYSIS_REQMGR","TASKNAME_PK",,,0
    "CMS_ANALYSIS_REQMGR","JOBGROUP_ID_PK","P","JOBGROUPS",,,,,,"ENABLED","NOT DEFERRABLE","IMMEDIATE","VALIDATED","USER NAME",,,"11-FEB-2014","CMS_ANALYSIS_REQMGR","JOBGROUP_ID_PK",,,0
    "CMS_ANALYSIS_REQMGR","ID_PK","P","FILETRANSFERSDB",,,,,,"ENABLED","NOT DEFERRABLE","IMMEDIATE","VALIDATED","USER NAME",,,"08-NOV-2017","CMS_ANALYSIS_REQMGR","ID_PK",,,0
    "CMS_ANALYSIS_REQMGR","PK_TASKLFN_PART_DBMS","P","FILEMETADATA",,,,,,"ENABLED","NOT DEFERRABLE","IMMEDIATE","VALIDATED","USER NAME",,,"19-SEP-2017","CMS_ANALYSIS_REQMGR","PK_TASKLFN_PART_DBMS",,,0
    "CMS_ANALYSIS_REQMGR","SYS_C003292131","C","FILETRANSFERSDB","""TM_ID"" IS NOT NULL,"""TM_ID"" IS NOT NULL",,,,"ENABLED","NOT DEFERRABLE","IMMEDIATE","VALIDATED","GENERATED NAME",,,"07-NOV-2017",,,,,0
    "CMS_ANALYSIS_REQMGR","SYS_C003292132","C","FILETRANSFERSDB","""TM_USERNAME"" IS NOT NULL,"""TM_USERNAME"" IS NOT NULL",,,,"ENABLED","NOT DEFERRABLE","IMMEDIATE","VALIDATED","GENERATED NAME",,,"07-NOV-2017",,,,,0
    "CMS_ANALYSIS_REQMGR","SYS_C003292133","C","FILETRANSFERSDB","""TM_TASKNAME"" IS NOT NULL,"""TM_TASKNAME"" IS NOT NULL",,,,"ENABLED","NOT DEFERRABLE","IMMEDIATE","VALIDATED","GENERATED NAME",,,"07-NOV-2017",,,,,0
    "CMS_ANALYSIS_REQMGR","SYS_C003292134","C","FILETRANSFERSDB","""TM_DESTINATION"" IS NOT NULL,"""TM_DESTINATION"" IS NOT NULL",,,,"ENABLED","NOT DEFERRABLE","IMMEDIATE","VALIDATED","GENERATED NAME",,,"07-NOV-2017",,,,,0
    "CMS_ANALYSIS_REQMGR","SYS_C003292135","C","FILETRANSFERSDB","""TM_DESTINATION_LFN"" IS NOT NULL,"""TM_DESTINATION_LFN"" IS NOT NULL",,,,"ENABLED","NOT DEFERRABLE","IMMEDIATE","VALIDATED","GENERATED NAME",,,"07-NOV-2017",,,,,0
    "CMS_ANALYSIS_REQMGR","SYS_C003292136","C","FILETRANSFERSDB","""TM_SOURCE"" IS NOT NULL,"""TM_SOURCE"" IS NOT NULL",,,,"ENABLED","NOT DEFERRABLE","IMMEDIATE","VALIDATED","GENERATED NAME",,,"07-NOV-2017",,,,,0
    "CMS_ANALYSIS_REQMGR","SYS_C003292137","C","FILETRANSFERSDB","""TM_SOURCE_LFN"" IS NOT NULL,"""TM_SOURCE_LFN"" IS NOT NULL",,,,"ENABLED","NOT DEFERRABLE","IMMEDIATE","VALIDATED","GENERATED NAME",,,"07-NOV-2017",,,,,0
    "CMS_ANALYSIS_REQMGR","SYS_C003292138","C","FILETRANSFERSDB","""TM_FILESIZE"" IS NOT NULL,"""TM_FILESIZE"" IS NOT NULL",,,,"ENABLED","NOT DEFERRABLE","IMMEDIATE","VALIDATED","GENERATED NAME",,,"07-NOV-2017",,,,,0
    "CMS_ANALYSIS_REQMGR","SYS_C003292139","C","FILETRANSFERSDB","""TM_PUBLISH"" IS NOT NULL,"""TM_PUBLISH"" IS NOT NULL",,,,"ENABLED","NOT DEFERRABLE","IMMEDIATE","VALIDATED","GENERATED NAME",,,"07-NOV-2017",,,,,0
    "CMS_ANALYSIS_REQMGR","SYS_C003292140","C","FILETRANSFERSDB","""TM_TYPE"" IS NOT NULL,"""TM_TYPE"" IS NOT NULL",,,,"ENABLED","NOT DEFERRABLE","IMMEDIATE","VALIDATED","GENERATED NAME",,,"07-NOV-2017",,,,,0
    "CMS_ANALYSIS_REQMGR","SYS_C003292143","C","FILETRANSFERSDB","""TM_TRANSFER_STATE"" IS NOT NULL,"""TM_TRANSFER_STATE"" IS NOT NULL",,,,"ENABLED","NOT DEFERRABLE","IMMEDIATE","VALIDATED","GENERATED NAME",,,"07-NOV-2017",,,,,0
    "CMS_ANALYSIS_REQMGR","SYS_C003292144","C","FILETRANSFERSDB","""TM_PUBLICATION_STATE"" IS NOT NULL,"""TM_PUBLICATION_STATE"" IS NOT NULL",,,,"ENABLED","NOT DEFERRABLE","IMMEDIATE","VALIDATED","GENERATED NAME",,,"07-NOV-2017",,,,,0
    "CMS_ANALYSIS_REQMGR","SYS_C003292145","C","FILETRANSFERSDB","""TM_LAST_UPDATE"" IS NOT NULL,"""TM_LAST_UPDATE"" IS NOT NULL",,,,"ENABLED","NOT DEFERRABLE","IMMEDIATE","VALIDATED","GENERATED NAME",,,"07-NOV-2017",,,,,0
    "CMS_ANALYSIS_REQMGR","SYS_C003292146","C","FILETRANSFERSDB","""TM_START_TIME"" IS NOT NULL,"""TM_START_TIME"" IS NOT NULL",,,,"ENABLED","NOT DEFERRABLE","IMMEDIATE","VALIDATED","GENERATED NAME",,,"07-NOV-2017",,,,,0
    "CMS_ANALYSIS_REQMGR","SYS_C003292147","C","FILETRANSFERSDB","""TM_CREATION_TIME"" IS NOT NULL,"""TM_CREATION_TIME"" IS NOT NULL",,,,"ENABLED","NOT DEFERRABLE","IMMEDIATE","VALIDATED","GENERATED NAME",,,"07-NOV-2017",,,,,0
    "CMS_ANALYSIS_REQMGR","SYS_C003068733","C","FILETRANSFERSDB","""TM_JOBID"" IS NOT NULL,"""TM_JOBID"" IS NOT NULL",,,,"ENABLED","NOT DEFERRABLE","IMMEDIATE","NOT VALIDATED","USER NAME",,,"08-NOV-2017",,,,,0
    "CMS_ANALYSIS_REQMGR","SYS_C003336810","C","TASKS","""TM_TASKNAME"" IS NOT NULL,"""TM_TASKNAME"" IS NOT NULL",,,,"ENABLED","NOT DEFERRABLE","IMMEDIATE","VALIDATED","GENERATED NAME",,,"04-FEB-2018",,,,,0
    "CMS_ANALYSIS_REQMGR","SYS_C003336811","C","TASKS","""TM_TASK_STATUS"" IS NOT NULL,"""TM_TASK_STATUS"" IS NOT NULL",,,,"ENABLED","NOT DEFERRABLE","IMMEDIATE","VALIDATED","GENERATED NAME",,,"04-FEB-2018",,,,,0
    "CMS_ANALYSIS_REQMGR","SYS_C003336812","C","TASKS","""TM_JOB_SW"" IS NOT NULL,"""TM_JOB_SW"" IS NOT NULL",,,,"ENABLED","NOT DEFERRABLE","IMMEDIATE","VALIDATED","GENERATED NAME",,,"04-FEB-2018",,,,,0
    "CMS_ANALYSIS_REQMGR","SYS_C003336813","C","TASKS","""TM_SPLIT_ALGO"" IS NOT NULL,"""TM_SPLIT_ALGO"" IS NOT NULL",,,,"ENABLED","NOT DEFERRABLE","IMMEDIATE","VALIDATED","GENERATED NAME",,,"04-FEB-2018",,,,,0
    "CMS_ANALYSIS_REQMGR","SYS_C003336814","C","TASKS","""TM_SPLIT_ARGS"" IS NOT NULL,"""TM_SPLIT_ARGS"" IS NOT NULL",,,,"ENABLED","NOT DEFERRABLE","IMMEDIATE","VALIDATED","GENERATED NAME",,,"04-FEB-2018",,,,,0
    "CMS_ANALYSIS_REQMGR","SYS_C003336815","C","TASKS","""TM_USER_SANDBOX"" IS NOT NULL,"""TM_USER_SANDBOX"" IS NOT NULL",,,,"ENABLED","NOT DEFERRABLE","IMMEDIATE","VALIDATED","GENERATED NAME",,,"04-FEB-2018",,,,,0
    "CMS_ANALYSIS_REQMGR","SYS_C003336816","C","TASKS","""TM_CACHE_URL"" IS NOT NULL,"""TM_CACHE_URL"" IS NOT NULL",,,,"ENABLED","NOT DEFERRABLE","IMMEDIATE","VALIDATED","GENERATED NAME",,,"04-FEB-2018",,,,,0
    "CMS_ANALYSIS_REQMGR","SYS_C003336817","C","TASKS","""TM_USERNAME"" IS NOT NULL,"""TM_USERNAME"" IS NOT NULL",,,,"ENABLED","NOT DEFERRABLE","IMMEDIATE","VALIDATED","GENERATED NAME",,,"04-FEB-2018",,,,,0
    "CMS_ANALYSIS_REQMGR","SYS_C003336818","C","TASKS","""TM_USER_DN"" IS NOT NULL,"""TM_USER_DN"" IS NOT NULL",,,,"ENABLED","NOT DEFERRABLE","IMMEDIATE","VALIDATED","GENERATED NAME",,,"04-FEB-2018",,,,,0
    "CMS_ANALYSIS_REQMGR","SYS_C003336819","C","TASKS","""TM_USER_VO"" IS NOT NULL,"""TM_USER_VO"" IS NOT NULL",,,,"ENABLED","NOT DEFERRABLE","IMMEDIATE","VALIDATED","GENERATED NAME",,,"04-FEB-2018",,,,,0
    "CMS_ANALYSIS_REQMGR","SYS_C003336820","C","TASKS","""TM_ASYNCDEST"" IS NOT NULL,"""TM_ASYNCDEST"" IS NOT NULL",,,,"ENABLED","NOT DEFERRABLE","IMMEDIATE","VALIDATED","GENERATED NAME",,,"04-FEB-2018",,,,,0
    "CMS_ANALYSIS_REQMGR","SYS_C003336821","C","TASKS","""TM_DBS_URL"" IS NOT NULL,"""TM_DBS_URL"" IS NOT NULL",,,,"ENABLED","NOT DEFERRABLE","IMMEDIATE","VALIDATED","GENERATED NAME",,,"04-FEB-2018",,,,,0
    "CMS_ANALYSIS_REQMGR","SYS_C003336822","C","TASKS","""TM_PUBLICATION"" IS NOT NULL,"""TM_PUBLICATION"" IS NOT NULL",,,,"ENABLED","NOT DEFERRABLE","IMMEDIATE","VALIDATED","GENERATED NAME",,,"04-FEB-2018",,,,,0
    "CMS_ANALYSIS_REQMGR","SYS_C003336823","C","TASKS","""TM_JOB_TYPE"" IS NOT NULL,"""TM_JOB_TYPE"" IS NOT NULL",,,,"ENABLED","NOT DEFERRABLE","IMMEDIATE","VALIDATED","GENERATED NAME",,,"04-FEB-2018",,,,,0
    "CMS_ANALYSIS_REQMGR","SYS_C003336824","C","TASKS","""TM_SAVE_LOGS"" IS NOT NULL,"""TM_SAVE_LOGS"" IS NOT NULL",,,,"ENABLED","NOT DEFERRABLE","IMMEDIATE","VALIDATED","GENERATED NAME",,,"04-FEB-2018",,,,,0
    "CMS_ANALYSIS_REQMGR","CHECK_TM_PUBLICATION","C","TASKS","tm_publication IN ('T', 'F'),"tm_publication IN ('T', 'F')",,,,"ENABLED","NOT DEFERRABLE","IMMEDIATE","NOT VALIDATED","USER NAME",,,"04-FEB-2018",,,,,0
    "CMS_ANALYSIS_REQMGR","CHECK_TM_PUBLISH_GROUPNAME","C","TASKS","tm_publish_groupname IN ('T', 'F'),"tm_publish_groupname IN ('T', 'F')",,,,"ENABLED","NOT DEFERRABLE","IMMEDIATE","NOT VALIDATED","USER NAME",,,"04-FEB-2018",,,,,0
    "CMS_ANALYSIS_REQMGR","CHECK_TM_SAVE_LOGS","C","TASKS","tm_save_logs IN ('T', 'F'),"tm_save_logs IN ('T', 'F')",,,,"ENABLED","NOT DEFERRABLE","IMMEDIATE","NOT VALIDATED","USER NAME",,,"04-FEB-2018",,,,,0
    "CMS_ANALYSIS_REQMGR","CK_TM_NONVALID_INPUT_DATASET","C","TASKS","tm_nonvalid_input_dataset IN ('T', 'F'),"tm_nonvalid_input_dataset IN ('T', 'F')",,,,"ENABLED","NOT DEFERRABLE","IMMEDIATE","NOT VALIDATED","USER NAME",,,"04-FEB-2018",,,,,0
    "CMS_ANALYSIS_REQMGR","SYS_C003649074","C","CMP3$10027924","""TM_TASKNAME"" IS NOT NULL,"""TM_TASKNAME"" IS NOT NULL",,,,"ENABLED","NOT DEFERRABLE","IMMEDIATE","VALIDATED","GENERATED NAME",,,"12-MAY-2020",,,,,0
    "CMS_ANALYSIS_REQMGR","SYS_C003649075","C","CMP3$10027924","""TM_TASK_STATUS"" IS NOT NULL,"""TM_TASK_STATUS"" IS NOT NULL",,,,"ENABLED","NOT DEFERRABLE","IMMEDIATE","VALIDATED","GENERATED NAME",,,"12-MAY-2020",,,,,0
    "CMS_ANALYSIS_REQMGR","SYS_C003649076","C","CMP3$10027924","""TM_JOB_SW"" IS NOT NULL,"""TM_JOB_SW"" IS NOT NULL",,,,"ENABLED","NOT DEFERRABLE","IMMEDIATE","VALIDATED","GENERATED NAME",,,"12-MAY-2020",,,,,0
    "CMS_ANALYSIS_REQMGR","SYS_C003649077","C","CMP3$10027924","""TM_SPLIT_ALGO"" IS NOT NULL,"""TM_SPLIT_ALGO"" IS NOT NULL",,,,"ENABLED","NOT DEFERRABLE","IMMEDIATE","VALIDATED","GENERATED NAME",,,"12-MAY-2020",,,,,0
    "CMS_ANALYSIS_REQMGR","SYS_C003649078","C","CMP3$10027924","""TM_SPLIT_ARGS"" IS NOT NULL,"""TM_SPLIT_ARGS"" IS NOT NULL",,,,"ENABLED","NOT DEFERRABLE","IMMEDIATE","VALIDATED","GENERATED NAME",,,"12-MAY-2020",,,,,0
    "CMS_ANALYSIS_REQMGR","SYS_C003649079","C","CMP3$10027924","""TM_USER_SANDBOX"" IS NOT NULL,"""TM_USER_SANDBOX"" IS NOT NULL",,,,"ENABLED","NOT DEFERRABLE","IMMEDIATE","VALIDATED","GENERATED NAME",,,"12-MAY-2020",,,,,0
    "CMS_ANALYSIS_REQMGR","SYS_C003649080","C","CMP3$10027924","""TM_CACHE_URL"" IS NOT NULL,"""TM_CACHE_URL"" IS NOT NULL",,,,"ENABLED","NOT DEFERRABLE","IMMEDIATE","VALIDATED","GENERATED NAME",,,"12-MAY-2020",,,,,0
    "CMS_ANALYSIS_REQMGR","SYS_C003649081","C","CMP3$10027924","""TM_USERNAME"" IS NOT NULL,"""TM_USERNAME"" IS NOT NULL",,,,"ENABLED","NOT DEFERRABLE","IMMEDIATE","VALIDATED","GENERATED NAME",,,"12-MAY-2020",,,,,0
    "CMS_ANALYSIS_REQMGR","SYS_C003649082","C","CMP3$10027924","""TM_USER_DN"" IS NOT NULL,"""TM_USER_DN"" IS NOT NULL",,,,"ENABLED","NOT DEFERRABLE","IMMEDIATE","VALIDATED","GENERATED NAME",,,"12-MAY-2020",,,,,0
    "CMS_ANALYSIS_REQMGR","SYS_C003649083","C","CMP3$10027924","""TM_USER_VO"" IS NOT NULL,"""TM_USER_VO"" IS NOT NULL",,,,"ENABLED","NOT DEFERRABLE","IMMEDIATE","VALIDATED","GENERATED NAME",,,"12-MAY-2020",,,,,0
    "CMS_ANALYSIS_REQMGR","SYS_C003649084","C","CMP3$10027924","""TM_ASYNCDEST"" IS NOT NULL,"""TM_ASYNCDEST"" IS NOT NULL",,,,"ENABLED","NOT DEFERRABLE","IMMEDIATE","VALIDATED","GENERATED NAME",,,"12-MAY-2020",,,,,0
    "CMS_ANALYSIS_REQMGR","SYS_C003649085","C","CMP3$10027924","""TM_DBS_URL"" IS NOT NULL,"""TM_DBS_URL"" IS NOT NULL",,,,"ENABLED","NOT DEFERRABLE","IMMEDIATE","VALIDATED","GENERATED NAME",,,"12-MAY-2020",,,,,0
    "CMS_ANALYSIS_REQMGR","SYS_C003649086","C","CMP3$10027924","""TM_PUBLICATION"" IS NOT NULL,"""TM_PUBLICATION"" IS NOT NULL",,,,"ENABLED","NOT DEFERRABLE","IMMEDIATE","VALIDATED","GENERATED NAME",,,"12-MAY-2020",,,,,0
    "CMS_ANALYSIS_REQMGR","SYS_C003649087","C","CMP3$10027924","""TM_JOB_TYPE"" IS NOT NULL,"""TM_JOB_TYPE"" IS NOT NULL",,,,"ENABLED","NOT DEFERRABLE","IMMEDIATE","VALIDATED","GENERATED NAME",,,"12-MAY-2020",,,,,0
    "CMS_ANALYSIS_REQMGR","SYS_C003649088","C","CMP3$10027924","""TM_SAVE_LOGS"" IS NOT NULL,"""TM_SAVE_LOGS"" IS NOT NULL",,,,"ENABLED","NOT DEFERRABLE","IMMEDIATE","VALIDATED","GENERATED NAME",,,"12-MAY-2020",,,,,0
    "CMS_ANALYSIS_REQMGR","TMP$$_CK_TM_NONVALID_INPUT_0","C","TASKS_INT","tm_nonvalid_input_dataset IN ('T', 'F'),"tm_nonvalid_input_dataset IN ('T', 'F')",,,,"ENABLED","NOT DEFERRABLE","IMMEDIATE","VALIDATED","USER NAME",,,"07-JUL-2015",,,,,0
    "CMS_ANALYSIS_REQMGR","TMP$$_CHECK_TM_PUBLISH_GROU0","C","TASKS_INT","tm_publish_groupname IN ('T', 'F'),"tm_publish_groupname IN ('T', 'F')",,,,"ENABLED","NOT DEFERRABLE","IMMEDIATE","VALIDATED","USER NAME",,,"07-JUL-2015",,,,,0
    "CMS_ANALYSIS_REQMGR","SYS_C002580633","C","TASKS_INT","""TM_TASKNAME"" IS NOT NULL,"""TM_TASKNAME"" IS NOT NULL",,,,"ENABLED","NOT DEFERRABLE","IMMEDIATE","VALIDATED","GENERATED NAME",,,"11-FEB-2014",,,,,0
    "CMS_ANALYSIS_REQMGR","SYS_C002580634","C","TASKS_INT","""TM_TASK_STATUS"" IS NOT NULL,"""TM_TASK_STATUS"" IS NOT NULL",,,,"ENABLED","NOT DEFERRABLE","IMMEDIATE","VALIDATED","GENERATED NAME",,,"11-FEB-2014",,,,,0
    "CMS_ANALYSIS_REQMGR","SYS_C002580635","C","TASKS_INT","""TM_JOB_SW"" IS NOT NULL,"""TM_JOB_SW"" IS NOT NULL",,,,"ENABLED","NOT DEFERRABLE","IMMEDIATE","VALIDATED","GENERATED NAME",,,"11-FEB-2014",,,,,0
    "CMS_ANALYSIS_REQMGR","SYS_C002580636","C","TASKS_INT","""TM_SPLIT_ALGO"" IS NOT NULL,"""TM_SPLIT_ALGO"" IS NOT NULL",,,,"ENABLED","NOT DEFERRABLE","IMMEDIATE","VALIDATED","GENERATED NAME",,,"11-FEB-2014",,,,,0
    "CMS_ANALYSIS_REQMGR","SYS_C002580637","C","TASKS_INT","""TM_SPLIT_ARGS"" IS NOT NULL,"""TM_SPLIT_ARGS"" IS NOT NULL",,,,"ENABLED","NOT DEFERRABLE","IMMEDIATE","VALIDATED","GENERATED NAME",,,"11-FEB-2014",,,,,0
    "CMS_ANALYSIS_REQMGR","SYS_C002580638","C","TASKS_INT","""TM_USER_SANDBOX"" IS NOT NULL,"""TM_USER_SANDBOX"" IS NOT NULL",,,,"ENABLED","NOT DEFERRABLE","IMMEDIATE","VALIDATED","GENERATED NAME",,,"11-FEB-2014",,,,,0
    "CMS_ANALYSIS_REQMGR","SYS_C002580639","C","TASKS_INT","""TM_CACHE_URL"" IS NOT NULL,"""TM_CACHE_URL"" IS NOT NULL",,,,"ENABLED","NOT DEFERRABLE","IMMEDIATE","VALIDATED","GENERATED NAME",,,"11-FEB-2014",,,,,0
    "CMS_ANALYSIS_REQMGR","SYS_C002580640","C","TASKS_INT","""TM_USERNAME"" IS NOT NULL,"""TM_USERNAME"" IS NOT NULL",,,,"ENABLED","NOT DEFERRABLE","IMMEDIATE","VALIDATED","GENERATED NAME",,,"11-FEB-2014",,,,,0
    "CMS_ANALYSIS_REQMGR","SYS_C002580641","C","TASKS_INT","""TM_USER_DN"" IS NOT NULL,"""TM_USER_DN"" IS NOT NULL",,,,"ENABLED","NOT DEFERRABLE","IMMEDIATE","VALIDATED","GENERATED NAME",,,"11-FEB-2014",,,,,0
    "CMS_ANALYSIS_REQMGR","SYS_C002580642","C","TASKS_INT","""TM_USER_VO"" IS NOT NULL,"""TM_USER_VO"" IS NOT NULL",,,,"ENABLED","NOT DEFERRABLE","IMMEDIATE","VALIDATED","GENERATED NAME",,,"11-FEB-2014",,,,,0
    "CMS_ANALYSIS_REQMGR","SYS_C002580643","C","TASKS_INT","""TM_ASYNCDEST"" IS NOT NULL,"""TM_ASYNCDEST"" IS NOT NULL",,,,"ENABLED","NOT DEFERRABLE","IMMEDIATE","VALIDATED","GENERATED NAME",,,"11-FEB-2014",,,,,0
    "CMS_ANALYSIS_REQMGR","SYS_C002580644","C","TASKS_INT","""TM_DBS_URL"" IS NOT NULL,"""TM_DBS_URL"" IS NOT NULL",,,,"ENABLED","NOT DEFERRABLE","IMMEDIATE","VALIDATED","GENERATED NAME",,,"11-FEB-2014",,,,,0
    "CMS_ANALYSIS_REQMGR","SYS_C002580645","C","TASKS_INT","""TM_PUBLICATION"" IS NOT NULL,"""TM_PUBLICATION"" IS NOT NULL",,,,"ENABLED","NOT DEFERRABLE","IMMEDIATE","VALIDATED","GENERATED NAME",,,"11-FEB-2014",,,,,0
    "CMS_ANALYSIS_REQMGR","SYS_C002580647","C","TASKS_INT","""TM_JOB_TYPE"" IS NOT NULL,"""TM_JOB_TYPE"" IS NOT NULL",,,,"ENABLED","NOT DEFERRABLE","IMMEDIATE","VALIDATED","GENERATED NAME",,,"11-FEB-2014",,,,,0
    "CMS_ANALYSIS_REQMGR","SYS_C002580648","C","TASKS_INT","""TM_SAVE_LOGS"" IS NOT NULL,"""TM_SAVE_LOGS"" IS NOT NULL",,,,"ENABLED","NOT DEFERRABLE","IMMEDIATE","VALIDATED","GENERATED NAME",,,"11-FEB-2014",,,,,0
    "CMS_ANALYSIS_REQMGR","TMP$$_CHECK_TM_PUBLICATION0","C","TASKS_INT","tm_publication IN ('T', 'F'),"tm_publication IN ('T', 'F')",,,,"ENABLED","NOT DEFERRABLE","IMMEDIATE","VALIDATED","USER NAME",,,"11-FEB-2014",,,,,0
    "CMS_ANALYSIS_REQMGR","TMP$$_CHECK_TM_SAVE_LOGS0","C","TASKS_INT","tm_save_logs IN ('T', 'F'),"tm_save_logs IN ('T', 'F')",,,,"ENABLED","NOT DEFERRABLE","IMMEDIATE","VALIDATED","USER NAME",,,"11-FEB-2014",,,,,0
    "CMS_ANALYSIS_REQMGR","SYS_C002580652","C","JOBGROUPS","""TM_JOBGROUPS_ID"" IS NOT NULL,"""TM_JOBGROUPS_ID"" IS NOT NULL",,,,"ENABLED","NOT DEFERRABLE","IMMEDIATE","VALIDATED","GENERATED NAME",,,"11-FEB-2014",,,,,0
    "CMS_ANALYSIS_REQMGR","SYS_C002580653","C","JOBGROUPS","""TM_TASKNAME"" IS NOT NULL,"""TM_TASKNAME"" IS NOT NULL",,,,"ENABLED","NOT DEFERRABLE","IMMEDIATE","VALIDATED","GENERATED NAME",,,"11-FEB-2014",,,,,0
    "CMS_ANALYSIS_REQMGR","SYS_C002580654","C","JOBGROUPS","""PANDA_JOBDEF_STATUS"" IS NOT NULL,"""PANDA_JOBDEF_STATUS"" IS NOT NULL",,,,"ENABLED","NOT DEFERRABLE","IMMEDIATE","VALIDATED","GENERATED NAME",,,"11-FEB-2014",,,,,0
    "CMS_ANALYSIS_REQMGR","SYS_C002580655","C","JOBGROUPS","""TM_USER_DN"" IS NOT NULL,"""TM_USER_DN"" IS NOT NULL",,,,"ENABLED","NOT DEFERRABLE","IMMEDIATE","VALIDATED","GENERATED NAME",,,"11-FEB-2014",,,,,0
    "CMS_ANALYSIS_REQMGR","SYS_C003261352","C","FILEMETADATA","""TM_TASKNAME"" IS NOT NULL,"""TM_TASKNAME"" IS NOT NULL",,,,"ENABLED","NOT DEFERRABLE","IMMEDIATE","VALIDATED","GENERATED NAME",,,"19-SEP-2017",,,,,0
    "CMS_ANALYSIS_REQMGR","SYS_C003261353","C","FILEMETADATA","""FMD_OUTDATASET"" IS NOT NULL,"""FMD_OUTDATASET"" IS NOT NULL",,,,"ENABLED","NOT DEFERRABLE","IMMEDIATE","VALIDATED","GENERATED NAME",,,"19-SEP-2017",,,,,0
    "CMS_ANALYSIS_REQMGR","SYS_C003261354","C","FILEMETADATA","""FMD_ACQ_ERA"" IS NOT NULL,"""FMD_ACQ_ERA"" IS NOT NULL",,,,"ENABLED","NOT DEFERRABLE","IMMEDIATE","VALIDATED","GENERATED NAME",,,"19-SEP-2017",,,,,0
    "CMS_ANALYSIS_REQMGR","SYS_C003261355","C","FILEMETADATA","""FMD_SW_VER"" IS NOT NULL,"""FMD_SW_VER"" IS NOT NULL",,,,"ENABLED","NOT DEFERRABLE","IMMEDIATE","VALIDATED","GENERATED NAME",,,"19-SEP-2017",,,,,0
    "CMS_ANALYSIS_REQMGR","SYS_C003261356","C","FILEMETADATA","""FMD_PUBLISH_NAME"" IS NOT NULL,"""FMD_PUBLISH_NAME"" IS NOT NULL",,,,"ENABLED","NOT DEFERRABLE","IMMEDIATE","VALIDATED","GENERATED NAME",,,"19-SEP-2017",,,,,0
    "CMS_ANALYSIS_REQMGR","SYS_C003261357","C","FILEMETADATA","""FMD_LOCATION"" IS NOT NULL,"""FMD_LOCATION"" IS NOT NULL",,,,"ENABLED","NOT DEFERRABLE","IMMEDIATE","VALIDATED","GENERATED NAME",,,"19-SEP-2017",,,,,0
    "CMS_ANALYSIS_REQMGR","SYS_C003261358","C","FILEMETADATA","""FMD_TMP_LOCATION"" IS NOT NULL,"""FMD_TMP_LOCATION"" IS NOT NULL",,,,"ENABLED","NOT DEFERRABLE","IMMEDIATE","VALIDATED","GENERATED NAME",,,"19-SEP-2017",,,,,0
    "CMS_ANALYSIS_REQMGR","SYS_C003261359","C","FILEMETADATA","""FMD_LFN"" IS NOT NULL,"""FMD_LFN"" IS NOT NULL",,,,"ENABLED","NOT DEFERRABLE","IMMEDIATE","VALIDATED","GENERATED NAME",,,"19-SEP-2017",,,,,0
    "CMS_ANALYSIS_REQMGR","SYS_C003261360","C","FILEMETADATA","""FMD_SIZE"" IS NOT NULL,"""FMD_SIZE"" IS NOT NULL",,,,"ENABLED","NOT DEFERRABLE","IMMEDIATE","VALIDATED","GENERATED NAME",,,"19-SEP-2017",,,,,0
    "CMS_ANALYSIS_REQMGR","SYS_C003261361","C","FILEMETADATA","""FMD_TYPE"" IS NOT NULL,"""FMD_TYPE"" IS NOT NULL",,,,"ENABLED","NOT DEFERRABLE","IMMEDIATE","VALIDATED","GENERATED NAME",,,"19-SEP-2017",,,,,0
    "CMS_ANALYSIS_REQMGR","SYS_C003261362","C","FILEMETADATA","""FMD_CREATION_TIME"" IS NOT NULL,"""FMD_CREATION_TIME"" IS NOT NULL",,,,"ENABLED","NOT DEFERRABLE","IMMEDIATE","VALIDATED","GENERATED NAME",,,"19-SEP-2017",,,,,0

    95 rows selected.
    ```

## Jobs, procedures and functions - legacy

Reference: content imported from [twiki / Notes about oracle database management](https://twiki.cern.ch/twiki/bin/view/CMSPublic/NotesAboutDatabaseManagement#New_PL_SQL_code_to_drop_Oracle_p)

!!! warning

    The content in this section will be moved into previous sections, then dropped from this page

### Automatic table partitioning and partition dropping

~~We have two tables (FILETRANSFERSDB,FILEMETADATA) and these tables create
a new partition every month. So we have to remove the 90 days older
partition, that is why we need partition dropping exercise every month.
Before November 2020, We were using a manual method to remove partition,
which is defined above.~~

~~We are using new PL/SQL code for partition dropping since November 2020.
This new PL/SQL code exists in [ code.sql](https://gitlab.cern.ch/crab3/craboracledb/-/blob/master/code.sql) file
of [craboracledb](https://gitlab.cern.ch/crab3/craboracledb/-/tree/master) repository.
This repository having two branch:~~

- ~~master - We will use this branch for our production database.~~

- ~~qa- We will use this branch for our preprod/dev databases.~~

~~We created database objects(function, procedures, tables and scheduler
jobs) in our database with the help of this new code.~~

~~This new code is based on scheduler jobs which are similar to crontab in
Linux and execute at defined intervals. These jobs correspond to a
defined procedure in the code.~~

~~Currently, We have the following three scheduler jobs:~~

1. ~~JOB_DEL_PART_MAIN - This job will execute DELETE_PARTITION_MAIN
procedure and collect all info related to partition dropping.~~

2. ~~JOB_DEL_PART_FILETRANSFERSDB - This job will execute
DEL_PART_FILETRANSFERSDB procedure and remove oldest partition of
FILETRANSFERSDB table,selected by DELETE_PARTITION_MAIN procedure.~~

3. ~~JOB_DEL_PART_FILEMETADATA - This job will execute
DEL_PART_FILEMETADATA procedure and remove the oldest partition of
FILEMETADATA table,selected by DELETE_PARTITION_MAIN procedure.~~

#### How to execute code.sql in our Oracle database

Goto the desire place where you want to clone this repository and then
execute following command to clone the repo:

```bash
git clone <https://:@gitlab.cern.ch:8443/crab3/craboracledb.git>
cd craboracledb

```


Select branch according to database i.e. for preprod select qa and for
master select master.

```bash
git checkout qa


```

Login to the desired database, which is already defined in this twiki
page. Verify that you are at the right place.

NOTE: You can execute any Linux command at SQL prompt, prefixed with
exclamation mark !

```plaintext

SQL\> !ls
code.sql README.md sql_client.c

```

```plaintext

Now, to execute code.sql file, use the following command:

SQL\> @code.sql
Function created.
Function created.
Function created.
Function created.
Function created.
Function created.
Function created.
Procedure created.
Procedure created.
Procedure created.
Procedure created.
Procedure created.
Procedure created.
Procedure created.
PL/SQL procedure successfully completed.
PL/SQL procedure successfully completed.
```

If you see the above output it means everything is OK and you will also
receive an email having all the info.

So, If you want to change anything in the code then make appropriate
changes in the code.sql of the repository and execute it like above.

#### How to disable scheduler jobs

If you want to disable/stop scheduler jobs to prevent next partition
dropping, execute following command:

```plaintext

SQL\> execute DISABLE_JOBS;
```

After execution of the above command you will receive an email having
all details.

#### How to enable scheduler jobs

If you want to enable/start scheduler jobs to schedule next partition
dropping, execute following command:

```plaintext
SQL\> execute ENABLE_JOBS;

```

After execution of the above command you will receive an email having
all details.
