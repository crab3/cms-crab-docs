# Database FAQs

## Table is locked ("SQL Error [54] [61000]: ORA-00054")

Full error: `SQL Error [54] [61000]: ORA-00054: resource busy and acquire with NOWAIT specified or timeout expired`

Stop crabserver and SQL client (dbeaver, sqlplus).

Then, check running SQL in [Session Manager](https://session-manager.web.cern.ch/) if there is any query stuck. Verify the queries and and kill it through the SessionManager Web UI.

## What to do if your Oracle database is growing too much

If you are using a DB which has no automatic partition dropping, you may need to cleanup manually

This is the simple procedure (list of SQL commands) to clean everything older than 1 month from the filemetadata and filetransfers tables.
 

```text
select * from user_ts_quotas;
delete from filetransfersdb where tm_creation_time  < ADD_MONTHS (SYSDATE, -1);
PURGE RECYCLEBIN;
alter table filetransfersdb enable row movement;
alter table filetransfersdb shrink space;
select * from user_ts_quotas;
delete from filemetadata where fmd_creation_time < ADD_MONTHS (SYSDATE, -1);
PURGE RECYCLEBIN;
alter table filemetadata enable row movement;
alter table filemetadata shrink space;
select * from user_ts_quotas;
```

If you want to cleanup everything older than 1 week, use
```
delete from filemetadata where fmd_creation_time < (SYSDATE - 7);
```
etc.

Also note that anything you can be rolled back !

If you don't commit nothing will happen (until you run some drop or alter table as that would issue an automatic commit).
So if you don't like what you see issue a : ROLLBACK 

COMMIT; (if you like what you see after the delete)

ROLLBACK; (if you don't)


