# CRAB REST Introduction


!!! danger "NOTICE"
    **Issue [#7408](https://github.com/dmwm/CRABServer/issues/7408) has changed on how process get spawn in order to streaming lot to container stdout. Please see the latest illustrate picture from [#7463-comment](https://github.com/dmwm/CRABServer/issues/7463#issuecomment-1344306384)**.

CRAB REST mainly storing state of our service by contact with Oracle DB directly. Service provide RESTAPI for CRABClient, TaskWorker and Publisher.

Some configuration (such as schedd node) are stored in [CRAB3ServerConfig](https://gitlab.cern.ch/crab3/CRAB3ServerConfig). CRAB REST has feature to reload this configuration every 30 minutes and cached the value if somehow server cannot fetch config.

## How it run

Because we still rely on install and deploy machanism from VM era. Here is explanation on how it work when it run inside the container.


![process-execution-in-crabserver.png](../../images/process-execution-in-crabserver.png)

Link to original scripts: [manage](https://github.com/dmwm/deployment/blob/715c10d1539aba8466f58c4bdb98bb38cc3a35ec/crabserver/manage), [run.sh](https://github.com/dmwm/CMSKubernetes/blob/095d996f8d8d2e0e6e4d9e4aa5f75573900a04fe/docker/crabserver/run.sh), [monitor.sh](https://github.com/dmwm/CMSKubernetes/blob/095d996f8d8d2e0e6e4d9e4aa5f75573900a04fe/docker/crabserver/monitor.sh), [wmc-httpd](https://github.com/dmwm/WMCore/blob/fe218da90b6b381760d8b59b73d7a1b4456e1b87/bin/wmc-httpd)

Note:

- Entrypoint is how the container runs the first process (PID 1). It overrides `ENTRYPOINT` of Dockerfile by `command` in [crabserver.yaml](https://github.com/dmwm/CMSKubernetes/blob/e3de55fa56b215dba7deb5d1d49c22a00a6a6936/kubernetes/cmsweb/services/crabserver.yaml#L185).
- Edge arrow line: the parent call the child.
- Box with dot line: Process spawn but terminate later.

Here is the output from `ps` command, which correlates to the graph above

```
[_crabserver@crabserver-695f7d8c55-stxdg data]$ ps -aefj --forest
UID          PID    PPID    PGID     SID  C STIME TTY          TIME CMD
_crabse+  301222       0  301222  301222  0 15:48 pts/0    00:00:00 bash
_crabse+  301352  301222  301352  301222  0 15:48 pts/0    00:00:00  \_ ps -aefj --forest
_crabse+       1       0       1       1  0 Nov17 ?        00:00:00 /bin/bash /opt/setup-certs-and-run/setup-certs-and-run.sh
_crabse+      10       1       1       1  0 Nov17 ?        00:00:00 /bin/bash /data/run.sh
root         146      10       1       1  0 Nov17 ?        00:00:00  \_ sudo /usr/sbin/crond -n
root         171     146       1       1  0 Nov17 ?        00:00:00      \_ /usr/sbin/crond -n
_crabse+     100       1       1       1  0 Nov17 ?        00:00:01 rotatelogs /data/srv/logs/crabserver/crabserver-%Y%m%d-crabserver-695f7d8c55-stxdg.log 86400
_crabse+     103       1     101     101  0 Nov17 ?        00:00:00 python3 /data/srv/HG2211b-cb31bdf17981ab54e1d311df7b1f0e57/sw.crab_master/slc7_amd64_gcc630/cms/crabse
_crabse+     105     103     101     101  0 Nov17 ?        00:02:23  \_ python3 /data/srv/HG2211b-cb31bdf17981ab54e1d311df7b1f0e57/sw.crab_master/slc7_amd64_gcc630/cms/cr
_crabse+     142       1       1       1  0 Nov17 ?        00:00:04 /bin/bash /data/cmsweb/bin/process_monitor.sh /data/srv/state/crabserver/crabserver.pid crabserver :18
_crabse+     229     142       1       1  0 Nov17 ?        00:00:36  \_ process_exporter -pid 105 -prefix crabserver -address :18270
_crabse+  301351     142       1       1  0 15:48 ?        00:00:00  \_ sleep 15
```

Things get interesting when `wmc-httpd` script run, then executes `main()` of [WMCore/REST/Main.py](https://github.com/dmwm/WMCore/blob/c4055fb1e352efd555844f5edcd2184896efd5cb/src/python/WMCore/REST/Main.py#L508). When execution line reach [start\_daemon()](https://github.com/dmwm/WMCore/blob/c4055fb1e352efd555844f5edcd2184896efd5cb/src/python/WMCore/REST/Main.py#L389), it does:

- Execute `rotatelogs` as a subprocess and change stdout (plus stdin and stderr) of *current process* to stdout of `rotatelogs`.
- double fork itself to establish itself as daemonize process. I call it `crabserver_daemon`.
- Then fork itself again at [line 464](https://github.com/dmwm/WMCore/blob/c4055fb1e352efd555844f5edcd2184896efd5cb/src/python/WMCore/REST/Main.py#L464), which is serving the request. I call the child process `crabserver_serve`.

After `wmc-httpd` executes double fork, the original `wmc-httpd` process get terminated, and [run.sh](https://github.com/dmwm/CMSKubernetes/blob/e3de55fa56b215dba7deb5d1d49c22a00a6a6936/docker/crabserver/run.sh) continue execution and call [monitoring.sh](https://github.com/dmwm/CMSKubernetes/blob/e3de55fa56b215dba7deb5d1d49c22a00a6a6936/docker/crabserver/monitor.sh), then finally call `crond`.
