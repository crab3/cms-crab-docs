# Alerts, Alarms and Restarts

??? note "imported from twiki.cern.ch"
    imported from [this](https://twiki.cern.ch/twiki/bin/view/CMSPublic/ CRABServerPodAlertRestartPrometheus) twiki page

It is possible to automatically restart a pod on cmsweb k8s production cluster.

The current inrfastructure allows to achieve it via PodManager and Prometheus,
and we currently do it only when the number of open fds on a pod is too high.

## CRAB Server k8s pod - alerts and automatic restart based on Prometheus metrics

### Quick info

It is possible to automatically restart a `crabserver` k8s pod based on
the value of various metrics.

In particular, we currently have two rules in place:

1. ~~(2021-09-rule-1) If a pod has more than 75 open fds (averaged over
10m), then an alert is raised and an email notification is sent~~
1. (2021-09-rule-2) If a pod has more than 100 open fds (averaged over
30m), then an alert is raised, an email notification is sent and the pod
itself is restarted

### Rules 2021-09-rule-1 and -2: HTTP 503 errors

#### The problem

Since spring 2021, after migrating the `crabserver` to the k8s cluster,
when the load on the system gets higher we see that some POST/PUT
requests to the `crabserver` fail with error 503. Sometimes the problem
affects a single pod and solved itself after a few minutes, while some
other times restarting the failing pod was enough to make the problem
disappear. However, it happened that restarting the failing pod made the
problem move to other pods. In these cases restarting all the pods was
the only viable option.

We noticed that the http 503 errors are correlated with an increase in
the number of open fds per pod.

Detailed discussion available at

-   [jira/CMSKUBERNETES-80](https://its.cern.ch/jira/browse/CMSKUBERNETES-80)
-   [dmwm/CRABserver/6708](https://github.com/dmwm/CRABServer/issues/6708)

#### The band-aid: automatically restarting the pod based on prometheus metrics

While trying to replicate the problem and while looking for a proper
solution, we convened that adding some tailored alerts and automatically
restarting the failing pods could help us in the unfortunate event that
this problem present itself again.

The current CMSWEB k8s clusters infrastructure provides:

-   retrieval of certain metrics from the pods with `prometheus`
-   the possibility to set alerts based on the value of these metrics
    (Step 1). This creates alerts in [`alertmanager`
    ](https://cms-monitoring.cern.ch/alertmanager/#/alerts)
-   the possibility to send notifications when an alert is recorded
    (Step 2)
-   the possibility to perform some action on the pod that originated
    the alert, such as restarting it (Step 3)

##### Step 1

References to prometheus and alertmanager:

- [prometheus docs - overview](https://prometheus.io/docs/alerting/latest/overview/)
- [prometheus docs - configuration](https://prometheus.io/docs/alerting/latest/configuration/)
- [Presentation about CMSMonitoring/AlertManager](https://www.dropbox.com/s/8p2qlr58vduey06/CMSMonitoring_AlertManager.pdf?dl=0)
- Reference of CMS monitoring infrastructure [https://cmsmonit-docs.web.cern.ch/](https://cmsmonit-docs.web.cern.ch/)

The list of available metrics is retrieved within a pod with:

```bash
> curl -s http://localhost:18270/metrics | grep crab | egrep -v "HELP|TYPE" 
crabserver_cpu_percent 0 
[...]
crabserver_process_open_fds 7
[...]
```

The alerts are configured with a `yaml` file ending with `.rules` .
Every alert file can be tested with `promtool` 
and the configuration of the test is contained in a yaml file ending with `.test`.

If you want to set alerts based on these metrics, it is necessary to
change the alert files:

-   in [dmwm/CMSKubernetes](https://github.com/dmwm/CMSKubernetes) :
    `kubernetes/cmsweb/monitoring/prometheus/rules/crabserver.rules`
-   in [gitlab/cmsmonitoring/cmsmon-config](https://gitlab.cern.ch/cmsmonitoring/cmsmon-configs/) : edit the file `prometheus/crabserver.rules`

The changes in these two repos need to be almost identical. The only
diference is that the `host:` directive need to be present only in
`CMSKubernetes` repo.

The value used to check if an alert should be triggered can be directly
the prometheus metric or a function of those metrics. A description of
the available function is availble on the official documentation, at
[https://prometheus.io/docs/prometheus/latest/querying/functions/](https://prometheus.io/docs/prometheus/latest/querying/functions/)
In particular, we are interested in `aggregation_over_time`, since we want
to smoothen the metric with a rolling average.

For example, we wanted to add an alert when the average over time (for
30m) of the metric `crabserver_process_open_fds` is above 100, so we
needed to add

```yaml
# crabserver.rules
groups:
- name: crabserver
    rules:
#[...]
    - record: avg_open_fds_30m
    expr: avg_over_time(crabserver_process_open_fds[30m])
#[...]
    - alert: CRAB server service has high number of fds
    expr: avg_open_fds_30m > 100
    for: 1m
    labels:
        severity: high
        tag: cmsweb
        service: crab
        host: "{{ $labels.host }}"                                      # this line is not necessary in the gitlab repo
        action: Please restart CRAB server on {{ $labels.instance }}
    annotations:
        summary: "CRAB {{ $labels.env }} environment"
        description: "{{ $labels.env }} has high level of fds {{ $value }} (avg 30m) for more than 1m"
#[...]
```

Notice that we set two time intervals:

-   length of the interval over which to compute the rolling average.
    Selecting 30m means that the variable used to trigger the alert is
    the rolling average of the metric `crabserver_process_open_fds`
    over the last 30m. This value has to be considered as a generic metric
-   lentgh of the interval for which the generic metric need to be over
    the threshold. Since the data is retrieved every 1m, selecting
    `1m` means "as soon as"

It is also necessary to change the test files:

-   in [dmwm/CMSKubernetes](https://github.com/dmwm/CMSKubernetes) :
    `kubernetes/cmsweb/monitoring/prometheus/rules/crabserver.test`
-   in
    [gitlab/cmsmonitoring/cmsmon-config](https://gitlab.cern.ch/cmsmonitoring/cmsmon-configs/) : `prometheus/crabserver.test`

```yaml
# crabserver.test
# [...]
- interval: 1m
    input_series:
    - series: 'avg_open_fds_30m{env="prod",instance="test-instance",host="k8s-test"}' # no `,host="k8s-test"` in gitlab repo
    values: '101+1x100'
    alert_rule_test:
        - eval_time: 10m
        alertname: CRAB server service has high number of fds
        exp_alerts:
            - exp_labels:
                    severity: high
                    tag: cmsweb
                    service: crab
                    host: k8s-test                                            # this line is not necessary in the gitlab repo
                    action: Please restart CRAB server on test-instance
                    instance: test-instance
                    env: prod
                exp_annotations:
                    summary: "CRAB prod environment"
                    description: "prod has high level of fds 111 (avg 30m) for more than 1m"
    # [...]
```

The offical reference on alert unit test is available at
[prometheus docs - unit tests](https://www.prometheus.io/docs/prometheus/latest/configuration/unit_testing_rules/)

While how to set the rules may be straightforward, how to set the
correct values for the alert test is not so much. You need to know that:

-   prometheus test an alert by generating a synthetic set of values
    that emulate the values of the generic metric over time. We define
    these values insdide the list argument of the key `values:`. 
    The first value of the
    list correspond to time=0, then every element of this list is
    separated by each other in time by the `interval:` . prometheus checks
    that the emulated value of the generic metric corresponding to
    `eval_time` after time=0
    triggers the alert, and that the alert that would have been
    triggered matches the one provided in the unit test.
-   `values: 'A+BxC'`: is expanded into the list
    `[A, A+B, A+2*B, ..., A+C*B]`. This is a nice convenient way of
    generating the list of values of the generic metric.
-   since the interval is set to 1m, it means that the first value (A)
    correspond to the first minute, the second (A+B) to the second
    minute, ... the last value is (A+100\*B) correspond to 100m after
    the beginning
-   since `eval_time` is 10m, it means that the unittest checks if the alert
    would be fired with the value =A+10*B`.
-   In our case, `avg_open_fds_30m` after 10m is `101+10\*1 = 111`, which
    is greater than 100, so this test passes.

You can check what you have written with

-   You can check that the alert syntax is correct with the command
    (available in lxplus, without sourcing any environment) `/cvmfs/cms.cern.ch/cmsmon/promtool check rules kubernetes/cmsweb/monitoring/prometheus/rules/crabserver.rules`
-   You can check that the alert matches the values that you expect
    `/cvmfs/cms.cern.ch/cmsmon/promtool test rules kubernetes/cmsweb/monitoring/prometheus/rules/crabserver.test`

Such changes have been implemented in the PRs

-   test values
    [dmwm/CMSKubernetes/814](https://github.com/dmwm/CMSKubernetes/pull/814)
-   production-ready values
    [dmwm/CMSKubernetes/816](https://github.com/dmwm/CMSKubernetes/pull/816)
-   test values
    [gitlab/cmsmonitoring/cmsmon-config/11](https://gitlab.cern.ch/cmsmonitoring/cmsmon-configs/-/merge_requests/11)
-   production-ready values
    [gitlab/cmsmonitoring/cmsmon-config/12](https://gitlab.cern.ch/cmsmonitoring/cmsmon-configs/-/merge_requests/12)

##### Step 2

Based on the alerts just defined, it is possible to send notifications.
The settings are kept in `alertmanager/alertmanager.yaml` [gitlab/cmsmonitoring/cmsmon-config](https://gitlab.cern.ch/cmsmonitoring/cmsmon-configs/)

It is possible to receive notifications through the following channels

-   email
-   Whatever supports webhooks, such as slack (already implemented),
    mattermost, telegram, ...

Example of changes have been implemented in
[gitlab/cmsmonitoring/cmsmon-config/13](https://gitlab.cern.ch/cmsmonitoring/cmsmon-configs/-/merge_requests/13)

##### Step 3

It is then necessary to change the `PodManager/config` so that it reacts to the alert in `alertmanager`. The configuration is kept in the
file `podmanager/config.json` in the repo 
[gitlab/cmsweb-k8s/services_config/](https://gitlab.cern.ch/cmsweb-k8s/services_config/), where the `prod` branch contains the configuration for the
prod cluster and the `preprod` branch contains the configuration for the
testbed cluster. It is necessary to provide a PR for both branches.

Reference changes are:

-   `preprod`:
    [gitlab/cmsweb-k8s/services\_config/109](https://gitlab.cern.ch/cmsweb-k8s/services_config/-/merge_requests/109)
-   `prod`:
    [gitlab/cmsweb-k8s/services\_config/110](https://gitlab.cern.ch/cmsweb-k8s/services_config/-/merge_requests/110)

#### Other approaches

If this "band-aid" proves not to be efficient, then we may explore other
automatic restarting procedures, such as creating alerts based on
generic metric available in grafana. This approach may not be too
straightforward and has been currently put on hold.

