# CRAB REST APIs

## Find-your-way guide to code in [CRABInterface directory](https://github.com/dmwm/CRABServer/tree/master/src/python/CRABInterface) and how it gets called inside the REST

URL's to access the REST have the format: `cmsweb/crabserver/instance/api?par1=val1&par2=v2...` the first token after`api?` could also be `subresource=...`

CRABREst is instantiated via the WMCore's [DatabaseRESTApi](https://github.com/dmwm/WMCore/blob/707fcee02c264805ac2968ffc32c677d39731997/src/python/WMCore/REST/Server.py#L1691C7-L1691C22) class which uses the `instance` string in the URL to pick the DB-access params from `/data/srv/current/auth/crabserver/CRABServerAuth.py`.

CRAB Server starts like this:

 1. `run.sh` starts [WMCore/REST/Main](https://github.com/dmwm/WMCore/blob/master/src/python/WMCore/REST/Main.py) via the `/usr/local/bin/wmc-httpd` command

 2. `REST/Main` reads the configuration from `/data/srv/current/config/crabserver/config.py`

3. in the configuration we define a `view` section and fill it with the content of the `data` section below
```text
views = conf.section_('views')
data = views.section_('data')
``` 

4. in that way, when `WMCore/REST/Main` runs and goes through its `install_application` method, it will [install](https://github.com/dmwm/WMCore/blob/707fcee02c264805ac2968ffc32c677d39731997/src/python/WMCore/REST/Main.py#L273-L288) the `craberver` application on  path `/crabserver` using code from `CRABInterface.RESTBaseAPI.RESTBaseAPI` inside `CherryPy`

5. [RESTBaseAPI](https://github.com/dmwm/CRABServer/blob/a1e9ca08f0972cb8a4db48570e9fd0ee91fd79b2/src/python/CRABInterface/RESTBaseAPI.py) subclasses `DatabaseRESTApi` which provides connection to DB.

6. The field after `instance` in the URL is `api` and that is mapped to code in [RESTBaseApi](https://github.com/dmwm/CRABServer/blob/247a64e437c99ac046cc882c6d1eb82785aa1c2b/src/python/CRABInterface/RESTBaseAPI.py#L58-L65) which defines which `REST*` file is used from `CRABServer/src/python/CRABInterface`
```python
 self._add( {'workflow': RESTUserWorkflow(app, self, config, mount, extconfig), 
             'info': RESTServerInfo(app, self, config, mount, extconfig), 
             'filemetadata': RESTFileMetadata(app, self, config, mount), 
             'workflowdb': RESTWorkerWorkflow(app, self, config, mount), 
             'task': RESTTask(app, self, config, mount), 
             'filetransfers': RESTFileTransfers(app, self, config, mount), 
             'fileusertransfers': RESTFileUserTransfers(app, self, config, mount), 
            }) 
```

7. there is a tricky twist for `workflow` API due to the fact that original developers envisioned the possibility to have a backend agnostic server which could deal with workflows using HTTcondor or a different scheduler. So `RESTUserWorkflow` [uses](https://github.com/dmwm/CRABServer/blob/a1e9ca08f0972cb8a4db48570e9fd0ee91fd79b2/src/python/CRABInterface/RESTUserWorkflow.py#L34) internally [DataUserWorkflow.py](https://github.com/dmwm/CRABServer/blob/a1e9ca08f0972cb8a4db48570e9fd0ee91fd79b2/src/python/CRABInterface/DataUserWorkflow.py) which defines its internal `workflowManager` according to what is specified in the configuration file. In this way `workflow` API eventually result in  using code from [HTCondorDataWorkflow.py](https://github.com/dmwm/CRABServer/blob/master/src/python/CRABInterface/HTCondorDataWorkflow.py)
```
data.workflowManager = 'HTCondorDataWorkflow'
```


8. in `CRABInterface` directory the files named `REST*` are what initially servers the HTTP call for that API, internally they validate call parameters and act on it. For some API's all code is in the `REST*` file, for some there are auxiliary files `Data*.py`

Ideally we could merge code from `HTCondorDataWorkflow` into `DataUserWorflow` and get rid of the `data.worflowManager` param in the configuration

