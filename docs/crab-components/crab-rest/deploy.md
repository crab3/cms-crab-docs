# Deploying CRAB REST

## Setup for new environment

- Request new k8s test cluster from CMSWEB team by create CMSKubernetes Jira ticket. [Example CMSKubernetes-144](https://its.cern.ch/jira/browse/CMSKUBERNETES-144)
- Add new environment to `ServerUtilities.py` and WebUI in order to easily select REST/DB instance in client. [Example commit](https://github.com/dmwm/CRABServer/commit/64257a3a69d740822ee5f95a12a50e30b35975e0).
- Add new machine to CMS Prometheus target list to use in [Node Exporter Full CRAB VMs](https://monit-grafana.cern.ch/d/rYdddlPWkk/node-exporter-full-crab-vms?orgId=11&refresh=1m) dashboard. Create JIRA ticket to CMSMONIT to add it for us ([example ticket](https://its.cern.ch/jira/browse/CMSMONIT-517)).

## Build the Docker image

The docker image is part of [Build-Deploy-Test](../../crab-infrastructure/crab-cicd/gitlab/pipelines/build-deploy-test.md) pipeline.

If this is the first time you deploy on a new environment, you can grab any release tag to deploy (e.g., `registry.cern.ch/cmscrab/crabserver:v3.241019-stable`).

## Configuration

CRAB REST configuration is in [the helm chart](https://github.com/dmwm/CMSKubernetes/blob/8bde12f667d03b0033c05d33699e427385dc2b4e/helm/crabserver/config/prod/config.py).

## Deploy

We deploy services on Kubernetes using Helm chart from [helm/crabserver](https://github.com/dmwm/CMSKubernetes/blob/master/helm/crabserver), but as templating tools to generate K8s manifests and apply to cluster using plain `kubectl` command.

See [CRAB K8s Helm Chart](../../crab-infrastructure/crab-k8s/helm/introduction.md) for the instruction on how to use it.

## Secret

Use [deploy-secrets.sh](https://gitlab.cern.ch/crab3/crab-secrets/-/blob/master/deploy-secrets.sh) to deploy DB and S3 credentials on Kubernetes.

For instruction, please see [How to use](../../crab-secrets/how-to-use.md#apply-secrets-to-kubernetes) in crab-secrets section.
