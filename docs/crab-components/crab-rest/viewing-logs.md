# Viewing logs

We changed the logs configuration of REST to stream it directly to the container stdout. Please see <https://github.com/dmwm/CRABServer/issues/7408> for more detail.

Here is how to view the logs.

## Live logs

You can simply use `kubectl` to view live logs from a single pod like this:

```
kubectl -n crab logs crabserver-67c6f87957-xnzg4 --tail=1 -f
```

`--tail=1` to ask kube-apiserver to get only the last log line. Otherwise, you will get all logs from the latest logs file ([ref](https://kubernetes.io/docs/concepts/cluster-administration/logging/#log-rotation)). Then, follow by `-f` to poll a new log line like tail command.

We recommend using `stern` to get logs from multiple pods:

```
/cvmfs/cms.cern.ch/cmsmon/stern -n crab crabserver --tail=1
```

The pod-query, `crabserver`, is regular expression or a Kubernetes resource in the form `<resource>/<name>` ([ref](https://github.com/stern/stern/blob/master/README.md)).

You can pipe the logs to `grep` only specific lines. For example, grep also display 5 lines before (`-B5`) and after (`-A5`) after matching line:

```
/cvmfs/cms.cern.ch/cmsmon/stern -n crab crabserver --tail=1 | grep -B5 -A5 Exception
```

For more advanced use cases, please consult <https://cms-http-group.docs.cern.ch/k8s_cluster/k8s_logs/>.

## S3 logs: Logs that longer than 15 minutes

CMSWEB set up fluentd to flush the container logs to Openstack S3 every 15 minutes. And another service sync logs files to `vocms0750` machine in `/data/s3/logs/cmsweb/crab-logs/crabserver/`, seperate into `YYYY/MM/DD` directory, each file has a name with suffix with `startAt=HH:mm_n.txt`.

You can use Bash's Glob ([ref](https://tldp.org/LDP/abs/html/globbingref.html)) to select logs files from a specific time, i.e., `2023/03/*/*startAt=12:00*` to grep all logs from March 2023, but only the files that contain logs from 12:00-12:15.

For example, to get logs that contain `Exception` in the line, for all logs files that match the date and time condition per above explanation:

```
grep -Hn -R -P 'Exception' /data/s3/logs/cmsweb/crab-logs/crabserver/2023/03/*/*startAt=12:00*
```

Addition arguments from `grep`: `-Hn` will print file name and line number, `-R` to make grep recursive into subdirectory, `-P` is using PCRE regexps.

To get specific pods from logs file, use `Podname=<podname>` (Please see explanation about logs suffix in "Log Stream" section below):

```
grep -Hn -R -P 'Exception' /data/s3/logs/cmsweb/crab-logs/crabserver/2023/03/*/*startAt=12:00*
```

## Logs stream

CRAB REST has 2 different logs stream:

- **Cherrypy logs**: all logs from WMCore code are streaming to cherrypy logs, including http access, DB trace logs, and internal cherrypy logs.
- **CRAB logs**: CRAB REST code using this stream. It gets initialized in [RESTBaseAPI.py](https://github.com/dmwm/CRABServer/blob/e2e09dde7ebedf1450df67450e3c77bda03c113f/src/python/CRABInterface/RESTBaseAPI.py#L156-L193).

Both logs stream got `- Podname=<podname> Type={cherrypylog,crablog}` append to every logs line. So you can use `grep` to separate logs stream.

For example, `grep Type=crablog` to view only CRAB logs:

```
grep -Hn -R -P 'Podname=crabserver-79cc8b5644-6vnrd' /data/s3/logs/cmsweb/crab-logs/crabserver/2023/03/*/*startAt=12:00* | grep Exception
```

## Note for logs retention policy

- **Live logs**: 10MiB (Kubelet's default config).
- **S3 logs**: 90 days.
