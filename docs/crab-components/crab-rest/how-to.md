## CRAB REST - How-to guides

### validate deployment of crabserver on a cmsweb cluster

Make sure that you can access crabserver with curl

```bash
curl --cert $X509_USER_PROXY --key $X509_USER_PROXY https://cmsweb.cern.ch/crabserver/prod/info
```

Make sure that you can see the crabserver pods running in the `crab`
namespace

```bash
kubectl get pods -n crab
```

For prod, preprod and testbed clusters, make sure that you see the logs in s3:

```bash
ssh vocms0755
# prod
ls -lrt /data/s3/logs/cmsweb/crab-logs/crabserver/2024/08/08/*
# testbed
ls -lrt /data/s3/logs/cmsweb-testbed/crab-logs/crabserver/2024/08/08/*
# preprod
ls -lrt ??
```




### Access cmsweb with token

References

- [cms-http-group](https://cms-http-group.docs.cern.ch/k8s_cluster/benchmark/#sps): At the moment their guides only offer some example, not complete description
- [early experiments by Stefano](https://twiki.cern.ch/twiki/bin/view/Sandbox/TokensTips#How_to_check_token_validity_etc)

ACHTUNG! This is still under development and information contained in this webpage can become obsolete pretty quickly (2022-08-25)

Accessing cmsweb with tokens is currently only available in cmsweb test clusters. cms-http-group docs will use `test6` as an example, we (CRAB) will need to use `test2` and `test11`.

Which ports are used in the prod/preprod clusters

- `443` and `8843` can be accessed with a valid certificate both from a webpage and from curl

Which ports are used in the test clusters

- `443`: from a web browser. Behind CERN SSO, which provides a token for accessing resources at this port
- `8843`: just for curl. you need to already have a valid token.

#### first of all: get a token

Services

- [CERN SSO](https://auth.cern.ch)
- [CMS INDIGO IAM](https://cms-auth.web.cern.ch)

Tools

- [oidc-agent](https://github.com/indigo-dc/oidc-agent)

overview (the tick means that somebody from crab team tested it and wrote down an example):

- [x] CLI. x509 -> token for cmsweb
- [x] CLI. application -> cern sso
- [ ] CLI. x509 -> iam. not trivial so far, mey need to scrape [web gui](https://github.com/indigo-iam/iam/issues/193)
- [ ] CLI. application -> iam token. should be [possible](https://indigo-iam.github.io/v/v1.7.1/docs/tasks/user/getting-a-token/)
- [x] web. cern sso -> iam
- [x] web. x509 -> iam

Resources:

- [202111 - pre-GDB - WLCG Auth & IAM users Meeting](https://indico.cern.ch/event/876810/contributions/4590472/attachments/2341358/3991702/20211108-IAM-Users-Workshop-IAM-Status-Evolution.pdf)

##### x509 certificate or proxy on voms -> token for cmsweb

From a pristine lxplus shell

```bash
curl -i -L --key ~/.globus/userkey.pem --cert ~/.globus/usercert.pem \
-d grant_type=client_credentials https://cmsweb-test11.cern.ch:8843/token
```

or export the path of your proxy file into `$X509_USER_PROXY` and then

```bash
# 2023 09 21 - ok
curl -i -L --key $X509_USER_PROXY --cert $X509_USER_PROXY \
-d grant_type=client_credentials https://cmsweb-testbed.cern.ch/token

# 2023-09-21: broken
curl -i -L --key $X509_USER_PROXY --cert $X509_USER_PROXY \
-d grant_type=client_credentials https://cmsweb-test11.cern.ch:8843/token
```

NB: if you do not want to use the curl `-i` parameter, just load the crab client environment

Moreover, you can also get a token from the web browser, visiting [https://cmsweb-test11.cern.ch/token](https://cmsweb-test11.cern.ch/token)

If I understand correctly, the `/token` endpoint gets the token

- from CERN SSO when you browse the webpage at port `443`
- from CMS IAM [https://cms-auth.web.cern.ch/](https://cms-auth.web.cern.ch/) when you access from curl

then the cmsweb frontend can accept the tokens from multiple identity providers, so it does not make any difference who issued the specific token, just use whatever you have.

Since these tokens have a limited lifetime (~20min from the broswer, ~10min from curl), it is convinient to have a command that exports the token into an environment variable. You will need to run this often!

Hence, save the token with

```bash
export token=$(curl -s -k -L --key $X509_USER_PROXY --cert $X509_USER_PROXY -d grant_type=client_credentials https://cmsweb-test11.cern.ch:8843/token | jq .access_token | tr -d '"')
```

check the content of the token by using an online tool, such as [jwt.io](https://jwt.io) or the command line with

```bash
jq -R 'split(".") | .[0],.[1] | @base64d | fromjson' <<< $(echo "${token}")
```

```plaintext
{
  "alg": "RS256",
  "kid": "8c7c",
  "typ": "JWT"
}
{
  "scope": "read:/store write:/store/user/dmapelli",
  "ver": "scitoken:2.0",
  "exp": 1661437993,
  "jti": "3cf533bb-302c-420b-b13a-05ad728f3314",
  "iat": 1661437393,
  "iss": "https://cmsweb-test11.cern.ch",
  "nbf": 1661437393,
  "sub": "dmapelli"
}
```

Similarly, get the expiration with

```bash
date -d @$(jq -R 'split(".") | .[1] | @base64d | fromjson | .exp' <<< $(echo "${token}"))
```
```plaintext
Thu Aug 25 16:33:13 CEST 2022
```

Validate your token with

```bash
curl -i -L -H "Authorization: bearer $token" \
https://cmsweb-test11.cern.ch:8843/token/validate
```

a valid output looks like

```plaintext
> curl -i -L -H "Authorization: bearer $token" https://cmsweb-test11.cern.ch:8843/token/validate
HTTP/2 200
content-type: text/plain; charset=utf-8
content-length: 223
date: Thu, 25 Aug 2022 08:00:11 GMT

{"scope":"read:/store write:/store/user/dmapelli","ver":"scitoken:2.0","exp":1661414986,"jti":"e8d0933c-8be3-452c-b999-a546dc7deaad","iat":1661414386,"iss":"https://cmsweb-test11.cern.ch","nbf":1661414386,"sub":"dmapelli"}
```

##### application -> token

Panos suggests registering an application in IAM, then use the application credentials (cliend id, password) to
authenticate with IAM and get a token.

This may be convinient for applications, it is definitely not an option for users.

Something similar can be achieved with CERN SSO:

- [auth cern - api access](https://auth.docs.cern.ch/user-documentation/oidc/api-access/)

Keep in mind that CERN SSO provides the following endpoints: [https://auth.docs.cern.ch/services/instances/](https://auth.docs.cern.ch/services/instances/)

Example: dario registered an application on [https://application-portal-qa.web.cern.ch](https://application-portal-qa.web.cern.ch)

- application identifier: `dmapelli-user-token`
- SSO Registration tab, redirect URIs: whatever
- SSO Registration tab, base URL: whatever
- SSO Registration tab, flag "My application will need to get tokens using its own client ID and secret"
- SSO Registration tab, click on icon with the eye "view or regenerate secret". write client-id and client-secret down

then go to lxplus, save client id and secret to a file, then you can get an access token.

```bash
curl --location --request POST "https://keycloak-qa.cern.ch/auth/realms/cern/api-access/token" \
--header 'Content-Type: application/x-www-form-urlencoded' \
--data-urlencode 'grant_type=client_credentials' \
--data-urlencode "client_id=$(cat ~dmapelli/crab/creds/cern_sso_qa-dmapelli_user_token.txt | head -n 1)" \
--data-urlencode "client_secret=$(cat ~dmapelli/crab/creds/cern_sso_qa-dmapelli_user_token.txt | tail -n 1)" \
--data-urlencode "audience=dmapelli-user-token"
```

exmaple: dario registered a client on CMS IAM, get a token following instructions [here](https://gist.github.com/andreaceccanti/5b69323b89ce08321e7b5236de503600)

```bash
curl -s -f -L -v \
-u "$(cat ~dmapelli/crab/creds/cmsiam-dmapelli_test_iam_client.txt | head -n 1):$(cat ~dmapelli/crab/creds/cmsiam-dmapelli_test_iam_client.txt | tail -n 1)" \
-d "client_id=$(cat ~dmapelli/crab/creds/cmsiam-dmapelli_test_iam_client.txt | head -n 1)" \
-d "scope=openid profile email" \
 https://cms-auth.web.cern.ch/devicecode 
```


#### use a token to access CRAB REST resources

This should be as easy as getting a valid token, then

```bash
curl -i -L -H "Authorization: bearer $token" \
https://cmsweb-test11.cern.ch:8843/crabserver/dev/info
```

However, at the moment (2022-08-25) this fails with [1]. I suspect that we need to adapt the code that extracts the username from a request. At the moment we have functions to get it from proxy files / headers, we need a new function that takes it from the token itself. Some development on our side will be required.

We think that there may be something off with the headers. We will check with the `/httpgo` endpoint. See

- https://cmsweb-test11.cern.ch/httpgo
- https://cmsweb-testbed.cern.ch/httpgo
- https://cmsweb-prod.cern.ch/httpgo
- https://cmsweb.cern.ch/httpgo

### more info and logs

[1]

```plaintext
> curl -i -L -H "Authorization: bearer $token" https://cmsweb-test11.cern.ch:8843/crabserver/dev/info
HTTP/2 403
content-type: text/html;charset=utf-8
date: Thu, 25 Aug 2022 08:02:33 GMT
response-proto: HTTP/1.1
response-status: 403 Forbidden
response-status-code: 403
response-time: 9.027063ms
response-time-seconds: 0.009029145
server: CherryPy/17.4.0
x-content-type-options: bla
content-length: 750

<!DOCTYPE html PUBLIC
"-//W3C//DTD XHTML 1.0 Transitional//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"></meta>
    <title>403 Forbidden</title>
    <style type="text/css">
    #powered_by {
        margin-top: 20px;
        border-top: 2px solid black;
        font-style: italic;
    }

    #traceback {
        color: red;
    }
    </style>
</head>
    <body>
        <h2>403 Forbidden</h2>
        <p>You are not allowed to access this resource.</p>
        <pre id="traceback"></pre>
    <div id="powered_by">
      <span>
        Powered by <a href="http://www.cherrypy.org">CherryPy 17.4.0</a>
      </span>
    </div>
    </body>
</html>
```

### troubleshoot 

#### curl to crabserver ui fails with "dial tcp: lookup ..."

It can happen, especially on testN k8s clusters, that curl requests to crabserver rest
fail with a "dial tcp: lookup ..." error, and looking at the logs does not give any information,
as if the request never made it to our application.

On 2023-05, we noticed that this correlated with a small vm used for the test cluster node.
We asked Aroosha to increase the size to `m2.xlarge` and we stopped seeing this problem.

In case you encounter this problem again, you should ask the cmsweb operator to help you.
If the problem is with a test cluster and the cmsweb operator is not around, 
we crab operators can temporarily solve the issue by restarting the k8s node.

First of all, make sure that the problem relies on the node itself having problems.

This is how healthy nodes look like:

```plaintext
> kubectl get nodes
NAME                                  STATUS   ROLES    AGE    VERSION
cmsweb-test11-h4mlhouwfm45-master-0   Ready    master   308d   v1.22.9
cmsweb-test11-h4mlhouwfm45-node-0     Ready    <none>   308d   v1.22.9

> kubectl describe node cmsweb-test11-h4mlhouwfm45-node-0
[...]
Conditions:
  Type                    Status  LastHeartbeatTime                 LastTransitionTime                Reason                       Message
  ----                    ------  -----------------                 ------------------                ------                       -------
  KernelDeadlock          False   Wed, 17 May 2023 02:35:19 +0200   Wed, 03 May 2023 15:34:56 +0200   KernelHasNoDeadlock          kernel has no deadlock
  ReadonlyFilesystem      False   Wed, 17 May 2023 02:35:19 +0200   Wed, 03 May 2023 15:34:56 +0200   FilesystemIsNotReadOnly      Filesystem is not read-only
  CorruptDockerOverlay2   False   Wed, 17 May 2023 02:35:19 +0200   Wed, 03 May 2023 15:34:56 +0200   NoCorruptDockerOverlay2      docker overlay2 is functioning properly
  NetworkUnavailable      False   Wed, 03 May 2023 15:34:52 +0200   Wed, 03 May 2023 15:34:52 +0200   CalicoIsUp                   Calico is running on this node
  MemoryPressure          False   Wed, 17 May 2023 02:35:47 +0200   Mon, 15 May 2023 14:43:57 +0200   KubeletHasSufficientMemory   kubelet has sufficient memory available
  DiskPressure            False   Wed, 17 May 2023 02:35:47 +0200   Mon, 15 May 2023 14:43:57 +0200   KubeletHasNoDiskPressure     kubelet has no disk pressure
  PIDPressure             False   Wed, 17 May 2023 02:35:47 +0200   Mon, 15 May 2023 14:43:57 +0200   KubeletHasSufficientPID      kubelet has sufficient PID available
  Ready                   True    Wed, 17 May 2023 02:35:47 +0200   Mon, 15 May 2023 14:43:57 +0200   KubeletReady                 kubelet is posting ready status
[...]
```

If `node-0` status is "NotReady" while all other statuses do not indicate any problem, 
then the node has a problem that k8s could not detect and you should simply restart it.

In particular, all the members of the egroup `cms-service-crab-sysadmins` have access
to the openstack project "CMS Webtools Mig" that owns the VMs used for the test clusters.
Make sure that when you select this project, you also specify the "cern" region.
Then look in "Project / Compute / Instances" the node that you are interested in
and force a VM hard reboot from the web ui. K8s will automatically handle the restart 
of the node and if you are lucky your node will not experience any more troubles
for al least a few hours.
