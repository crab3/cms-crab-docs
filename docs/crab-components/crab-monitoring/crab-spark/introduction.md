# CRAB Spark Pipeline

We have a pipeline that aggregates data from HDFS with spark and sends the
results to Opensearch to be accessed via Grafana.

We rely on what the CMS O&C MONIT team saves into HDFS to join information
coming from different sources as we need. For example, it is possible to
join the information from the "condor raw" metrics with the CRAB TaskDB table.

Data is accessible in the [CRAB Usage (spark)](https://monit-grafana.cern.ch/d/d07e49f1-7779-42ad-a9ff-d54dd4b9b829/crab-usage-spark?orgId=11) dashboard.

## Data source

Input data is contained in CMS HDFS directories:

- "crab task db":
    - `/project/awg/cms/crab/tasks/{date}`
- "condor raw metrics":
    - `/project/monitoring/archive/condor/raw/metric/{date}`
- "rucio rules history":
    - `/project/awg/cms/rucio/{date}/rules_history/`

You can list the content of the HDFS directories with

```bash
# ssh lxplus9
kinit
export KRB5CCNAME=$(klist | grep FILE | cut -d":" -f2,3 | xargs)

# yeah, these scripts can only be sourced from bash, no zsh
bash
source /cvmfs/sft.cern.ch/lcg/views/LCG_105a_swan/x86_64-el9-gcc13-opt/setup.sh
source /cvmfs/sft.cern.ch/lcg/etc/hadoop-confext/hadoop-swan-setconf.sh analytix

hadoop fs -ls /project/awg/cms/
```

reference: [CERN IT hadoop user guide](https://hadoop.web.cern.ch/getstart/client_cvmfs/)

## Spark Scripts

All spark related scripts are in [crab-spark](https://github.com/dmwm/CRABServer/tree/master/src/script/Monitor/crab-spark) directory.

## Build and deploy

We build special docker image for running spark, `registry.cern.ch/cmscrab/crabspark` from the [cicd/monit\_spark/Dockerfile](https://github.com/dmwm/CRABServer/blob/19b1bf76091ffe6a275683be5adf716310714ff6/cicd/monit_spark/Dockerfile).

This deploy in `crab-prod-tw02` as a cronjob using [crabspark_cron_daily.sh](https://gitlab.cern.ch/ai/it-puppet-hostgroup-vocmsglidein/-/blob/bc61a79da85d72e4db90b3fbca07a369f456e9ee/code/templates/crabtaskworker/crabspark_cron_daily.sh.erb) with [crabspark.pp](https://gitlab.cern.ch/ai/it-puppet-hostgroup-vocmsglidein/-/blob/qa/code/manifests/crabtaskworker/crabspark.pp?ref_type=heads).

Cronjob are running daily and push data to [os-cms.cern.ch](https://os-cms.cern.ch).

Note that set `enable_spark_client: true` to enable running spark from our TW machines.

### Note

By computing data for one week, it should take time around 2 minutes or less for each data source (1 data pulling file), except condor data which spends around 5-10 minutes.

### References

The plan of this project is available on [codimd](https://codimd.web.cern.ch/n0zKtr4KTZq2gquFpoQJdA?view).
