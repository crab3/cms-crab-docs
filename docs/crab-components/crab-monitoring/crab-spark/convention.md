# Conventions

## General

1. Please use [taskdb notebook](https://github.com/dmwm/CRABServer/blob/master/src/script/Monitor/crab-spark/notebooks/crab_taskdb.ipynb) as master template.
2. Assume start date is inclusive, end date is exclusive.
    - `--start 2024-10-14`, `--end 2024-10-16`; this mean you will get metrics from 2024-10-14 00:00:00 to 2024-10-15 23:59:59.
3. However, the HDFS data partition use the end date. E.g., `--end 2024-10-16`, the path should be `/project/awg/cms/rucio/2024-10-16/rules_history/`.
4. Pass arguments to notebook via environment variables.
5. Extra column,
    1. `timestamp` come from the column that you want use as x-axis in time series graph (For example, we use `EXPIRES_AT` as `timestamp` for `rules_history` table.
    2. `type` is the field we use to filter only index we want to search. Similar to `metadata.type` in monit-opensearch.
6. Opensearch instance for spark is [os-cms.cern.ch](https://os-cms.cern.ch).
7. Test index: `crab-test-*`, Prod index: `crab-prod`

## Data specific

- 90% of Condor metrics need 2 days to flush all metrics from data pipeline to HDFS. So we run spark job from data 2 days ago (`--ndaysago 2`).
- If raw data is snapshot, and it is read only, you can use data from the latest dump.
