# Backfill

"Backfill" is, you go "back" in time on the day that does not have any data from pipeline and "fill" the result.

Basically, you recompute the data for the date when the data pipeline broke.

There are 3 situations we encounter so far:

## Transient errors

Some services went down on that day which cause the pipeline to break.

Rerun the pipeline is enough.


## Error in query

This case, you need to recompute the whole index.

Depend what is the policy of index, but for the 2024-10-31 master branch, we create monthly index, so you need to recompute the whole month.

Just do:
- delete index(es) of that month(s).
- Set time range 1 month, e.g., `--start 2024-09-01` and `--end 2024-10-01` (better to compute 1 month at a time)

## Raw data not available

This is really depend on raw data.

If raw data have only data for that date. You simply lost metrics.

But if raw data contains the data from the beginning of time (sqoop dump from taskdb for example), we can use latest available raw data.

For example, `crab-tape-recall` graph say

- We do not have metrics from 2024-10-01 to 2024-10-09.
- That mean, sqoop dump from Rucio `rules_history` does not available from 2024-10-02 to 2024-10-10.

So, you can simply use sqoop dump from 2024-10-11, or the latest avaiable is also fine.

This one you need run in the notebook (need code change).

- Change `secretpath` to production credential.
- Set `PROD = True`.
- Set `START_DATE = 2024-10-01`.
- Set `END_DATE = 2024-10-10`.
- At path to sqoop dump (`HDFS_RUCIO_RULES_HISTORY`), replace `{END_DATE}` with the latest one.
- Run notebook.
