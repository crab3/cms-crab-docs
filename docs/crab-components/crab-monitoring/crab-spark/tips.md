# Tips & Tricks

## Query

Some usefull spark SQL query.

#### Getting first and the last record to check the time range of query result.

```sql
tmpdf.createOrReplaceTempView("tmpdf")
query2 = """\
WITH
a AS (
SELECT * FROM tmpdf
ORDER BY timestamp
LIMIT 1
),
b AS (
SELECT * FROM tmpdf
ORDER BY timestamp Desc
LIMIT 1
)
SELECT timestamp, from_unixtime(timestamp/1000, 'yyyy-MM-dd HH:mm:ss') FROM a
UNION ALL
SELECT timestamp, from_unixtime(timestamp/1000, 'yyyy-MM-dd HH:mm:ss') FROM b
"""
tmpdf2 = spark.sql(query2)
tmpdf2.show(10, False)
```
