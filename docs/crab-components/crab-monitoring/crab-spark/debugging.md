# Debugging

## Run notebook

You can run the spark code interactively via [SWAN](https://swan-k8s.cern.ch).

Suggestion is clone CRABServer repo into SWAN directory (`/eos/home-X/XXXX/SWAN_projects`) and open it in web UI.

You can do git commit/push/pull from there (Beware: EOS multifile operations quite slow).

## Run shell

You can also run interactive session on `crab-preprod-tw02` crab TW machines. This is convinient to process a backlog of data
that has not been processed yet or to debug a problem with the spark cronjobs.

```bash
# ssh into crab-preprod-tw02
docker run  --rm --net=host -i -t --name crabsparkdebug \
    -v /cvmfs:/cvmfs:shared \
    -v /data/certs/monit.d/monit_spark_crabtest.txt:/data/certs/monit.d/monit_spark_crabtest.txt \
    -v /data/certs/monit.d/monit_spark_crab.txt:/data/certs/monit.d/monit_spark_crab.txt \
    -v /data/certs/keytabs.d/cmscrab.keytab:/data/certs/keytabs.d/cmscrab.keytab \
    -v /data:/hostdata \
    registry.cern.ch/cmscrab/crabspark:<tagname> bash
```

### Debugging and recalculation

Since there is no method to delete specific data in Opensearch, when you need to add information to the documents of an index or error occurs in dataframe such as data duplication or wrong schema is uploaded, you have to delete entire data, and upload them again from the beginning, manually.

To do so, first of all you need to delete the data in particular index.

in Dev Tools of `os-cms.cern.ch`

- List all indexes with `GET /_cat/indices`
- Delete the data in particular index by running `DELETE /<os index>`

In case you want to change the name of the index, you will need to recompute the data and upload it into a new index.

Just change the name in the notebook, opensearch will create the new index for us. There is no need to create the index manually in advance.

Then you need to create a new index pattern: From the opensearch web gui

- Menu on the left, go to the bottom, click on "dashboard management".
- then "index patterns", "create index pattern", write a name with an asterisk `crab-test-taskdb-*`, make sure it finds the proper indexes.
- select time field.
