# CRAB Logstash

Logstash is another way we send CRAB metrics to Opensearch.

## Logstash configuration

The configuration is now in [prod/logstash/logstash.conf](https://github.com/dmwm/CMSKubernetes/blob/7d0965701c7f1fdbeb25f49ca1f209757dc02695/helm/crabserver/config/prod/logstash/logstash.conf) of CMSKubernetes repository as part of helm chart. It is deployed in the same cluster and namespace as the REST.

Filebeat scrape logs from application and send only logs that can be be parsed by Logstash. We append `tag` to the logs in Filebeat to indicate the log type, make it easy to parse in Logstash.

Logstash produces a JSON document for each log that is successfully parsed and sent to Opensearch to be stored and searched by Grafana later.


## CRAB REST

After log are printed to stdout of container, Filebeat daemonset (`filebeat-crab`) read the container's logs back and filter only some log lines to sent to Logstash. Please see [issue #7408](https://github.com/dmwm/CRABServer/issues/7408) on how container logs works, which is different compare to the rest of CMSWEB service.

There are 2 log types produced by REST services:

1. `crabhttpcall`, which is the [cherrypy's access log](https://github.com/dmwm/WMCore/blob/3af3dad4c3160e75161e6249eb86033c18d7172f/src/python/WMCore/REST/Main.py#L97-L140). REST will produce this log every http request. The format is similar to apache's access log.
2. `crabrest`, which is logs by [RESTBaseAPI's logger](https://github.com/dmwm/CRABServer/blob/ce559a11ef02c447f909ee166b7d48f218eb021d/src/python/CRABInterface/RESTBaseAPI.py#L156). The logs is basically generic log but we add code in the service to produce specific log format for parsing with grok pattern to produce the metrics.


## CRAB TaskWorker/Publisher

CRAB TaskWorker and Publisher have same mechanism as CRAB REST. Filebeat read the log files and then send to the same Logstash as REST, in Kubernetes cluster via NodePort.

The applications produce generice logs, written directly to log directory. So the [node's Filebeat](https://gitlab.cern.ch/ai/it-puppet-hostgroup-vocmsglidein/-/blob/7cc376875fb824fa66684607bad37e8b28049498/code/manifests/crabtaskworker/filebeat.pp) (`crabtwfilebeat`) is to read those files, filter, and send them to Logstash.

All logs from TaskWorker are labeled as `crabtaskworker`, and `crabpublisher` for Publisher logs.
