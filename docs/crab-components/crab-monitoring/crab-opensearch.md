# How to send data to opensearch

## Send metrics to Opensearch

Official documentation on how to achieve this from cern it monit team is available
at [https://monit-docs.web.cern.ch/metrics/http/](https://monit-docs.web.cern.ch/metrics/http/).

We have two producers, that send data to two different indexes on two different 
opensearch instances;

| producer | url | secret | opensearch instance | index |
|-|-|-|-|-|
| crab | https://monit-metrics.cern.ch:10014/crab | `monit_metrics_crab.json` | https://monit-opensearch-lt.cern.ch | [monit_prod_crab_raw_*](https://monit-opensearch-lt.cern.ch/dashboards/goto/31d6e610962e003ce8adc89217e18896) |
| crab-test | https://monit-metrics.cern.ch:10014/crab-test | `monit_metrics_crabtest.json` | https://monit-opensearch.cern.ch | [monit_prod_crab-test_raw_*](https://monit-opensearch.cern.ch/dashboards/goto/8b45ed92144437fc57e87481f1c964cc) |

You can retrieve the producer secret file from `tbag` on `aiadm.cern.ch`

```
tbag showkeys --hg vocmsglidein
tbag show --hg vocmsglidein monit_metrics_crab.json
tbag show --hg vocmsglidein monit_metrics_crabtest.json
```

Such files are copied to the machines managed by puppet the teigi module, as
done in this [merge request](https://gitlab.cern.ch/ai/it-puppet-hostgroup-vocmsglidein/-/merge_requests/118)

### data format

The data format should follow this schema

```json
{
  "producer": "myproducer",
  "type": "mytype",
  ...
  "mymetricfield1": "value1"
}
```

where:

- `myproducer` should be `crab` or `crab-test`
- `mytype` should be different for every script / application. This should be used in grafana to filter

### New script

When you want to send new data to opensearch:

- send the new data specifying a new `type`, so that you can easily filter for it in opensearch and grafana
- the new type should already be available.
- if it is not, then something may be broken on kafka. Open a ticket to cern IT monit team to ask for help, see [this](https://cern.service-now.com/service-portal?id=ticket&is_new_order=true&table=u_request_fulfillment&sys_id=dfcc89678378c61070ceba65eeaad37b) ticket for an example
- You should trigger a new scan of the fields in the index. If it is `monit-opensearch` you can do it yourself, if it is on `monit-opensearch-lt` then you need to ask cern it to do it, see [this](https://cern.service-now.com/service-portal?id=ticket&table=u_request_fulfillment&n=RQF2576874) ticket. Browse to "Management" / "Dashboard Management" / "index patterns" / "monit_prod_crab-test_raw_*", then click on top right

