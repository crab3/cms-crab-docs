# CRAB Monitoring Pipelines

![crab-monitoring-diagram.png](../../images/monitoring/crab-monitoring-diagram.png)

CRAB monitoring pipelines that we maintain ourselves consists of 4 pipelines:

1. [Cronjob of monitoring scripts](./crab-crontabs.md). Contains 4 scripts in total to fetch data from CRAB environment. The scripts construct the JSON document and send it to [monit-opensearch-lt.cern.ch](https://monit-opensearch-lt.cern.ch) directly.
2. [REST logs](./crab-logstash.md#crab-rest), where REST writes logs to stdout, and `filebeat-crab` read it back and forward to our Logstash, then parse and send to [monit-timberprivate.cern.ch](https://monit-timberprivate.cern.ch). The diagram above includes the flow where the `fluentd` also reads all log lines and upload them to CERN's S3.
3. [TaskWorker/Publisher log](./crab-logstash.md#crab-taskworkerpublisher), same as REST logs. But node's Filebeat inside VM talk to Logstash inside K8S via NodePort.
4. [Spark cronjob](./crab-spark.md), where we run spark jobs to join condor jobs, task table and rucio table together.

## Traceback from Grafana

To trace back from Grafana panel, you can use the datasource name as a key and look up in [datasources.json](https://github.com/dmwm/CMSMonitoring/blob/master/static/datasources.json) to find the Opensearch endpoint that store contains our data.

The `datasources.json` contains:

- Key (e.g. `monit_crab`) which is the datasource name, the same name as you see in panel's data source.
- `database`: Opensearch's index pattern.
- `url`: Opensearch instance.

after get the endpoint and index pattern, use diagram above to traceback to origin of the data.

Also, you can use endpoint url to open Opensearch Dashboard to inspect the data inside.
