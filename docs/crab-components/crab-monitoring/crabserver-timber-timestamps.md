# How to interpret the timestamps crabserver REST timber

## first test: curl

### Conclusion

The plots in [CRAB timber + pods](https://monit-grafana.cern.ch/d/qUVV6S0Gk/crab-timber-pods?orgId=11) use on the x axis `data.rec_date`, so the time on the x axis is when the frontend received the request, not when it finished serving it.
This is consistent with:

 - timber collects information from FrontEnd `access.log` which is produced by Apache and documented in https://httpd.apache.org/docs/2.4/logs.html
 - the `data.rec_date` comes from the timestamp at the beginning of each log record which is indeed the time when the query was received by Apache (see above doc)

### how to replicate the test

We can run a long query (usual GET to /filemetadata) to better interpret the timestamps.

This should take 1m20s from lxplus, moving around 170MB.

```bash
# lxplus
date ; curl --cert $X509_USER_PROXY --key $X509_USER_PROXY  "https://cmsweb-testbed.cern.ch/crabserver/prod/filemetadata?taskname=220506_133045%3Amabarros_crab_GS_Jpsi_20to40_Dstar_DPS_2016posVFP_13TeV_06-05-2022&filetype=EDM" >> /dev/null ; date
```

### results

curl logs

```bash
# lxplus
> curl ... # same as above
Fri May 20 17:48:55 CEST 2022
  % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                                 Dload  Upload   Total   Spent    Left  Speed
100  168M    0  168M    0     0  2221k      0 --:--:--  0:01:17 --:--:-- 2199k
Fri May 20 17:50:13 CEST 2022
```

crabserver REST logs, notice that:

- 17:48:55 - we start the select query
- 17:48:59 - cherrypy internals print the log line, it finished the query and received the data from the DB
- 17:50:13 - WMCore internals release the connection, giving it back to the pool

```plaintext
[20/May/2022:17:48:55]  RESTSQL:oKVvETCqpuUZ prepare [SELECT                            job_id AS jobid,                            fmd_outdataset AS outdataset,                            fmd_acq_era AS acquisitionera,                            fmd_sw_ver AS swversion,                            fmd_in_events AS inevents,                            fmd_global_tag AS globaltag,                            fmd_publish_name AS publishname,                            fmd_location AS location,                            fmd_tmp_location AS tmplocation,                            fmd_runlumi AS runlumi,                            fmd_adler32 AS adler32,                            fmd_cksum AS cksum,                            fmd_md5 AS md5,                            fmd_lfn AS lfn,                            fmd_size AS filesize,                            fmd_parent AS parents,                            fmd_filestate AS state,                            fmd_creation_time AS created,                            fmd_tmplfn AS tmplfn,                            fmd_type AS type,                            fmd_direct_stageout AS directstageout FROM filemetadata                     WHERE tm_taskname = :taskname                     AND rownum <= CASE :howmany WHEN -1 then 100000 ELSE :howmany END                     AND fmd_type IN (SELECT REGEXP_SUBSTR(:filetype, '[^,]+', 1, LEVEL) FROM DUAL CONNECT BY LEVEL <= REGEXP_COUNT(:filetype, ',') + 1)                     ORDER BY fmd_creation_time DESC]
[20/May/2022:17:48:55]  RESTSQL:oKVvETCqpuUZ execute: () {'taskname': '220506_133045:mabarros_crab_GS_Jpsi_20to40_Dstar_DPS_2016posVFP_13TeV_06-05-2022', 'filetype': 'EDM', 'howmany': -1}
[20/May/2022:17:48:59] crabserver-67c5b76d84-8r28k 188.185.10.188 "GET /crabserver/prod/filemetadata?taskname=220506_133045%3Amabarros_crab_GS_Jpsi_20to40_Dstar_DPS_2016posVFP_13TeV_06-05-2022&filetype=EDM HTTP/1.1" 200 OK [data: 7312 in - out 3654143 us ] [auth: OK "/DC=ch/DC=cern/OU=Organic Units/OU=Users/CN=dmapelli/CN=817881/CN=Dario Mapelli" "" ] [ref: "" "curl/7.70.0" ]
[20/May/2022:17:50:13]  RESTSQL:oKVvETCqpuUZ release with rollback
```

frontend logs, retrieved from `monit-timberprivate`, 

- search query: [monit-timberprivate - query](http://cern.ch/go/6w6J)
- specific log line [monit-timberprivate - view single document](https://monit-timberprivate.cern.ch/kibana/app/discover#/doc/571b2d60-cd1c-11ec-9cca-9f64af2c2bfb/monit_private_cmswebk8s_logs_frontend-2022-05-20?id=802cc063-ad4f-6088-a62c-2fafd3d747df)

from the single document, focus on the folliwing lines:

- `data.@timestamp`: `May 20, 2022 @ 17:50:16.260`, which matches the time curl exited
- `data.logtime`: `1,653,061,816,260`, which matches the time curl exited, with `date -d @1653061816.260`
- `metadata.kafka_timestamp`: `1653061818257`, which mathced the time curl exited (with just 2s delay), with `date -d @1653061818.257`
- `data.tstamp`: `20/May/2022:17:48:55 +0200`, which matched the time curl started
- `data.date_object`: `May 20, 2022 @ 17:48:55.000`, which matches the time curl started
- `data.rec_date`: `May 20, 2022 @ 17:48:55.000`, which matched the time curl started

Having a look at the config of the logstash that parses the data coming from the FE [in CMSKubernetes](https://github.com/dmwm/CMSKubernetes/blob/18722884bd48315690634af2114f95438cf74255/kubernetes/cmsweb/monitoring/logstash.conf), we get that:

- (generic) `data.@timestamp` is used to build `data.logtime`, [here](https://github.com/dmwm/CMSKubernetes/blob/18722884bd48315690634af2114f95438cf74255/kubernetes/cmsweb/monitoring/logstash.conf#L4)
- (frontend) The HTTPDATE in the log line is loaded inside `data.tstamp` variable, [here](https://github.com/dmwm/CMSKubernetes/blob/18722884bd48315690634af2114f95438cf74255/kubernetes/cmsweb/monitoring/logstash.conf#L20)
- (frontend) `data.tstamp` is used to build `data.date_object`, [here](https://github.com/dmwm/CMSKubernetes/blob/18722884bd48315690634af2114f95438cf74255/kubernetes/cmsweb/monitoring/logstash.conf#L47)
- (frontend) `data.date_object` is used to build the variables `data.rec_timestamp`, `data.rec_date`, `metadata.timestamp`, [here](https://github.com/dmwm/CMSKubernetes/blob/18722884bd48315690634af2114f95438cf74255/kubernetes/cmsweb/monitoring/logstash.conf#L51)

## second test, with cmsweb-ping

I performed a test similar to the above, but instead of using `curl` from lxplus, 
I used `cmsweb-ping` from inside the pod.
My purpose was to determine if the delay that we measure between the cherrypy log and the
client receiving all the data is caused by the FE or by crabserver itself.
Since `cmsweb-ping` is run from inside the pod, it bypasses the FE.

From a preprod pod i run the command

```plaintext
[_crabserver@crabserver-5f74bc77bc-llzn8 data]$ date; cmsweb-ping --authz=/etc/hmac/hmac -verbose 1 --url="http://localhost:8270/crabserver/prod/filemetadata?taskname=220506_133045%3Amabarros_crab_GS_Jpsi_20to40_Dstar_DPS_2016posVFP_13TeV_06-05-2022&filetype=EDM" > /dev/null; date
Thu Jun  2 14:44:59 CEST 2022
Thu Jun  2 14:46:02 CEST 2022
```

The correlated crabserver logs are

```plaintext
> kubectl -n crab logs --tail=100 -f $(kubectl -n crab get pod | grep "crabserver-.*.*" | cut -d" " -f1 | head -n 1) crabserver-filebeat | grep filemetadata
...
[02/Jun/2022:14:44:59]  RESTSQL:JOedplKnbxMV ENTER cms_analysis_reqmgr_r@cmsr CRABInterface.RESTBaseAPI.RESTBaseAPI (GET prod filemetadata) inuse=0 idle=1 queue=0 - Podname=crabserver-5f74bc77bc-llzn8 Type=cherrypylog
[02/Jun/2022:14:44:59]  RESTSQL:JOedplKnbxMV prepare [SELECT                            job_id AS jobid,                            fmd_outdataset AS outdataset,                            fmd_acq_era AS acquisitionera,                            fmd_sw_ver AS swversion,                            fmd_in_events AS inevents,                            fmd_global_tag AS globaltag,                            fmd_publish_name AS publishname,                            fmd_location AS location,                            fmd_tmp_location AS tmplocation,                            fmd_runlumi AS runlumi,                            fmd_adler32 AS adler32,                            fmd_cksum AS cksum,                            fmd_md5 AS md5,                            fmd_lfn AS lfn,                            fmd_size AS filesize,                            fmd_parent AS parents,                            fmd_filestate AS state,                            fmd_creation_time AS created,                            fmd_tmplfn AS tmplfn,                            fmd_type AS type,                            fmd_direct_stageout AS directstageout FROM filemetadata                     WHERE tm_taskname = :taskname                     AND rownum <= CASE :howmany WHEN -1 then 100000 ELSE :howmany END                     AND fmd_type IN (SELECT REGEXP_SUBSTR(:filetype, '[^,]+', 1, LEVEL) FROM DUAL CONNECT BY LEVEL <= REGEXP_COUNT(:filetype, ',') + 1)                     ORDER BY fmd_creation_time DESC] - Podname=crabserver-5f74bc77bc-llzn8 Type=cherrypylog
[02/Jun/2022:14:45:11] crabserver-5f74bc77bc-llzn8 127.0.0.1 "GET /crabserver/prod/filemetadata?taskname=220506_133045%3Amabarros_crab_GS_Jpsi_20to40_Dstar_DPS_2016posVFP_13TeV_06-05-2022&filetype=EDM HTTP/1.1" 200 OK [data: 412 in - out 12392078 us ] [auth: OK "" "" ] [ref: "" "Go-http-client/1.1" ] - Podname=crabserver-5f74bc77bc-llzn8 Type=cherrypylog
...
```

Timeline:

- 14:44:59 - the client starts the request
- 14:44:59 - the server receives the request.
- 14:45:11 - cherrypy finishes processing the request
- 14:46:02 - the client finished receiving the data

This feels similar to what we observed with `curl`.

Conclusion: the ~1MB/s limit is not imposed by the FE but by crabserver/cherrypy instead.

