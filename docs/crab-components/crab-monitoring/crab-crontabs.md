# CRAB monitoring crontab

## Inventory

We are currently running the following scripts inside a docker container in a crontab:

| machine | script | service name| output | 
|-|-|-|-|
| crab-prod-tw02, crab-dev-tw02 | [GenerateMONIT.py](https://github.com/dmwm/CRABServer/blob/master/src/script/Monitor/GenerateMONIT.py) | TaskWorker_monit_generatemonit | opensearch, type: `taskworker`, `schedd`  |
| crab-prod-tw02, crab-dev-tw02 | [ReportRecallQuota.py](https://github.com/dmwm/CRABServer/???) | TaskWorker_monit_reportrecallquota | opensearch, type: `reportrecallquota`| 
| crab-prod-tw02, crab-dev-tw02 | [aso_metrics_ora.py](https://github.com/dmwm/CRABServer/blob/master/src/script/Monitor/aso_metrics_ora.py) | TaskWorker_monit_asometrics | opensearch, type: `asoless` | 
| crab-prod-tw02, crab-dev-tw02 | [CheckTapeRecall.py](https://github.com/dmwm/CRABServer/blob/master/scripts/Utils/CheckTapeRecall.py) | TaskWorker_monit_checktaperecall | opensearch, type: `checktaperecall` | 

| producer | producer enabled on VMs | crontab running on VM | which is read from grafana dashboard |
|-|-|-|-|
| crab      | [crab-prod-tw02](https://gitlab.cern.ch/ai/it-puppet-hostgroup-vocmsglidein/-/blob/847146c82b69e4af6fef67ffd8694a917c17acb6/data/fqdns/crab-prod-tw02.cern.ch.yaml#L50), [crab-preprod-tw02](https://gitlab.cern.ch/ai/it-puppet-hostgroup-vocmsglidein/-/blob/847146c82b69e4af6fef67ffd8694a917c17acb6/data/fqdns/crab-preprod-tw02.cern.ch.yaml#L16) | `crab-prod-tw02` | [CRAB Overview](https://monit-grafana.cern.ch/d/CsnjLe6Mk/crab-overview?orgId=11&refresh=1h) |
| crab-test | crab-dev-* (this is the [default](https://gitlab.cern.ch/ai/it-puppet-hostgroup-vocmsglidein/-/blob/847146c82b69e4af6fef67ffd8694a917c17acb6/code/manifests/crabtaskworker/certificate.pp#L121)) | `crab-dev-tw02` | [CRAB Overview - test](https://monit-grafana.cern.ch/goto/Xd2BbFCIg?orgId=11) |


## Deploy new version of monitoring image

First of all you want to deploy the new monitoring image to `crab-dev-tw02`
and check that it runs properly.

Use the gitlab pipeline to build the image by creating a new tag and pushing 
it to gitlab with 

```
git push gitlab -o ci.variable="BUILD=t" $TAG
```

It will build a new `crabtwmonit:v3.latest` docker image. After 1 hour at max,
this new image will be pulled by `crab-dev-tw02` and will be used to run
the monitoring scripts.

You can check that the new scripts are working smoothly using the opensearch
index for `crab-test` and its corresponding grafana dashboard

If you like the result, then you can deploy this new monit image tag
in production changing the version in the 
[hiera](https://gitlab.cern.ch/ai/it-puppet-hostgroup-vocmsglidein/-/blob/890996c626cad2b0b7696b4bcc9568e680bf269b/data/fqdns/crab-prod-tw02.cern.ch.yaml#L51) 
file of `crab-prod-tw02`.

## How to check what is running

We run the monitoring scripts as cronjobs in a taskworker machine using the `runContainer.sh` script.

In order to get a list of the scripts that are running, ssh into the tw machine, 
change the user so crab3 with `sudo su crab3`, 
then run `crontab -l` and look for entries that use `runContainer.sh`.

The anatomy of a crontab entry is:

- `export loguuid=$(uuidgen)`: we use this uuid to uniquely identify the execution of a monitoring script
- `test -f /home/crab3/runContainer.sh`
- `bash /home/crab3/runContainer.sh`: we use runContainer.sh to run the monitoring scripts
- `-v 20240126-monitpython`: use `v3.YYMMDD.monittw` for scripts that require TW environment and `YYYYMMDD-monitpython` for script that require a plain python environment
- `-s TaskWorker_monit_asometrics`: use a specific `TaskWorker_monit_xxxx` for this script
- `-u ${loguuid}`: runContainer writes to the file `/tmp/monit-${loguuid}.txt` the stdout of the script run inside the docker container. If the script is successfull, i.e. it exits with a zero exit code, then runContainer deleted this file.
- `-c "python3 /data/srv/monit/aso_metrics_ora.py"`: specify the command that should be run inside the container
- `>/dev/null 2>&1` 
- `[ -f /tmp/monit-${loguuid}.txt ] && cat /tmp/monit-${loguuid}.txt`: if this file is present, then the script failed. If we cat it to stdout, cron will send it via email.

## How to debug

If something goes wrong, you will likely be notified in one or two ways, based 
on the script that is failing and how the grafana panels are set up:

- you will receive an email notification from cron
- you will receive an alert from grafana complaining that the data source does not contain data

### First of all: check your email

Every monitoring script writes to stdout and to a logfile.

If something failed, you should have received an email with the content of the stdout of the script run inside 
the docker container. You can find the same text in one of the files `/tmp/monit-*.txt`.

The email subject should also contain the service name, which should be of the form `TaskWorker_monit_xxxx`. 

You can retrieve the stdout of the latest execution a docker container with 

```plaintext
docker logs TaskWorker_monit_xxxx
```

You can find the corresponding logfile in `/data/container/TaskWorker_monit_xxxx/logs/*.txt`. Use the unix timestamp of the file `/tmp/monit-*.txt` to select the proper logfile.

You should have access to all the information you need to understand what happened.

### run the command yourself

If the logs are not enough, if you think that the problem is gone, then you may want to run the script yourself. 

Starting from a crontab such as

```plaintext
export loguuid=$(uuidgen); test -f /home/crab3/runContainer.sh && bash /home/crab3/runContainer.sh -v 20240126-monitpython -s TaskWorker_monit_asometrics -u ${loguuid} -c "python3 /data/srv/monit/aso_metrics_ora.py" >/dev/null 2>&1 ; [ -f /tmp/monit-${loguuid}.txt ] && cat /tmp/monit-${loguuid}.txt
```

remove what is not needed, add `-x` so that you can trace every command:

```plaintext
bash -x /home/crab3/runContainer.sh -v 20240126-monitpython -s TaskWorker_monit_asometrics -c "python3 /data/srv/monit/aso_metrics_ora.py"
```

## how to change, build and push

Once you have indentified the bug, or if you want to add some new features, find the monitoring script
in the CRABServer repository and edit it. You can do this interactively from any TW machine

Start a container using a monit image, the service name "TaskWorker_monit" has
been setup in puppet for this purpose

```bash
bash ./runContainer.sh -v v3.latest -s TaskWorker_monit
```

runContainer should exit right away, then you can exec into the container 

```bash
docker exec -it TaskWorker_monit bash
```

now you can cd into `/data/repos/CRABServer` and run your favourite script

```bash
cd /data/repos/CRABServer
python3 src/script/Monitor/ReportRecallQuota.py
```

??? example "Example - ReportRecallQuota"

    ```plaintext
    crab3@crab-dev-tw02:/data/repos/CRABServer$ python3 src/script/Monitor/ReportRecallQuota.py
    Starting new HTTP connection (1): cms-rucio.cern.ch:80
    http://cms-rucio.cern.ch:80 "GET /ping HTTP/1.1" 200 21
    No value present for key: "host_to_choose_choice['http://cms-rucio.cern.ch']"
    NeedRegenerationException
    no value, waiting for create lock
    value creation lock <dogpile.cache.region.CacheRegion._LockWrapper object at 0x7f000737daf0> acquired
    No value present for key: "host_to_choose_choice['http://cms-rucio.cern.ch']"
    Calling creation function for not-yet-present value
    Cache value generated in 0.000 seconds for key(s): "host_to_choose_choice['http://cms-rucio.cern.ch']"
    Released creation lock
    http://cms-rucio.cern.ch:80 "GET /accounts/whoami HTTP/1.1" 303 209
    http://cms-rucio.cern.ch:80 "GET /accounts/crab_server HTTP/1.1" 200 226
    rucio client initialized: {'version': '34.4.2'} {'status': 'ACTIVE', 'deleted_at': None, 'updated_at': '2020-04-29T15:40:28', 'suspended_at': None, 'email': 'Stefano.Belforte@cern.ch', 'account': 'crab_server', 'account_type': 'SERVICE', 'created_at': '2020-04-29T15:40:28'}
    http://cms-rucio.cern.ch:80 "GET /accounts/crab_tape_recall/usage/local HTTP/1.1" 200 None
    http://cms-rucio.cern.ch:80 "GET /accounts/crab_input/usage/local HTTP/1.1" 200 None
    Starting new HTTPS connection (1): monit-metrics.cern.ch:10014
    /usr/local/lib/python3.8/site-packages/urllib3/connectionpool.py:1043: InsecureRequestWarning: Unverified HTTPS request is being made to host 'monit-metrics.cern.ch'. Adding certificate verification is strongly advised. See: https://urllib3.readthedocs.io/en/1.26.x/advanced-usage.html#ssl-warnings
      warnings.warn(
    https://monit-metrics.cern.ch:10014 "POST /crab-test HTTP/1.1" 200 None
    ```

Then build the new image and push it to the docker registry. Or make the CI/CD 
pipeline build it for you.

You can now go back to the taskworker machine and check if the new script 
suits your needs running `bash -x /home/crab3/runContainer.sh [...]` 
from your console. 

Once you are satisfied with a new tag, you can test it in crab-dev-tw02
just by tagging it with `v3.latest` and pushing to the registry.
There is a crontab to pull the desired tag for the monit image, the crontabs
will start working and you will start seeing data coming in into the test
grafana dashboard.

Finally, deploy the new monit image changing the tag in [puppet](https://gitlab.cern.ch/ai/it-puppet-hostgroup-vocmsglidein/-/blob/847146c82b69e4af6fef67ffd8694a917c17acb6/data/fqdns/crab-prod-tw02.cern.ch.yaml#L51)

## How add a new monitoring script in a docker container

If you want to add a new monitoring script, then:

- then edit the the docker file in `CRABServer/cicd/monit_pypi/Dockerfile`, 
  so that your new script is added to the docker image
- build the docker image, push to the registry. dario uses a tw machine
- you should be able to run the following script on a tw machine, create the missing directories

    ```
    bash -x /home/crab3/runContainer.sh -v 20240126-monitpython -s TaskWorker_monit_reportrecallquota -c "python3 /data/srv/monit/ReportRecallQuota.py"
    ```

- read the logs with `docker logs TaskWorker_monit_reportrecallquota`
- then make the changes persistent editing puppet

    - create a new manifest file for the specific monitoring script using `code/manifests/crabtaskworker/generatemonit.pp` as a template. for example call it `code/manifests/crabtaskworker/newmonitscript.pp`
    - add the new class `class { 'hg_vocmsglidein::crabtaskworker::newmonitscript': }` to ``code/manifests/crabtaskworker.pp`
    - add the hiera variable `enable_newmonitscript: true` to the machine where you want to run the script on as a crontab
    - create the proper directories for having persistent application logs. add `'/data/container/TaskWorker_monit_newmonitscript','/data/container/TaskWorker_monit_newmonitscript/logs','/data/container/TaskWorker_monit_newmonitscript/cfg',` to `code/manifests/crabtaskworker/indocker.pp`
    - run puppet on the target machine.

