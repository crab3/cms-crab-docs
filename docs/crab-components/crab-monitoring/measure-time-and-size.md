# Measure time and size in code

There are 2 functions to measure things in our code (see [ServerUtilities.py#L886-L952](https://github.com/dmwm/CRABServer/blob/87489db86c04ec4bfa6b59ed5e9ba2dd2fce83e6/src/python/ServerUtilities.py#L886-L952)):

- `MeasureTime` (as context manager): measure time with `time.perf_counter` and print output to logger. Example
- `measure_size`: measure size of the object with `sys.getsizeof` in recursive manner.

The log output of logs file will look like this:
```
2022-08-09 15:38:06,055:YdmwtrFzoQjS:INFO:ServerUtilities:MeasureTime:seconds - modulename=CRABInterface.RESTCache label='get.download.generate_presigned_post' tot=0.0012 proc=0.0012 thread=0.0012
2022-07-19 01:46:38,171:FYOnteLbENPV:INFO:ServerUtilities:MeasureTime:seconds - modulename=CRABInterface.RESTBaseAPI label='RESTBaseAPI.executemany' tot=0.0007 proc=0.0003 thread=0.0001
2022-07-19 01:46:38,208:LPaomRBQFtYO:INFO:ServerUtilities:MeasureTime:seconds - modulename=CRABInterface.RESTBaseAPI label='RESTBaseAPI.query_load_all_rows' tot=0.0014 proc=0.0009 thread=0.0008
2022-07-19 01:46:38,207:LPaomRBQFtYO:INFO:RESTBaseAPI:MeasureSize:bytes - modulename=CRABInterface.RESTBaseAPI label='RESTBaseAPI.query_load_all_rows' obj_size=10100 get_size_time=0.000227
```
This logs will parse in logstash and store in `crabrest` index in ES.

## Usage (and best practice)

MeasureTime and measure_size have 3 common arguments,

- `logger`: logger object which you want python output the result.
- `modulename`: should be `__name__`. It is the one of fields to filter in kibana/grafana.
- `label`: any `[A-Za-z0-9_-]`. Should be function name like `RESTBaseAPI.query_load_all_rows`. It is the one of fields to filter in kibana/grafana.

Please see [RESTBaseAPI.py#L122-L151](https://github.com/dmwm/CRABServer/blob/87489db86c04ec4bfa6b59ed5e9ba2dd2fce83e6/src/python/CRABInterface/RESTBaseAPI.py#L122-L151) as example on how to use it.


## Data in ES

Kibana link to this data: <http://cern.ch/go/tlf7>

In kibana, we can filter each by `log_type`, ` modulename` and `label` which are arguments of `MeasureTime` and `measuer_size` function. `trace_id` (currently SQL trace from WMCore) also available for use when you want to see surrounding logs.

Example `log_type` of `measure_time`

![kibana-measure-size-example.png](../../images/measure-time-and-size/kibana-measure-time-example.png)

Example `log_type` of `measure_size`

![kibana-measure-size-example.png](../../images/measure-time-and-size/kibana-measure-size-example.png)
