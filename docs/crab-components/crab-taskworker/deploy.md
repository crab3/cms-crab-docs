# Deploy TaskWorker

Currently we deploy TaskWorker inside Docker container, in VM machine.

New VM name should be similar to `crab-dev-tw01`, spawn in `CMS CRAB` openstack project, inside `vocmsglidein/crabtaskworker` hostgroup.

## Create virtual machine

### Preparing the puppet configurations

Before creating the machine you need to prepare the Hiera data in [ai/it-puppet-hostgroup-vocmsglidein](https://gitlab.cern.ch/ai/it-puppet-hostgroup-vocmsglidein/). You can copy [crab-dev-tw01.cern.ch.yaml](https://gitlab.cern.ch/ai/it-puppet-hostgroup-vocmsglidein/-/blob/master/data/fqdns/crab-dev-tw01.cern.ch.yaml), put it same direcotry and rename to FQDN of new VM, change values according to your machine.

### Creating the virtual machine

- First, set few variables in advance:

    ```bash
    #! /bin/bash
    set -o allexport
    OS_PROJECT_NAME="CMS CRAB"
    hostname='crab-dev-tw03'
    foremanenv='crabdev_taskworker'
    openstackflav='m2.medium'
    osrelease='cc7'
    set -o allexport
    ```

- Check if the machine does already exist in the current OpenStack project, and if so delete it and wait for the DNS record for this hostname to be freed (it may take between 20 min and 1h in case you are behind CERN firewall and you are using the CERN DNS servers):

    ```bash
    # ai-kill ${hostname} # remove comment when run in aiadm
    while (host $hostname > /dev/null 2>&1); do clear; echo Waiting for the DNS record of $hostname to be cleared.; sleep 10;done; echo Ready to go
    ```

- Create the machine via the `ai-bs`

    ```bash
    ai-bs  -g vocmsglidein/crabtaskworker \
        --$osrelease \
        --foreman-environment $foremanenv  \
        --landb-mainuser CMS-SERVICE-CRAB3HTCONDOR \
        --landb-responsible CMS-SERVICE-CRAB3HTCONDOR-ADMINS \
        --nova-flavor $openstackflav \
        --landb-ipv6ready \
        -v \
        $hostname
    ```

- You should see long output from openstack and the ai tool which must end up with:

    ??? quote "ai-bs logs"
        ```
        Issuing put on https://teigi75.cern.ch:8201/roger/v1/state/crab-priv-tw01.cern.ch/
        With headers: {'Content-Type': 'application/json', 'Accept-Encoding': 'deflate', 'Accept': 'application/json'}
        With data: {"hostname": "crab-priv-tw01.cern.ch", "appstate": "build"}
        Starting new HTTPS connection (1): teigi75.cern.ch
        https://teigi75.cern.ch:8201 "PUT /roger/v1/state/crab-priv-tw01.cern.ch/ HTTP/1.1" 401 381
        Resetting dropped connection: teigi75.cern.ch
        https://teigi75.cern.ch:8201 "PUT /roger/v1/state/crab-priv-tw01.cern.ch/ HTTP/1.1" 204 0
        Returned (204)
        ----------------------------------------------------------------------
        * Your machine is booting and the network is being configured right now,
          Puppet will run immediately after a successful boot process.
        * It typically takes around 30 minutes between this command is
          executed and the first Puppet report arrives to Foreman:
          https://judy.cern.ch/hosts/crab-priv-tw01.cern.ch/config_reports
          (although this depends a lot on the complexity of your configuration)
        * After the initial configuration, if you've set rootegroups or
          rootusers in Foreman or Hiera you should be able to log in as
          root using your Kerberos credentials. The LANDB responsible
          has also root access by default.
        * You can check the status of the node creation request by running:
          'openstack server show crab-priv-tw01'
        * A custom LANDB responsible/user has been set. It will be visible
          in Foreman a few minutes after the node is booted. In the
          meantime, the Foreman owner will be the issuer of this command.
          (tivanov@CERN.CH)
        * In case of problems, if you provided a SSH key when creating the node
          use it to log into the box and take a look at /var/log/cloud-init*.log.
          Console log can be retrieved by using 'openstack console log show'.
        ----------------------------------------------------------------------
        ```

- Wait until the machine has been build for real:

    ```bash
    until (host $hostname > /dev/null 2>&1); do clear; echo Waiting for the build process to finish.; sleep 10;done; until (ping -c 1 $hostname > /dev/null 2>&1); do echo Waiting for $hostname to show up. ; sleep 10;done; echo Ready to go
    ```

## Deploy TaskWorker and Publisher

!!! Note
    For Publisher, it is same procedure as TaskWorker. Change the service name to `Publisher_schedd`.

There are two ways to do this:

1. Trigger Jenkins job [CRABServer\_Deploy\_TW](https://cmssdt.cern.ch/dmwm-jenkins/job/CRABServer_Deploy_TW/). You need to edit `Environment` choice and add newly create cluster. No change need on VM side which already configured by Puppet.

    ![crabserver-deploy-tw-jenkins-screenshot.png](../../images/crabserver-deploy-tw-jenkins-screenshot.png)

2. Run [runContainer.sh](https://github.com/dmwm/CRABServer/blob/master/src/script/Container/runContainer.sh) script in VM with `crab3` user. Note that you need to delete container yourself everytime before run `runContainer.sh`.

    ```bash
    sudo -i -u crab3
    docker stop TaskWorker && docker rm TaskWorker
    ./runContainer.sh -v v3.221205 -s TaskWorker
    ```

    ??? quote "Example"
        ```
        [tseethon@crab-dev-tw03 ~]$ sudo -i -u crab3
        [crab3@crab-dev-tw03 ~]$ ls
        auth  crab_deploy_user.patch  current  enabled  runContainer.sh
        cfg   cron.wrapper.sh         dummy    logs     state
        [crab3@crab-dev-tw03 ~]$ ./runContainer.sh # Help is very useful when you forget scirpt's arguments.
        Make sure to set both -v and -s variables.

        Usage example: ./runContainer.sh -v v3.201118 -s TaskWorker
                -v TW/Publisher version
                -s which service should be started: Publisher, Publisher_schedd, Publisher_asoless, Publisher_rucio or TaskWorker
                -r docker hub repo, if not provided, default points to 'cmssw'
        [crab3@crab-dev-tw03 ~]$ docker stop TaskWorker && docker rm TaskWorker
        TaskWorker
        TaskWorker
        [crab3@crab-dev-tw03 ~]$ ./runContainer.sh -v v3.221205 -s TaskWorker
        e86be5332cc5059ae3c8aaaee0e79eb30fff712472c445648996ed25025deb8d
        Sleeping for 3 seconds.
        Running containers:
        CONTAINER ID   IMAGE                                               COMMAND                  CREATED         STATUS         PORTS     NAMES
        e86be5332cc5   registry.cern.ch/cmscrab/crabtaskworker:v3.221205   "/bin/sh -c 'sh /dat…"   3 seconds ago   Up 3 seconds             TaskWorker
        ```

## additional services

We run additional services on the TW machines, namely some monitoring scripts.

Some of them need to write files to eos, which requires authentication.
We decided to have a cern service account for that purporse called `cmscrab`.

### how to transfer ownership of the service account

On 2023-06, dario is the owner of `cmscrab` service account.

If you want to transfer the ownership to somebody else, then

- browse [account.cern.ch](https://account.cern.ch/account/Management/MyAccounts.aspx)
- then click on "Change Account Owner"

### how to create a service account

- browse [account.cern.ch](https://account.cern.ch/account/Management/MyAccounts.aspx)
- click on "new account". select "Service", then "next"
- fill in the details, then wait for the confirmation email about the creation
- then browse [user portal](https://users-portal.web.cern.ch/identities/cmscrab) for the user that you are interested in
- set the initial password
- the account should now be active

TODO: check that the account is active


- [x] ssh with lxplus with your own account, get a kerberos tiket for the service account, `kinit cmscrab@CERN.CH`
- [ ] `ssh -F /dev/null -o PreferredAuthentications=password -o PubkeyAuthentication=no  cmscrab@lxplus.cern.ch`

### write to an EOS directory from a TW machine

Facts:

- you need a kerberos token to write to the CERN eos instance
- you can get a kerberos token with a kerberos keytab
- It is not recommended to use a keytab of a regular user in a VM, since other
  members of the team can use it to impersonate the colleague

Solution:

- create a service account, see [here](../../crab-infrastructure/crab-service-account.md#create-the-service-account)
- create a keytab for the service account and copy that to the VMs, see [here](../../crab-infrastructure/crab-service-account.md#create-the-keytab-for-the-service-account)
- give the service account access to the specific resource that you want the VM to access, see [here](../../crab-infrastructure/crab-service-account.md#give-the-service-account-access-to-the-cmsweb-eos-directory).
