# Migrate CRAB TaskWorker to Alma9

Guide to migrate TW to alma9. Service is already containerized so migration is straightforward.

Make sure machine is in `qa` Puppet environment, then rebuild it with `ai-rebuild` command, and it is done.

## In-details

TW alma9 support is ready in `qa` environment (from [MR#172](https://gitlab.cern.ch/ai/it-puppet-hostgroup-vocmsglidein/-/merge_requests/172)).

Only take action on rebuilding step and we good to go.

!!! note
    In this guide, we use `crab-dev-tw03` as example.

1. Make sure machine is in `qa` environment .
    ```
    [tseethon@aiadm01 ~]$ ai-foreman showhost crab-dev-tw03.cern.ch
    +-----------------------+-----------------------------+--------------------------+---------------+
    | Name                  | Hostgroup                   | Environment              | OS            |
    +-----------------------+-----------------------------+--------------------------+---------------+
    | crab-dev-tw03.cern.ch | vocmsglidein/crabtaskworker | qa                       | AlmaLinux 9.3 |
    +-----------------------+-----------------------------+--------------------------+---------------+
    ```
    If not, run `ai-foreman` command at `aiadm.cern.ch`;
    ```bash
    ai-foreman updatehost -e qa crab-dev-tw03.cern.ch
    ```

2. (Optional) Remove voms package configuration from hiera. [Example.](https://gitlab.cern.ch/ai/it-puppet-hostgroup-vocmsglidein/-/commit/be208724)
    ```
    osrepos_epel_exclude_pkgs:
      - voms-clients-java
      - voms-api-java

    voms::install::clientpkgs:
      - voms-clients-cpp
    ```
     These lines are not needed anymore. We actually do not use it in TW, but it is nice for debugging anyway.

3. Run rebuild in `aiadm.cern.ch`.
    ```bash
    export OS_PROJECT_NAME="CMS CRAB";
    ai-rebuild crab-dev-tw03.cern.ch --alma9
    ```
    `--alma9` flag will select the latest Alma9 image for us.

4. Waiting for machine to rebuild. Watching [Report page](https://judy.cern.ch/hosts/crab-dev-tw03.cern.ch/config_reports).

    After some time has passed, you will see 2 report with unusual apply number as show in the picture below.

    ![puppet-rebuild-waiting.png](../../images/puppet-rebuild-waiting.png)

    Now you can ssh to the rebuilt machine. If not, make sure machine is in `qa` and force restart once from Openstack WebUI/CLI.

5. Run `puppet agent -tv` as root, at least 2 times. Then, reboot.

6. Deploy TW.
