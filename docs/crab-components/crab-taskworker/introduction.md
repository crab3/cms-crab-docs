# CRAB TaskWorker

For deployment, please see [Deploy TaskWorker](./deploy.md)

## Requirement to run TaskWorker

### Service in VM

- **Docker Deamon** We run TaskWorker/Publisher inside docker.
- **CVMFS cms.cern.ch** We take from CVMFS the CMS SITECONF and the grid security directories, to avoid the need to run crontabs to keep CA-related lists up to date. It mount into container [with runContainer.sh](https://github.com/dmwm/CRABServer/blob/5d445711be6aee49192c1f2aafb3d739a91d5713/src/script/Container/runContainer.sh#L50) script.

### Service certificate

We need to talk with CRABServer and CMS rucio, which is behind CMSWEB auth. It need grid certificate to authenticate with. You need to register your service certificate with:

- **VOMS CMS** CMSWEB only talk with CMS user. Ask Stefano Belforte or Andreas Pfeiffer to add new service certificate.
- **CRIC Service Account** to map certificate to user/group (For user certificate, CRIC automatically query user from CERN database). Asking for help from Stephan Lammel.
Please send email to Facility team (Stephan Lammel, <mailto:lammel@fnal.gov>) with subject and body like this:

??? quote "Email"
    ```
    Subject: Add new CRAB serviceaccount to CRIC

    Hello Stephan,

    Could you please add new CRAB serviceaccount `tw/crab-dev-tw04.cern.ch` and `schedd/crab-preprod-scd03.cern.ch` to CRIC?
    We have 2 new VMs for testing the puppet code.
    Please find attached public key of service certificate of new machines.

    Best,
    Wa, Thanayut Seethongchuen
    ```

- **rucio Service Account** to read data location with CMS Rucio service. (Not sure if rucio have it own authorization or get service account's group from CRIC. Currently, we can read rucio using service account without need intervention from rucio operators.)

### Host certificate

 TaskWorker uses host certificate to talk with myproxy service (see below)

## Access to myproxy.cern.ch
We need **MyProxy access** for certificate renewal in grid.

#### Request proxy renewal rights

Open a [GGUS](https://ggus.eu/?mode=ticket_submit) ticket and assign to CERN-ROC, WLCG operation people there will know how to route it to myproxy support team in SNOW (helpdesk usally does not know)

Tickeet message template:
```
Dear Myproxy support,
Please add the following host certificate to myproxy.cern.ch  authorized retrievers

/DC=ch/DC=cern/OU=computers/CN=<hostname>

This is a new development/production machine for CMS grid submission

Many thanks
```

Where <hostname> is the full host name as shown by the command `hostname -f`. Run `openssl x509 -in /etc/grid-security/hostcert.pem -noout -subject` and verify that the host certificate is correct in the message body.

It will open new SNOW ticket and route directly to CERN IT myproxy's team.

[Example](https://ggus.eu/?mode=ticket_info&ticket_id=166121)

#### Proxy Renewal allow list

You need to add your new VM in [cmsweb-rest-config.json](https://gitlab.cern.ch/crab3/CRAB3ServerConfig/-/blob/ed7e34dd3cd4833048144643176ee22e5685acf1/cmsweb-rest-config.json#L5) in `delegate-dn` key, to make user short-life grid certificate renewable by VM.


### Token to submit tasks to Schedd

TaskWorker need token to communicate with CRAB Schedd. Token are put in `tbag` manually and deploy using Puppet (see [certificate.pp](https://gitlab.cern.ch/ai/it-puppet-hostgroup-vocmsglidein/-/blob/cf5d7030e8e29f778e8b79224603e1597c530a7c/code/manifests/crabtaskworker/certificate.pp#L72-88])).

Currently we use same token for all environment. You can list token using `tbag showkeys --hg vocmsglidein/crabtaskworker` in aiadm machine. Output will look like this:

```
[tseethon@aiadm08 ~]$ tbag showkeys --hg vocmsglidein/crabtaskworker
{
     "vocmsglidein/crabtaskworker": {
          "secrets": [
               "tseethon_test_secret",
               "tw_condor_token_global_pool",
               "tw_condor_token_itb_pool",
               "tw_crab_dev_tw01_servicecert_rucio_user",
               "tw_crab_dev_tw01_servicekey_rucio_user"
          ]
     }
}
```


## Test it if everything work

### Connection to CRABServer
Connecting to `/info` API with service cert.

```bash
curl --cert /data/certs/servicecert.pem --key /data/certs/servicekey.pem https://cmsweb-testbed.cern.ch/crabserver/preprod/info
```

It should have out put like:

```json
{"desc": {"columns": ["null"]}, "result": [
 {"crabserver": "Welcome", "version": "v3.220414"}
]}
```

Or, run against troubleshoot URL,

```bash
curl --cert /data/certs/servicecert.pem --key /data/certs/servicekey.pem https://cmsweb-testbed.cern.ch/auth/trouble
```

It should have output like this:

```
<p>Your browser offered valid DN '/DC=ch/DC=cern/OU=computers/CN=tw/crab-dev-tw02.cern.ch'.</p><p>Your certificate is valid from Mar 26 02:02:12 2022 GMT to Apr 22 11:20:16 2023 GMT; 366 days of validity remain.</p><p>Your certificated passed basic validation.</p><p>Your certificate is a CMS VO member.</p><p>Your certificate is mapped to the username 'service@crab-dev-tw02.cern.ch' in CRIC.</p><p>For more details please see <a href='https://twiki.cern.ch/twiki/bin/view/CMS/DQMGUIGridCertificate'>certificate setup instructions</a> for the most commonly needed steps.</p>
```
### myproxy renewal

- Use CRAB client with your own grid certificate to submit some task in lxplus (to any env).
- Look at client logs `crab.log`, search for `myproxy-init` or `myproxy-info` and find argument `-l <somehash_40chars>` supply to command. `<somehash_40chars>` is your myproxy username. We will use it later
- Go to your machine and run

  ```bash
  export X509_USER_CERT=/data/certs/hostcert.pem
  export X509_USER_KEY=/data/certs/hostkey.pem
  myproxy-logon -d -n -s myproxy.cern.ch -o /tmp/px -l <myproxy username>
  ```

  It should have output like this:

  ```bash
  [root@crab-preprod-tw01 TaskManager]$ export X509_USER_CERT=/data/certs/hostcert.pem
  [root@crab-preprod-tw01 TaskManager]$ export X509_USER_KEY=/data/certs/hostkey.pem
  [root@crab-preprod-tw01 TaskManager]$ myproxy-logon -d -n -s myproxy.cern.ch -o /tmp/px -l be1f4dc5be8664cbd145bf008f5399adf42b086f
  A credential has been received for user be1f4dc5be8664cbd145bf008f5399adf42b086f in /tmp/px.
  ```

  If it not success, try user `crab3` instead.

### Rucio

Start TaskWorker container, go inside container and stop TaskWorker process.

```bash
~/runContainer.sh -r registry.cern.ch/cmscrab -v v3.220824 -s TaskWorker
docker exec -it TaskWorker bash
./stop.sh
```

sourcing crab environment with `source env.sh` (assume you are in `/data/srv/TaskManager` directory) and then run python scripts below:

```python
import logging
from WMCore.Configuration import ConfigurationEx
from RucioUtils import getNativeRucioClient
logging.basicConfig(level=logging.DEBUG)
config = ConfigurationEx()
config.section_("Services")
config.section_("TaskWorker")

config.TaskWorker.cmscert = '/data/certs/servicecert.pem'
config.TaskWorker.cmskey = '/data/certs/servicekey.pem'

config.Services.Rucio_host = 'https://cms-rucio.cern.ch'
config.Services.Rucio_account = 'crab_server'
config.Services.Rucio_authUrl = 'https://cms-rucio-auth.cern.ch'
config.Services.Rucio_caPath = '/etc/grid-security/certificates/'
rucioClient = getNativeRucioClient(config=config, logger=logging.getLogger())
```

If success, you will see log like this:

```text
INFO:root:Initializing native Rucio client
DEBUG:urllib3.connectionpool:Starting new HTTPS connection (1): cms-rucio-auth.cern.ch:443
DEBUG:urllib3.connectionpool:https://cms-rucio-auth.cern.ch:443 "GET /auth/x509 HTTP/1.1" 200 0
DEBUG:urllib3.connectionpool:Starting new HTTPS connection (1): cms-rucio.cern.ch:443
DEBUG:urllib3.connectionpool:https://cms-rucio.cern.ch:443 "GET /ping HTTP/1.1" 200 27
INFO:root:Rucio server v.1.29.2.post1 contacted
DEBUG:dogpile.lock:NeedRegenerationException
DEBUG:dogpile.lock:no value, waiting for create lock
DEBUG:dogpile.lock:value creation lock <dogpile.cache.region.CacheRegion._LockWrapper object at 0x7faf451013d0> acquired
DEBUG:dogpile.lock:Calling creation function for not-yet-present value
DEBUG:dogpile.lock:Released creation lock
DEBUG:urllib3.connectionpool:https://cms-rucio.cern.ch:443 "GET /accounts/whoami HTTP/1.1" 303 228
DEBUG:urllib3.connectionpool:Starting new HTTP connection (1): cms-rucio.cern.ch:80
DEBUG:urllib3.connectionpool:http://cms-rucio.cern.ch:80 "GET /accounts/crab_server HTTP/1.1" 200 226
INFO:root:Rucio client initialized for crab_server in status ACTIVE
```

If it fail, it will contains log like this (copy from `logs/twlog.txt`):

```
2022-04-06 14:07:03,696:DEBUG:connectionpool,452:https://cms-rucio-auth.cern.ch:443 "GET /auth/x509 HTTP/1.1" 401 129
2022-04-06 14:07:03,702:INFO:Worker,50:Uploading failure message to the REST:
Process-1: I just had a failure for Cannot authenticate.
Details: Cannot authenticate to account crab_server with given credentials
```
