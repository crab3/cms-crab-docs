# FTS

## FTS_Transfer.py temp storage cleanup

It starts from [this discussion in MatterMost](https://mattermost.web.cern.ch/cms-o-and-c/pl/5k9hupsaipnsddnqgiwugbbmba), and Wa wonders why files always get deleted on the temp area (/store/temp) when FTS Transfers are failed, despite the fact that the policy on the temp area is to delete files that are longer than 15 days.

In summary, `FTS_Transfer.py` have internal logic to delete files that failing transfer [here](https://github.com/dmwm/CRABServer/blob/18af83f510e5ff8579ad30a5073cc97af9c060a9/scripts/task_process/FTS_Transfers.py#L206-L216).

Comment from Stefano: *There are no comments in code nor GH commit history, but I think the reason for deleting the tmp file after a failure is simply "clean up". Job will be resubmitted and may run at a different site, in which case the old copy in tmp will stick around wasting disk. And if job re-run at same site, generally speaking, FTS "overwrite if it exist" may or may not work well, and making sure that destination file is not there "can only help".*

You can inspect the logs from `SPOOL_DIR/task_process/transfers/remove_files.log` in schedd machine, and in `SPOOL_DIR/task_process/transfers/transfer_inject.log` for general `FTS_Transfers.py`'s logs, you will see logs look like this:

```
2023-05-09 09:23:01,173: Failure reason: TRANSFER [1] DESTINATION MAKE_PARENT HTTP 403 : Permission refused
2023-05-09 09:23:01,173: file XFER FAIL will remove davs://webdav.recas.ba.infn.it:8443/cms/store/temp/user/tseethon.d6830fc3715ee01030105e83b81ff3068df7c8e0/tseethon/test-workflow/GenericTTbar/autotest-1683616279/230509_071121/0000/output_5.root
```
