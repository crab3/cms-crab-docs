# CRAB TaskWoker How-To

## CVMFS failed to mount

Sometimes CVMFS fails to mount and cannot access `/cvmfs/cms.cern.ch/` directory. We saw it happens in `crab-dev-tw02` after TaskWorer/Publisher container started and some times passed.

To restore it back, either

- Restart machine or
- Restart CVMFS relate services:
    - Delete all container.
    - Clear CVMFS process, and start it back
        ```bash
        systemctl stop autofs
        umount -l /cvmfs/cms.cern.ch` # ignore if there is error.
        kill $(ps aux | grep cvmfs | awk '{print $2}')
        systemctl start autofs
        ```
    - Run `cvmfs_config probe`. If output is not `OK`, restart machine.

## Run Rucio commands in the TW container

Current Rucio client wants a proxy. In the container start directory:

```bash
source env.sh -g
#export CRYPTOGRAPHY_ALLOW_OPENSSL_102=1
voms-proxy-init --cert /data/certs/robotcert.pem --key /data/certs/robotkey.pem -voms cms
export RUCIO_ACCOUNT=crab_server
rucio whoami
```

## Free-up space on host disk

When you receive an email alert with subject "[Alert] DISK: Disk size critical 
at crab-dev-tw02.cern.ch from diskTest.sh@crab-dev-tw02.cern.ch", then:

- if it is a production or preproduction machine: this is a serious alert and should be investigated immediately
- if it is a dev machine, then it is up to the developer responsible for that VM 
to investigate and clean up

Investigate what is actually taking up space with `ncdu`, which is a super powerful command! :)

```bash
ssh crab-*-tw**
sudo su

ncdu -x /
# -x to make ncdu stick to the filesystem where the
# path is mounted. aka skip data disks, /afs, /eos, etc.
```

In case of a development machine, usually the disk fills up because there are too many docker
images on the host disk, and you will notice that most of the space is taken up by 
`/var/lib/docker/overlay2`

you can confirm with

```bash
docker system df
```

remove images that do not have a tag and that are not connected to any container: this is unlikelyto have any effect because it is already done automatically on TW VMs

```bash
docker image prune
```

remove images with a tag but that do not have container associated with them

```bash
docker image prune -a
```

If you did not free up enough space, then you need to stop and remove some container, then run again `docker image prune -a`

inspect which container are present on your vm with


```bash
docker ps -a --format 'table {{.ID}}\t{{.Image}}\t{{.CreatedAt}}\t{{.Status}}'

# them remove the container with
docker container rm $container_id
# or 
docker container rm $container_name
```

you can remove all container that have been created before a certain date with

```bash
# (this command will take some time)
docker container prune --force --filter "until=2022-03-20T13:10:00"

# then remove again all images that are not attached to any container
docker image prune -a
```

You can also remove individual images with

```bash
# get list
docker image ls

# remove
docker rmi $image:$tag
```


## systemd needreload

Ref: https://monit-docs.web.cern.ch/metrics/collectd/#collectdctl

If when you login into a TW machine, the motd reports

```plaintext
Collectd alarm systemd_needreload in FAILURE state
Last login: Tue Apr 30 12:41:22 2024 from 2001:1458:d00:17::5da
```

then you need to identify the value that is in failure state

```bash
[root@crab-prod-tw01 ~]# collectdctl listval state=FAILURE
crab-prod-tw01.cern.ch/systemd-needreload/boolean-NeedDaemonReload
```

take action to fix it

```bash
[root@crab-prod-tw01 ~]# systemctl daemon-reload
```

reload the value state

```bash
[root@crab-prod-tw01 ~]# collectdctl evalstate crab-prod-tw01.cern.ch/systemd-needreload/boolean-NeedDaemonReload
```

make sure that no value is in failure state anymore

```bash
[root@crab-prod-tw01 ~]# collectdctl listval state=FAILURE
[root@crab-prod-tw01 ~]#
```

## Testing new certificates with MyProxy server

!!! note
    You do not need to hash the userDN, any login name supply to `-l` arg is fine.
    CRABClient do [hashing userDN (bytes encoded) in client side](https://github.com/novicecpp/CRABClient/blob/601976558edb92eb2382aa0460b64324e4420b0b/src/python/CRABClient/CredentialInteractions.py#L139-L141) as login name. Then execute `myproxy-init` [here](https://github.com/novicecpp/CRABClient/blob/601976558edb92eb2382aa0460b64324e4420b0b/src/python/CRABClient/ProxyInteractions.py#L130-L151) and later in TW [here](https://github.com/dmwm/CRABServer/blob/bfdeedbebd1e34688eca0e88991a080debd83fb4/src/python/TaskWorker/Actions/MyProxyLogon.py#L87).

1. Client: Put delegation to myproxy server. You need subject of certificate you want to test and provide to `myproxy-init`. client cert path can override using usual `X509_USER_CERT` and `X509_USER_KEY` envvars:
    ```bash
    myproxy-init -d -n -s myproxy.cern.ch \
        -x -R '/DC=ch/DC=cern/OU=Organic Units/OU=Users/CN=crabint1/CN=373708/CN=Robot: CMS CRAB Integration 1' \
        -x -Z '/DC=ch/DC=cern/OU=Organic Units/OU=Users/CN=crabint1/CN=373708/CN=Robot: CMS CRAB Integration 1' \
        -l 'tseethon_CRAB_testcert1' \
        -t 168:00 -c 3600:00
    ```
2. Server: make sure you use proper cert by providing correct path to `X509_USER_CERT` and `X509_USER_KEY`

    ```bash
    export X509_USER_CERT=/data/certs/crabint1/robotcert.pem
    export X509_USER_KEY=/data/certs/crabint1/robotkey.pem

    myproxy-logon -d -n -s myproxy.cern.ch \
    -o /tmp/1 \
    -l "tseethon_CRAB_testcert1" \
    -t 168:00 \
    --verbose
    ```

## Some useful `openssl` commands

### Check cert/key pair

In case you want to crosscheck cert/key pair, you can check it using `openssl x509 -modulus` function (Thanks to [SUSE Support page](https://www.suse.com/support/kb/doc/?id=000018423))
``` bash
export X509_USER_CERT=/data/certs/crabint1/robotcert.pem
export X509_USER_KEY=/data/certs/crabint1/robotkey.pem
openssl x509 -noout -modulus -in $X509_USER_CERT | openssl md5 > /tmp/crt.pub
openssl rsa -noout -modulus -in $X509_USER_KEY | openssl md5 > /tmp/key.pub
diff /tmp/crt.pub /tmp/key.pub # should not print anything
```

### Print x509 subject name

```
openssl x509 -noout -subject -in /data/certs/crabint1/robotcert.pem
```
