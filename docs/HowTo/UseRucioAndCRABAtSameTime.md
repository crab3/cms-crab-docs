# How to use Rucio inside CMSSW env on lxplus7/8
## e.g. at the same time a using CRAB Client

**Note**: rucio commands which require `gfal-*`, like `rucio download` will not work

1. python3 only. Stick to CMSSW > 12_6
2. in order to use python client
```bash
export PYTHONPATH=$PYTHONPATH:/cvmfs/cms.cern.ch/rucio/x86_64/rhel8/py3/current/lib/python3.6/site-packages/
```
3. in order to use also Rucio CLI
```bash
export PATH=$PATH:/cvmfs/cms.cern.ch/rucio/x86_64/rhel8/py3/current/bin
```
4. and then the configuration
```bash
export RUCIO_HOME=/cvmfs/cms.cern.ch/rucio/current/
export RUCIO_ACCOUNT=`whoami` 
```

yes, use `rucio/x86_64/rhel8` on both lxplus7 and lxplus8.
