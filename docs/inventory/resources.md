# Resources

## Documents

* [CMS CRAB Documentation](https://cmscrab.docs.cern.ch): the site you are browsing now. New docs site for CRAB team. Writing in MarkDown format and store in git repository. Limit access to CERN account.
* [CMS CRAB User Guide](https://cmscrab-userguide.docs.cern.ch): separate site for user documentation. Open to public.
* [CRAB3 Software guide](https://twiki.cern.ch/twiki/bin/view/CMSPublic/SWGuideCrab): Index page of CRAB documentation. We are in migrating to the new site above. But, it user docs still remain here.
* [CMM Offline WorkBook](https://twiki.cern.ch/twiki/bin/view/CMSPublic/WorkBook): workbook for using resource in CMS Offline group. Some page are obsolete.


## Repositories

* <https://github.com/dmwm/CRABServer> Main CRAB code (contains everything you need).
    * <https://github.com/dmwm/WMCore> Main library we use to create CRAB system.
* <https://github.com/dmwm/CRABClient> The Client for user to interact with CRAB system.
* <https://github.com/dmwm/CMSKubernetes/> CMSWEB Kubernetes manifests. We store our K8s manifests in this repo.
* <https://github.com/dmwm/deployment/> DeprecaOlder scripts for deploy CRAB in VM. We still use it when build CRABServer image.
* <https://gitlab.cern.ch/crab3> Our group in Gitlab.
* <https://gitlab.cern.ch/ai/it-puppet-hostgroup-vocmsglidein> Puppet configuration for service.
    * <https://gitlab.cern.ch/ai/it-puppet-module-vocmshtcondor> Puppet HTCondor module setup by SI to install and configure HTCondor in Schedd machine.


## LXPLUS

### Server

#### lxplus.cern.ch

Terminal server for all users at CERN.

#### lxtunnel.cern.ch

This server is use only for ssh tunnel. E.g., `ssh lxtunnel.cern.ch -D 11122` or [sshuttle](https://github.com/sshuttle/sshuttle).

#### aiadm.cern.ch

Accessing the Puppet resources and internal tools like `tbag`, `ai-*`.

### Phonebook
One very useful software provided by LXPLUS is the `phonebook`, which you can get their contact (email address) in case you need to ask for the permission, by run:

```
phonebook -a <firstname> <surname>
```
