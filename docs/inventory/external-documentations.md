# External Documentations

The documentation we are using for find some answer.

* [CERN Puppet](https://config.docs.cern.ch/)
* [CERN Openstack](https://clouddocs.web.cern.ch/)
* [CERN Hadoop](https://hadoop.docs.cern.ch/)
* [CMSWEB](https://cms-http-group.docs.cern.ch/)
* [CMS MONIT](https://cmsmonit-docs.web.cern.ch/)
* [The Documentation (docs.cern.ch)](https://how-to.docs.cern.ch/)
* [CERN Linux (Kerberos)](https://linux.web.cern.ch/docs/kerberos-access/)
* [Security Tokens (IETF spec)](https://datatracker.ietf.org/doc/html/rfc8693)

