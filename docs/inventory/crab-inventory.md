# CRAB Inventory

This is considered to be the most up-to-date list of resources that we as CRAB operators have at our disposal.

## Oracle DBs

| use                       | name                    | database        | owner   | pwd shared with other operators |
|---------------------------|-------------------------|-----------------|---------|---------------------------------|
| prod (full, alter tables) | cms_analysis_reqmgr     | cmsr            | stefano | yes                             |
| prod (r/w, application)   | cms_analysis_reqmgr_r/w | cmsr            | stefano | yes                             |
| preprod                   | cmsweb_analysis_preprod | int2r           | stefano | yes                             |
| dev                       | cmsweb_analysis_dev     | int2r           | stefano | yes                             |
| devtwo                    | cmsweb_crab_9r_dev      | ~~int9r~~ int2r | stefano | yes                             |
| devthree                  | cmsweb_crab_9r_preprod  | ~~int9r~~ int2r | stefano | yes                             |
| devfour                   | cms_analysis_reqmgr     | int2r           | stefano | yes                             |

## K8S clusters

NOTE: `cluster` is cluster name from `clusters[].name` field in kubeconfig, not the file name.

| Name    | Cluster                    | Note |
|---------|----------------------------|------|
| prod    | cmsweb-k8s-prodsrv         |      |
| preprod | cmsweb-k8s-prodsrv-v1.22.9 |      |
| test2   | cmsweb-test2               |      |
| test12  | cmsweb-test12              |      |
| test14  | cmsweb-test14              |      |

Ref:

- [https://cms-http-group.docs.cern.ch/](https://cms-http-group.docs.cern.ch/)
- [https://gitlab.cern.ch/cmsweb-k8s/users_config](https://gitlab.cern.ch/cmsweb-k8s/users_config)

## TW VMs

| Hostname          | Puppet Env            | Note                |
|-------------------|-----------------------|---------------------|
| crab-prod-tw01    | production            |                     |
| crab-prod-tw02    | production            |                     |
| crab-preprod-tw01 | qa                    |                     |
| crab-preprod-tw01 | qa                    |                     |
| crab-dev-tw01     | qa                    |                     |
| crab-dev-tw02     | (see in judy.cern.ch) |                     |
| crab-dev-tw03     | (see in judy.cern.ch) |                     |
| crab-dev-tw04     | (see in judy.cern.ch) | Puppet test machine |
| crab-dev-tw05     | (see in judy.cern.ch) |                     |

- [openstack.cern.ch](https://openstack.cern.ch)
- [judy.cern.ch](https://judy.cern.ch/?search=vocmsglidein%2Fcrabtaskworker&page=1)

## Environment mapping

| REST            | DB Instance | TW                     | Owner   |
|-----------------|-------------|------------------------|---------|
| prod            | prod        | crab-prod-tw{01,02}    |         |
| preprod         | preprod     | crab-preprod-tw{01,02} |         |
| test2           | dev         | crab-dev-tw01          | Stefano |
| test12          | devthree    | crab-dev-tw03          | Krittin |
| test14          | devfour     | crab-dev-tw05          | Vijay   |

Note that we have more DB instances then available K8s test clusters, devtwo DB instance is "free" and can be accessed in "super-expert mode" by properly changing configuration files and Oracle secrets.

Ref:

- [SERVICE_INSTANCES](https://github.com/dmwm/CRABServer/blob/ce559a11ef02c447f909ee166b7d48f218eb021d/src/python/ServerUtilities.py#L47-L62)
- Puppet's hiera file. For example, [crab-dev-tw01.cern.ch.yaml](https://gitlab.cern.ch/ai/it-puppet-hostgroup-vocmsglidein/-/blob/454701f3d9b10ae906e24f437e625c2d84d8c631/data/fqdns/crab-dev-tw01.cern.ch.yaml) from Stefano's env.

## schedd VMs

| pool   | puppet env | hostname |
| - | - | - |
| global | crab_schedd_master | vocms0106 |
| global | crab_schedd_master | vocms0107 |
| global | crab_schedd_master | vocms0119 |
| global | crab_schedd_master | vocms0120 |
| global | crab_schedd_master | vocms0121 |
| global | crab_schedd_master | vocms0122 |
| global | crab_schedd_master | vocms0137 |
| global | crab_schedd_master | vocms0144 |
| global | crab_schedd_master | vocms0155 |
| global | crab_schedd_master | vocms0194 |
| global | crab_schedd_master | vocms0195 |
| global | crab_schedd_master | vocms0196 |
| global | crab_schedd_master | vocms0197 |
| global | crab_schedd_master | vocms0198 |
| global | crab_schedd_master | vocms0199 |
| global | qa                 | vocms059 |
| itb    | qa                 | crab-preprod-scd03 |
| itb    | qa                 | vocms068 |
| itb    | qa                 | vocms069 |
| global | qa                 | crab-sched-901.cern.ch |
| global | qa                 | crab-sched-903.cern.ch |


Ref:

- [openstack.cern.ch](https://openstack.cern.ch)
- [judy.cern.ch](https://judy.cern.ch/?search=vocmsglidein%2Fcrabschedd&page=1)
- `condor_status -sched -con 'CMSGWMS_Type=="crabschedd"' -pool cmsgwms-collector-global.cern.ch -af machine`
- `condor_status -sched -con 'CMSGWMS_Type=="crabschedd"' -pool cmsgwms-collector-itb.cern.ch -af machine`

## gitlab ci runner VMs

| puppet env | hostname |
| - | - |
| qa | crab-ci-runner01.cern.ch |

## Service accounts

| account    | id                                                         | owner            | last check |
|------------|------------------------------------------------------------|------------------|------------|
| `cmscrab`  | "uid=162295(cmscrab) gid=2766(def-cg) groups=2766(def-cg)" | dario mapelli    | 2023 08 22 |
| `crabint1` | "uid=165838(crabint1) gid=1399(zh) groups=1399(zh)"        | stefano belforte | 2024 04 22 |

we use those service accounts for:

- crab spark, running on `crab-dev-tw04`, kerberos keytab generated manually by dario
- create a robot certificate which we use for authenticating with Rucio and myproxy.cern.ch
  - robot s:
     - `/DC=ch/DC=cern/OU=Organic Units/OU=Users/CN=cmscrab/CN=817881/CN=Robot: cms crab`
     - `/DC=ch/DC=cern/OU=Organic Units/OU=Users/CN=crabint1/CN=373708/CN=Robot: CMS CRAB Integration 1`

We have multiple service accounts in order to separate use in Production and Test/Dev instances
to give us freedom to test and experiment w/o fear of impacting production
The integration Robot cert. is also used to submit test jobs in the CI/CD pipeline

## Rucio accounts

| account | purpose |
| - | - |
| `crab_server` | generic unprivileged, connected to TW's DN's, to make Rucio queries |
| `crab_tape_recall` | to create rules for tape recall |
| `crab_input` | to create rules which lock on disk input datasets |
| `crab_test_group` | to exercise writing to /user/group/rucio/crab_test/  <br />  List members via  `rucio-admin account list-identities crab_test_group` |


## Puppet Secrets ([tbag](https://cmscrab.docs.cern.ch/technical/crab-puppet/tools.html#tbag))

### Hostgroup

| Hostgroup                     | keyname                       | description                             |
|-------------------------------|-------------------------------|-----------------------------------------|
| `vocmsglidein/crabtaskworker` | `tw_condor_token_global_pool` | HTCondor token to talk with CRAB Schedd |
|                               | `tw_condor_token_itb_pool`    | HTCondor token to talk with CRAB Schedd |
|                               | `crabint1_robotcert`          | crabint1 Robot Certificate Cert         |
|                               | `crabint1_robotkey`           | crabint1 Robot Certificate Key          |

### Host

| Hostname                                       | keyname                           | description                                                   |
|------------------------------------------------|-----------------------------------|---------------------------------------------------------------|
| `crab-{dev,preprod,prod}-tw{01,02,03,04}`      | `tw_rucio_robotcert`              | CRAB Robot Certificate Cert   TO DEPRECATE                    |
|                                                | `tw_rucio_robotkey`               | CRAB Robot Certificate Key    TO DEPRECATE                    |
| `crab-prod-tw{01,02}`                          | `cmscrab_robotcert`               | cmscrab Robot Certificate Cert                                |
|                                                | `cmscrab_robotkey`                | cmscrab Robot Certificate Key                                 |
| schedd hostname (e.g, `vocms059`, `vocms0199`) | `token_global_$(schedd_hostname)` | HTCondor token for Schedd to talk with GLOBAL Pool negotiator |
|                                                | `token_tier0_$(schedd_hostname)`  | HTCondor token for Schedd to flock to T0 pool                 |
|                                                | `token_itb_$(schedd_hostname)`    | HTCondor token for Schedd to talk with ITB Pool negotiator    |

## Opensearch indexes

| opensearch instance | index pattern | used by | access from |
|-|-|-|-|
| https://monit-opensearch-lt.cern.ch | `monit_prod_crab_raw_*` | cronjob, production | internet |
| https://monit-opensearch.cern.ch | `monit_prod_crab-test_raw_*` | cronjob, test | internet |
| https://monit-timberprivate.cern.ch | `monit_prod_crab_logs_[...]-*` | TW, Publ, REST filebeat+logstash logs | cern GPN |
| https://os-cms.cern.ch | `crab-[...]-*`, in `crab` tenant | spark cornjobs | internet |
| https://monit-timber.cern.ch | `monit_prod_crab_logs_[...]-*` | deprecated | ??? |

## e-groups for CRAB
Membership in these e-groups grants special rights. They may contain other people besides CRAB operators (e.g. Submission Infrastructure people have `ssh` and `sudo` permission on our HTCondor schedulers (AccessPoints) too.

| name | purpose                                                                                                            |
| - |--------------------------------------------------------------------------------------------------------------------|
| cms-crab-HighPrioUsers | O&C L1's can put (few) user names here. CRAB jobs from them will be matched first and have no quota limit in HTCondor |
| cms-eos-web-CRAB | members can write to /eos/project/c/cmsweb/www/CRAB/                                                               |
| cms-service-crab-operators | receives mails sent to root account on VM's <br /> controlled via puppet                                 |
| cms-service-crab-sysadmins | can do `ssh`-to `sudo`-on and do docker commands on VM's <br /> controlled via puppet                    |
| cms-service-crab3htcondor | ??? Not used in puppet. Only somethin in people's address books ?                                         |
| cms-service-crab3htcondor-monitor | ??? Not used in puppet. Obsolete ?                                                                |
| cms-htcondor-admins | receives messages (schedd restart e.g.) from HTCondor. Not limited to CRAB nodes                                |


## all e-groups

All the following ones are e-groups that you should belong to. Of course you may be a member of many others too.


**NOTE: membership for cms-service-crab* groups will come automatically once you are a member of crab-operators**


| e-group                           | Description         | How to join             |
|-----------------------------------|-----------------------------------| -- |
|  ai-admins, LxAdm-Authorized-Users | ssh access to `aiadm.cern.ch` machine which have tool to manage the CRAB VMs in openstack   | subscribe |
| cms-openstack-vocmscrab-admins    | To access openstack project  | subscribe |
| cms-service-crab-operators        | master list of CRAB Operators. Also mailling list for our team). | ask your L2 | 
| cms-service-crab3htcondor         | Announcements from IT to CRAB3-HTCondor Project (label=landb-mainuser in Openstack) | automatic for members of cms-service-crab-operators |
| cms-service-crab3htcondor-admins  | Admins of the cms-service-crab3htcondor e-group (label=landb-responsible in Openstack)  | automatic for members of cms-service-crab-operators |
| cms-service-crab-sysadmins        | Access system in CRAB (ssh to `crab-*-tw*` and CRAB schedd machine / [CRAB3 gitlab repository](https://gitlab.cern.ch/crab3), to the "CMS Webtools Mig" openstack project where the cmsweb k8s test clusters are) | automatic for members of cms-service-crab-operators |
| cms-service-crab-test             | For sync your certificate identity to `crab_test` Rucio user  via CRIC | automatic for members of cms-service-crab-operators |
| cms-service-webtools              | Mailling list for CMSWEB operators (CRAB system live inside CMSWEB Kubernetes cluster)  | subscribe |
| cmsweb-registry                   | To push crabserver images to the cern registry (need to be confirm) | subscribe |
| es-timber-cmsweb_kibana           | Access `cmsweb` tenant in [Opensearch timber private](https://monit-timberprivate.cern.ch/),  containing logs of crabserver REST. | subscribe |
| oracle-cms-databases-users        | allows the user to create schemas in the CMS Oracle DB instances, such as int2r | subscribe |
| cms-htcondor-admins               | Receives messages from HTCondor (scheduler restarts e.g.), for for all machines, not CRAB ones only | ask Marco Mascheroni |


These groups are from old documentation, not sure if we still need it.

| e-group                           | Description                                                                              |
|-----------------------------------|---------------------------------------------------|
| cms-service-kibana                | Admin users of the CMS Kibana dashboards AFS area |

For the group that need approval:

- If owner is Stefano Belforte, ask Stefano.
- If owner is Liz Sexton-Kennedy, open the [CMSVOC Jira](https://its.cern.ch/jira/projects/CMSVOC) ticket (do not tag the owner).
- If owner is Valentin Kuznetsov, open the [CMSKubernetes Jira](https://its.cern.ch/jira/projects/CMSKUBERNETES) ticket (do not tag the owner).
- Otherwise, send email to group owner.
