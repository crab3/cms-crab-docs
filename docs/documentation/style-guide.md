# Style Guide

We follow the style guide from [OSG-HTC Technology Documentation](https://osg-htc.org/technology/documentation/style-guide/).

In addition to those:

- We prefer Level 1 and Level 2 heading to use `#` and `##`.
- Use fences for all code block, include code block inside list.
- Try to make pages work on both MkDocs and Gitlab.

    - In particular, if some formatting does not work as expected, then it is possible that you need to enable some specific formatting extension. Have a look at the offical mkdocs doc at [https://squidfunk.github.io/mkdocs-material](https://squidfunk.github.io/mkdocs-material/setup/extensions/python-markdown-extensions/)

- A tip for section links, try to open page in local mkdocs server and copy it from permalink.
- Image should use `![image1.png](images/image1.png)` or html `<img src="./images/image1.png" alt="image1.png" width="25%"/>` if you want to resize (gitlab does not support resize image in Markdown format, yet).
- Save image in repo. Put it to `docs/images` directory and adjust relative link accordingly.
- use lowercase and dash (`-`) for files/directories name.
- If page still in draft, please put "info" [admonition](https://squidfunk.github.io/mkdocs-material/reference/admonitions/) at the top of the page. Like:

    ```
    !!! note
        This page is currently in draft.
    ```

    Will rendered like

    !!! note
        This page is currently in draft.

- Do not worry about review before publishing. Commit and push to master directly. Review will happen at a later time.

- Mark content review with badges:

    - If someone writes/reads a page, insert at the beginning of the webpage `![review: partial](../images/badges/review-partial.svg)`, which renders to ![review: complete](../images/badges/review-partial.svg)
    - After Stefano approves the content of a webpage, we mark that page with `![review: complete](../images/badges/review-complete.svg)`, which renders to ![review: complete](../images/badges/review-complete.svg)

