# Guide - How to organize content in this webpage

We strive to follow the [diataxis](https://diataxis.fr/) approach to
write good documentation

| | Learning, beginner | Day to Day |
|-|-|-|
| **Action**    | tutorial    | how-to    |
| **Knowledge** | explanation | reference |



