# Migrating from Twiki

## Use pandoc

[pandoc](https://pandoc.org/MANUAL.html) help us convert Twiki page to Markdown. But it is not perfect, we need to edit whole page manually.

To use, run:

```bash
pandoc -f twiki -t markdown --wrap=none --shift-heading-level-by=1 crab-client.twiki
```

Where arguments of pandoc are:

- `-f twiki` is "from Twiki (input format)".
- `-t markdown` is "to Markdown (output format format)".
- `--wrap=none` (optional) to disable text wrap.
- `--shift-heading-level-by=1` (optional) for increase/decrease header level (not needed onpandov v2.5 or later)
- `crab-client.twiki` is input we want to convert, copy from Twiki's edit page.

Example output of [CRABClient page](https://twiki.cern.ch/twiki/bin/view/CMSPublic/CMSCrabClient), [Twiki](../files/crab-client.twiki.txt) to [Markdown](../files/crab-client.md.txt) with command above, and result [(CRAB Client Markdown version)](https://cmscrab.docs.cern.ch/techincal/crab-client.html) after manually edit.

What pandoc cannot do (Wa: as far as I know):

- All of function native to CERN/CMS Twiki (css/macro).
- Cannot recognize numbered lists. If you have code block inside numbered list, it will treat as text.
- Escape all dot (`.`), qoute (`'`), and double quote (`"`) with backslash.


## Best practice

- Always put a new link to Markdown page in Twiki, like this image below:

    ![link-to-md.png](../images/link-to-md.png)

- Also, put warning message inside page too.

    ```
    %RED%<strong>WARNING!!! This page has been moved to the [[https://cmscrab.docs.cern.ch][new site]]. Please read/edit this page on the new site instead.</strong>%ENDCOLOR%
    ```

    ![warning-page-moved.png](../images/warning-page-moved.png)
