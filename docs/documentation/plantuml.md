# PlantUML ?

We recommend using PlantUML for creating diagrams. Since it is a code-based tool, you can easily make changes to the diagram by updating the PlantUML code and simply regenerating it.

## How?

1. Put PlantUML code in `docs/plantuml/` with extension `.puml`.
2. Generate image using command line (You can download binary from the [official website](https://plantuml.com/download)):
    ```bash
    /usr/bin/java -jar '/path/to/plantuml.jar' path-to-plantuml-file.puml
    ```
    Output will have the same filename but with extensions `.png.`
3. Put your generated image in previous step to `docs/images` and add to markdown like normal image.

## Alternative: Mermaid

for future diagrams we may want to explore also [Mermaid](https://mermaid.js.org/) and possibly decide if to change.
```
On 05/12/2024 11:07, Thanayut Seethongchuen wrote:
> Hi Stefano,
>
> While browsing for a new tools, I found mermaid.js [1],
> the diagram/chart renderer tool alternative to PlantUML,
> which supports direct rendering in MkDocs [2] and GitHub [3]
> In case you are interested :)
>
> Best,
> Thanayut Seethongchuen
>
> [1] https://mermaid.js.org/
> [2] https://squidfunk.github.io/mkdocs-material/reference/diagrams/#configuration
> [3] https://github.com/ClusterCockpit/cc-metric-collector/blob/main/docs/configuration.md?plain=1 
```

