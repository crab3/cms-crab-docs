# View MkDocs in local

## pycharm
Open an `.md` file in PyCharm, it will appear as a vertically splitted window
with source at left and rendering at right.

## mkdocs serve

First, install MkDocs with material `mkdocs-material` via pip. If you use `pipenv`, you can run `pipenv sync` on root of this git directory.

Then, run `mkdocs serve` from the top directory of gitlab clone (to have config) and browse <http://localhost:8000/> (you can change listen address/port with `-a ip:port`). `mkdocs serve` make browser refresh automatically if files is changed.

### container

If you do not want to use your local python, then we suggest using an 
[apptainer](https://apptainer.org/) (new name for singularity) container.

See the [apptainer docs](https://apptainer.org/docs/admin/main/installation.html) for instructions 
on how to install it. Then you can get the latest `fedora` docker image,
run it with apptainer, install pandoc, pip and mkdocs.

The first time that you want to use the container, you need to:

```bash
# tested with: fedora 36

laptop $ # install apptainer
laptop $ sudo dnf install apptainer

laptop $ # download the container, convert it from docker format to apptainer format
laptop $ mydir=path/to/fedora-docker-202210
laptop $ apptainer build --sandbox $mydir docker://fedora

laptop $ # install packages inside the container, sudo is required
laptop $ sudo apptainer run --writable $mydir
container $ dnf install pandoc
container $ dnf install python3-pip

laptop $ # install packages inside the container, sudo not required
laptop $ apptainer run --writable --cleanenv --env LC_ALL=C $mydir
container $ cd path/to/cms-crab-docs
container $ python3 -m pip install --user mkdocs mkdocs-material pymdown-extensions mkdocs-glightbox mkdocs-git-revision-date-localized-plugin
container $ # everything should be ready for serving the website locally
container $ mkdocs serve
container $ # pandoc should also be available
container $ cd docs/...
container $ pandoc $file.twiki --from twiki --to markdown --no-highlight -o $file.md

```

The next time time, you can just start the container and everything 
is already available inside it:

```bash
laptop $ mydir=path/to/fedora-docker-202210
laptop $ apptainer run --writable --cleanenv --env LC_ALL=C $mydir
container $ cd path/to/cms-crab-docs
container $ mkdocs serve
container $ cd docs/...
container $ pandoc $file.twiki --from twiki --to markdown --no-highlight -o $file.md

```

further apptainer docs:

- [cli reference - build](https://apptainer.org/docs/user/main/cli/apptainer_build.html)
- [cli reference - run](https://apptainer.org/docs/user/main/cli/apptainer_run.html)

### troubleshoot

**Achtung!** If your browser does not load the local mkdocs when your laptop is 
offline, then it may be because it can not load the fonts from 
[https://fonts.googleapis.com](https://fonts.googleapis.com) . In order to continue using the local mkdocs
even without all the fonts, you can open the developer window, right-click on the
request to the google fonts that is timing out, click on "block this URL",
then reload the web page.

## Gitlab

Unfortunately, you need to push to remote and view it in gitlab website.

We suggest opening a new branch, pushing new change, and viewing it on gitlab.

