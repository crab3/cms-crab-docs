# CRAB Validation


## Jenkins 

a few tests can be triggered in Jenkins to validate (partially) new versions of CRABClient and/or CR
ABServer.
CRAB covers so many use cases that there is not full validation possible (including proper handling of all possible errors which may happen in the distributed computing system).

Therefore tests below MUST be executed, but can not replacer ad hoc testing of bug fixes and/or new
featires.

Jenkins tests are currently documented in [CRAB Twiki](https://twiki.cern.ch/twiki/bin/view/CMSPublic/Crab3Testing)


