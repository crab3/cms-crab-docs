# CRAB Testing

## Unit test

!!! warning
    (Wa) CRAB unit test is still in early development stage. This is first time for me to write proper unitttest. Code and design will change a lot to make more practical, easy to code and fit into our use case.


I use [pytest](https://docs.pytest.org/) to write unit test and use it as test runner because test files will have less code compare to python built-in `unittest`. But, I try to use test library directly from `unittest` as much as possible. Such as `unittest.mock`, except some tools like `pytest.fixture` that can easily convert to setUp/tearDown method, in case we want to convert back to `unittest`.

### Running the test


#### Setup

- Create virtual environment and source it. See https://docs.python.org/3/tutorial/venv.html
- Install pytest
    ```bash
    pip install -U pytest==7.2.2
    ```
- export `src/python` to `$PYTHONPATH` (you need to do it manually everytime after source virtualenv.
    ```bash
    cd /path/to/CRABServer
    export PYTHONPATH=$PWD/src/python:$PYTHONPATH
    ```

#### Execute unit test

- Run all test:
    ```bash
    pytest test/python
    ```
- Run single file:
    ```bash
    pytest test/python/ASO/Rucio/test_BuildTaskDataset.py
    ```
