# CRABServer REST: tests, stress tests, performance benchmarks

![review: partial](https://img.shields.io/badge/review-partial-fcba03)

We collect here some instructions on how to test, stress test and benchmark
the CRAB REST server.

## GET

It is relatively straighforward to perform tests of GET endpoints.

### curl

In order to perform a single call to a GET endpoind, you can use curl with:

```bash
# lxplus, default env
voms-proxy-init --rfc --voms cms -valid 192:00
export X509_USER_PROXY=/tmp/x509up_u$UID

curl --cert $X509_USER_PROXY --key $X509_USER_PROXY \
  https://cmsweb-testbed.cern.ch:8443/crabserver/preprod/info
```

Or you can use a cookiefile to authenticate with the new go-based FE with

```bash
# lxplus, default env.
auth-get-sso-cookie -o cookiefile.txt -u https://cmsweb-test2.cern.ch
curl --cookie cookiefile.txt https://cmsweb-test2.cern.ch/crabserver/dev/info
```

Notice that in the latter case, 
the host used to retrieve the cookie needs to be the same as the 
host queried by `curl`, otherwise you will get something like

```plaintext
<a href="https://auth.cern.ch/auth/realms/cern/protocol/openid-connect/auth?client_id=cms-go&amp;redirect_uri=https%3A%2F%2Fcmsweb-test2.cern.ch%2Fcallback&amp;response_type=code&amp;scope=openid+profile+email&amp;state=49f389ee-d33c-4dc8-bcca-f48fbba546bf">Found</a>.
```

Keep in mind that if you want to check all the headers, you can use the `-v`
curl option.

### cmsweb-ping

If you want to perform a simple GET request bypassing the FE, you can use the 
[cmsweb-ping](https://github.com/dmwm/cmsweb-exporters/blob/master/cmsweb-ping.go)
utility written by Valentin, whose main purpose is launching readiness
and liveness probes of the cmsweb REST services.

```bash
# form inside a pod, for example in preprod
cmsweb-ping \
--authz=/etc/hmac/hmac -verbose 1 \
--url="http://localhost:8270/crabserver/preprod/info"
```

### hey

The easiest tool that we can use to make concurrent requests is `hey`. 
The main documentation on how to 
use this tool is provided by cmsweb team in their 
[docs](https://cms-http-group.docs.cern.ch/k8s_cluster/benchmark/).

The main `hey` repository is [rakyll/hey](https://github.com/rakyll/hey), 
but we use a [fork](https://github.com/vkuznet/hey/tree/x509-csv-fixes)
from Valentin that allows to loging to cmsweb FE with a x509 proxy file.

Simple example of testing a GET API enddpoint

```bash
# from lxplus
voms-proxy-init --rfc --voms cms -valid 192:00
export X509_USER_PROXY=/tmp/x509up_u$UID

/cvmfs/cms.cern.ch/cmsmon/hey \
-disable-keepalive -T "*/*" -A "*/*" \
-N 1 \
-n 100 -c 10 \
https://cmsweb-testbed.cern.ch/crabserver/preprod/info
```

If you pass to `hey` a file with a list of URLs, `hey` will randomly pick
a URL every time it make a request. 

```bash
cat urls.txt 
> https://cmsweb-testbed.cern.ch:8443/crabserver/preprod/info
> https://cmsweb-test1.cern.ch:8443/crabserver/dev/info

/cvmfs/cms.cern.ch/cmsmon/hey \
-disable-keepalive T "*/*" -A "*/*" \
-N 1 \
-n 10 -c 5 \
-o csv -U ./urls.txt
```

It would be also possible to use `hey` to test POST/PUT requests, but
it is not trivial to come up with the correct data to use in a POST/PUT
request for crabserver endpoints. 

### how to make your requests appear in graphana

The default "User-Agent" set by `curl` is `curl/x.y.z` and the one by 
`hey` is `Go-http-client/1.1`, which are
excluded in the graphana dashboards that show data from timbers, both in
FE and crabserver one. We suggest to always set a user agent that match the pattern
`CRAB*/x.y.z`, so that we can monitor in grafana the resutls of the tests.

Moreover, at the moment (2022-06) all the requests to `/info` do not appear in graphana,
despite being shown in kibana (tracked in [7261](https://github.com/dmwm/CRABServer/issues/7261)),
so we suggest benchmarking with

```bash
# same setup as curl above
curl --cert $X509_USER_PROXY --key $X509_USER_PROXY  \
-H "User-agent: CRABStressTest/0.0.0" \
"https://cmsweb-testbed.cern.ch:8443/crabserver/preprod/workflowdb?limit=4&workername=crab-preprod-tw01&getstatus=SUBMITTED"
```

```bash
# same setup as hey above
/cvmfs/cms.cern.ch/cmsmon/hey \
-disable-keepalive -T "*/*" -A "*/*" \
-N 1 \
-n 100 -c 10 \
-H "User-Agent: CRABStressTest/0.0.0" \
"https://cmsweb-testbed.cern.ch/crabserver/preprod/workflowdb?limit=4&workername=crab-preprod-tw01&getstatus=SUBMITTED"
```


## POST/PUT

When we want to run a test that involves a POST/PUT request, it is easier to use
some CRAB client API.

### Ad hoc scripts for a specific POST/PUT endpoint

An example is the script 
[StressTest_REST_POST.py](https://github.com/dmwm/CRABServer/blob/master/scripts/Utils/StressTest_REST_POST.py),
that uses the `filetransfers` POST endpoint to mark the files of a task as
published. This script uses the async/await python library to make concurrent
requests from the client.


