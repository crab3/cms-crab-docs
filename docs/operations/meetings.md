# Meetings

See all meeting list in O&C in [Indico](https://indico.cern.ch/category/1366/overview?period=week) page.

* CRAB DevOps Meeting: Tuesday 11.00 - This is mandatory. Notify in advance if you cannot attend.
* Facilities meeting: Monday 16.00 - We are obliged to report the status of the CRAB service to Facilities service team weekly. See more in [Facilities meeting weekly report](facilities-meeting-weekly-report.md).
* General C&O meeting: Wednesday 15.00 - not mandatory but highly recommend to here the news what going on in our O&C group.

You can freely join other meeting to ask some question relate to your tasks.
