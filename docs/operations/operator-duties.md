
###    Constant:
 *       act on current plan agreed in weekly CRAB DevOps meeting, log relevant operational changes in e-log
    * e-log: we keep track of important events in operations via messages in the [CRAB Operations](https://cms-talk.web.cern.ch/c/offcomp/ais/crab-ops/284) cms-talk forum
 *       check MatterMost/Mail routinely for important communications of sudden problem report 
###   Every day:
 *       at least twice a day check dashboards in last 2 and 7 days and look for unusual patterns.
            Some of those are known and lead to [CRABStandardCorrectiveActions](https://twiki.cern.ch/twiki/bin/view/CMSPublic/CRABStandardCorrectiveActions)
###    Every week:
 *       attend relevant meetings (CentralOperations, Weekly General C&O, CRAB DevOps)
###    Every month:
 *       pay attention to monthly mails from automatic Oracle procedure which drops old partitions and ensure that old partition are properly removed
 *       make sure to keep documentation up to date with whatever changes have been done recently 
###    Every year:
 *       make sure that yearly certificate renewal for all services and hosts is done automatically and properly on all hosts which we operate well before current certificates expire to guaranteed smooth operations 


