# CRAB Shutdown

Detailed instructions on how to turn off CRAB in case of interventions, for 
example in case of an Oracle DB downtime.

## Oracle downtime

We will describe here how the CRAB team should respond to an Oracle planned 
downtime.

### CRAB shutdown - do not drain schedds

- stop prod and preprod TW, Publisher
- change holdsigkill for all running dagmans

    ```bash
    # condor_qedit -con 'jobuniverse==7&&jobstatus==2&&crab_userhn=="dmapelli"' HoldKillSig \"SIGKILL\"
    wassh -c vocmsglidein/crabschedd condor_qedit -con 'jobuniverse==7&&jobstatus==2' HoldKillSig \"SIGKILL\"
    ```

- condor hold running dagmans, set hold reason. see [here](https://htcondor.readthedocs.io/en/latest/man-pages/condor_hold.html). Running jobs will continue to run, no new jobs will be submitted, no pre/post-job scripts will be run. use wassh from aiadm

    ```bash
    # condor_hold -con 'jobuniverse==7&&jobstatus==2&&crab_userhn=="dmapelli"' -reason DMORACLETEST -subcode 20231211
    wassh -c vocmsglidein/crabschedd condor_hold -con 'jobuniverse==7&&jobstatus==2' -reason ORACLEOFF -subcode 20231212
    ```

- stop prod and preprod REST
- (wait for oracle ops to be over)
- restart prod and preprod REST
- make sure that rucio and dbs are up
- change holdsigkill for all hold dagmans with a certain reason.


    ```bash
    # condor_qedit -con 'jobuniverse==7&&jobstatus==5&&crab_userhn=="dmapelli"&&HoldReason=="DMORACLETEST (by user condor)"&&HoldReasonSubCode==20231211' HoldKillSig \"SIGUSR1\"
    wassh -c vocmsglidein/crabschedd condor_qedit -con 'jobuniverse==7&&jobstatus==5&&HoldReason=="ORACLEOFF (by user condor)"&&HoldReasonSubCode==20231212' HoldKillSig \"SIGUSR1\"
    ```

- release all dagmans with a certain reason, see [here](https://htcondor.readthedocs.io/en/latest/man-pages/condor_release.html). the dagmans will run the postjob, that requires rucio for the stageout.

    ```bash
    # condor_release -con 'jobuniverse==7&&jobstatus==5&&crab_userhn=="dmapelli"&&HoldReason=="DMORACLETEST (by user condor)"&&HoldReasonSubCode==20231211'
    wassh -c vocmsglidein/crabschedd condor_release -con 'jobuniverse==7&&jobstatus==5&&HoldReason=="ORACLEOFF (by user condor)"&&HoldReasonSubCode==20231212'
    ```

- restart prod and preprod TW, Publisher.

We used this procedure once, on December 2023, see this cms-talk 
[post](https://cms-talk.web.cern.ch/t/crab-downtime-2023-12-12/32592).

Instead of using `wassh`, which may cause problems with quotes expansions, you can use the ansible playbooks provided
at "dmwm/CRAB/script/Utils/crab-shutdown".

### CRAB shutdown - drain schedds

We performed such operations at least once in our past, on September 2021. 
At the time we wrote some instructions and kept them in the github issue 
[6781](https://github.com/dmwm/CRABServer/issues/6781).

 date | time (CERN time) | who|  action | effect on users
 --- | --- | --- | --- | ---
 Monday Sep 27 | evening | CRAB Ops | change TW config to reject SUBMIT and RESUBMIT [1]|  crab submit will fail, crab resubmit will be ignored, all other crab commands will work.  Running tasks and jobs will not be affected.
Tuesday Sep 28 | afternoon | CRAB Ops | list all running dagman's [2] and condor_hold them[3]. Exact time depending  on load, the fewer damans are active, the longer we can let them run | no more postJobs, no failed jobs resubmissions, no output transfers. Running jobs will be allowed to terminate gracefully and be logged by condor.
Tuesday Sep 28 | evening/late-night | CRAB/SI Ops | as root issue `condor_off` on all CRAB schedds | all running jobs will be terminated. No more access to CRAB DB from schedds
Wednesday Sep 29 | 7:30 am | CRAB Ops | stop pro and preprod  RESTs[4], TWs[5], Publishers[5] | no crab command will work
Wednesday Sep 29 | 8:00 am | CERN DB support | stop CMSR Oracle | no effect
Wednesday Sep 29 | afternoon| CERN DB support | declare update over and DB functional | no effect
Wednesday Sep 29 | afternoon | CRAB Ops | restart central services (RESTs[6], TWs[7], PBs[7]) | crab status works again, still no (re)submission
Wednesday Sep 29 | afternoon | CRAB Ops | condor_on and release held DAGMANs[8] one schedd at a time | failed jobs will be restarted, ongoing tasks will resume resubmission-on-error, output transfers, publication
Wedesday Sep 29 | evening | CRAB Ops | enable again (RE)SUBMIT to TW [1] | back to normal

ACTIONS

* [1] edit [TaskWorkerConfig.py.erb](https://gitlab.cern.ch/ai/it-puppet-hostgroup-vocmsglidein/-/blob/master/code/templates/crab/crabtaskworker/TaskWorkerConfig.py.erb#L112)
* [2]  as root on each schedd [*]:
    * `condor_q -con 'jobuniverse==7&&jobstatus==2' -af clusterid > Running_DAGs`
    *  and save the list in some other place than /root on the schedd
* [3]  as per [DagmanResubmitter.py](https://github.com/dmwm/CRABServer/blob/11daa05d7d0866a1dbafbc67960cb70327501e76/src/python/TaskWorker/Actions/DagmanResubmitter.py#L121-L136), run as root
    ```
    for dag in `cat Running_DAGs`; do
    condor_qedit $dag  HoldKillSig \"SIGKILL\"
    condor_hold $dag
    done
    ```
    * check that there is no more running job in universe 7 and in case qedit, hold and add to list
* [4] `kubectl -n crab scale deployment crabserver --replicas=0`  (for cmsweb and cmsweb-testbed)
* [5] `docker stop TaskWoker; docker stop Publisher_schedd` on crab prod-tw0*
* [6] for cmsweb and cmsweb-testbed ` kubectl -n crab scale deployment crabserver --replicas=5`
* [7] on crab prod-tw01/2: `docker start TaskWorker; docker start Publisher_schedd`
* [8] as root on each schedd
    * condor_on
    * wait for condor daemons to be running (e.g. condor_q works)  then
    ```
    for dag in `cat Running_DAGs`; do
    condor_qedit $dag  HoldKillSig \"SIGUSR1\"
    condor_release $dag
    done
    ```
    * wait until number of jobs in the schedd (e.g. from condor_q) is stable and go to next schedd


[*] list of schedds: `condor_status -sched -con 'CMSGWMS_Type=="crabschedd"' -pool cmsgwms-collector-global.cern.ch -af machine` and `condor_status -sched -con 'CMSGWMS_Type=="crabschedd"' -pool cmsgwms-collector-itb.cern.ch -af machine`. Can skip vocms059, so that we can keep working using test instance.
