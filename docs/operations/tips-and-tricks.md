# Tips and Tricks

How to tackle possible operational issues.<br>
This is a collection of FAQ-style one-liners. Using links to longer docs where possible

## Miscellaneous
 1. High number of DNS queries: [how to check and fix](https://cms-talk.web.cern.ch/t/redeploy-tw-and-restart-nscd-in-prod/38855/1)

## Kubernetes
 1. look at secrets: `kubectl -n crab get secret crabserver-secrets  -o jsonpath='{.data.CRABServerAuth\.py}' | base64 -d`
 2. restart pods (gently) : `kubectl -n crab rollout restart deployment/crabserver`


## Puppet
 1. tell if agent is running: as `root`

 ```cat $(puppet config print  agent_disabled_lockfile); echo ""```
 
 2. disable agent: as `root`

 ```puppet agent --disable "Stefano says: keep stable"```

