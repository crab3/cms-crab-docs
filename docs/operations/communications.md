# Communication

## Internal to CRAB Team

mainly via CRAB DevOps Mattermost channel. A private channel to be used as chat room

## With other CMS Computing Operators/Experts

We mostly use Mattermost.
But other people can reach us via CMS Talk [Analysis Infrastructure and Support (AIS)/CRAB Development](https://cms-talk.web.cern.ch/c/offcomp/ais/crabdev/209)
or via some of the existing e-groups which we use for controlling e.g. puppet access and are thus exposed to others :

 * cms-service-crab-operators@cern.ch
 * cms-service-crab3htcondor@cern.ch

## With Users

### from users to us
We expect users to report problems, feedback, questions via [CMS Talk Computing Tools](https://cms-talk.web.cern.ch/c/offcomp/comptools/87) channel. The e-mail to use is described in [CRAB documentation](https://twiki.cern.ch/twiki/bin/view/CMSPublic/SWGuideCrab#Getting_support) and often also printed by `crab status` command.

If you receive a request for help via private chat or mail, ignore it, or tell the sender to use the proper channel.

### from us to users

We expect users to read [CRAB SWGuide](https://twiki.cern.ch/twiki/bin/view/CMSPublic/SWGuideCrab) and look up [CRAB FAQs](https://twiki.cern.ch/twiki/bin/view/CMSPublic/CRAB3FAQ) on the Twiki.
A new FAQ is e.g. a good way to make new information on specific topics available to users.

We usually do not advertise new releases.

There is good evidence that new features get communicated around in the user community by internal
information spread among users (which often belong to wide communities with their own information channels).
There are also many people who subscribe to the CMS Talks Computing Tools channel on CMS Talk and
take notice when we tell someone "there's a new feature to solve your problem".
Many users are also good at searching CMS Talk message archive.

So the need to communicate information to users is limited to special cases where new functionality is added, or a major disruption is in progress.
For the latter, a posting to CMS Talk Computing Tools is usually sufficient. People with a problem will look there, and no need to bug who is not using CRAB.

For communicating important new features when we want to reach also those who are not actively using CRAB now,  we have these possibility in order of "impact"

1. a message in CMS Talk

2. a quick presentation/news at weekly O&C meeting

3. a presentation at a CMS O&C Week (twice a year)

4. a small presentation/announcement at CMS Weekly General Meeting

5. usually O&C gets to present status at every CMS Week (twice a year) even if not in a dedicated talk, a "one line/one slide" could be inserted

