# CRAB User Troubleshooting

User report/question forum: [cms-talk computing-tools](https://cms-talk.web.cern.ch/c/offcomp/comptools/87)

This is a collection of what Stefano tell us, operators, how to troubleshoot user question/report in cms-talk.

*Note: the information comes from chat with Stefano in MatterMost, and I (Wa) have edited some text to add some context to make it as documentation*


## Job queuing in grid

**Q:** I am bit curious about queuing job relate to these cms-talk [[1]](https://cms-talk.web.cern.ch/t/crab-jobs-idle-from-5-6-days/11073) [[2]](https://cms-talk.web.cern.ch/t/crab-jobs-are-in-idle-state-in-most-run/10903)

- how do you know number of job for specific user are currently running?
- how do you know if it because fair share? do we have magic number indicating that the user gets capped by fair share?
- How do you know specific job are waiting in queue on specific site like you answer in [2].
- From [1], I only see that this user job taking long time to process (4-8hours). Is this normal for MC job?
- Some time site go down or have problem, where do get those info about site status?

**A:** Well.. in the end, it takes experience and there's no substitute for that. I can also be wrong here, but, let's go into details, as it is the only possible thing, bare with me a bit while I explain.

[2] was very easy, the answer was in crab status output that the user reported:

```
Jobs status: finished 68.5% (669/977)
idle 26.0% (254/977)
running 1.1% ( 11/977)
transferring 4.4% ( 43/977)
```

which simply says "some jobs are running, some completed, some are pending" i.e. business as usual in a batch system. Therefore "if they are not all done yet, must mean that there are not enough resources at the target site(s), since there must be more than 11 CPU's there, it must mean that other CPU's are used by other jobs/users, i.e. fair share"., Yes this requires some trust/confidence in HTCondor, which we need to have. If jobs do not run, either there is a bad requirement, or it is fair share. Since some jobs run, requirements are OK, so it must be fair share, aka "CPU's are busy"

[1] was trickier, since in this case all jobs in the task are idle it could be a bad requirement (too much memory ? too many cores ?) or a down site like TIFR (`T2_IN_TIFR` has some issues with pilot [Ticket](https://ggus.eu/index.php?mode=ticket_info&ticket_id=156728)). So I looked, and input data is on disk at 3 large sites including CERN, so it can't be the data location.
To do this I use the pointer to DAS for input dataset in the crabserver UI (from the HELP URL)

![Image Pasted at 2022-5-31 21-19.png](../images/crab-user-troubleshooting/Image Pasted at 2022-5-31 21-19.png)

which leads to:

![Image Pasted at 2022-5-31 21-20.png](../images/crab-user-troubleshooting/Image Pasted at 2022-5-31 21-20.png)

Than I looked at requirements in SPOOL_DIR/Job.1.submit (simply grep for DESIRED), nothing strange. (*Q: SPOOL\_DIR/job.1.submit? what is that? where is that file*)

So I thought, maybe it is simply fair share, i.e. this user has lots of jobs running, or which recently ran, and little patience. So I look at overview of CRAB running tasks in grafana

i.e. this (look at red arrow)

![Image Pasted at 2022-5-31 21-28.png](../images/crab-user-troubleshooting/Image Pasted at 2022-5-31 21-28.png)

here I selected last 7 days, then user `dekumar` and here we are:

![Image Pasted at 2022-5-31 21-30.png](../images/crab-user-troubleshooting/Image Pasted at 2022-5-31 21-30.png)

3.8K running at this moment. 6k completed, 11k Pending. LIke the user in [2] those are signs of a healthy system where simply there is a limit to how many CPU's you can grab at same time

(yeah, we do not have a nice way to look at running-jobs-by-user)

there is this https://monit-grafana.cern.ch/goto/UDRzxA97k?orgId=11 but you cant' sort by number of jobs (it only sorts by lexycographic order, not numeric, as the guy who was there before Ceyhun and set this up explained to me).

About site status, here:
https://cmssst.web.cern.ch/sitereadiness/sum_report.html

Also I use this to tell if a site is running pilots or not and how many
http://cms-htcondor-monitor.t2.ucsd.edu/letts/production.html
A very nice page IMHO, I have a permanent tab with it in my Firefox.

>  this user job taking long time to process (4-8hours). Is this normal for MC job?

I wish it was the norm, often users send jobs which are way too  short (<20min). We'd like every job to last a few hours, and rather run fewer, longet lasting jobs, then zilions of short ones.  Anyhow, yes, 4-8 hours is nothing to worry about and actually it is good.
