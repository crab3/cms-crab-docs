# Facilities meeting weekly report

## Why

We, as one of services developed and maintained by CMS O&C, are obliged to report weekly status of CRAB to "Facility Services" L2 area.

Note that this meeting only interest the status of services, i.e., why CRAB is down last week. You also can ask the question relate to the sites.

## Where
Facilities & Services, Site Support and Central Services in indico [Infrastructures and Resources](https://indico.cern.ch/category/7689/) category.
<br />11 July 2022: <https://indico.cern.ch/event/1180424/>

- Twiki report: <https://twiki.cern.ch/twiki/bin/view/CMS/FacilitiesServicesMeeting>
- For CRAB report part: <https://twiki.cern.ch/twiki/bin/viewauth/CMS/FacilitiesServicesMeetingCRAB>

## When

**Every Monday at 16:00**

*Beware the Twiki page take a minute or two to upload, open, and edit page.*


## How

### Best case scenario

Best case scenario mean everything run smoothly even something break internally but not directly affect users (like Publisher cannot publish some of users output).

Do the following step

- Download these two images (rename the images with the date of the meeting):
    - run-202204025.png: <https://monit-grafana.cern.ch/render/d-solo/CsnjLe6Mk/crab-overview?from=now-7d&to=now&height=300&orgId=11&panelId=74&refresh=1h&timeout=3600&tz=Europe%2FRome&width=600>
    - You can change `from` argument to `from=now-14d` to cover two weeks instead of one.
- Browse "FacilitiesServicesMeetingCRAB" (see the link above).
- Click 'Attach' and upload the image.
- Back to "FacilitiesServicesMeetingCRAB", click "Edit" botton (top right of the page)
    - Change the image URL (changing the date that correspond to the day). Use `%ATTACHURLPATH%` to refer to url of attach image uploaded in this page. In example:
    ```
    <img alt="run.png" src="%ATTACHURLPATH%/run-20230327.png" />
    ```
    - Update the detail, e.g., `Everything running smoothly. Nothing to report.`.
    - Save it.
