# DAY1: CRAB3 Team New Member Setup

Welcome to CERN and Welcome to CRAB team!

This document is a checklist of things a new CRAB3 operator/developer should do in order to get started with CRAB3.

Before reading this page, you should already have a CERN computing account in order to access all CMS and CERN services. If you don't have a CERN computing account, please go to the Users Office/[CMS secretariat](https://twiki.cern.ch/twiki/bin/view/CMSPublic/DrupalContactCMS) and finish your paperwork to get your account.

## Join `CRAB DevOps` room in CMS O&C MatterMost channel!

[MatterMost](https://mattermost.web.cern.ch) is open source Slack clone, our main communication channel between our team member.

Please join [CRAB DevOps](https://mattermost.web.cern.ch/cms-o-and-c/channels/crab-devops) to chat with everyone in our team. It is a private room, only us, 2 AIS L2 and 2 CRAB CatA (and some people who we are working with for EPR).

You need to ask a CRAB member to let you in. If you have not joined the `CMS O&C` team yet, please also ask for the invitation link. Additionally, join the Cat-As channel in the `CMS Experiment` team.

**We are open and welcome for any discussion!**

### A bit about MatterMost

You need to, at least, login to [CERN MatterMost](https://mattermost.web.cern.ch), either with web browser or application. It will redirect you to CERN's Gitlab login, which redirects you to CERN's SSO if it is your first time logging in.

Your working contract needs to be activated (PJAS/USER/FELLOW/etc.) before you can login into MatterMost.

## CERN Terminal Server: LXPLUS

[LXPLUS](https://lxplusdoc.web.cern.ch) is the Linux machine provided by CERN IT where **everybody** can remote login to it and use the CERN resources via `ssh` remote client. [CRABClient](https://github.com/dmwm/CRABClient/), the client-side software from our group is available on LXPLUS (via CMS's CVMFS). [CRABServer](https://github.com/dmwm/CRABServer/) is the server-side software which you can git clone as well for easy access. 

Please try `ssh <cernusername>@lxplus.cern.ch` command and provide CERN Computing password to login.

Please consult full documentation on how to access ssh without typing password everytime from [SSH and Kerberos](../../development/environment/ssh-and-kerberos.md).

## Have a valid CERN computer account in `zh` group (CMS)

Make sure your account is in `zh` group (GID 1399). You can check it in account management page <https://account.cern.ch/account/CERNAccount/AccountStatus.aspx>. It should look like this:

![cern-account-zh2.png](../../images/day1/cern-account-zh2.png)

If not, contact iCMS administrator (TBD contact email/channel TBD). P.S. Andreas Pfeiffer is the current iCMS administrator.

## Grid certificate

You need a grid certificate to run all CRAB3 commands, browse [CMSWEB](https://cmsweb.cern.ch) for DAS, CRABserver UI, and other CMSWEB services on web browser. You also need to install the Certificate Authorities on your browser to trust certfificate from those sites.

Search google or ask a member of the CRAB team if you do not know what certificate authentication is (and feel free to ask any other questions!).

### Obtain your grid certificate

To obtain your grid certificate, visit ["Request a new Grid User certificate"](https://ca.cern.ch/ca/user/Request.aspx?template=EE2User) and follow instructions there. You will get your `myCertificate.p12` file when done.

**NOTE: Store your password somewhere safe. You will need to decrypting private key when you import into your browser and setup CRABClient.**

### CERN Certificate Authority

Because CERN Grid Certificate is signed by the CERN Grid Certificate Authority, which is not trusted by OS/Firefox by default.

To download, please visit ["Trust the CERN Certification Authority"](https://cafiles.cern.ch/cafiles/certificates/Cern.aspx) and follow the instructions below.

### Install CA and your certificate in Firefox

We recommend to use Firefox for development because it has its own certificate storage, to avoid messing up the CA installed by OS.

Note: Screenshots are from Firefox version 101.0 on Fedora 36.

1. After you downloads all CA file from website, go to "Settings" (`about:preferences`) and search with keyword `certificate`, Click "View Certificates.."
    ![ca_install_firefox1.png](../../images/day1/ca-install-firefox1.png)

2. In "Authorities" tab, Click "Import...", select CA file (one at a time).
    ![ca_install_firefox2.png](../../images/day1/ca-install-firefox2.png)

    After selection, Firefox will ask if you trust this CA, tick first check box `Trust this CA to identify websites` (the second box is optional).

    ![ca_install_firefox3.png](../../images/day1/ca-install-firefox3.png)

3. Open <https://cmsweb.cern.ch/>. Firefox should not report untrusted website anymore. If not, restart firefox and try again.


4. Go to "Your Certifcates" tab and click "Import...". Select your `myCertificate.p12` file, then Firefox will ask your certifcate password. Type your password and click "OK" to import it.
   ![ca_install_firefox4.png](../../images/day1/ca-install-firefox4.png)

5. Try to open <https://cmsweb.cern.ch/auth/trouble/>, Firefox will ask you to select your certificate to send it to server. If you import correctly, Firefox will pop up select certificate dialog box. You will see certificate with your name in it. Click OK and you will see the CMSWEB troubleshooting page. Ignore output from the website for now.
    ![ca_install_firefox5.png](../../images/day1/ca-install-firefox5.png)

### Register your grid certificate to VOMS and Sign AUP

Please consult [How to register and sign AUP in the CMS VO](https://twiki.cern.ch/twiki/bin/view/CMSPublic/SWGuideLcgAccess#How_to_register_in_the_CMS_VO).

Basically, you do not need to register your cert to VOMS server anymore. But you still need to sign AUP on CMS IAM website.

To verify, login into LXPLUS machine and run the following command.

```bash
voms-proxy-init --rfc --voms cms -valid 192:00
```

Successfully run command will have logs look like this:

??? quote
    ```
    [tseethon@lxplus965 ~]$ voms-proxy-init --rfc --voms cms -valid 192:00
    Contacting voms-cms-auth.app.cern.ch:443 [/DC=ch/DC=cern/OU=computers/CN=cms-auth.web.cern.ch] "cms"...
    Remote VOMS server contacted succesfully.


    Created proxy in /tmp/x509up_u145867.

    Your proxy is valid until Tue Jul 23 18:36:38 CEST 2024
    [tseethon@lxplus965 ~]$
    ```

## Storage space for CRAB output

You will need to run CRAB tasks for testing. CRAB tasks produce outputs which needs to store on some grid Tier 2 sites. As a CRAB developer, you are eligible to request storage space at `T2_CH_CERN`.

There are 2 kinds of space:

### CERN CMS EOS (FTS ASO)

The CMS EOS at CERN is managed by CMS VOC. We need it for storing CRAB outputs.

Open a new ticket in [CMSVOC Jira](https://its.cern.ch/jira/projects/CMSVOC)
with Title:
`Storage space at T2_CH_CERN for CRAB operator` and this message (replace the dots with your CERN username !):
```text
Hi, I am the new CRAB Operatore and I need 100GB of user quota on /eos/cms/store/user/ in order to test CRAB submission. My CERN username is .....
```

 

1. Open <https://its.cern.ch/jira/projects/CMSVOC>, click "Create" button.

    ![jira_cmsvoc1.png](../../images/day1/jira-cmsvoc1.png)

2. Put title in "Summary" and message in "Description". Make sure `Project` is `CMS VOC (CMSVOC)` and `Issue Type` is `Task`. No need to fill other fields.

    ![jira_cmsvoc2.png](../../images/day1/jira-cmsvoc2.png)


### Rucio

Another space is Rucio managed space. Rucio, in short, is a software to manage data placement with the sites around the world.

Open ticket with [CMS Data Transfer Request](https://its.cern.ch/jira/projects/CMSTRANSF) to request Rucio quota. You can use [CMSTRANSF-894](https://its.cern.ch/jira/browse/CMSTRANSF-894) as a template to request quota.

And later you need to join the `cms-service-crab-test` e-group (see e-group section below) to use `crab_test_group` Rucio account.

## Get your CRAB Operator role in CRIC

CRIC is the resource inventory for all services in O&C which is provided through a nice JSON API. CRIC also stores user roles which is used in CRAB.

Ask CRIC administrator, Stephan Lammel or Stefano Belforte to give you CRAB Operator role there. To be specific, your CERN username should be a member of `CMS_CRAB3_Operator`. You can see your account info in CRIC using search tools in <https://cms-cric.cern.ch/accounts/account/list/>.

![cric_user_info.png](../../images/day1/cric-user-info.png)

### Check your permission after getting approved

By this point, you should have enough permissions to submit a CRAB task and use most CMS O&C resources. Please visit <https://cmsweb.cern.ch/auth/trouble>. Your page should look like this:

![cmsweb_auth_trouble.png](../../images/day1/cmsweb-auth-trouble.png)

## 2FA (OTP and Yubikey)

CERN SSO and most VMs provided by CERN (include crab) require to have 2-Factor Authentication.

There are 2 options: OTP and Yubikey. You need to do both for backup.

- yubikey: Please ask a CatA colleauge who does not have yubikey yet before [open ticket](https://auth.docs.cern.ch/user-documentation/two-factor-authentication/#getting-a-yubikey) to order key in batch using O&C budget.  Ask L1 (currently [Phat Srimanobhas](mailto:norraphat.srimanobhas@cern.ch)) for the budget code by email. Please summarize how much it costs per person and in total when sending email to L1 (cost 50 CHF/key as describe in docs).
- OTP: recommended application evolve with time. Check what is currently adviced at [this page](https://cern.service-now.com/service-portal?id=kb_article&n=KB0006587). You can change to a different app later, much much easier if you also have yubikey

Please follow instructions from [official Auhorization Service docs](https://auth.docs.cern.ch/user-documentation/two-factor-authentication/#setting-up-a-2nd-factor-authentication-method).

## E-Group

E-groups serve a dual role in CERN computing infrastructure, they are primarily mailing lists, but many tools utilize e-groups to assign specific right to people, avoiding the need to individually list all users. You will find many e-groups at CERN. For example, you need to join `cms-service-crab-sysadmins` to get ssh access to CRAB virtual machine. You can join these e-groups by yourself, but if you need any help, you can ask anyone around since e-groups are quite common.

Go to [E-Group Main page](https://e-groups.cern.ch/e-groups/EgroupsSearchForm.do), search the name, and click "Subscribe" botton. Most e-groups need approval from admins/owner. Please send an email to them to get approval.

![e-group.png](../../images/day1/e-group.png)

List of all e-groups and descriptions is in [CRAB Inventory](../../inventory/crab-inventory.md#e-group). You need to subscribe to them all, but you can keep reading this page to see what each e-group is used for.

For summer students, please ask your supervisor before joining any e-group.


## Repositories

GitHub:

* <https://github.com/dmwm/CRABServer> Main CRAB code (contains everything you need).
* <https://github.com/dmwm/CRABClient> Client for user to interact with CRAB system.

Create your account and send it to Stefano to add you to DMWM group and grant developer access to `CRABServer` and `CRABClient` repositories.

CERN Gitlab:

* <https://gitlab.cern.ch/crab3> Our group in Gitlab.
* <https://gitlab.cern.ch/ai/it-puppet-hostgroup-vocmsglidein> puppet configuration for service deployment in VM (TaskWorker/Publisher/Schedd).

For `crab3` group, join `cms-service-crab-sysadmins` e-group. For puppet repo, join `cms-service-glideinwms-admins` and `ai-admins`.

## Openstack Project

We currently deploy some of our services inside VM machine. All of them are Puppet managed.

List of e-groups you need to join to get access: `cms-openstack-vocmscrab-admins` `cms-service-crab3htcondor` `cms-service-crab3htcondor-admins` `cms-service-crab-sysadmins`.

## Puppet terminal server (a.k.a. ai-admins)

[ai-admins](https://config.docs.cern.ch/details/access.html) is terminal server for managing Puppet managed virtual machine.

Make sure you setup 2-factor authentication and join `ai-admins`/`LxAdm-Authorized-Users` e-group.

Once you join all of Openstack and ai-admins e-groups, try to access to our VM with command `ssh -J <cernusername>@aiadm.cern.ch <cernusername>@crab-dev-tw04.cern.ch`. You will be asked 2FA code once.

## crab-secrets

Most of crab credentials is committed in [crab-secrets](https://gitlab.cern.ch/crab3/crab-secrets) in Gitlab. You need to provide your GPG key to us to re-ecrypt the files so you can decrypt it.
Please consult [crab-secrets](https://cmscrab.docs.cern.ch/technical/crab-secrets.html#add-new-key-to-crab-secrets) and ping one of the CRAB members to re-encrypt those key.

## CMSWEB Kubernetes Cluster

We deploy CRABServer (one of our services) inside CMSWEB kubernetes cluster. To have access to testbed and production cluster, open the [CMSKubernetes Jira](https://its.cern.ch/jira/projects/CMSKUBERNETES) ticket.

Follow [Testbed Clusters](https://cms-http-group.docs.cern.ch/k8s_cluster/cmsweb_testbed_cluster_doc/) and [Production Clusters](https://cms-http-group.docs.cern.ch/k8s_cluster/cmsweb_production_cluster_doc/) CMSWEB docs on how to access to testbed(preprod) and production Kubernetes cluster.

## Jenkins

We run CI/CD on [Jenkins from DMWM team](https://cmssdt.cern.ch/dmwm-jenkins/). You need to ask [Alan Malta Rodrigues](mailto:alan.malta@cern.ch) (send an email) to give you access to the CRAB pipeline.

## Docker registry

To deploy the TaskWorker and the Publisher manually, you need to push custom docker image to Docker Registry (Harbor) at `registry.cern.ch`.

In order to do so, you need to be a member of the [cmscrab](https://registry.cern.ch/harbor/projects/1891/members) project.  Ask Stefano to add you there.

You also need to access [cmsweb](https://registry.cern.ch/harbor/projects/1771/repositories/crabserver) project, request permission from CMSWEB team through [CMSKubernetes Jira](https://its.cern.ch/jira/projects/CMSKUBERNETES).

## Grafana CRAB

Our monitoring dashboards are in Grafana (for example: [CRAB Overview](https://monit-grafana.cern.ch/d/CsnjLe6Mk/crab-overview?orgId=11&from=now-3d&to=now&refresh=1h)). You need to ask Nikodemas Tuckus from CMS MONIT team through [CMSMONIT Jira](https://its.cern.ch/jira/projects/CMSMONIT)) to give you edit access.

## Opensearch

We use elasticsearch to store the metrics in and plot in Grafana. There are many instances and it is setup with different e-groups.
Make sure you have access to following instances:

- [Opensearch MONIT](https://monit-opensearch.cern.ch/), anybody that have CERN account.
- [MONIT Timber Private](https://monit-timberprivate.cern.ch/dashboards/), join `es-timber-cmsweb_kibana` e-group.
- [es-cms](https://es-cms.cern.ch/), (need to ask Nikodemas Tuckus, CMSMONIT CatA, which e-group you need to join).

Make sure you have access to all the instances above.

## Logging Machine

Logs from applications on the Kubernetes cluster are stored in S3 and sync back to `vocms0750` machine. To have access to the machine, open the [CMSKubernetes Jira](https://its.cern.ch/jira/projects/CMSKUBERNETES) ticket.

## Communication Channels

### MatterMost

We use instant messaging, Mattermost <https://mattermost.web.cern.ch/> (OSS Slack's clone) to chat with our colleages in CMS O&C.

Other channels we recommend to join (sorted alphanumerically): `Computing Operations`, `DMWM` `Facility Services`, `Monitoring & Analytics`, `Submission Infrastructur`, `Web Serv & Security`.

[IT-dep](https://mattermost.web.cern.ch/it-dep) team is a good source to get news on what happend to our infrastructure. Recommended channel: `IT Status Board`, `Cloud Containers`, `Puppet`, `DownForEveryoneOrJustMe`, `Linux`.

### Email

We often use emails for discussion with people outside our team. Please join `cms-service-crab-operators` and always cc `cms-service-crab-operators` everytime when the issue relates to CRAB.

### Zoom

CERN provides Zoom for conference meeting/video call. Try to access CRAB room (link is in MM `~DevOps` channel's description).

## Offices at CMS Center

To access the CMS center in the Buiding 354 where some of our colleagues work, you need to fill in the form on [adams](https://www.cern.ch/adams/check.php?acp_code=YSALC-01526) (same link as the QR code at the RFID scanner).

CMS Secretariat will automatically approve your request but you may need to wait 1 day before you can access it.
