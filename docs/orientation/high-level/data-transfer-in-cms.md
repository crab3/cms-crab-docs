!!! note
    This page is currently in draft.

# Notes on Data Transfer in CMS

## Disclaimer
these are live notes from a chat were Stefano was summarizing "all about CMS data transer"
in one hour for CRAB Operators training. They are in no way supposed to replace proper
documentation of tools named here and may contain errors, but overall should give
the gist of the system and help new people not to feel lost.

## Basics
Data transfer in our system (Rucio + FTS + Davs + gridftp + gfal + xrdcp )

Data are created in 2 ways: detector, or simulation. Here we describe the first flow, which is more complex, simulation is sort of "the same" but we start from a simulated output of Tier0 (called DIGI).

Data from detector are created at P5 and are processed “promptly” (currently means within 48h) by Tier0 (a large chuck of CERN CPU cluster at Meyrin)
 
- P5 : owned and managed by CMS
- Computers at “CERN” owned and managed by IT

Tier0 output goes to tape at T1’s. This is decided “centrally”,
operators assign data streams to various T1’s according to CMS policies.

Once data is on Tape, months later it is RE-RECONSTRUCTED. This is is the “R” in P&R.


For reconstruction data has to be put on disk first, on a dataset basis.
A “reconstruction Campaign” is planned by humans high above our pay grade.

Production needs to find one “place” on disk for each dataset.
This is done semi-automatically by WMA microagent(s).

## Rucio

To make those data movements we use Rucio. Rucio is a “rule based” data management system. A declarative system in a way.

You tell Rucio “make sure that all datasets whose name matches this RegExp have one replica on disk and keep it there until 1 year from now”, Many rules are actually removed before they expire. So WMA creates many such rules, which in practice looks like “put dataset A on disk at one of these 3 or 4 sites, all blocks at one site” or if dataset is large (like AOD’s) rule will be
“Put this dataset on disk at this 5 sites spreading blocks around as useful”.

Rucio  does 2 things:

  1. Make sure that there is disk space by deleting old files which are not locked. Other than this, there is no data deletion in Rucio.

  1. Make data move around as needed, until rule is satisfied.

Rucio never gives up trying. Its goal is to make data location on disk match the rules.
That's why we call this "Data MANAGEMENT".  Different from “DATA TRANSFER” which would
work based on requests like “replicate this set of files from here to there”.

## FTS

To MOVE data, Rucio relies on a DATA TRANSFER SYSTEM,
since data is moved in files, we have FileTransferService (FTS),
developed by CERN IT-SD-PDS (storage and data management / physics data services).

You tell FTS things like “copy this list of files from site A to site B trying up to N times (N default to 3)”.
FTS only knows about files, not blocks or datasets, which are CMS concepts.
FTS copies and/ore deletes files. If is fed list of files to move.

FTS does not know what else is there, does not know about blocks or datasets.
Once FTS gives up on retries it is over.
FTS tries to move a file 3 times, if those fails. No more retries.

Someone (Rucio or CRAB) needs to issue a new FTS submit_transfer commands.

FTS file transfers are like grid jobs. They are executed, retried, and in case called failed,
and failure flag is passed to the submitter who is supposed to take intelligent
decisions.

## SRM

FTS needs to issue actual file transfer command,
so needs protocols to talk with storage servers, things like http, ftp,  rcp….xrdcp…
But usually we (i.e. FTS) do not read or write files from/to server to “where code runs”,
my local disk.
No wget.
We use 3rd Party Transfer. Our storage servers support this, it is not trivial. The story is:

 - FTS connects to storage in site A (“connect” includes authentication!)
 - FTS connects to storage in site B
 - FTS tells A: send file suchandsuch to B
 - FTS tells B: receive file suchandsuch from A
 - FTS goes to do other things while A talks with B and do the work

Gridftp and xrtood are examples of storage servers which understand 3rd party transfer.
This served us well for 10+ years, wrapping gridftp, xrtoot, dcache and
other storage server with a common protocol called SRM.
In time people got fed up with SRM and up to last year most sites
were simply running gsiftp servers and doing 3rd party transfers
aka 3rd party copy or (TPC) happily.

## gfal

To move files with SRM, the community developed an ad-hoc client called gfal-copy. Gfal-copy also offers gfal-ls, gfal-rm and so on.

For you: `gfal-*` is a CLI (and also a set of libraries which you an use in C++ etc.) which understands how to talk with gsiftp, xrootd, dcache, storm, webdav and other storage servers. FTS is built around gfal.

## xrootd

In time every site in CMS at least was required to run an xrootd server.
Xrootd have many nice features. One is that since everybody has a storage server
which understands xrootd protocol, you can use xrdcp command to read a file from anywhere using xrootd protocol. This is the base for AAA federation, if you do not find one file where you run, you open like rootd://redirectorhostname/store/data/blahblah/.root and it will find it anywhere in the world where it is and stream it to you. You do need to specify the specific site where a file is located, the redirector will find the appropriate site for you.
 
Xrootd understands file data structure and will serve from a remote storage only the root branches which are used. Possibly saving very large factors in performance.

Redirectors in xrootd: redirectors hosts pass around queries for file open to all storage servers registered with each redirector, so that a client will be served the file from “whoever has it”. No rule in case multiple servers have it.

Moreover, xrootd is designed around network glitches. It automatically reconnects to a server when it comes back online and restart the transfer from where it left off, or if the server does not come back online, it will look for the file elsewhere with the redirector.

## DAVS, WebDAV

DAVS comes in because we want to use storage servers which just understands http.
We are fed up with gsiftp (`gsi*` stuff was developed 20 years ago by Globus collaborations who went private some years ago and it is not supported since years). So want to use industry standard storage servers: https. HTTP offers PUT and GET. But not COPY. You can not tell an http server to exchange a file with another http server (3rd party copy TPC). People came up with a new protocol called WebDAV which enhance HTTP protocol with TPC. “davs” AFAIK is the string to put in the file urls: davs://my.server.here//file.root. CMS just completed the task to have all sites running storage servers with WebDAV capability.


