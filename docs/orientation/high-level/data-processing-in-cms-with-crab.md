!!! note
    This page is currently in draft.

# Notes on Data Processing in CMS

## Disclaimer
these are live notes from a chat were Stefano was giving a high level introduction
to CMS computing for CRAB Operators training. They are in no way supposed to replace proper
documentation of tools named here and may contain errors, but overall should give
the gist of the system and help new people not to feel lost.

## Data and datasetes

CMS produces PB’s of data. Those are organized into “datasets”.
One dataset is a collection of files, from 100TB to 10GB .
Files are limited to 5 GB of size, most files tha we use are in the 2GB range,but there are small ones.

Data flow through various processing steps. Aka reconstruction and analysis.

CMS detector produces what is called RAW data.

RAW are snapshots of collisions inside CMS detector. A list of electronic hits in the detectors. Think of one RAW Event as a set of pixels from a camera.
RECO step means to connect dots to find path followed by charged particles (tracks) and clusters of energy depositions in external detectors (jets).

## Data Processing

Very computing intensive. Especially finding traks (aka pattern recognition). Finding track parameters (fitting) is not so terribile. 

RECO data is more bytes than RAW. And ~nobody uses it as a source for physics analysis.

There are data reduction steps.

2 ways:
Skimming: pick some events intead of all the events. Input rate (rate at whic RAW are produced is called trigger rate, and for CMS is 1KHz currently) this is the rate “to tape”.
Slimming: reduce the amount of data for each event: keep only the fitted track parameters, not the list of hits in each track. Keep only some of the (thousands of tracks) produced in each event. Recue the precision with which some quantities are stored. If there is something that we can only measure with 10% accuracy, do not need to store in a double precision 64-bit float.
IN this way from RECO we produce so called Analysis Oriented Datasets (AOD) and then MINIAOD and finally one data format (those are called data Tiers in CMS) [there are two kind of Tiers in CMS computing: sites in the world wide grid are divided in Tier0/1/2/3, CMS data formats are called Tiers RAW/RECO/SIM/GEN/AOD/MINIAOD/NANOAOD)
After RECO each data tier reduces information to save space.

Physicsts, especially students, Phd students and postDoc, need to write physics papers. Where they describe one physical process and usually check if a given theoretical idea is compatible with CMS data or not. So that we can confirm current understanding, exclude new hypothesis, or discover new phenomena (new Physics).
Physics papers just have plots. Histograms, points.
People usually use programs like Root (root.cern.ch) to process data organized as n-tuple and produces plots possibly with fits and statistic analysis.
Almost all physics analysis start with n-tuples:
Data are organazied by rows and columns, one row for each event, many many columns (#tracks, #of clusters, list of Pt fo each track etc. etc.)
Root is very good at processing this kind of format. E.g. Can tell that your code only needs track parameters and only reads from from the files, w/o the expense of moving all other data from disk to memory.
It is not uncommon for analysis work in this step to be I/O limited.

Organized processing (RAW → RECO / ADO/ MINIAOD/ NANOAOD) is done centrally by the P&R team using tools developed and supported by the DMWM CMS team, in particular WMCore/WMAgent.

## CRAB

Chaotic work by final users id done by single users or small groups (2 to 20 people) using CRAB and Root or similar things.

Think of what users do mostly as “use CRAB to read *AOD* and write out ntuples for Root). Every group uses slightly different ntuple format and of course different data selection criteria. Each aim to having at the end of the process a small enough set of ntuples to be able to put them on their desktop and produce plots with Root “interactively” i.e. iterate on the full data in almost real time as people “try different ways to interpret and classify the data”.

At least that’s the goal. It used to be common way in previous generation of experiments, but CMS has so much data that is always a challenge and people are coming up with different solutions all of the times. 
There is big effort ongoing in CMS to move everybody to use NANOAOD (25KBytes/event) vs. (2MBytes of RAW data).

LHC analysis still is about 2 big data reduction efforts:
  Understand and compress the information in each event. CMS is like a 40Megapixed camera taking one picture every 40nsec)
  Reduce the number of events to look at , really depends on the physics, some require full statistics and are computing intensive to carry through at each step.

Think of CRAB as “a giant ntuplizer”. We did an analysis of CRAB use in RUn2 (2016-18) and 75% of CRAB CPU time was used by CMS users to produce private ntuples.
Typically a user starts with a list of datasets (1 to 100) and tells CRAB : run this executable on all files in dataset D and bring back the output to the storage server at site T2_CH_CERN or T2_IT_Bari…. .

## CRAB Tasks


CRAB work is organized about the “TASK” concept.
1 dataset = 1 task   startying from input on DISK
(CMS keeps all NANO* and MINI* on disk, but larger data tiers are usually only on tape, and thus not accessible for processing until a tape recall is done which requires up to several days).
CRAB limits strongly the access to data which are not on disk.


User requests come to CRAB as “tasks”. One task means “process one dataset”. CRAB will try its bets to run the executable on all data, retry some errors, allow user to request more retries, and after 4 weeks from initial submission, everything is purged from the system.

WHen a task is submitted, the list of input data is FIXED and the application to be used is tarballed in a so-called sandbox and frozen as well.
Afer “crab submit” (a command typed e.g. at lxplus prompt) nothing can be changed.

Every task brings with it also some pieces of CRAB code used to stir the actual jobs which run on the grid to process the data. Those pieces of code are taken from the CRAB version current at the time the task was submitted and also tarballed and shipped to the sched machine. So this code is constant for the duration of the task  (1 month).

COROLLARY: CRAB central services, which runs as a unique instance, need to be compatible at the same time with old and new versions of that code.


## CRAB Client and quick example


Users interact with CRAB via “CRAB Client” = a thin python client application with a CLI which can be run from e.g. lxplus or wherever the user like (any SL7 machine will do) which is in
https://github.com/dmwm/CRABClient

All the code used in central services and in the job steering mentioned above is in https://github.com/dmwm/CRABServer

CMS Offline Workbook 
https://twiki.cern.ch/twiki/bin/view/CMSPublic/WorkBook

https://twiki.cern.ch/twiki/bin/view/CMSPublic/SWGuideCrab 



From lxplus:
 Need to setup CMS SW environment first
 and get a GRID credential (x509 certificate and VOMS proxy)

crab submit
crab status
crab resubmit
crab getoutput
crab getlog

Monitoring links  “will appear”...

belforte@lxplus706/TC3> crab status -d crab_20211222_131320/
CRAB project directory:        /afs/cern.ch/work/b/belforte/CRAB3/TC3/crab_20211222_131320
Task name:            211222_121331:belforte_crab_20211222_131320
Grid scheduler - Task Worker:    crab3@vocms0119.cern.ch - crab-dev-tw01
Status on the CRAB server:    SUBMITTED
Task URL to use for HELP:    https://cmsweb-test2.cern.ch/crabserver/ui/task/211222_121331%3Abelforte_crab_20211222_131320
Dashboard monitoring URL:    https://monit-grafana.cern.ch/d/cmsTMDetail/cms-task-monitoring-task-view?orgId=11&var-user=belforte&var-task=211222_121331%3Abelforte_crab_20211222_131320&from=1640171612000&to=now
Status on the scheduler:    COMPLETED

Jobs status:                    finished             100.0% (5/5)

Publication status of 1 dataset(s):    done                 100.0% (5/5)
(from CRAB internal bookkeeping in transferdb)

Output dataset:            /GenericTTbar/belforte-Stefano-Test-211206-bb695911428445ed11a1006c9940df69/USER
Output dataset DAS URL:        https://cmsweb.cern.ch/das/request?input=%2FGenericTTbar%2Fbelforte-Stefano-Test-211206-bb695911428445ed11a1006c9940df69%2FUSER&instance=prod%2Fphys03

