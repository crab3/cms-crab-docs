!!! note
    This page is currently in draft.
CRAB Overview
=============

![review: partial](https://img.shields.io/badge/review-partial-fcba03)

CRAB stands for CMS Remove Analysis Builder. It is a tool meant to enable CMS physicists to exploit remote (Grid) computing resources for data analysis in a transparent way.

-   CRAB = CMS Remote Analysis Builder
-   A tool to hide users from "Grid", born in ORCA times !
-   Largely successful (`crab submit, crab status, crab resubmit - done`).
-   Enable users to smoothly move from processing one file locally to processing multiple datasets
    -   1 dataset ⇒ 1 task ⇒ many jobs
-   But also: allow for privateMC and for replacing cmsRun with user script
-   Those "many jobs" run on the CMS distributed compyting system (the Grid) and their output is temporarely saved on the local storage of the site where they run
-   Later an asynchronous process moves all job outputs from the temporary area at the execution site to a single storage server selected by the user (Asynchronous Stage Out = ASO)

Almost all issues faced by users are coming from the unreliability of the Grid infrastructure, with data read errors dominating (exit code 8021, 8028). There is no software cure to "there is always a site with some problems", only money (increase redundancy of the infrastructure)

CRAB components and services it depends on
------------------------------------------

CRAB is a thin client running inside CMSSW plus a set of servers and services running on various infrastructures and cooperating. CRAB is a DataBase-centric application, status of user requests (tasks) is kept in Oracle DB and components which act on it try (but did not succeed) to be fully stateless.

Here's is a list or various software components:

-   Client (runs in CMSSW environment and has no external dependencies, it runs on both python2 or python3)
-   REST server (provides access to the Oracle DB, and a few other global functions). Runs inside CMSWEB K8s cluester
-   TaskWorker (maps each user's "submit this task" request into a set of jobs
    and submits to HTCondor a script to orchestrate execution of those on the Grid as a DAG.
    Runs as a docker container on a VM configured via puppet
-   DAGMAN(s) is an HTCondor component to schedule DAG execution, but in order to do the job it needs CRAB scripts to prepare the environemnt and to deal wth job retries, a concurrent process to parse and cache condor status, and processes to do ASO. All those scripts are put in a tarball by the TaskWorker and sent to the HTCondor scheduler and run there.
    - in practice TaskWorker submits one HTCondor job to the remote scheduler,
    this job executes one script (dagman bootstrap) in the scheduler universe
    which executes the DAGMAN and stars another job in the local universe
    which runs the process which oversight task execution (task process). The task process runs
    status caching and ASO scripts at regular intervals.
-   Publisher. Inserts list of output files for this task into DBS as a USER dataset. Runs as a docker container on a VM configured via puppet (uses same contrainer as TaskWorker, just runs a different script)
-   Job wrapper on grid WN's. A handful of bash and python scripts, put in a tarball by the TW at `task submit` time and shipped to each WN together with the few CRAB and WMCore internal they depend on.
-   Tools to monitor everything
-    HTCondor submission demon (schedd) runs on dedicated VM's (called =sched(s)
    or grid schedulers). There are multiple of them to share load.
    Configuration is managed via puppet.


Managing CRAB requires to know how all the external dependencies work and to follow and guide their evolution Below is a high level view of the CRAB components and of major external services it depends on. Monitoring infrastructure is not shown

 <br>
![crab-diagram.png](../../images/crab-diagram.png)
<br>

Technologies CRAB uses
----------------------

here's a visual list of most (but not all) technologies and tools used in CRAB
which CRAB DeveloperOperators (DevOps) need to be aware f and use for their work.
Level of detail knowledge needed on each, of course, varies.

![crab-technologies.png](../../images/crab-technologies.png)

CRAB Instances
--------------

We can not run a single instance of CRAB services, we need multiple ones where
to debug and test changes before moving to production.
While some of the externals we depend on (e.g. myroxy.cern.ch) exist
in a single instance only, others have multiple instances as well
(e.g. Rucio or DBS) but all CRAB instances use the same Rucio and the same DBS.
So the diagram above is not simply "replicated", even if the various
CRAB components (in red) are. A somehow old in the details, but still good,
introduction to what a CRAB instance is and how it is defined in practice is in the GH wiki :
 <https://github.com/dmwm/CRABServer/wiki/INSTANCES:--versions,-REST-hosts,-clusters,-DataBases>

CRAB versions and tags
----------------------

While instances (above) refer to multiple physical instantiations of CRAB services with the main signature of having each a separated DataBase underneath, software comes with tags like `v3.yymmdd` (e.g. `v3.220105`) and we have three CRABClient versions always available to users (see CMSCrabClient) :

-   `crab-prod`. `crab` is an alias for this and that what users are expected to use. We control which tag is deployed as `crab-prod`
-   `crab-pre` is used to mean `crab-previous` and points to an older tag than `crab-prod`. is the fall-back in case we screw up the tag in production (rolling back may take weeks)
-   `crab-dev` is for our use in testing new crab client tags

Security and Authentication in CRAB
-----------------------------------

There are three topics:

1.  Security as: who has access to servers where services run and secrets are stored.
2.  Authentication of CRAB services among themselves and with external services, either internal to CMSWEB like DBS or external to CMS like CRIC, Rucio, FTS or HTCondor
3.  Authentication of users of the service: how do user get and use the credentials to talk to CRAB and how CRAB makes use of them when doing actions on user's behalf like copy ouput of grid jobs which runs 3 weeks after initial `crab submit` to user home storage

Up to 2021, all of those have been based on standard Grid security machinery (X509 certificates and proxies with VOMS extensions) implemented via the Globus Security Infrastructure (GSI for short). In 2022 we are introducing tokens (usually [scitokens](https://scitokens.org/) based on [Json Web Token](https://jwt.io/) or `jwt` for short).

As of February 2022 the authentication between TaskWorker and HTCondor sched\'s has moved to use [IDTOKENS](https://htcondor.readthedocs.io/en/latest/admin-manual/security.html#token-authentication) introduced in [HTCondor v9](https://htcondor.readthedocs.io/en/latest/admin-manual/security.html#highlights-of-new-features-in-version-9-0-0) (Important note, HTCondor latest stable release 9.4 does not support GSI anymore, our scheds currently run 9.1.2). Over the course of the year we may look into moving other service-service interaction to `Scitoken`. User authentication is expected to stay based on X509 longer, but X509 must go away for Run4.

Now some details about the 3 topics above

### Access to servers

-   Access (ssh) to VM (sched's, TW, PB): Controlled via puppet,
 only members of e-group <cms-service-crab-sysadmins@cern.ch>.
Users can not log on our VM\'s
-   Access inside CMSWEB K8s clusters: controlled via namespace and authorized user list by CMS http group

### Authentication between services:

-   mostly via GSI using service certificates
    -
```
[<root@crab-dev-tw01> ~]# openssl x509 -subject -dates -noout -in /data/certs/servicecert.pem
subject= /DC=ch/DC=cern/OU=computers/CN=tw/crab-dev-tw01.cern.ch
notBefore=Jul 13 01:56:02 2021 GMT
notAfter=Aug 17 01:56:02 2022 GMT
[<root@crab-dev-tw01> ~]#
```
-   renew every year, via puppet. But litches happen almost every year
- That's how TW/PB get access to CRABServer and DB.
  Containers get access to credentials via mounting host disk volume where they are
- CRABServer REST on K8s is more difficult to deal with in this respect, try to avoid.
 It makes just one call to CRIC now, want to remove it.
- CRABServer REST authenticates with Oracle DB via password, managed via K8s secrets
(eventually a file pushed from admin private directory to K8s and visible
to whoever can log inside the running container (CRAB Admins).
- CRAB scripts running on sched's use user's proxy to authenticate with CRABServer.
Looking forward to using scitokens instead.
- TW submiission to HTCondor schedulers uses condor IDTOKENS
 (persistent `jwt` based token stored on TW VM's disks, same as service certificates).
 See [TaskWorker installation guide](https://twiki.cern.ch/twiki/bin/view/CMSPublic/CRABDocker#Setup_HTCondor_IDTOKENS_for_remo)

### Authentication for users

The picture below shows credentials and user files flow. Credentials are needed both to move files and to issue commands, the latter are not shown. Refer to text below the picture for full details.

![crab-proxy-flow.png](../../images/crab-proxy-flow.png)

-   based on X509 certificates and VOMS proxies. Transition to tokens foreseen on a "few year" timescale
-   a game of public short lived vs. private long lived keys. Proxies used on remove grid nodes are protected, but kept short lived since ... things can happen.
-   user gets a public certificate and a private key encrypted with a secret passphrase. cert and key are stored on private disk.
-   certificate lasts 1 year, think of it as a passport, says who you are
-   users use passphrase and private key to obtain a short lived "proxy".
  7-day validity maximum. Proxy also has VOMS extention : add some Virtual Organization
   (i.e. CMS) specific info. Main purpose is to certify that you are a
    CMS member in good status. Think of it as a VISA stamp on your passport.
    Says what you can do.
-   users use VOMS proxy to authenticate with CMSWEB during `crab submit`
        when they write a record on CRAB DataBase via CRABServer REST interface.
-   users need to upload files (sandbox) to [s3.cern.sh](https://s3.cern.ch). This is done via getting
       a short lived PreSignedURL from CRABServer via X509 authentication.
      CRABServer authenticates with S3 to create the PreSignedURL
      using internally stored OpenStack tokens in the K8s secrets.
-   CMSWEB picks up the user DN in the authentication step and pass it over
    to CRABServer which stores it in the DataBase as part of the submitted task info.
-   The user proxy is NOT sent around
-   CRABClient also stores a medium-lived (1 month) credential on the MYPROXY SERVER
   `myproxy.cern.ch`. This requires access to user certificate and private key.
    This is why every couple of weeks crab client asks you to type the passphrase,
    so it can execute the proper `myproxy-init` command for you.
    We have to strike a balance between convinience (not inserting the certificate password
    every time we issue a crab client command) and always having a credential lasting 1m in myproxy.
    Renewing the credential every 2w is the result of this balance.
-   `myproxy.cern.ch` is a critical credential store, and very well guarded by CERN WLCG Operations (CERN IT). We have absolutely no access.
-   CRABServer happily ignores user proxy, it only passes around the DN
-   CRAB TaskWorker needs the user VOMS proxy so that it can be associated to each user job,
-   CRAB TaskWorker gets the DN from the DB, then retrieves a grid proxy (no VOMS extension) from `myproxy.cern.ch` (7-days validity) and adds VOMS extension to it via `voms-proxy-init` so it obtains a valid VOMS proxy for the user and stores it on local disk. CRAB TaskWorker proxy store is less secure then myproxy, mostly because we rely on ssh to limit access and do not continuously monitor for hacking. But credentials there have shorter life.
-   Authorization of who can retrieve a user proxy from myproxy.cern.ch is controlled in 2 ways:
    -   TW host machine needs to be in a list of authorized hosts and authenticate via its `hostcert.perm/hostkey.pem`. This list of course is much more than CMS services. There is a single myproxy.cern.ch service for all groups/experiments.
    -   CRABClien retrieves from CRABServer a list of host names where we run a TaskWorker and creates credentials in myproxy which can only be used by those hosts.
-   The TW uses HTCondor API's to copy the user proxy to the scheduler where the user task is submitted, so that local condor scheduler has access to it.
-   The lifetime of a task is one month, longer than the validity of the proxy stored on TW disk. So the TW runs a periodic ProxyRenewal action where it goes through all the active tasks and renews the corresponding proxies if needed (i.e. gets a new one from myproxy) them as needed. When a new proxy is created, it is also pushed to the proper condor scheduler.
-   In this way every CRAB task which is executing always has a valid VOMS proxy with the user identity in its directory. This proxy is used by both the HTCondor scheduler and the CRAB scripts. See section below

### Uses of the user proxy in the HTC sceduler and Grid

1.  jobs which run in the remote Grid WorkerNode's (WN's) need a valid proxy for two reasons:
    -   read CMS data which are restricted to CMS members (VOMS membership proof must be given).
    -   copy job output from WN local disk to site storage server or, if that fails, directly
  to remote user storage server. Both require proper (grid) authentication.
2.  HTCondor does not use the proxy for job submission to the grid, but will make sure
 that the remote running jobs has a valid proxy.
 HTCondor keeps the proxy in the WN uptodate with the file pushed (and periodically renewed) by
  the TaskWorker on the sched job directory.
3.  CRAB scripts which run on the scheduler use the same proxy to authenticate with CRABServer REST and update status on Oracle DB
4.  CRAB scripts which deal with output transfer use the local proxy
 the one pushed periodically by the TaskWorker) to delegate a credential to CERN's
 File Transfer Service (FTS) with every request to copy an output file
 from the execution site's `/store/temp/user/..` to the destination site `/store/user/...`
 Files are moved from execution sites's storage to user home sites's storage via
  Third Part Copy (TPC) i.e. via a command sent by FTS server to both storage servers
  to do the transfer between themselves w/o data going through FTS.
  The preferred protocol for the transfer is now WebDav while we still
   support gsiftp and srm which are going to be phased out during 2022.
