# The BIG questions

- What is CMS ?
- What Computing and Offline do ?
- Do you know where you are in CERN ?
- what is my role in the grand scheme of things ?
- Who are my colleagues ? what do they do ?
- How does my work contribute to science progress ?
- Unknown keyword?
    - L1, L2, CatA?
    - EPR ?
    - (more) ?

Those are (some) great questions that you are likely asking yourself already !
Do not be afraid to ask those and more, your L2's are there to give you help and guidance.

An illustrated version with some minimal answers is in [this document](https://indico.cern.ch/event/1404700/contributions/5905540/attachments/2843435/4971020/CRAB%20for%20DevOps.pdf) which was prepared as a quick introduction during the (30min) interviews with new perspective DevOps


But as a (great) start, ask your favorite L2 or colleague to go with you through this
excellent presentation: [O&C Newcomers Induction Course](https://indico.cern.ch/event/1256427/sessions/481923/attachments/2612309/4513702/230316%20-%20Induction%20Course.pdf)
(slides source is in [GoogleDoc](https://docs.google.com/presentation/d/13wrt6OsW35ZNchTtjNSY-UH920seqXSaGvWr76RPioI/edit#slide=id.g21726b35714_0_35))

And

- [Up-to-date L2 groups in O&C.](https://twiki.cern.ch/twiki/bin/view/CMS/OfflineAndComputingHome)
- [Spring 2024 Operator Jamboree](https://indico.cern.ch/event/1340661/#b-555262-devops-jamboree), a good source for knowing who are our colleagues.  Some slides also included what they do for their group.
