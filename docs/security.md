# Security

## Background
Why is important, what incidents may happen, who's who, etc. etc.

## Reaction to incidents

### Ban/remove one user
CMS security team may ask you to ban one specific user from running task and remove all
currenlty running jobs or that user. 
<br>
Usually they will have the user's certificate DN (abuse will be detected
on some grid site and tied to someone's grid proxy)

#### Action list:

1. (if needed) Find the mapping from the DN to the username [1]
2. Ban the username in CRAB TaskWorker(s) configuration [2].
TW will immediately fail any new submission for that username as soon as it
starts to process it.
3. Remove all running jobs and DAGs for that username [3].

Take note of the time you do 2. and 3. and report it to security team
respectively as the time access to CRAB is blocked (no more tasks accepted)
and the time all currently running tasks and jobs are removed.

#### Forensic

TaskWorker logs can be used to find how much the user had submitted, where etc.
(although this info is in OpenSearch as well) and in combination with info
in schedulers SPOOL_DIR
(e.g. via [CRABServer UI](https://cmsweb.cern.ch/crabserver/ui) and task name)
it is possible to find the exact configurations and code which  was submitted.
If access to the sandbox is needed, it can be found in the SPOOL_DIR

#### How to

[1] For CERN certificates the username is part of the DN. For other certificate you may use [CRIC](https://cms-cric.cern.ch/accounts/account/list/).

[2] Need to modify TaskWorkerConfig.py on all machines inserting the usermane to be banned in this line
```python
config.TaskWorker.bannedUsernames = ['mickeymouse', 'donaldduck']
```
For permanent change do this in [puppet](https://gitlab.cern.ch/ai/it-puppet-hostgroup-vocmsglidein/-/blob/master/code/templates/crabtaskworker/taskworker/TaskWorkerConfig.py.erb)
and force a puppet agent run. Then **restart all TW's to make them pick the new config**
e.g. (BEWARE: untested !!)
```bash
# log onto aiadm, then
wassh -c vocmsglidein/crabtaskworker -l root 'puppet agent -t'
wassh -c vocmsglidein/crabtaskworker  'docker restart TaskWorker --time 180'
```

If you need to do this in a hurry, modify the file locally in production TW by changing
the symbolic link `/data/srv/TaskManager/current/TaskWorkerConfig.py` to point to a different file:
Put that file outside the container, so this change is safe against docker restarts.
Never change directly the `TaskWorkerConfig.py` file since it gets overwritten by puppet !
```bash
belforte@lxplus771/~> ssh crab-prod-tw01
belforte@crab-prod-tw01/~> docker exec -ti TaskWorker bash
[crab3@crab-prod-tw01 TaskManager]$ cd current/
[crab3@crab-prod-tw01 current]$ cp TaskWorkerConfig.py /data/hostdisk/TaskWorker/cfg/TaskWorkerConfig-TEMP.py
[crab3@crab-prod-tw01 current]$ rm TaskWorkerConfig.py 
[crab3@crab-prod-tw01 current]$ ln -s /data/hostdisk/TaskWorker/cfg/TaskWorkerConfig-TEMP.py TaskWorkerConfig.py 
[crab3@crab-prod-tw01 current]$ vi TaskWorkerConfig.py 
[crab3@crab-prod-tw01 current]$ cd ..
[crab3@crab-prod-tw01 TaskManager]$ ./stop.sh 
[crab3@crab-prod-tw01 TaskManager]$ ./start.sh -c 
```

And then do the change in puppet anyhow to make sure all TW's are aligned.
Do not forget to restart all TW's.

Once puppet change has been pushed in, restore symbolic 
link for `TaskWorkerConfig.py` if it was changed.

In about one hour a user can submit about 8000 tasks. 
With TW default options (6 slaves, polling every 30s) it can ban up to 12 tasks/minute, 
720 tasks/hour. It means that even if a user is banned,
the TW can get strangled processing the tasks from a user only. 
On 2024-04 we tested changing the TW options (20 slaves, polling every 10s), 
to ban tasks from a user at a rate of 7200 tasks/hour and everything worked fine.

[2, bis]

In case of a real emergency, it is possible to change the the task status manually
with a sql query. You will need to change the username and set the time from when
to start failing all the tasks

```sql
SET SERVEROUTPUT ON
DECLARE
  TYPE t_tab IS TABLE OF tasks.tm_taskname%TYPE;
  l_tab t_tab;
BEGIN
  UPDATE tasks
  SET    tm_task_status = 'SUBMITFAILED',
         TM_TASK_FAILURE = 'CRAB operator marked this task as failed manually'
  WHERE  tm_username = 'pcutrina' AND
         tm_start_time > to_date('2023-03-27 16:00', 'YYYY-MM-DD hh24:mi')
  RETURNING tm_taskname BULK COLLECT INTO l_tab;

  IF l_tab.COUNT > 0 THEN
    FOR i IN l_tab.first .. l_tab.last LOOP
        DBMS_OUTPUT.put_line('UPDATE TASK=' || l_tab(i));
    END LOOP;
  END IF;

  COMMIT;
END;
/
```

??? example "example of the output"

    ```sql
    SQL> DECLARE
    2    TYPE t_tab IS TABLE OF tasks.tm_taskname%TYPE;
    3    l_tab t_tab;
    4  BEGIN
    5    UPDATE tasks
    6    SET    tm_task_status = 'SUBMITFAILED',
    7           TM_TASK_FAILURE = 'CRAB operator marked this task as failed manually'
    8    WHERE  tm_username = 'pcutrina'
    9    RETURNING tm_taskname BULK COLLECT INTO l_tab;
    10
    11    FOR i IN l_tab.first .. l_tab.last LOOP
        DBMS_OUTPUT.put_line('UPDATE TASK=' || l_tab(i));
    13    END LOOP;
    14
    15    COMMIT;
    16  END;
    17  /
    UPDATE TASK=221101_101145:pcutrina_crab_-T1_UK_RAL-1667297502
    UPDATE TASK=221101_101148:pcutrina_crab_-T2_CH_CERN-1667297506

    PL/SQL procedure successfully completed.
    ```

[3] `ssh` to aiadm and execute
```bash
wassh -c vocmsglidein/crabsched  'sudo condor_rm -con CRAB_UserHN==\"usenramehere\"'
```
if you can not/want not to use aiadm, log on each scheduler host and execute `sudo condor_rm` there, or use a script from e.g. lxplus

[3 bis]

A softer alternative would be to kill all the running tasks of the user: get the list of running tasks on all schedds for `dmapelli` with

```bash
condor_q -global -con 'jobuniverse==7&&jobstatus==2&&crab_userhn=="dmapelli"' -af CRAB_Workflow
```

save the output to a file `tasks.txt`, then

```bash
for t in $(cat tasks.txt); do 
  crab kill --task=$t --killwarning 'CRAB Operator dmapelli had to kill your task.' --proxy=$X509_USER_PROXY; 
done
```
