# **Splitting in CRAB**

## Introduction

We call splitting the process of turning a TASK request into a set of JOBs

One TASK means (depending on `jobType.pluginName` in crabConfig.py)

* Analysis: i.e there is an Input dataset : process the given dataset (blocks or files also supported as input)  
* privateMC: i.e. there is no input: produce the given number of events

## Splitting for privateMC
Kind of trivial: user indicates either
 * a. Total \#events and \#events per job  
 * b. Total \#jobs and Total \#event

CRAB simply creates N jobs as indicated.

## Splitting for Anaysis
When there is input data we offer various possibilities:  
We have a BIG **constrain: Can only stop at lumi boundary** (see below)  
So Inputs to each grid job is a list of files and/or of lumis.
In practice a list of (run,lumi) paris is what uniquely defines what to process. CRAB also passes the list of files where to find those.  

Different splitting algorithms can be used.
User selects the splitting algorithm via configuration parameter `Data.splitting`.
Choices are: by file, by lumi, by event-aware-lumi (a refinement of by event), automatic

* By File: user indicates how many files to process in each job  
  * Easy peasy. Splitting process is “trivial”. No lumi information is needed in the job.  
  * Problems  
    * jobs will take varying time to run  
    * A file make take too long to process (we have 2 days max)  

Because of above constrain on halting processing only at luminosity boundary,
we can not allow splitting data files by events, and introduced split-by-lumi  
* By Lumi: user indicates how many lumisections to process in each job.
But still each lumi can have a variable number of events and jobs may have (very ) different running times.  
* Event-aware-lumi: refinement of the previous one where user indicates a number
of events per job and CRAB creates jobs using split by lumi
but assigning a different number of lumis to each job in order
to approximate the required number of events.

	  
The problem with all of them is that the user has to tell “how much work to do in each job”. It is not trivial. You need to run your code on “representative” sample and do some math. Users stinlk at this.

### Automatic Splitting
Automatic splitting was introduced to make it automatic to pick the correct
number of numberOfUnitsPerJobs value.  Overall design is in [CrabServer wiki](https://github.com/dmwm/CRABServer/wiki/Automatic-Splitting:-User)
Automatic splitting does two things:

1. Runs 5 “probe” jobs on 5 different files to figure out time/event and decides unitsPerJob
for event-aware-lumi splitting targeting 8-hour jobs (user can set a longer time in crab config file)
2. As a refinement, automatic-splitting machinery also sets a time limit on cmsRun
to make sure jobs fit into a the target time and runs “tails jobs”
to process the lumis which were left out and to reprocess what had been assigned to failed jobs.
A finer splitting i.e. less lumis per jobs is used in tial jobs, since 
Experimental evidence is that shorter jobs have higher chance of completing successfully
(but we can not make all our jobs very short because it is bad for scaling).
Automatic splitting runs 2 such tail stages.
   * *Note:* the time limit on jobs is implemented via a feature of `cmsRun` which makes it stops
gracefully at the end of the lumi which was being processed when time expired. The list or processed
lumis is sent back via the FrameworkJobReport (FJR) and used by the automatic splitting
code to properly prepare the tail jobs.



### Job ID's for automatic splitting
Bfore automatic splitting, each task had N jobs, defined a-priori before task was submitted to scheduler,
named 1,2,3…N (jobId, CRAB\_Id…)  
With automatic splitting, the number of jobs is not defined until you have run the
probes and can still change because of the fails.
So developers came up with changing jobId from a number to a string !

The 5 probe jobs are numbered as `0-n`  i.e.: `0-1, 0-2,... 0-5`  
Those job output is used to derive the optimal number of events per job but it is
otherwise discarded and they are ignored in reporting, ASO and publication.
After probes, a new CRABServer/TaskManager/Actions/Splitter.py instance
is run and it creates the “processing” jobs with "standsard" naming `N`: 1, 2, 3,.... N  
But.. there can be tails.
Splitter cose is run in the scheduler at later times during the execution of the processing jobs  and create tail jobs: 1-1, 1-2,...1-M and possibly 2-1, 2-2, …,2-O  
Current implementation runs up to two tails.  
So we have 4 possible “stages” (each of them a DAGMAN !) during automatic splitting, with respec to
simply one (called `conventional`) when using non-automatic splitting. Namely

|    Stage         | JobId             | DAGMAN description file |
|--|-------------------|-------------------------|
| Probe        | 0-1, 0-2, ... 0-5 | RunJobs.dag             |  
| Processing   | 1, 2, ...         | RunJobs0.subdag         |       
| Tail 1        | 1-1, 1-2, ...     | RunJobs1.subdag         |
| Tail 2         | 2-1, 2-2, ...     | RunJobs2.subdag         |
| Conventional   | 1, 2, …           | RunJobs.dag             | 

The [DAG description file](https://htcondor.readthedocs.io/en/latest/automated-workflows/dagman-introduction.html#describing-workflows-with-dagman)
is prepared by `TaskWorker/Actions/DagmanCreator.py` and is to be found in the SPOOL_DIR in the
scheduler host.
Grepping for  `"stage "` string in GH will show which piece of code makes use of that.

Probe jobs are “special” output is discarded and there is no publication. Processing phase is not started if all probe fail. Some probe failures is accepted. Probe jobs run for 15 minutes using random files/lumis as input.

Processing jobs have a time limit at 8 hours (configurable by the user).

Tail 1 is created when processing is 80% completed. So to deal with failures w/o waiting for the longish tail of everything done. 

Tail 2 is created when processing and Tail 1 are completed, to deal with “tails”. Number of lumis per job is decreased in Tail 1 wrt processing and further in Tail 2\.

## Data acquisition and data hierarchy in CMS

### Events, Lumis, Files,  Runs,  Blocks, Datasets
#### Glossary:
* **EVENT** : data representing the snapshot in the CMS detector containing our best
attempt to retrieve information from one collision in the accelerator  
* **DAQ**: the set of tools which record the event data for non-real-time processing 
* **TRIGGER**: the act of initiating DAQ, and the system which makes the decision
* **LUMI** (luminosity section): a fixed (short time interval) in which a number of events
were collected (possibly 0). Identified by a unique number within a run.
Event number is unique within a lumi, but counter restarts in each new lumi.
* **RUN**: a continuous period of dector data taking. Identified by a unique number.

Events are uniquely identified by 3 integers: (run, lumi, event)<br>
Lumis are uniquely identified by 2 integers: (run, lumi)


#### data taking, lumis, pile up (i.e. why an EVENT is not a single pp interaction)
Protons travel in the accelerator in “bunches”, very dense packets of particles.
The relevant metric is called “luminosity”: think of it as density x collision frequency
(frequency: speed of bunches is always the same, `c` = speed of light,
so collision frequency gets increased by having more bunches with reduced space separation).
When bunches “collide” there can be multiple proton-proton
interactions happening at same time: this is called "in-time pile-up".
CMS detector figures out if at that time there was a `p-p` collision
worth studying. This process is called TRIGGER.

When trigger “fires” the data from all detector pieces are recorded.
This process is called DAQ (Data AcQuisition). Those data are called EVENT.

The event data will in general contain data from multiple p-p
interactions happened in same bunch crossing, the in-time pile-up.
Since bunch crossings happen at a short time separation compared to the time for electric charge
collection in the detector, there is also "out-of-time pile-up": charge deposited in the detector
by interactions in previous crossings which is recorded together with signal from the crossing which
fired the trigger.

Trigger and Data acquisition (DAQ) happen at P5 (the CMS detector location in the ring).
Then data is sent to Meyrin to CERN EOS. Output is is so called Streamer format, we do not
care for the details of that.

Data at Meyrin is processed by Tier0, output is RAW data (reformatted Streamer) and PRE-RECO data.

A run indicates a period of data taking in the detector (hours).
A run never extends over multiple accelerator fill-dump cycles,
could be shorter if some conditions change in the detector:
change in trigger, some subdetector had a problem, DAQ had to be restarted…  

Luminosity sections (lumis) were originally defined as
a time interval in which the accelerator luminosity is constant.
Luminosity changes in time because e.g. proton bunches gets fewer particles
as those are removed by collisions and/or bunch size grows due to imperfections
in magnetic confinement and simple electrostatic repulsion.
Physicists need to know what is the instantaneous luminosity when a collision is recorded,
in order to compare data with theory: theory predicts probability of a specific
process in a collision, we measure frequency of processes, luminosity connects the two.
Luminosity-section is defined as a time interval where luminosity
changes slowly enough that we can safely assume it to be constant inside it.
A lumi-section is “several seconds”. (namely 23.3 s = 2^18 orbits).
Duration of lumis is fixed, hardcoded in CMS DAQ and did not change from year 2000.
This is likely to change in HL-LHC (Run4) with Computing
being able to define an "atomic unit of processing" disconnected from the needs of DAQ
and Luminosity bookkeeping [ref.](https://indico.cern.ch/event/1309090/#69-atomic-unit-of-processing)

For us who deal with data processing
* a lumi contains N (>=0) events.
* Probability of a give interesting process during that time changes:
according to the event type selected and the average luminosity in that interval.
Especially when we select “rare” events, the number in each lumi can change a lot.
* Therefore the number of events in one lumi is different for each dataset
and changes from one lumi to another.  
* There can be lumis with no events (after we selected for rare events).
We still need to record them. We need to know which lumis were “looked at” when
computing the eventual number of events selected in an analysis,
so that we can transform number of events into probability
and compare with theory. Simply finding 10 Higgs bosons is not useful.
We need “10 Higgs in 10 picobarn^-1 (unit of luminosity)”, not "10 Higgs last year"   
* Each lumi has a “luminority” value which was measured and stored in a DataBase. 
* Corollary: we always process while lumis and to make like 
* easier only close files at luminosity section boundary.  

### Recap

* EVERY FILE HAS A FULL SET OF LUMIS. No lumis is split across files.
   * (Alas there are exceptions, but so fare not relevant for CRAB)

* a LUMI is always in a single file
* a RUN = multiple lumis, multiple files
* We always process whole lumis and always close files at a lumi boundary
* When CRAB publishes a data in DBS, it stores there the list of lumis processed in input.


Files, Blocks are only for data processing convenience:
simply to divide a multiple TB sample into manageable objects.

DATASET: is a Physics concepts, describes data which went through a certain selection path,
different triggers in the detector (or different MC simulation processes),
different offline selection, different alignment/calibration…


* DATASET has many file, runs, lumis, events  
* DATASET is broken into BLOCKS

BLOCK = set of files. Simply for organizational convenience.
So far we only track data location at sites by block.
Note: in DBS we have datasets/blocks which are mapped in Rucio to containers/datasets .
Rucio names are like because we needed to adapt to names used by ATLAS. 


* Every block as a (set of) location(s) in Rucio (one or multiple replicas)  
* Every block is either fully replicated or not replicated at all at a given site  
* Datsets can be scattered across multiple sites, some blocks here, some blocks there.
Which by the way we in CRAB do not like because makes things more complicated,

## DATA TIERS = the lifetime of one event

* RAW : data as they come fom the detector : list of “hits” i.e. “points (and/or times) where particles interacted with the detectors, possibly associated with amount of charge released”  
* PRERECO : same thing as RECO done in quasi-real time at Tier0 (~48h after data taking). Is not supposed to give “publication quality results”. Mostly to allow quick feedback on the data taking.  
* RECO : after CMSSW did its best to reconstruct tracks (trajectories of charged particles bent by the detector magnets) and cluster of deposited energy in the “calorimeters”. Uses our “best” understanding of detector alignment and calibration. Happens months after data taking.  
* AOD : Analysis Objects Data : reconstructed tracks and energy clusters are “transformed” into particles and jets, basically add higher level objects to the event.  
* MINIAOD: reduced version of AOD. Some root branches are dropped. E.g. hits not associated to tracks are not here. You can re-fit a track with better alignment but not re-run track finding. You can re-compute Jet energy with new calorimeter calibration, but not redo Jet clustering.  
* NANOAOD: super-squeezed information. Only high level objects are kept, no hits, and measurements are stored with reduced number of bits. Same events as MINI. Currently more than half of CMS Physics analysis can use simply NANO.