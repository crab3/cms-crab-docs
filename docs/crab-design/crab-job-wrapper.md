# CRAB Job Wrapper

## Introduction

When a job runs on a grid node, CRAB needs to provide the proper environment:

1. OperatingSystem
2. python environment for running the CRAB wrapper and utility scripts 
3. CMSSW environment for the user application
    * No matter what the user runs, we always run it in a CMSSW environment.
4. libraries and other files needed by the user applications, a.k.a. "user sandbox"


Point 1. is done by the GlideinWMS pilot which will start a singularity container in accordance
with value of `MY.REQUIRED_OS` classAd in the job. This ad is set in the jobs submission JDL (`Job.<N>.submit` files
in the SPOOL_DIR)<br>
Inside the singularity container the starting directory (`cwd`) for our job is always `/srv`

Points 2. to 4. are taken care of by the Job Wrapper: a script which is set as "executable" in the job JDL

The Job Wrapper has an "onion" structure, with a few scripts wrapping other scripts. All code is in [CRABServer/scripts](https://github.com/dmwm/CRABServer/tree/master/scripts)

More detailed documentation is inside each script. In here we outline the structure and the goal of each layer

## Job Submission

Jobs are submitted with these lines in the JDL (among other things)
```text
Executable = gWMS-CMSRunAnalysis.sh
Output = job_out.$(CRAB_Id)

CRAB_Archive = sandbox.tar.gz
CRAB_ISB = "https://cmsweb.cern.ch/S3/crabcache_prod"  # but this is actually not used !
CRAB_Id = <the crab job number in this task>

Arguments = "-a $(CRAB_Archive) --sourceURL=$(CRAB_ISB) --jobNumber=$(CRAB_Id) --cmsswVersion=$(CRAB_JobSW)
             --scramArch=$(CRAB_JobArch) '--inputFile=$(inputFiles)' '--runAndLumis=$(runAndLumiMask)'
             --lheInputFiles=$(lheInputFiles) --firstEvent=$(firstEvent) --firstLumi=$(firstLumi)
             --lastEvent=$(lastEvent) --firstRun=$(firstRun) --seeding=$(seeding)
             --scriptExe=$(scriptExe) --eventsPerLumi=$(eventsPerLumi) --maxRuntime=$(maxRuntime)
             '--scriptArgs=$(scriptArgs)' -o $(CRAB_AdditionalOutputFiles)"

transfer_input_files = CMSRunAnalysis.sh, cmscp.py, CMSRunAnalysis.tar.gz, sandbox.tar.gz,
                       run_and_lumis.tar.gz, input_files.tar.gz, submit_env.sh, cmscp.sh
```
Other variables in the `Arguments` list like `fistLumi` or `scriptExe` are set in the DAG description file `RunJobs.dag` prepared by `DagmanCreator.py`

Therefore job execution on the worker node starts with `gWMS-CMSRunAnalysis.sh`. 

Everything that gets printed to `stdout` when the job is running goes in `job_out.$(CRAB_Id)`
and is brought back to the scheduler's SPOOL_DIR by HTCondor.
The PostJob will then move this to the WEB_DIR and rename it as `job_out.<CRAB_Id>.<retry_#>.txt`

## Job Wrapper

![crab-job-wrapper.png](../images/crab-job-wrapper.png)

### **`gWMS-CMSRunAnalysis.sh`**
1. dumps host/OS/proxy info and HTC classAds
2. saves current "start" job environmnet in a file so that it can be restored later as needed (see `submit_env.sh`)
3. starts two scripts one after the other:
   1. `CMSRunAnalysis.sh` : another wrapping shell around user application
       * all input arguments to the condor job are passed to this script
   2. `cmscp.sh` : performs local stage out, i.e. copies output files to /store/user/temp on local RSE 
4. logs summary info and [chirps](https://htcondor.readthedocs.io/en/latest/man-pages/condor_chirp.html) job exit code

### **`CMSRunAnalysis.sh`**
1. sets up python environment from `/cvmfs/cms.cern.ch/COMP`
2. untars `CMSRunAnalysis.tar.gz` to extract `WMCore.zip` (all WMcore python files!) and a few other scripts used in the wrapper
2. adds `WMCore.zip` to `$PYTHONPATH` 
3. runs `CMSRunAnalysis.py` again passing to it the argument list for the job (see the `Arguments` above)

### **`CMSRunAnalysis.py`**
1. untars `sandbox.tar.gz` file in current directory (`/srv`) and also expand any tar file found in there.So that user will find all files which they may have added via `JobType.inputFiles` configuration parameter.
3. initializes WMCore and SCRAM
4. prepares the CMSSW_X_Y_Z directory, the equivalent of `cmsrel` command
5. expands `sandbox.tar.gz` again in there so to restore libraries other files from original $CMSSW_BASE (aka scram directory in user submission host), but does not expand tarfiles which may be in there
6. performs PSet tweaking : i.e. adapts generic PSet provided by the user to the specific of this jobs (input list or lumi/ev for MC)
7. runs cmsRun or script_exe. The `cwd` when they are run is `/srv`
8. checks exit code, FrameWorkJobReport.xml, prepares FJR.json, computes output file PSet hash for DBS, prints trimmed (4k lines max) output from the application to stdout

### New **`CMSRunAnalysis.py`**
1. initializes WMCore and SCRAM
2. prepares the CMSSW_X_Y_Z directory, the equivalent of `cmsrel` command
3. untars `sandbox.tar.gz` file in current directory (`/srv`) and also expand any tar file found in there.So that user will find all files which they may have added via `JobType.inputFiles` configuration parameter.
5. expands `sandbox.tar.gz` again in `/srv/CMSSW_X_Y_Z` so to restore libraries other files from original $CMSSW_BASE (aka scram directory in user submission host), but does not expand tarfiles which may be in there
   * after we modifty CRABClient to put files from $CMSSW_BASE into a CMSSW_X_Y_Z directory in `sanbox.tar.gz` this will not be needed anymore !
6. performs PSet tweaking : i.e. adapts generic PSet provided by the user to the specific of this jobs (input list or lumi/ev for MC)
7. runs cmsRun or script_exe. The `cwd` when they are run is `/srv`
8. checks exit code, FrameWorkJobReport.xml, prepares FJR.json, computes output file PSet hash for DBS, prints trimmed (4k lines max) output from the application to stdout

### **`cmscp.sh`**
1.  sets up python environment from `/cvmfs/cms.cern.ch/COMP` (uses same script as `CMSRunAnalysis.sh)
2. runs `cmscp.py`
   * Description of `cmscp.py` is out of scope here, see comments in the source