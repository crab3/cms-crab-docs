# CRAB Rucio ASO Design and Implementation

## Feature

1. Files are transferred from Temp_RSE to destination RSE (`Site.storageSite`) using Rucio machinery.
1. Separate Transfers Container and Publish Container.
1. Multiple Publish Container.

## TO DO (wishlist?)

1. **transfer logs ?** (added by Stefano)
1. CI test.
1. Run test to make sure `tm_output_dataset` can be shared between task.
1. Coding: Unify bookkeeping attribute in `Transfer()` to single object where
    1. Unitfy `read*` and `update*` to single method.
    1. Mutate the attribute always trigger bookkeeping state to files.
1. Coding: fix some naming convention (e.g., `containerRuleID`).
1. Better Publish Container name than `/FakeDataset`
1. Close dataset when task finish.
1. Monitoring (errors and performance).
1. Cleanup replica entry in Rucio from temp_RSE.

## Out of scope

1. Manage individual files in Rucio container (add/remove files requested by users).

## System Design

### Transfer Container

- Creating only one container.
    - Format: `/a/b_TRNASFER.<taskname_hash>/USER`.
    - Attach Rucio rule where `rse_expression` is destination site (e.g., `rse_pression=T2_CH_CERN`)
        - TODO: This rule should have lifetime only `job_expired_date+7days`.
- Making transfer by add the new files from temp_RSE to Rucio container.
    - Technically, add the new fiels to Rucio dataset and attach the Rcuio dataset to Rucio container.
    - Need to resolve PFNs by ourselves by get info from Rucio `lfns2pfns` and add it back Rucio. Required by Rucio for read files from Temp_RSE.
    - If add files failed (`add_replicas()` raise exception for any reason), job can retry.
- Tracking the transfer status
    - if: `OK` add to Publish Container.
    - else: no action in ASO code. PostJob will mark it fail after 7 days (2 days for FTS ASO).

### Publish Container

- Creating mutiple container where:
    - One container per output file entries in `tm_edm_outfiles`, `tm_tfile_outfiles`, and `tm_outfiles`
    - See name format in [Publish Container Name format](#publish-container-name-format) section below.
- Attach rules to all containers.
    - Expression is the same as Transfer Container
    - No expiration time.
    - Rule from Publish container does not use in ASO, only for user to manage output files.
- Use LFN from `OK` lock as Replica DID and  to its own container.

### Rucio Dataset

- Close dataset:
    - Transfer Container: only close dataset when it full.
    - Publish Container: beside datset is full
        - when 6 hours pass.
        - task finish.
- Need to close dataset to use it as a marker for update info to Publisher via FiletransfersDB.


### PostJob

- Change `tm_output_dataset` scheme.
    ```
    if (tm_publication==False) then
       tm_output_dataset = '/FakeDataset'
    else
        tm_output_dataset = '/GenericTTbar'
    ```
    See table matrix of `tm_output_dataset` in "Expected output dataset name" below.

### Publish Container name format

- If `/FakeDataset/`, append of file name to **ProcessedDataset** part.
- Else, leave it as is.
    - Require to be the same as `tm_output_dataset` to make Publisher work properly.

**Example 1**

The inputs:

- output files:
    - output.root (EDM)
    - myfile.txt (misc)
- output dataset (from `compute_outputdataset_name()`): `/GenericTTbar/tseethon-ruciotransfer-1699972252-94ba0e06145abd65ccb1d21786dc7e1d/USER/`
- `tm_publication==True`

The outputs should be:

- `tm_output_dataset`: `/GenericTTbar/tseethon-ruciotransfer-1699972252-94ba0e06145abd65ccb1d21786dc7e1d/USER/`
- Rucio container name:
    - output.root: `/GenericTTbar/tseethon-ruciotransfer-1699972252-94ba0e06145abd65ccb1d21786dc7e1d/USER/`
    - myfile.txt: `/FakeDataset/fakefile-FakePublish-09b5e9b8cf72d5f9068ec96fa4158a96_myfile.txt/USER`

**Example 2**

The inputs are same as "example 1" except:

- `tm_publication==False`

The outputs should be:

- `tm_output_dataset`: `/FakeDataset/fakefile-FakePublish-09b5e9b8cf72d5f9068ec96fa4158a96/USER`
- Rucio container name:
    - output.root: `/FakeDataset/fakefile-FakePublish-09b5e9b8cf72d5f9068ec96fa4158a96_output.root/USER`
    - myfile.txt: `/FakeDataset/fakefile-FakePublish-09b5e9b8cf72d5f9068ec96fa4158a96_myfile.txt/USER`

### Publisher

(Wa: I am not 100% sure. Never touch Publisher code)

- **Only when Rucio dataset is closed.**
    - Update field `tm_block_complete` to `OK` and `tm_dbs_blockname` to Rucio Dataset name in `filetransfersdb` table.
- Later Publisher will pick information from both fields to publish files to DBS phys03.
    - Read the whole Rucio dataset, but publish individual file.

### Clean up

- Delete all output files in local stageout in temp area after transfer success.
    - Best effort (Run `gfal-rm` without tracking progress).
- TODO: cleanup replicas entry that point to temp_RSE.

## Code

### Design

Keep in mind:

- All operations must be idempotent.
    - Run again after crashed/killed on any step should yield the same result.
    - To get the picture, see [BuildDBSDataset.py#L58-L90](https://github.com/dmwm/CRABServer/blob/c8eb1c7dcb10d7fabd405fd30e8ae2e1f06835a1/src/python/ASO/Rucio/Actions/BuildDBSDataset.py#L58-L90).
- Do not retry on transient errors (could not connect Rucio/CRAB REST). Lets `task_proc_wrapper.sh` do the jobs.
- Keep `self.__init__()` of action class simple as much as possible! Otherwise it will be hard to write the unittest.
    - But not 100% sure if it the correct practice.
- REST is for update only. Do not care what previous value there. Let PostJob.py take care of it.
- For Rucio dataset, never reuse dataset for different container (hard to debug).

### Structure

- [scripts/task\_process/RUCIO_Transfers.py](https://github.com/dmwm/CRABServer/blob/c8eb1c7dcb10d7fabd405fd30e8ae2e1f06835a1/scripts/task_process/RUCIO_Transfers.py): only entrypoint for calling `Main.py`, where we have exported `PYTHONPATH` to `CRAB3.zip` which included all `src/python/ASO`.
- [Main.py](https://github.com/dmwm/CRABServer/blob/c8eb1c7dcb10d7fabd405fd30e8ae2e1f06835a1/src/python/ASO/Rucio/Main.py): parsing args, initialize `RunTransfer()` class and execute `algorithm()` method.
    - Most args are for run integration test, Default value is for run in production.
    - [algorithm()](https://github.com/dmwm/CRABServer/blob/c8eb1c7dcb10d7fabd405fd30e8ae2e1f06835a1/src/python/ASO/Rucio/RunTransfer.py#L33) method is similar to TaskWorker's [Handler.py/handleNewTask()](https://github.com/dmwm/CRABServer/blob/c8eb1c7dcb10d7fabd405fd30e8ae2e1f06835a1/src/python/TaskWorker/Actions/Handler.py#L146C5-L146C18)
- [RunTransfer.py](https://github.com/dmwm/CRABServer/blob/c8eb1c7dcb10d7fabd405fd30e8ae2e1f06835a1/src/python/ASO/Rucio/RunTransfer.py): initialize all clients, `Transfer()` object, and execute the step in `self.algorithm()`.
- [Transfer.py](https://github.com/dmwm/CRABServer/blob/c8eb1c7dcb10d7fabd405fd30e8ae2e1f06835a1/src/python/ASO/Rucio/Transfer.py): Storing all states to use across process.
    - `transfers.txt`, `ReRestInfoForFileTransfers.json`, and all bookkeeping state are loaded at `RunTransfers.algorithm()` AND before execute any actions.
        - Trade-off code complexity with process consume more memory.
    - `Transfer()` object required to pass through every class action as first args. Action class are freely to do whatever its needed.
        - In example, store LFN to PFN from [RegisterReplicas.py#L380-L398](https://github.com/dmwm/CRABServer/blob/c8eb1c7dcb10d7fabd405fd30e8ae2e1f06835a1/src/python/ASO/Rucio/Actions/RegisterReplicas.py#L380-L398) and use it in [Cleanup.py#L60](https://github.com/dmwm/CRABServer/blob/c8eb1c7dcb10d7fabd405fd30e8ae2e1f06835a1/src/python/ASO/Rucio/Actions/Cleanup.py#L60).
- Action class: do the jobs.
    - `self.execute()` is the main logic, will be executed in `RunTransfer.algorithm()`.

### Env

- Inputs
    - From PostJob (in `SPOOL_DIR/task_process` directory):
        - `transfers.txt`: transfers dicts.
        - `RestInfoForFileTransfers.json`: connection info to CRAB REST, and proxyfile name.
    - Proxyfile `SPOOL_DIR/<randomhashname>`.
- Logs are in `SPOOL_DIR/task_process/rucio_transfers.log`.
    - Logger is initialized in `self.__init__`, use the root name `RucioTransfer` (e.g., `self.logger = logging.getLogger("RucioTransfer.Actions.Cleanup")`)
- Workdir (`CWD`) always at `SPOOL_DIR`.
- States are bookkept in `SPOOL_DIR/task_process/transfers` directory.


## Test

### Manually run the integration test

To Run locally:

1. Submit a task.
1. Copy files of submitted task from `SPOOL_DIR`. See instruction in [test/Playbook/ASORucio](https://github.com/dmwm/CRABServer/blob/master/test/Playbook/ASORucio/README.md)
    1. Ensure files extracts from `TaskManager-Run.tar.gz` and `CRAB3.zip` are removed. Current directory in `sys.path` has higher precedence than `PYTHONPATH`.
1. Init env. Mae sure PYTHONPATH are export to `src/python` (TODO: write the docs).
1. Run
    ```bash
    YOUR_CRAB_REPO_PATH=/data/fork/CRABServer
    cd path/to/local/SPOOL_DIR
    $YOUR_CRAB_REPO_PATH/scripts/task_process/RUCIO_Transfers.py --force-publish
    ```
    - Run with `--force-publishname` to create new container.
        - However, it still replace the values in DB store by Schedd (TODO: be able to specify custom database enpoint.)
    - Cleanup the states by remove `SPOOL_DIR/task_process/transfers` dirs to run again.

## Reference

### Expected output dataset name

Update verion ["Multple Publish Ccontainer"](https://github.com/dmwm/CRABServer/issues/7976). Only apply to task that has `outputDataset` tag.
Original version is in <https://github.com/dmwm/CRABServer/issues/7976#issuecomment-1810701759>.

| case | output edm | output tfile | output misc | publication | output dataset name                            | note                                                                          |
|------|------------|--------------|-------------|-------------|------------------------------------------------|-------------------------------------------------------------------------------|
| 1    | x          |              |             | T           | /GenericTTbar                                  |                                                                               |
| 2    |            | x            | (x)         | T           |                                                | fail in crab submit, `tm_publication` is `T` but no file to be publish        |
| 3    | x          |              | x           | T           | /GenericTTbar (edm) <br />/FakeDataset (misc) | Success, but only publish edm                                                 |
| 4    | \>1        |              |             | T           |                                                | fail in crab submit, not support multiple output dataset (deprecated feature) |
| 5    | x          |              |             | F           | /FakeDataset                                   |                                                                               |
| 6    |            | x            | (x)         | F           | /FakeDataset                                   |                                                                               |
| 7    | x          |              | x           | F           | /FakeDataset                                   |                                                                               |
| 8    | \>1        |              |             | F           | /FakeDataset                                   |                                                                               |

## Glossary

- RSE: Rucio term for storage site for the space Rucio manage. In this doc we only mean the "user output area, config.", i.e. `/store/{user,group}/rucio`.
- Temp_RSE: Rucio term for storage site where rucio only has read access. In this doc we only focus on "Grid local stageout for CRAB" i.e. `/store/temp/{user,group}`.
- Transfer dicts: the list of transfer files which contain all metadata needed by ASO.
- DBS Dataset name: consist of three part. In example `/GenericTTbar/tseethon-watest-1/USER`
    - `/GenericTTbar`: Primary dataset name.
    - `tseethon-watest-1`: Processed dataset name.
    - `/USER`: Data tier.
