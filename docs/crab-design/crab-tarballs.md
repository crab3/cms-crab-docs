# CRAB TARBALLS

## Introduction
CRAB uses tarballs to move files across services
 - CRAB Client
 - Task Worker
 - Scripts running on schedulers (HTCondor AP's)
 - Job Wrapper for grid jobs

All tarballs are zipped with `-j` option (`bzip2` protocol)
even if for historical reasons the file name is `xxxx.tar.gz`. 
This is handled transparently in `tar` commands, but if you try to use `gunzip` you will get an error.

Some tarballs are handled via upload-to and later retrieve-from S3.
Some are only passed around via HTCondor.

## Tarballs
A summary is in table below. Be aware that file name in disk of file downloaded from S3 can (sigh)
be different at different times. The horrible hashes of the first two can be found in Task table in the DB
for the columns `tm_user_sandbox` and `tm_debug_files`

"object name in S3" indicates the object path in the `crabcache_prod` [container](https://openstack.cern.ch/project/containers/container/crabcache_prod) 

what | file name               | object name in S3                          | used for                                  | content                                                                                                 | created by               | used by 
----|-------------------------|--------------------------------------------|-------------------------------------------|---------------------------------------------------------------------------------------------------------|--------------------------| ------ 
user stuff | `sandbox.tar.gz` [1]    | `<username>/sandboxes/horriblehash.tar.gz` | passing user code to jobs                 | see [CRAB FAQ](https://twiki.cern.ch/twiki/bin/view/CMSPublic/CRAB3FAQ#What_are_the_files_CRAB_adds_to) | CRABClient               | [JobWrapper](https://github.com/dmwm/CRABServer/blob/master/scripts/CMSRunAnalysis.py), `crab preparelocal`
diagnostic info | `debug_files.tar.gz`    | `<username>/sandboxes/horriblehash.tar.gz` | making config and scriptExe easily available to Experts for debugging | PSet.py, crabConfig.py, scriptExe (if any)                                                              | CRABClient               | [AdjustSites.py](https://github.com/dmwm/CRABServer/blob/master/scripts/AdjustSites.py), [Server UI](https://cmsweb.cern.ch/crabserver/ui)
TW code (part)| `TaskManagerRun.tar.gz` | not in S3      | pass to AP python files from CRABServer GH repor needed to manage the DAG | python files neeeded at top leverl and a CRAB3.zip with all python dependencies [2]                     | TaskWorker build process | [dag_bootstrap.sh]() [dag_bootstrap_startup.sh]()
TW code (part)| `CMSRunAnalysis.tar.gz` | in S3 as part of `InputFiles.tar.gz`   | pass to JobWrapper the python depedencies it needs  | `WMCore.zip, TweakPSet.py, CMSRunAnalysis.py`                                                           | TaskWorker build process | [CMSRunAnalysis.py](https://github.com/dmwm/CRABServer/blob/master/scripts/CMSRunAnalysis.py)
 input files for each Grid job |  `input_files.tar.gz`  | in S3 as part of `InputFiles.tar.gz`  | customize PSet for cmsRun in each job     | N JSON files `job_input_file_list_N.txt` [5]                                                            | DagmanCreator            | JobWrapper, `crab preparelocal`
 input runs and lumis for each Grid job | `run_and_lumis.tar.gz` | in S3 as part of `InputFiles.tar.gz`  | customize PSet for cmsRun in each job     | N JSON files `job_lumis_N.json` [4]                                                                     | DagmanCreator            | JobWrapper, `crab preparelocal`
 mega tar ball | `InputFiles.tar.gz` | `<username/taskname>/runtimefiles`         | passing stuff from TW to APs and `crab preparelocal` | all files need by AP scripts  [5]                                                                       |       DagmanCreator | Uploader.py dag_bootstrap_startup.sh AdjustSites.py `crab preparelocal`


#### Notes:

[1] the file created by the client is in `<proj_dir>/inputs` and has a horrible name: `<hash>default.tar.gz`

[2] content is such that expanding the tar will put script for task process (status cache and ASO) in task_process subdirectory of SPOOL_DIR and leave in SPOOL_DIR files needed "at top level" and python dependencies including WCore after `CRAB3.zip` is unzipped

[3] files are JSON in spite of the `.txt` suffix, each is a list of input file nams (LFN's)

[4] each file contains a dictionary `{runmber:list of lumi ranges,...}` e.g. `{"320917": [[1404, 1404], [1432, 1432], [1442, 1442]]}`

[5] tarball content is "large" (but size limited < 2MB) and possibly evolving as we rationalize the code more:

filename | what it is
--|--
submit_env.sh | used to prepare environment in [JobWrapper](https://cmscrab.docs.cern.ch/crab-design/crab-job-wrapper.html)
CMSRunAnalysis.sh | bash wrappper for `CMSRunAnalysis.py`
cmscp.py | performs output stageout from local Grid WN to local site storage
cmscp.sh | wrapper to setup python env to run `cmscp.py`
CMSRunAnalysis.tar.gz | described above
run_and_lumis.tar.gz |  described above
input_files.tar.gz |  described above
input_args.json | list of dicionaries, one per job, with `{key:value}` with the informations needed by `CMSRunAnalysis.py`
gWMS-CMSRunAnalysis.sh | top [JobWrapper](https://cmscrab.docs.cern.ch/crab-design/crab-job-wrapper.html) script, does very little
RunJobs.dag | From DagmanCretor. DAG description file submitted from TW to AP
Job.submit | From DagmanCreator. JDL template used for submitting each job inside the DAG. `PreJob.py` will customize for each job
dag_bootstrap.sh | wrapper to prepare python env. to execute python scripts in PRE* and POST* of DAG
AdjustSites.py | Name is misleading. Initial script run by DAGMAN job in the AP, does several things.
site.ad.json | From DagmanCreator. List of possible sites each job group [6]. Used by PreJob to finalize submittion in ligh ot Black/White list
TaskManagerRun.tar.gz | described above
datadiscovery.pkl | information passed by TW Action DataDiscovery to DagmanCreator. Needed to run DagmanCreator in the AP for Automatic splitting
taskinformation.pkl | the `Task` dictionary used by TW Actions. Needed to run DagmanCreator in the AP for Automatic splitting
taskworkerconfig.pkl | the TaskWorker configuration object. Needed to run DagmanCreator in the AP for Automatic splitting
input_dataset_lumis.json | list of lumis in input dataset, shipped to WEB_DIR to allow e.g. `crab report` to fetch it
input_dataset_duplicate_lumis.json | historical relict, we do not have duplicate lumis since a long time.
splitting-summary.json | not used atm. But it is created by DagmanCreator and could nice info to show in crab status or for us to upload in DB

[6] job groups are the objects returned by `Actions/Splitter.py` A job group is a set of jobs which can run at same set of sites. 

## Tarball handing

## Possible improvements

* better and consistent names. Do not call JSON files `.txt`, avoid having both `input_files` and `InputFiles`
* ship dependencies via zipping the full repository, like done done already for WMCore.zip in the WN
* the three `.pkl` files in `InputFiles.tar.gz` could be a single file
* avoid duplicating WMCore in `TaskManagerRun.tar.gz` and `CMSRunAnalysis.tar.gz`
* reduce the number of files shipped via HTCondor as individual files, move untar/unzip to top level wrapper