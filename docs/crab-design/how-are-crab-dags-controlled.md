# How are CRAB DAGs controlled

## OVERVIEW

**SETUP: scripts running in the scheduler machine use python3 from the OS. We only add CRAB and WMCore needed files to \$PYTHONPATH.**

-   Historically this was to avoid having to install COMP env from rpm's like inside TW container.

-   now that we can source it from CVMFS we could change if needed.

-   So far sticking with python3 from OS works and it is simpler

Learn about DAGs in HTCondor: <https://htcondor.readthedocs.io/en/latest/users-manual/dagman-workflows.html>

Each Task is transformed into a DAG by TaskWorker. Task Worker submits a job to the scheduler using remote job submission. This job sets up environment and stars condor\_dagman, HTCOndor's DAG manager, a scheduler. The script is <https://github.com/dmwm/CRABServer/blob/master/scripts/dag_bootstrap_startup.sh>

TW submits a condor job with these classAds in the JDL (among others):

-   Cmd = "dag_bootstrap_startup.sh" - the command to be executed
-   Args = "RunJobs.dag" - file with DAG description to be passed to condor_dagman

The dag_bootstrap_startup,sh script (DSB.sh) runs on the scheduler machine in job Universe = 7 (i.e. Scheduler universe). Ref: <https://htcondor-wiki.cs.wisc.edu/index.cgi/wiki?p=MagicNumbers> <br />
This universe means that schedd will run this job locally, rather than e.g. sending to the grid (SI global pool) which for us is the HTCondor Vanilla universe (jobUniverse==5).

*NOTE ABOUT VERSIONS: Stuff which runs on the scheduler uses scripts from GitHub CrabServer/scripts/... area which is packed in a tarball during TaskWorker installation and transferred to the scheduler during submission. In this way code executed on the Scheduler machine for this taks is from the same version as the TW and will stay that way for the duration of the task. Different tasks executing on same scheduler can run different TW versions (i.e. different CrabServer tags).*

-   *Pro: code in a task is stable*
-   *Con: takes 1 moth to have all tasks use new code*
-   *Con: it is hard to change code for running tasks in case e.g. bugs are found*

*There is 1 TW, how do we test new version on only a fraction of tasks ?*

-   *Old Trick: create a file in /etc for the scheduler(s) where to test (so we can rump up in 1/15 increments) and check for that file e.g. in dag_bootstrap_startup
-   *New Trick: canary TW*

Dag_bootstrap_startup.sh sets among other trivial things, two classAds MAX_IDLE and MAX_POST

-   MAX_IDLE the maximum number of idle DAG nodes
-   MAX_POST is the maximum number of PostJobs running at same time

Then it runs AdjustSites.py (list of action here to be seen)

Then starts the TaskProcess : TP is another condor job which runs on the schedule machine, this time in Universe=LOCAL (JobUniverse=12), for no very good reasons. 12 and 7 both mean "run on scheduler machine", HTCondor documentation says that they want to deprecate 7 and leave only 12, anyhow there is absolutely no difference. Original developers thought it a good idea to use jobUniverse instead of other classAds to distinguish TP from DBS.sh (usually called Dagman for short).

Idea is:

-   Everything is a condor job: ensure proper restart after reboot, we can put limt on \# running etc. decide when to star, monitor via condor_q ...
-   DBS.sh: does some init then executes condor_dagman until job ends (weeks) so this job/process is often called "Dagman"
-   TP: runs for as long as Dagman runs, and a couple days more (until task is purged) and sort of keeps an eye on Dagman execution and does additional actions which are useful:
    -   Periodically reads condor_schedd and dagman logs and prepares a summary of status of all jobs in the DAG(task) which is read by crab status command via HTTP . So crab status does not put load on condor, but schedulers need to run Apache httpd and CMSWEB needs to act as redirect for read form crab status since schedulers are inside firewall.
    -   Periodically reads files prepared by PostJob and if there are jobs which require ASO it executes scripts to either
        -   Submit and monitor FTS jobs
        -   Add file to Rucio containers and create rules in Rucio

## INSIDE THE DAGs

Our DAGs are simple. Just a set of independent nodes with no dependency !

In condor every DAG nodes has 3 parts :

1.  PRE : runs a script
2.  JOB : submits Job for the sched (condor_dagman executs a condor_submit)
3.  POST : runs a script

The node description inside RunJobs.dag indicates if PRE, POST and JOB shoud be retried, how many times, at which interval.

PRE and POST steps use same <https://github.com/dmwm/CRABServer/blob/master/scripts/dag_bootstrap.sh> script with different argument "PREJOB" or "POSTJOB"

Dag_bootstrap.sh sets up the "TW+WMCore" python environment and runs TaskManagerBootstrap.py passing all its input argos to it: <https://github.com/dmwm/CRABServer/blob/master/src/python/TaskWorker/TaskManagerBootstrap.py>

This executes TaskWorker/Actions/PreJob.py or PostJob.py as appropriate. It also appears to contain obsolete code which is never executed and than do little more than redirect to PreJob/PostJob/PreDag as proper.

## WHAT PREJOB AND POSTJOB DO

PreJob basically customizes the JDL for the condor\_submit in the JOB step for this crabId \# (or job #), code is not straightforward to read as it relies on mixture of arg and env.vars. which are defined in various places, but it can be run with pdb as needed to understand: <https://twiki.cern.ch/twiki/bin/view/CMSPublic/Crab3OperatorDebugging#To_run_a_pre_job_on_the_schedd>

PostJob has two parts

1. Process job status to decide if to retry or not, where, when etc.

2. Find job output files, if ASO is needed writes files with info for TaskProcess, uploads Input and Output metadata to carb DB FILEMETADATA table as needed. If ASO is required also updates transfer status on DB TRANSFERSDB table. If ASO was called in, asks dagman to defer it (exit code 4) so that it runs again after 30 min until ASO is done or timeout is reached, then it exit with code 0 (if successful) or 2 (if failed) and this node is completed in Dagman. Description values returned by PostJob is in <https://github.com/dmwm/CRABServer/blob/42d5a7b8a7e1848f0789861111f3d01d22590981/src/python/TaskWorker/Actions/PostJob.py#L1707-L1718>

PostJob.py is the most horrible piece of CRAB code 3.5k lines in a single file. Hard to read and easy to get tricked in spite of valiant efforts of several people to at least put comments. It has two main classes: PostJob and ASOJob. Execution stars with PostJob and an ASOJob instance is created if ASO transfers are needed. Metadata upload in DB is part of PostJob. PostJob cab be easily run with pdb: <https://twiki.cern.ch/twiki/bin/view/CMSPublic/Crab3OperatorDebugging#To_run_a_post_job_on_the_schedd>

## BACK TO MAX\_IDLE AND MAX\_POST

**MAX\_IDLE**: condor_dagman will not execute all condor_submits "blindly". But will stop submission when there are MAX_IDLE jobs in idle status for this task. We set MAX_IDLE=1k so even if a task has 10K jobs (current maximum) they will only be submitted to schedd a few at a time according to how many runs. This is the reason for jobs in "unsubmitted" status in crab status output.

**MAX\_POST** is there to prevent too many PostJobs from running at same time (each is a py3 process, usign quite some memory), limit on schedd scaling up is mostly because of memory consumption by condor_shadows and our scripts, not CPU. Those limits were set years ago on different hardware, we could likely raise them now, if needed.


## ADJUST_SITES

Called by DBS.sh at initial submission (before DAGMAN is started) and at each crab resubmit.

It servers two purposes:

1.  At initial submission: create some needed files and directories. Also a favorite place to put some "ad hoc" code to customize behavior. It is the first python script to be executed. It would have been possible to add this logic to DBS.sh, but if it fails we have very few info and logs. It is easier not to touch DBS.sh and just add new features elsewhere, in the AdjustSites.py script, for example.

2.  At resubmit time allows for adjusting site white/black list (and other parameters too) as specified in crab resubmit options (initially only site b/w lists, hence the name)

LOGS (in SPOOL_DIR)

-   \_condor_stdout and \_condor_stderr
    -   Written by Dagman Bootstrap Startup bash script. Owned by \`crabtw\` user, neeed sudo to read them
    -   Contain the stdout and stderr from the job submitted by the TW, which executes the DBS.sh and ends with "Executing condor dagman ...".
-   Adjust_out.txt
    -   Written by adjust_sites.py
    -   It its the log (stdout) of that script. If task failed to bootstrap, look here
-   job_log
    -   Written by condor_schedd
    -   we direct condor to log here scheduler events for the jobs in vanilla universe in this task (i.e. grid jobs), in addition to its central logs. It is used e.g. in \`condor_q -userlog job_logs\` . Avoids looking up central condor log via e.g. condor_history which is very heavy and slow. Keeps job events history also after jobs are removed from condor queue (when simple condor_q does not find the job)
    -   Used to update the status cache file (task\_process/cache\_status.py), so that we do not have to do condor_q every time.
-   RunJobs.dag.dagman.out
    -   Written by condor_dagman
    -   output of DAGMAN process, shows all the actions taken and will have relevant error messages in case of problems. See [HTCondor Doc](https://htcondor.readthedocs.io/en/latest/users-manual/dagman-workflows.html#dag-monitoring)
-   RunJobs.dag.nodes.log
    -   Written by condor_dagman
    -   logs submit/start/end events about grid jobs submitted by DAGMAN. Used to tell DAG status in cache_status.py, but not useful for debug. There's a minimal description in [HTCondor Doc](https://htcondor.readthedocs.io/en/latest/users-manual/dagman-workflows.html#dag-recovery)
-   node_state
    -   Written by condor_dagman
    -   DAGMan can capture the status of the overall DAG and all DAG nodes in a node status file, such that the user or a script can monitor this status. This file is periodically rewritten while the DAG runs. See [HTCondor Doc](https://htcondor.readthedocs.io/en/latest/users-manual/dagman-workflows.html#capturing-the-status-of-nodes-in-a-file)
-   SPOOL_DIR/prejob_logs : contain the logs from all prejobs ! if your DAG node fails "w/o info" chances are that prejobs did not run, look here ! I also contains preDag logs in case of automatic splitting
-   postjob.X.Y.txt :
    -   Written by PostJob.py
    -   Contains the log of PostJob for retry number Y for "job" with CRAB_ID X. A "crab job" is mapped to several condor jobs, one for each retry. For each retry PostJob can run many times (e.g. when waiting for ASO), log information is appended to same file.

## ERROR HANDLING

**failures in bootstrap**: currently if something goes wrong in dagman bootstrap & C (e.g. can't talk with REST), everything is aborted. Aborted means: condor_dagman exits with an error code and DagmanBootsstratpStartup job is put in hold. *cache_status.py* (part of task_process) will detect that dagman job is in Hold and write proper information in *task_process*/*status_cache.pkl* file which is used by crab status CRABClient command. crab status will report a "task failed to bootstrap" and give a reason and user submits again (they may or may not notify us of this). If things go horribly wrong, the bootstrap does not manage to abort, task gets simply stuck in "waiting to bootstrap" and crab status command will print "If this persists report it to ..computingTools..."

**Failures in PreJob or PostJob**: hopefully all exceptions are catched and result in exiting with the status code which tells condor "please run me again". So we should be protected against transients like http calls to REST. Changes to the code which introduce bugs usually result in full task failure. DAGMAN will close things and put bootstrap job in hold. User (or developer) notice the failure and expert investigation is needed.

## TASK_PROCESS

Process which runs as a condor job in LOCAL universe (jobuniverse==12). Script is: *task_process/task_proc_wrapper.sh* Condor submission uses this JDL: *task_process/daemon.jdl* . Relevant ads in there:

-   Universe = local
-   Executable = task_process/task_proc_wrapper.sh
-   Arguments = 221114_110023:belforte_crab_20221114_120019
-   Log = task_process/daemon.PC.log → this is the condor log from sched
-   Output = task_process/daemon.out.$(Cluster).$(Process) → stdout of the script
-   Error = task_process/daemon.err.$(Cluster).$(Process) → stderr of the script


In CRAB code and documentation task_process is usually used to mean *task_proc_wrapper.sh* . But remember that this is not run as a script by e.g. DBS.sh, but is a separate condor job.

E.g. **task_process keeps running after condor_dagman completed (or failed) and DBS.sh exited**. Currently for 24h. With Rucio for ASO we may need to increase, so there will be more task_process running. May need to experiment and find out if there's a limit to schedd functionality from that many running jobs in local universe. Maybe as simple as "once we can do resubmissions in Rucio it will be enough to stick to the 24h limit".

Button line: a simple and very lightweight bash script which calls two python scripts (cache_status and ASO) every five minutes. Idea was to keep memory usage low. Scheduler machine scaling for us has been historically limited by RAM, not CPU/network/disk/htcondor-central-manager. We do not know how close to limits we are now.

When crab resubmit isissues, a new TP is submitted in case it was not running already.

## CACHE_STATUS

*task_process/cache_status.py* reads condor and dagman logs, prepares a couple python dictionaries with information on each job status/errors/used-resources and writes *task_process/status_cache.pkl* (used by CRABClient crab status command) and *task_process/status_cache.txt* (historic relic from py2 times, kept around because it is easier to read and because removing it requires some reformatting of code inside the script).

-   This script only has HTRCondor as dependency
-   Code is a bit intricate and requires a lot of HTCondor knowledge to be understood
-   Complexity comes from two things
    -   Condor log (job_log) keeps growins, up to 100MBs. Code needs to only read new info since last time. So needs some checkpointing of where we left.
    -   Parsing jobb_lot to find proper events to decide job status etc. is tricky and code here is mostly untouchted/ununderstood since Brian B. wrote it 10y ago

## ASO

So we either run *FTS_Transfers.py* or *RUCIO_Transfers.py*

Python3 scripts which read what to do from files written by PostJobs, do the "ASO thing" and report to PostJob via files and also updates some flags in CRAB DB transferdb Table

Logs are : *task_process/transfer_rucio.log* or *task_process/transfers/transfer_inject.log*

## RESTARTS

Since dagman (aka task) and task_process (aka the process which spy on the dagman, summarize status and perform ASO) run as condor jobs, they are automatically restarted after any interruption (reboots, condor update, schedd crash...)

Dagman stops when all nodes completed (success or fail). Can be restarted by a crab resubmit.

TP stops 24h after last change in condor logs. Will be restarted if a new dagman (menaing DBS.sh) runs.

## RESUBMITS

Resubmission exploit DAGMAN Rescue file/capbility and the fact that we place DBS.sh job in HOLD at the end. Details are tricky. Stefano does not fully understand the Dgamn Rescue thing.

Current resubmit procedure (note that you can crab resubmit some jobs also while Dagman is running !) uses a very dirty trick of editing dagman log/status files to make it do what we need. Again code untouched from Brian B. times. This led to problems when condor sched and crab resubmit where updating logs at same time. So we have a locking procedure (from condor). This was deemed ugly and dangerous (also having lockablelogs may impact a bit condor performance, at least is not their default configuration). We have this custom line:

*/etc/condor/config.d/82_cms_schedd_crabschedd_generic.config:ENABLE_USERLOG_LOCKING=True*

Condor Dagman developer introduced new features in DAGMAN exactly to allow our use case to be deal with by the Dagman w/o tricks. The corresponding changes in our code have not been done: <https://github.com/dmwm/CRABServer/issues/6270>

The new DAGMAN features will allow restart of failed DAG nodes before DAGMAN completes, in Stefano's understanding this will cover the most common use case: resubmission of failed jobs w/o waiting for all other jobs to complete, but will not allow the other use case of resubmitting jobs which succeeded. This latter use case is likely very little used and usefulness is uncertain (user lost an output file after it was moved from /store/temp to /store/user ?).

Maybe something can be done so that is also possible ? Maybe better allow user to create a new task containing only a certain list of jobids from the current one ? It should not be too difficult for DagmanCreator. Have a new command and a DagmanReCreator action ?

## HOW THINGS END

grep for TASKLIFETIME in the source code ! Also <https://github.com/dmwm/CRABServer/issues/4681#issuecomment-302336451>) and following comments. Enforcing a clear lifetime across the board was an important effort which spanned over months and involved many persons. It is possible that something slipped through.

## PREDAG and AUTOMATIC SPLITTING

Complicated, there' s documentation, but not fully up to date in CRABServer GH wiki. No documentation of the actual code and intermediate files. Very difficult to debug or improve. We currently support is "as is" and if at some point it breaks it badly may have to remove it. Nor Stefano, nor Marco M. understood this code yet.

Documentation: <br />
<https://github.com/dmwm/CRABServer/wiki/Automatic-Splitting:-Concept> <br />
<https://github.com/dmwm/CRABServer/wiki/Automatic-Splitting:-TaskWorker> <br />
<https://github.com/dmwm/CRABServer/wiki/Automatic-Splitting:-User> <br />

## Extra: Authentication/Authorization

*HOW IS PROXY RENEWAL FOR USERS DONE, WHAT HAPPENS WHEN THEY EXPIRE?*

!!! note
    Thoughts after the chat: could what's written below be replaced by a couple additional notes in <https://cmscrab.docs.cern.ch/technical/crab-overview.html#authentication-for-users> ?

    -   Stefano will try to merge in there

TaskWorker has a recurring action called ProxyRenewal which runs every 6 hours:

-   Query HTCondor to get list of all active tasks on all schedulers i.e. tasks which may need a valid X509 proxy, then make sure that there is a valid proxy for that user in the TW's /data/certs/creds directory (get a new one from myproxy if needed) and push it to the SPOOL_DIR of those tasks.
-   This may fail if:
    -   Problems in talking with scheduler.. Will try again
    -   User credential in myproxy is expired: e.g. user last used crab 2 weeks ago. This needs "details"
        -   Most crab client commands will automatically check myproxy credential and refresh it if neede, including crab status
            -   This is why every now and then crab command ask for your certificate's passphrase
        -   When a task is done (processed) e.g. with 2 fails and 2K OK, task stay around in TW "in case" user decide to issue a resubmit. But user may decide that the 2 failed do not matter. CRAB has no way, so tries to keep the task ready for resubmit for up to 4 weeks from submission date. But if user was happy and stops using crab, credential in myproxy will not be renewed.
        -   Early on we decided that asking users to issue and explicit myproxy-init every week or so was not going to work. So there was not even a crab command to do that. Recently (1 or 2 years ago) Stefano introduced "crab createmyproxy" mostly for use by HammerClound, but if needed is available to everybody.

Schedulers simply go with whatever TW puts there
