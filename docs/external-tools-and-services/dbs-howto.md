# How-To guide on how to interact with DBS

## Python DBSClient

Log in into lxplus, then install the dbsclient with pip

```bash
python3 --version
pip install --user dbs3-client
```

make sure that the client is installed: 

```bash
ls ~/.local/lib/python3.*/site-packages/
```

then you can use it from a python shell

```python
import dbs.apis.dbsClient as dbsClient
dbsUrl = 'https://cmsweb.cern.ch/dbs/prod/global/DBSReader'
dbs = dbsClient.DbsApi(url=dbsUrl)
allAODs = [d['dataset'] for d in dbs.listDatasets(dataset='/*/*/AOD')]
print(len(allAODs))
```

